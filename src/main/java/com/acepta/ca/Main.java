package com.acepta.ca;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.cert.X509Certificate;
import java.util.Random;

import com.acepta.ca.bean.AceptaBeanFactory;
import com.acepta.ca.bean.AdminBean;
import com.acepta.ca.persistence.Grupo;
import com.acepta.ca.persistence.Usuario;
import com.acepta.ca.security.util.PEM;
import com.acepta.ca.security.util.Password;

/**
 * Aplicacion para inicializar y administrar la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @author Alvaro Millalen
 * @version 1.0, 2007-09-01
 */
public class Main {
	private static final String SYSTEM_PROPERTY_PERSISTENCE_UNIT_NAME = "com.acepta.ca.persistenceUnitName";
	private static final String SYSTEM_PROPERTY_SECURITY_TOKEN_NAME = "com.acepta.ca.securityTokenName";
	
	private static final String DEFAULT_PERSISTENCE_UNIT_NAME = "aceptaca-persistence-unit";
	private static final String DEFAULT_SECURITY_TOKEN_NAME = "aceptaca-security-token";

	private static final String CAPTCHA_CHAR_TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	private static final int CAPTCHA_LENGTH = 8;
	private static final String VERSION = "2.0.0";

	private AceptaBeanFactory beanFactory;

	public Main() {
	}

	public void run(String[] args) {
		if (args.length != 0)
			execute(args);
		else
			help();
	}

	private void execute(String[] args) {
		setBeanFactory(createBeanFactory());
		try {
			executeCommand(args);
		}
		finally {
			getBeanFactory().close();
		}
	}

	private void help() {
		println("Administrador de la Autoridad Certificadora (C) 2020 Acepta.com S.A.");
		println("version "+VERSION);
		println("Use: admin [user|users|groups|authorities|password] [[add|mod|del] <rut> [-n <nombre>] [-g <grupo>[,...]]]");
	}
	
	private void executeCommand(String[] args) {
		try {
			String command = args[0];
			
			if (command.equals("user")) {
				String operation = args[1];
				String rut = args[2];
				String nombre = null;
				String[] grupos = null;
				
				for (int index = 3; index < args.length; index++) {
					String option = args[index];
					String value = args[++index];
					
					if (option.equals("-n")) {
						nombre = value;
					}
					else if (option.equals("-g")) {
						grupos = value.split(",");
					}
					else {
						throw new RuntimeException("Opcion invalida: " + option);
					}
				}
				
				if (operation.equals("add")) {
					addUsuario(rut, nombre, grupos);
				}
				else if (operation.equals("del")) {
					removeUsuario(rut);
				}
				else if (operation.equals("mod")) {
					modifyUsuario(rut, nombre, grupos);
				}
				else {
					throw new RuntimeException("Operacion invalida: " + operation);
				}
			}
			else if (command.equals("users")) {
				listUsuarios();
			}
			else if (command.equals("groups")) {
				listGrupos();
			}
			else if (command.equals("authorities")) {
				listAutoridades();
			}
			else if (command.equals("bootstrap")) {
				if (args.length >= 2) {
					String what = args[1];
					
					if (what.equals("security")) {
						bootstrap(true, false);
					}
					else if (what.equals("persistence")) {
						bootstrap(false, true);
					}
					else {
						throw new RuntimeException("Comando invalido: " + command + " " + what);
					}
				}
				else {
					bootstrap(true, true);
				}
			}
			else if (command.equals("update")) {
				if (args.length >= 2) {
					String what = args[1];
					
					if (what.equals("security")) {
						update(true, false);
					}
					else if (what.equals("persistence")) {
						update(false, true);
					}
					else {
						throw new RuntimeException("Comando invalido: " + command + " " + what);
					}
				}
				else {
					update(true, true);
				}
			}			
			else if (command.equals("password")) {
				encryptPassword();
			}
			else {
				throw new RuntimeException("Comando invalido: " + command);
			}
		}
		catch (IndexOutOfBoundsException exception) {
			throw new RuntimeException("Linea de comandos invalida");
		}
	}
	
	private void bootstrap(boolean security, boolean persistence) {
		checkCaptcha();
		
		println("Inicializando...");
		
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			adminBean.bootstrap(security, persistence);
		}
		finally {
			adminBean.close();
		}
		
		println("Listo");
	}

	private void update(boolean security, boolean persistence) {
		checkCaptcha();
		
		println("Actualizando...");
		
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			adminBean.update(security, persistence);
		}
		finally {
			adminBean.close();
		}
		
		println("Listo");
	}
	
	
	private void addUsuario(String rut, String nombre, String[] grupos) {
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			adminBean.addUsuario(rut, nombre, grupos);
		}
		finally {
			adminBean.close();
		}
	}
	
	private void removeUsuario(String rut) {
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			adminBean.removeUsuario(rut);
		}
		finally {
			adminBean.close();
		}
	}
	
	private void modifyUsuario(String rut, String nombre, String[] grupos) {
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			adminBean.modifyUsuario(rut, nombre, grupos);
		}
		finally {
			adminBean.close();
		}
	}

	private void listUsuarios() {
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			for (Usuario usuario : adminBean.findUsuarios()) {
				print(usuario.getRut() + " " + usuario.getNombre());
				for (Grupo grupo : usuario.getGrupos())
					print(" " + grupo.getNombre());
				println("");
			}
		}
		finally {
			adminBean.close();
		}
	}
	
	private void listGrupos() {
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			for (Grupo grupo : adminBean.findGrupos()) {
				println(grupo.getNombre());
			}
		}
		finally {
			adminBean.close();
		}
	}

	private void listAutoridades() {
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			for (X509Certificate certificate : adminBean.findCertificadosAutoridades()) {
				try {
					println(certificate.getSubjectDN().getName());
					println(PEM.encodeX509Certificate(certificate));
				}
				catch (IOException exception) {
					throw new RuntimeException(exception);
				}
			}
		}
		finally {
			adminBean.close();
		}
	}
	
	private void encryptPassword() {
		println("Ingrese la contrasena:");
		
		String password = readLine();
		
		println(Password.encryptPassword(password));
	}
	
	private AceptaBeanFactory getBeanFactory() {
		return beanFactory;
	}

	private void setBeanFactory(AceptaBeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
	
	private static AceptaBeanFactory createBeanFactory() {
		String persistenceUnitName = System.getProperty(SYSTEM_PROPERTY_PERSISTENCE_UNIT_NAME, DEFAULT_PERSISTENCE_UNIT_NAME);
		String securityTokenName = System.getProperty(SYSTEM_PROPERTY_SECURITY_TOKEN_NAME, DEFAULT_SECURITY_TOKEN_NAME);
		
		return new AceptaBeanFactory(persistenceUnitName, securityTokenName);
	}

	private void checkCaptcha() {
		String captcha = createCaptcha();
		
		println("Ingrese el texto: " + captcha);
			
		if (!captcha.equals(readLine()))
			throw new RuntimeException("Texto incorrecto");
	}

	private static String createCaptcha() {
		StringBuffer buffer = new StringBuffer();
		
		Random random = new Random();
		
		while (buffer.length() < CAPTCHA_LENGTH)
			buffer.append(CAPTCHA_CHAR_TABLE.charAt(random.nextInt(CAPTCHA_CHAR_TABLE.length())));
		
		return buffer.toString();
	}
	
	private static String readLine() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			return reader.readLine();
		}
		catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	private static void println(String message) {
		System.out.println(message);
	}
	
	private static void print(String message) {
		System.out.print(message);
	}

	public static void main(String[] args) {
		try {
			Main main = new Main();
			
			main.run(args);
		}
		catch (Exception exception) {
			System.out.println("ERROR: " + exception.getMessage());
			exception.printStackTrace();
		}
	}
}
