package com.acepta.ca;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import com.acepta.ca.bean.AceptaBeanFactory;
import com.acepta.ca.bean.AdminBean;
import com.acepta.ca.bean.CertificadoBean;
import com.acepta.ca.persistence.CRL;
import com.acepta.ca.security.util.PEM;
import java.security.cert.CertificateFactory;

/**
 * Aplicacion de apoyo en la ceremonia de inicializacion.
 * 
 * @author Alvaro Millalen
 * @version 1.0, 2010-07-28
 */
public class CerUtil {
	private static final String SYSTEM_PROPERTY_PERSISTENCE_UNIT_NAME = "com.acepta.ca.persistenceUnitName";
	private static final String SYSTEM_PROPERTY_SECURITY_TOKEN_NAME = "com.acepta.ca.securityTokenName";
	
	private static final String DEFAULT_PERSISTENCE_UNIT_NAME = "aceptaca-persistence-unit";
	private static final String DEFAULT_SECURITY_TOKEN_NAME = "aceptaca-security-token";

	private static final String VERSION = "2.0.0";
	private AceptaBeanFactory beanFactory;

	public CerUtil(){}
	
	private void run(String[] args){
		if (args.length!=0)
			execute(args);
		else
			help();
		
	}
	private void execute(String[] args){
		setBeanFactory(createBeanFactory());
		try{
			executeCommand(args);
		}
		finally{
			getBeanFactory().close();
		}
	   
	}
	private void help(){
		println("Utilitario ceremonia inicializacion Autoridad Certificadora (C) 2020 Acepta.com S.A.");
		println("version "+VERSION);
		println("Use: cerutil [exportcerts <dir>|comparecerts <certspath>|verifycrls] ");		
	}
	private void executeCommand(String[] args){
		try{
			String command = args[0];
			if (command.equals("exportcerts")){
				String dir = args[1];
				exportCerts(dir);
				
			}else if (command.equals("comparecerts")){
				String dir = args[1];
				compareCerts(dir);
				
			}else if (command.equals("verifycrls")){
				verifyCrls();
			}else
				throw new RuntimeException("Comando invalido");			
		}catch(IndexOutOfBoundsException exception){
			throw new RuntimeException("Comando invalido");
		}
	}
 
	private String getCN(X509Certificate cert) {
		String cn = cert.getSubjectDN().getName();
		   int start = cn.indexOf("CN=");
		   int end = cn.indexOf(",", start);
		   cn = cn.substring(start+3, end);
		return cn.trim();
	}
	private void exportCerts(String dir){
		AdminBean adminBean = getBeanFactory().createAdminBean();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String zipName = sdf.format(new Date())+"_certificates.zip";
			File zipFile = new File(dir, zipName);

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));
			for( X509Certificate cert : adminBean.findCertificadosAutoridades()){
			   try {
				   String nameCert = getCN(cert)+".pem";
				   out.putNextEntry(new ZipEntry(nameCert));
				   out.write(PEM.encodeX509Certificate(cert).getBytes());
				   out.closeEntry();
				} catch (IOException exception) {
					throw new RuntimeException(exception);
				}
			}
			out.close();
			println("[*] Completed:"+zipFile.getCanonicalPath());
		} catch (FileNotFoundException exception) {
			throw new RuntimeException(exception);
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}finally{
			adminBean.close();
		}
		
	}
	private byte[] getCertificateBackup(ZipFile zipFile, String nameCert) throws IOException{
		   InputStream in = zipFile.getInputStream(zipFile.getEntry(nameCert));
		   ByteArrayOutputStream out = new ByteArrayOutputStream();
		   byte[] buffer = new byte[2048];
		   int len = 0;
		   while((len=in.read(buffer))>0){
			   out.write(buffer,0, len);
		   }
		   in.close();
		   return out.toByteArray();	
	}
	private void compareCerts(String filePath){
		AdminBean adminBean = getBeanFactory().createAdminBean();
		boolean isAllOk = true;
		int countOK = 0;

		try{
			countOK++;
			ZipFile zipFile = new ZipFile(new File(filePath));
			for( X509Certificate cert : adminBean.findCertificadosAutoridades()){
				String nameCert = getCN(cert)+".pem";
				byte[] pem= PEM.encodeX509Certificate(cert).getBytes();
				byte[] certbackup = getCertificateBackup(zipFile, nameCert);
				if (Arrays.equals(certbackup, pem))
					countOK++;
				else{
					isAllOk = false;
					println(cert.getSubjectDN().getName()+" NOK!!");
				}
			}
		} catch (FileNotFoundException exception) {
			throw new RuntimeException(exception);
		} catch (ZipException exception) {
			throw new RuntimeException(exception);			
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}finally{
			adminBean.close();
		}
		if (isAllOk)
			println("[*] Completed. "+countOK+" certificates OK.");
	}
	private void verifyCrls(){
		AdminBean adminBean = getBeanFactory().createAdminBean();
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		try {
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
			for( String nombreAutoridad: certificadoBean.getNombresAutoridadesCertificadoras()){
				CRL crlData = certificadoBean.buscarUltimaCRLConEmisor(nombreAutoridad);
				X509CRL crl = (X509CRL) certificateFactory.generateCRL(new ByteArrayInputStream(crlData.getData()));
				X509Certificate certAutoridad = adminBean.findCertificadoAutiridad(nombreAutoridad);
				crl.verify(certAutoridad.getPublicKey());
			}
			println("[*] Completed. X CRL OK.");			
		} catch (CertificateException exception) {
			throw new RuntimeException(exception);
		} catch (CRLException exception) {
			throw new RuntimeException(exception);
		} catch (InvalidKeyException exception) {
			throw new RuntimeException(exception);
		} catch (NoSuchAlgorithmException exception) {
			throw new RuntimeException(exception);
		} catch (NoSuchProviderException exception) {
			throw new RuntimeException(exception);
		} catch (SignatureException exception) {
			throw new RuntimeException(exception);
		}finally{
			certificadoBean.close();
			adminBean.close();
		}
		
	}
	private static AceptaBeanFactory createBeanFactory() {
		String persistenceUnitName = System.getProperty(SYSTEM_PROPERTY_PERSISTENCE_UNIT_NAME, DEFAULT_PERSISTENCE_UNIT_NAME);
		String securityTokenName = System.getProperty(SYSTEM_PROPERTY_SECURITY_TOKEN_NAME, DEFAULT_SECURITY_TOKEN_NAME);
		
		return new AceptaBeanFactory(persistenceUnitName, securityTokenName);
	}	
	private AceptaBeanFactory getBeanFactory() {
		return beanFactory;
	}	
	private void setBeanFactory(AceptaBeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
	
	private void println(String text){
		System.out.println(text);
	}

	public static void main(String[] args) {
		try {
			CerUtil util = new CerUtil();
			util.run(args);
		} catch (Exception exception) {
			System.out.println("ERROR: " + exception.getMessage());
			exception.printStackTrace();
		}
	}

}
