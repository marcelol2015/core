package com.acepta.ca.persistence;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Administrador de las entidades persistentes de la Autoridad Certificadora.
 * 
 * <pre>
 * Modelo Entidad Relacion
 * -----------------------
 * 
 *                         +---o< SelloTiempo
 *                         |
 *  Autoridad -||--emisor--+---o< CRL
 *    =   =                |
 *    |   |                +---o< Certificado -o|--+
 *    | generador                                  |
 *    |   |                                        |
 *    |   +----|< NumeroSerie                      |
 *    |                                            |
 *    |  +--------------emitido-por----------------+
 *    |  |
 *    |  |
 *    o  =                      +---||- SolicitudPersona      -||---titular---||- Persona
 *    ^  |                      |
 *  Solicitud -||---hereda-de---+---||- SolicitudSitioWeb     -||---titular---||- SitioWeb
 *      |                       |
 *      =                       +---||- SolicitudSistema      -||---titular---||- Sistema
 *      |                       |
 *      |                       +---||- SolicitudSelloTiempo  -||---titular---||- Sistema
 *      |                       |
 *  almacenado                  +---||- SolicitudCAF      -||---titular---||- CAF
 *      |                       |
 *      -                       +---||- SolicitudCodigo   -||---titular---||- Compania
 *      ^
 *  Documento
 * 
 *  Usuario >o---pertenece----|< Grupo
 * </pre>
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaEntityManager {
	private EntityManager entityManager;

	public AceptaEntityManager(EntityManager entityManager) {
		setEntityManager(entityManager);
	}

	public void close() {
		getEntityManager().close();
	}

	/*
	 * Transacciones
	 */
	public void beginTransaction() {
		getEntityManager().getTransaction().begin();
	}

	public void commitTransaction() {
		getEntityManager().getTransaction().commit();
	}

	public void rollbackTransaction() {
		if (getEntityManager().getTransaction().isActive())
			getEntityManager().getTransaction().rollback();
	}
	
	/*
	 * Entidades
	 */
	public void persist(Object entity) {
		getEntityManager().persist(entity);
	}
	
	public void remove(Object entity) {
		getEntityManager().remove(entity);
	}
	
	/*
	 * Grupos
	 */
	public Grupo findGrupoWithNombre(String nombre) {
		Query query = getEntityManager().createNamedQuery("findGrupoWithNombre");
		
		query.setParameter("nombre", nombre);
		
		return (Grupo) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Grupo> findGrupos() {
		Query query = getEntityManager().createNamedQuery("findGrupos");

		return query.getResultList();
	}

	/*
	 * Usuarios
	 */
	public Usuario findUsuarioWithRut(String rut) {
		Query query = getEntityManager().createNamedQuery("findUsuarioWithRut");
		
		query.setParameter("rut", rut);
		
		return (Usuario) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Usuario> findUsuariosWithGrupo(String grupo) {
		Query query = getEntityManager().createNamedQuery("findUsuariosWithGrupo");
		
		query.setParameter("grupo", grupo);

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Usuario> findUsuarios() {
		Query query = getEntityManager().createNamedQuery("findUsuarios");

		return query.getResultList();
	}

	/*
	 * Autoridades
	 */
	public Autoridad findAutoridadWithNombre(String nombre) {
		Query query = getEntityManager().createNamedQuery("findAutoridadWithNombre");

		query.setParameter("nombre", nombre);

		return (Autoridad) query.getSingleResult();
	}

	/*
	 * Certificados
	 */
	@SuppressWarnings("unchecked")
	public Collection<Certificado> findCertificadosWithEmisor(Autoridad emisor) {
		Query query = getEntityManager().createNamedQuery("findCertificadosWithEmisor");

		query.setParameter("emisor", emisor);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<Certificado> findCertificadosWithEmisorEstado(Autoridad emisor, Certificado.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findCertificadosWithEmisorEstado");

		query.setParameter("emisor", emisor);
		query.setParameter("estado", estado);

		return query.getResultList();
	}

	public Certificado findCertificadoWithEmisorNumeroSerie(Autoridad emisor, BigInteger numeroSerie) {
		Query query = getEntityManager().createNamedQuery("findCertificadoWithEmisorNumeroSerie");

		query.setParameter("emisor", emisor);
		query.setParameter("numeroSerie", numeroSerie);

		return (Certificado) query.getSingleResult();
	}

	/*
	 * CRLs
	 */
	@SuppressWarnings("unchecked")
	public Collection<CRL> findCRLsWithEmisor(Autoridad emisor) {
		Query query = getEntityManager().createNamedQuery("findCRLsWithEmisor");
		
		query.setParameter("emisor", emisor);
		
		return query.getResultList();
	}

	public CRL findLatestCRLWithEmisor(Autoridad emisor) {
		Query query = getEntityManager().createNamedQuery("findLatestCRLWithEmisor");
		
		query.setParameter("emisor", emisor);
		
		return (CRL) query.getSingleResult();
	}
	
	/*
	 * Sellos de Tiempo
	 */
	public SelloTiempo findSelloTiempoWithEmisorNumeroSerie(Autoridad emisor, BigInteger numeroSerie) {
		Query query = getEntityManager().createNamedQuery("findSelloTiempoWithEmisorNumeroSerie");

		query.setParameter("emisor", emisor);
		query.setParameter("numeroSerie", numeroSerie);

		return (SelloTiempo) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SelloTiempo> findSellosTiemposWithEmisor(Autoridad emisor) {
		Query query = getEntityManager().createNamedQuery("findSellosTiemposWithEmisor");

		query.setParameter("emisor", emisor);

		return query.getResultList();
	}

	/*
	 * Solicitudes Personas
	 */
	public SolicitudPersona findSolicitudPersonaWithCodigo(String codigo) {
		Query query = getEntityManager().createNamedQuery("findSolicitudPersonaWithCodigo");

		query.setParameter("codigo", codigo);

		return (SolicitudPersona) query.getSingleResult();
	}
	
	
	public SolicitudPersona findSolicitudPersonaWithNumero(String numero) {
		Query query = getEntityManager().createNamedQuery("findSolicitudPersonaWithNumero");

		query.setParameter("numero", numero);

		return (SolicitudPersona) query.getSingleResult();
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudPersona> findSolicitudesPersonaWithEstado(Solicitud.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithEstado");

		query.setParameter("estado", estado);

		return query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudPersona> findSolicitudesPersonaWithRutTitular(String rutTitular) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithRutTitular");

		query.setParameter("rutTitular", rutTitular);

		return query.getResultList();
	}

	public Collection<SolicitudPersona> findSolicitudesPersonaWithRutTitularAndEstado(String rutTitular, Solicitud.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithRutTitularAndEstado");

		query.setParameter("rutTitular", rutTitular);
		query.setParameter("estado", estado);

		return query.getResultList();
	}

	public Collection<SolicitudPersona> findSolicitudesPersonaWithRutTitularAndEstadoAndTipoCert(String rutTitular, Solicitud.Estado estado, String nombre) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithRutTitularAndEstadoAndTipoCert");

		query.setParameter("rutTitular", rutTitular);
		query.setParameter("estado", estado);
		query.setParameter("nombre", nombre);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<SolicitudPersona> findSolicitudesPersonaWithCorreoTitular(String correoTitular) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithCorreoTitular");

		query.setParameter("correoTitular", correoTitular);

		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudPersona> findSolicitudesPersonaWithNumeroSerie(BigInteger numeroSerie) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithNumeroSerie");

		query.setParameter("numeroSerie", numeroSerie);

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudPersona> findSolicitudesPersonaWithEstadoCertificado(Certificado.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithEstadoCertificado");

		query.setParameter("estado", estado);

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudPersona> findSolicitudesPersonaWithExpiracionEntre(Date desdeFecha, Date hastaFecha) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesPersonaWithExpiracionEntre");

		query.setParameter("desdeFecha", desdeFecha);
		query.setParameter("hastaFecha", hastaFecha);
		
		return query.getResultList();
	}

	
	/*
	 * Solicitudes CAFs
	 */
	public SolicitudCAF findSolicitudCAFWithRutTipoFolio(String rutEmpresa, int tipoDTE, long folio) {
		Query query = getEntityManager().createNamedQuery("findSolicitudCAFWithRutTipoFolio");

		query.setParameter("rutEmpresa", rutEmpresa);
		query.setParameter("tipoDTE", tipoDTE);
		query.setParameter("folio", folio);

		query.setMaxResults(1);
		
		return (SolicitudCAF) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudCAF> findSolicitudesCAFWithRutEmpresa(String rutEmpresa) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesCAFWithRutEmpresa");
		
		query.setParameter("rutEmpresa", rutEmpresa);
		
		return query.getResultList();
	}

	/*
	 * Solicitudes Sitios Web
	 */
	public SolicitudSitioWeb findSolicitudSitioWebWithCodigo(String codigo) {
		Query query = getEntityManager().createNamedQuery("findSolicitudSitioWebWithCodigo");
		
		query.setParameter("codigo", codigo);
		
		return (SolicitudSitioWeb) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudSitioWeb> findSolicitudesSitioWebWithEstado(Solicitud.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesSitioWebWithEstado");
		
		query.setParameter("estado", estado);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudSitioWeb> findSolicitudesSitioWebWithDominio(String dominio) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesSitioWebWithDominio");
		
		query.setParameter("dominio", "%" + dominio.replace("\\", "\\\\").replace("_", "\\_").replace("%", "\\%"));
		
		return query.getResultList();
	}
	
	/*
	 * Solicitudes Sistema
	 */
	public SolicitudSistema findSolicitudSistemaWithCodigo(String codigo) {
		Query query = getEntityManager().createNamedQuery("findSolicitudSistemaWithCodigo");
		
		query.setParameter("codigo", codigo);
		
		return (SolicitudSistema) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudSistema> findSolicitudesSistemaWithEstado(Solicitud.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesSistemaWithEstado");
		
		query.setParameter("estado", estado);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudSistema> findSolicitudesSistemaWithNombre(String nombre) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesSistemaWithNombre");
		
		query.setParameter("nombre", nombre);
		
		return query.getResultList();
	}

    /*
	 * Solicitudes SelloTiempo
	 */
    public SolicitudSelloTiempo findSolicitudSelloTiempoWithCodigo(String codigo) {
        Query query = getEntityManager().createNamedQuery("findSolicitudSelloTiempoWithCodigo");

        query.setParameter("codigo", codigo);

        return (SolicitudSelloTiempo) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public Collection<SolicitudSelloTiempo> findSolicitudesSelloTiempoWithEstado(Solicitud.Estado estado) {
        Query query = getEntityManager().createNamedQuery("findSolicitudesSelloTiempoWithEstado");

        query.setParameter("estado", estado);

        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public Collection<SolicitudSelloTiempo> findSolicitudesSelloTiempoWithNombre(String nombre) {
        Query query = getEntityManager().createNamedQuery("findSolicitudesSelloTiempoWithNombre");

        query.setParameter("nombre", nombre);

        return query.getResultList();
    }

	/*
	 * Solicitudes Codigo
	 */
	public SolicitudCodigo findSolicitudCodigoWithCodigo(String codigo) {
		Query query = getEntityManager().createNamedQuery("findSolicitudCodigoWithCodigo");
		
		query.setParameter("codigo", codigo);
		
		return (SolicitudCodigo) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SolicitudCodigo> findSolicitudesCodigoWithEstado(Solicitud.Estado estado) {
		Query query = getEntityManager().createNamedQuery("findSolicitudesCodigoWithEstado");
		
		query.setParameter("estado", estado);
		
		return query.getResultList();
	}	
	
	/*
	 * Documentos
	 */
	@SuppressWarnings("unchecked")
	public Collection<Documento> findDocumentosWithSolicitud(Solicitud solicitud) {
		Query query = getEntityManager().createNamedQuery("findDocumentosWithSolicitud");
		
		query.setParameter("solicitud", solicitud);
		
		return query.getResultList();
	}
	
	/*
	 * Entity Manager
	 */
	private EntityManager getEntityManager() {
		return entityManager;
	}

	private void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
