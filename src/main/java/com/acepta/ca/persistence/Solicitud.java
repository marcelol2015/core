package com.acepta.ca.persistence;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Entidad persistente abstracta que representa una Solicitud de Certificado en la Autoridad Certificadora. 
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "TIPO", discriminatorType = DiscriminatorType.CHAR, length = 1, columnDefinition = "CHAR NOT NULL")
public abstract class Solicitud {
	public enum Estado {
		PENDIENTE, APROBADA, RECHAZADA, CERRADA, ANULADA
	};

	private Integer id;
	private String codigo;
	private Autoridad autoridad;
	private Certificado certificado;
	private Estado estado;
	private Date fecha;
	private int version;
	
	public Solicitud() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SOLICITUD")
	@TableGenerator(name = "SOLICITUD", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(nullable = false, unique = true, length = 20)
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@ManyToOne
	@JoinColumn(nullable = false)
	public Autoridad getAutoridad() {
		return autoridad;
	}

	public void setAutoridad(Autoridad autoridad) {
		this.autoridad = autoridad;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = true)
	public Certificado getCertificado() {
		return certificado;
	}

	public void setCertificado(Certificado certificado) {
		this.certificado = certificado;
	}
	
	@Enumerated
	@Column(nullable = false)
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public boolean isPendiente() {
		return getEstado() == Estado.PENDIENTE;
	}

	public boolean isAprobada() {
		return getEstado() == Estado.APROBADA;
	}

	public boolean isRechazada() {
		return getEstado() == Estado.RECHAZADA;
	}

	public boolean isCerrada() {
		return getEstado() == Estado.CERRADA;
	}	
	
	public boolean isAnulada(){
		return getEstado() == Estado.ANULADA;		
	}
	public void changeEstado(Estado estado) {
		Date fecha = new Date();

		setEstado(estado);
		setFecha(fecha);
	}
	
	@PrePersist
	protected void generateCodigo() {
		setCodigo(SolicitudCodigoGenerator.generateCodigo());
	}
}
