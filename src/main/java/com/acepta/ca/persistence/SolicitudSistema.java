package com.acepta.ca.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entidad persistente que representa una Solicitud de Certificado para Sistemas.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-05
 */
@Entity
@PrimaryKeyJoinColumn
@DiscriminatorValue("S")
@NamedQueries({
	@NamedQuery(name = "findSolicitudSistemaWithCodigo", query = "SELECT s FROM SolicitudSistema AS s WHERE s.codigo = :codigo"),
	@NamedQuery(name = "findSolicitudesSistemaWithEstado", query = "SELECT s FROM SolicitudSistema AS s WHERE s.estado = :estado"),
	@NamedQuery(name = "findSolicitudesSistemaWithNombre", query = "SELECT s FROM SolicitudSistema AS s WHERE s.sistema.nombre = :nombre")
})
public class SolicitudSistema extends Solicitud {
	private String numero;
	private int mesesVigencia;
	private Sistema sistema;
	
	public SolicitudSistema() {
	}

	@Column(unique = true, nullable = false, length = 31)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(nullable = false)
	public int getMesesVigencia() {
		return mesesVigencia;
	}

	public void setMesesVigencia(int mesesVigencia) {
		this.mesesVigencia = mesesVigencia;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	public Sistema getSistema() {
		return sistema;
	}

	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}
}
