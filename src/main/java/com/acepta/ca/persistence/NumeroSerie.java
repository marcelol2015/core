package com.acepta.ca.persistence;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * Entidad persistente del generador de Numeros de Serie para los Artefactos (certificados,
 * CRLs y sellos de tiempo) emitidos por una Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-03
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "AUTORIDAD_ID", "PROPOSITO" }))
public class NumeroSerie {
	public static enum Proposito {
		CERTIFICADO,
		CRL,
		SELLOTIEMPO
	}
	
	private Integer id;
	private Autoridad autoridad;
	private Proposito proposito;
	private BigInteger numero;
	private int version;
	
	public NumeroSerie() {
	}

	public NumeroSerie(Autoridad autoridad, Proposito entidad, BigInteger numero) {
		setAutoridad(autoridad);
		setProposito(entidad);
		setNumero(numero);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "NUMEROSERIE")
	@TableGenerator(name = "NUMEROSERIE", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(nullable = false)
	public Autoridad getAutoridad() {
		return autoridad;
	}

	public void setAutoridad(Autoridad autoridad) {
		this.autoridad = autoridad;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false)
	public Proposito getProposito() {
		return proposito;
	}

	public void setProposito(Proposito proposito) {
		this.proposito = proposito;
	}

	@Column(nullable = false)
	public BigInteger getNumero() {
		return numero;
	}

	public void setNumero(BigInteger numero) {
		this.numero = numero;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public synchronized BigInteger generarProximoNumero() {
		BigInteger numeroSerie = getNumero();
		
		setNumero(numeroSerie.add(BigInteger.ONE));
		
		return numeroSerie;
	}
}
