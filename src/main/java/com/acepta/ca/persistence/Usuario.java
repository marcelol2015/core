package com.acepta.ca.persistence;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

/**
 * Entidad persistente que representa un Usuario de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "findUsuarioWithRut", query = "SELECT u FROM Usuario AS u WHERE u.rut = :rut"),
	@NamedQuery(name = "findUsuariosWithGrupo", query = "SELECT u FROM Usuario AS u JOIN u.grupos AS g WHERE g.nombre = :grupo"),
	@NamedQuery(name = "findUsuarios", query = "SELECT u FROM Usuario AS u")
})
public class Usuario {
	private Integer id;
	private String rut;
	private String nombre;
	private Collection<Grupo> grupos;
	private int version;
	
	public Usuario() {
	}

	public Usuario(String rut, String nombre) {
		setRut(rut);
		setNombre(nombre);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "USUARIO")
	@TableGenerator(name = "USUARIO", allocationSize = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, unique = true, length = 10)
	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}
	
	@Column(nullable = false, length = 63)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToMany
	@JoinColumn(nullable = false)
	public Collection<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(Collection<Grupo> grupos) {
		this.grupos = grupos;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
