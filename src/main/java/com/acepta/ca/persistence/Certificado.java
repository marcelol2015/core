package com.acepta.ca.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Entidad persistente que representa un Certificado Electronico X.509 emitido por una Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "EMISOR_ID", "NUMEROSERIE" }))
@NamedQueries({
	@NamedQuery(name = "findCertificadosWithEmisor", query = "SELECT c FROM Certificado AS c WHERE c.emisor = :emisor"),
	@NamedQuery(name = "findCertificadosWithEmisorEstado", query = "SELECT c FROM Certificado AS c WHERE c.emisor = :emisor AND c.estado = :estado"),
	@NamedQuery(name = "findCertificadoWithEmisorNumeroSerie", query = "SELECT c FROM Certificado AS c WHERE c.emisor = :emisor AND c.numeroSerie = :numeroSerie")
})
public class Certificado extends Artefacto {
	public enum Estado {
		VALIDO, REVOCADO
	}

	public enum Razon {
		UNUSED, KEY_COMPROMISE, CA_COMPROMISE, AFFILIATION_CHANGED, SUPERSEDED, CESSATION_OF_OPERATION, CERTIFICATE_HOLD, PRIVILEGE_WITHDRAWN, AA_COMPROMISE
	}

	private Date validoDesde;
	private Date validoHasta;
	private String sujeto;
	private Estado estado;
	private Razon razon;
	private Date fecha;
	
	public Certificado() {
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getValidoDesde() {
		return validoDesde;
	}

	public void setValidoDesde(Date validoDesde) {
		this.validoDesde = validoDesde;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getValidoHasta() {
		return validoHasta;
	}

	public void setValidoHasta(Date validoHasta) {
		this.validoHasta = validoHasta;
	}

	@Column(nullable = false, length = 127)
	public String getSujeto() {
		return sujeto;
	}

	public void setSujeto(String sujeto) {
		this.sujeto = sujeto;
	}

	@Enumerated
	@Column(nullable = false)
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Enumerated
	public Razon getRazon() {
		return razon;
	}

	public void setRazon(Razon razon) {
		this.razon = razon;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public boolean isValido() {
		return getEstado() == Estado.VALIDO;
	}

	public boolean isRevocado() {
		return getEstado() == Estado.REVOCADO;
	}
	
	public boolean isVigente() {
		return isValido() && checkVigencia(); 
	}
	
	public boolean isExpirado() {
		return isValido() && !checkVigencia(); 
	}

	public boolean isSuspendido() {
		return isRevocado() && getRazon() == Razon.CERTIFICATE_HOLD;
	}
	
	public void changeEstado(Estado estado) {
		changeEstado(estado, null);
	}
	
	public void changeEstado(Estado estado, Razon razon) {
		Date fecha = new Date();
		
		setEstado(estado);
		setRazon(razon);
		setFecha(fecha);
	}
	
	private boolean checkVigencia() {
		Date fecha = new Date();
		
		return fecha.after(getValidoDesde()) && fecha.before(getValidoHasta());
	}
}
