package com.acepta.ca.persistence;

import com.acepta.ca.xml.util.CompressXML;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Entidad persistente que almacena en formato XML las Solicitudes de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@NamedQuery(name = "findDocumentosWithSolicitud", query = "SELECT d FROM Documento AS d WHERE d.solicitud = :solicitud ORDER BY d.fecha DESC, d.id DESC")
public class Documento {
	private Integer id;
	private Date fecha;
	private Solicitud solicitud;
	private byte[] data;
	private int version;
	
	public Documento() {
	}
	
	public Documento(Solicitud solicitud, byte[] data){
		Date fecha = new Date();
		
		setFecha(fecha);
		setData(data);
		setSolicitud(solicitud);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "DOCUMENTO")
	@TableGenerator(name = "DOCUMENTO", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	@Column(nullable = false)
	public byte[] getData(){
		return CompressXML.decompress(data);
	}

	public void setData(byte[] data){
		this.data = CompressXML.compress(data);
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
