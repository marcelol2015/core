package com.acepta.ca.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

/**
 * Entidad persistente que representa un Grupo de Usuarios de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "findGrupoWithNombre", query = "SELECT g FROM Grupo AS g WHERE g.nombre = :nombre"),
	@NamedQuery(name = "findGrupos", query = "SELECT g FROM Grupo AS g")
})
public class Grupo {
	private Integer id;
	private String nombre;
	private int version;
	
	public Grupo() {
	}

	public Grupo(String nombre) {
		setNombre(nombre);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "GRUPO")
	@TableGenerator(name = "GRUPO", allocationSize = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, unique = true, length = 63)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
