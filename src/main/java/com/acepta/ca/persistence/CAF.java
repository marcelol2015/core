package com.acepta.ca.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Entidad persistente que representa un Codigo de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
public class CAF {
	private static final int LLAVE_ID_PRODUCCION = 300;
	private static final int LLAVE_ID_CERTIFICACION = 100;
	
	private Integer id;
	private String rutEmpresa;
	private String razonSocial;
	private int tipoDTE;
	private long desdeFolio;
	private long hastaFolio;
	private Date fechaAutorizacion;
	private int llaveID;
	private int version;
	
	public CAF() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "CAF")
	@TableGenerator(name = "CAF", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 10)
	public String getRutEmpresa() {
		return rutEmpresa;
	}

	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}

	@Column(nullable = false, length = 40)
	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	@Column(nullable = false)
	public int getTipoDTE() {
		return tipoDTE;
	}

	public void setTipoDTE(int tipoDTE) {
		this.tipoDTE = tipoDTE;
	}

	@Column(nullable = false)
	public long getDesdeFolio() {
		return desdeFolio;
	}

	public void setDesdeFolio(long desdeFolio) {
		this.desdeFolio = desdeFolio;
	}

	@Column(nullable = false)
	public long getHastaFolio() {
		return hastaFolio;
	}

	public void setHastaFolio(long hastaFolio) {
		this.hastaFolio = hastaFolio;
	}

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	@Column(nullable = false)
	public int getLlaveID() {
		return llaveID;
	}

	public void setLlaveID(int llaveID) {
		this.llaveID = llaveID;
	}	

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public boolean isCertificacion() {
		return getLlaveID() == LLAVE_ID_CERTIFICACION;
	}
	
	public boolean isProduccion() {
		return getLlaveID() == LLAVE_ID_PRODUCCION;
	}
}
