package com.acepta.ca.persistence;

/**
 * Generador de Codigos de identificacion unicos para las Solicitudes de Certificado en la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class SolicitudCodigoGenerator {
	private static final String SOLICITUD_CODIGO_FORMAT = "%04d-%c-%04d-%c-%04d-%c";
	private static final String SOLICITUD_CODIGO_CHECKSUM = "K0123456789";

	public static String generateCodigo() {
		return format(generate());
	}

	private static String format(long code) {
		int d3 = (int) ((code / 1L) % 10000L);
		int d2 = (int) ((code / 10000L) % 10000L);
		int d1 = (int) ((code / 100000000L) % 10000L);

		return String.format(SOLICITUD_CODIGO_FORMAT, d1, checksum(d1), d2, checksum(d2), d3, checksum(d3));
	}

	private static char checksum(int value) {
		int checksum = 1;
		for (int digit = 0; value != 0; digit++) {
			checksum += (value % 10) * (9 - (digit % 6));
			value /= 10;
		}
		return SOLICITUD_CODIGO_CHECKSUM.charAt(checksum % 11);
	}

	private static synchronized long generate() {
		while (true) {
			try {
				Thread.sleep(1);

				return System.currentTimeMillis();
			}
			catch (InterruptedException exception) {
			}
		}
	}
}
