package com.acepta.ca.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

/**
 * Entidad persistente con la informacion de los Sitios Web de las empresas que han solicitado
 * la emision de Certificados Electronicos X.509 en la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-09
 */
@Entity
public class SitioWeb {
	private Integer id;
	private String dominio;
	private String rut;
	private String organizacion;
	private String unidadOrganizacional;
	private String estado;
	private String ciudad;
	private String pais;
	private int version;
	
	public SitioWeb() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SITIOWEB")
	@TableGenerator(name = "SITIOWEB", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 10)
	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	@Column(nullable = false, length = 63)
	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	@Column(nullable = false, length = 63)
	public String getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}

	@Column(length = 63)
	public String getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(String unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Column(length = 63)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(length = 63)
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	@Column(nullable = false, length = 2)
	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
