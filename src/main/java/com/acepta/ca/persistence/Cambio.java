package com.acepta.ca.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Entidad persistente que registra los cambios realizados en las solicitudes
 * y/o certificados electronicos X.509 de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
public class Cambio {
	private Integer id;
	private Date fecha;
	private String tipo;
	private Solicitud solicitud;
	private Certificado certificado;
	private String comentario;
	private int version;
	
	public Cambio() {
	}

	public Cambio(String tipo, Solicitud solicitud) {
		this(tipo, solicitud, null, null);
	}

	public Cambio(String tipo, Solicitud solicitud, String comentario) {
		this(tipo, solicitud, null, comentario);
	}
	
	public Cambio(String tipo, Certificado certificado, String comentario) {
		this(tipo, null, certificado, comentario);
	}

	public Cambio(String tipo, Solicitud solicitud, Certificado certificado, String comentario) {
		Date fecha = new Date();
		
		setFecha(fecha);
		setTipo(tipo);
		setSolicitud(solicitud);
		setCertificado(certificado);
		setComentario(comentario);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "CAMBIO")
	@TableGenerator(name = "CAMBIO", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(nullable = false, length = 15)
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@ManyToOne
	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	@ManyToOne
	public Certificado getCertificado() {
		return certificado;
	}

	public void setCertificado(Certificado certificado) {
		this.certificado = certificado;
	}

	@Column(length = 63)
	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
