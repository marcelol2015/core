package com.acepta.ca.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entidad persistente que representa una Solicitud de Certificado para Sitios Web.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@PrimaryKeyJoinColumn
@DiscriminatorValue("W")
@NamedQueries( {
	@NamedQuery(name = "findSolicitudSitioWebWithCodigo", query = "SELECT s FROM SolicitudSitioWeb AS s WHERE s.codigo = :codigo"),
	@NamedQuery(name = "findSolicitudesSitioWebWithEstado", query = "SELECT s FROM SolicitudSitioWeb AS s WHERE s.estado = :estado"),
	@NamedQuery(name = "findSolicitudesSitioWebWithDominio", query = "SELECT s FROM SolicitudSitioWeb AS s WHERE s.sitioWeb.dominio LIKE :dominio")
})
public class SolicitudSitioWeb extends Solicitud {
	private String numero;
	private String clave;
	private int mesesVigencia;
	private SitioWeb sitioWeb;

	public SolicitudSitioWeb() {
	}

	@Column(unique = true, nullable = false, length = 31)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(nullable = false, length = 31)
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(nullable = false)
	public int getMesesVigencia() {
		return mesesVigencia;
	}

	public void setMesesVigencia(int mesesVigencia) {
		this.mesesVigencia = mesesVigencia;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	public SitioWeb getSitioWeb() {
		return sitioWeb;
	}

	public void setSitioWeb(SitioWeb sitioWeb) {
		this.sitioWeb = sitioWeb;
	}
}
