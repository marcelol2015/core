package com.acepta.ca.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entidad persistente que representa una Solicitud de Certificado para Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@PrimaryKeyJoinColumn
@DiscriminatorValue("P")
@NamedQueries({
	@NamedQuery(name = "findSolicitudPersonaWithCodigo", query = "SELECT s FROM SolicitudPersona AS s WHERE s.codigo = :codigo"),
	@NamedQuery(name = "findSolicitudPersonaWithNumero", query = "SELECT s FROM SolicitudPersona AS s WHERE s.numero = :numero"),
	@NamedQuery(name = "findSolicitudesPersonaWithEstado", query = "SELECT s FROM SolicitudPersona AS s WHERE s.estado = :estado ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithRutTitular", query = "SELECT s FROM SolicitudPersona AS s WHERE s.titular.rut = :rutTitular ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithRutTitularAndEstado", query = "SELECT s FROM SolicitudPersona AS s WHERE s.titular.rut = :rutTitular and s.estado = :estado ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithRutTitularAndEstadoAndTipoCert", query = "SELECT s FROM SolicitudPersona AS s WHERE s.titular.rut = :rutTitular and s.estado = :estado and s.autoridad.nombre = :nombre ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithCorreoTitular", query = "SELECT s FROM SolicitudPersona AS s WHERE s.titular.correo = :correoTitular ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithNumeroSerie", query = "SELECT s FROM SolicitudPersona AS s WHERE s.certificado.numeroSerie = :numeroSerie ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithEstadoCertificado", query = "SELECT s FROM SolicitudPersona AS s WHERE s.certificado.estado = :estado ORDER BY s.fecha DESC"),
	@NamedQuery(name = "findSolicitudesPersonaWithExpiracionEntre", query = "SELECT s FROM SolicitudPersona AS s WHERE s.certificado.validoHasta BETWEEN :desdeFecha AND :hastaFecha ORDER BY s.fecha DESC")
})
public class SolicitudPersona extends Solicitud {
	private String numero;
	private String clave;
	private String recordatorio;
	private int mesesVigencia;
	private boolean pagada;
	private long montoPagado;
	private Persona titular;
	
	public SolicitudPersona() {
	}
	
	@Column(unique = true, nullable = false, length = 31)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(nullable = false, length = 31)
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column
	public String getRecordatorio() {
		return recordatorio;
	}

	public void setRecordatorio(String recordatorio) {
		this.recordatorio = recordatorio;
	}

	@Column(nullable = false)
	public int getMesesVigencia() {
		return mesesVigencia;
	}

	public void setMesesVigencia(int mesesVigencia) {
		this.mesesVigencia = mesesVigencia;
	}

	@Column(nullable = false)
	public boolean isPagada() {
		return pagada;
	}

	public void setPagada(boolean pagada) {
		this.pagada = pagada;
	}

	@Column(nullable = false)
	public long getMontoPagado() {
		return montoPagado;
	}

	public void setMontoPagado(long montoPagado) {
		this.montoPagado = montoPagado;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	public Persona getTitular() {
		return titular;
	}

	public void setTitular(Persona titular) {
		this.titular = titular;
	}
}
