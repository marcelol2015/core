package com.acepta.ca.persistence;

import javax.persistence.*;

/**
 * Entidad persistente que representa una Solicitud de Certificado para SelloTiempo.
 *
 * @author Angelo Estartus
 * @version 1.0, 2015-10-07
 */
@Entity
@PrimaryKeyJoinColumn
@DiscriminatorValue("T")
@NamedQueries({
        @NamedQuery(name = "findSolicitudSelloTiempoWithCodigo", query = "SELECT s FROM SolicitudSelloTiempo AS s WHERE s.codigo = :codigo"),
        @NamedQuery(name = "findSolicitudesSelloTiempoWithEstado", query = "SELECT s FROM SolicitudSelloTiempo AS s WHERE s.estado = :estado"),
        @NamedQuery(name = "findSolicitudesSelloTiempoWithNombre", query = "SELECT s FROM SolicitudSelloTiempo AS s WHERE s.sistema.nombre = :nombre")
})
public class SolicitudSelloTiempo extends Solicitud {
    private String numero;
    private int mesesVigencia;
    private Sistema sistema;

    public SolicitudSelloTiempo() {
    }

    @Column(unique = true, nullable = false, length = 31)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(nullable = false)
    public int getMesesVigencia() {
        return mesesVigencia;
    }

    public void setMesesVigencia(int mesesVigencia) {
        this.mesesVigencia = mesesVigencia;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(nullable = false)
    public Sistema getSistema() {
        return sistema;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }
}
