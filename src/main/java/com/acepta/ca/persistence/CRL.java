package com.acepta.ca.persistence;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "EMISOR_ID", "NUMEROSERIE" }))
@NamedQueries({
	@NamedQuery(name = "findLatestCRLWithEmisor", query = "SELECT c FROM CRL AS c WHERE c.emisor = :emisor AND c.id = (SELECT MAX(r.id) FROM CRL AS r WHERE r.emisor = :emisor)"),
	@NamedQuery(name = "findCRLsWithEmisor", query = "SELECT c FROM CRL AS c WHERE c.emisor = :emisor")
})
public class CRL extends Artefacto {
	private Date validoDesde;
	private Date validoHasta;
	
	public CRL() {
	}

	public CRL(Autoridad emisor, BigInteger numeroSerie, Date validoDesde, Date validoHasta, byte[] data) {
		setEmisor(emisor);
		setNumeroSerie(numeroSerie);
		setValidoDesde(validoDesde);
		setValidoHasta(validoHasta);
		setData(data);
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getValidoDesde() {
		return validoDesde;
	}

	public void setValidoDesde(Date validoDesde) {
		this.validoDesde = validoDesde;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getValidoHasta() {
		return validoHasta;
	}

	public void setValidoHasta(Date validoHasta) {
		this.validoHasta = validoHasta;
	}
}
