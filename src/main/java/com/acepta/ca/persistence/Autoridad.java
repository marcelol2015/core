package com.acepta.ca.persistence;

import java.math.BigInteger;
import java.util.Collection;
import java.util.NoSuchElementException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

/**
 * Entidad persistente que representa una Autoridad Certificadora o de Sellado de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@NamedQuery(name = "findAutoridadWithNombre", query = "SELECT a FROM Autoridad AS a WHERE a.nombre = :nombre")
public class Autoridad {
	private Integer id;
	private String nombre;
	private Autoridad emisor;
	private Collection<NumeroSerie> numerosSeries;
	private boolean avanzada;
	private int version;
	
	public Autoridad() {
	}

	public Autoridad(String nombre, boolean avanzada) {
		setNombre(nombre);
		setAvanzada(avanzada);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "AUTORIDAD")
	@TableGenerator(name = "AUTORIDAD", allocationSize = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, unique = true, length = 31)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToOne
	public Autoridad getEmisor() {
		return emisor;
	}

	public void setEmisor(Autoridad emisor) {
		this.emisor = emisor;
	}
	
	@OneToMany(mappedBy = "autoridad", cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	public Collection<NumeroSerie> getNumerosSeries() {
		return numerosSeries;
	}

	public void setNumerosSeries(Collection<NumeroSerie> numerosSeries) {
		this.numerosSeries = numerosSeries;
	}

	@Column(nullable = false)
	public boolean isAvanzada() {
		return avanzada;
	}

	public void setAvanzada(boolean avanzada) {
		this.avanzada = avanzada;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public BigInteger generarNumeroSerieCertificado() {
		return generarNumeroSerieParaProposito(NumeroSerie.Proposito.CERTIFICADO);
	}
	
	public BigInteger generarNumeroSerieCRL() {
		return generarNumeroSerieParaProposito(NumeroSerie.Proposito.CRL);
	}
	
	public BigInteger generarNumeroSerieSelloTiempo() {
		return generarNumeroSerieParaProposito(NumeroSerie.Proposito.SELLOTIEMPO);
	}

	private BigInteger generarNumeroSerieParaProposito(NumeroSerie.Proposito proposito) {
		for (NumeroSerie numeroSerie : getNumerosSeries())
			if (numeroSerie.getProposito() == proposito)
				return numeroSerie.generarProximoNumero();
		throw new NoSuchElementException();
	}
}
