package com.acepta.ca.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entidad persistente que representa una Solicitud de Certificado para la Firma de Codigo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-09
 */
@Entity
@PrimaryKeyJoinColumn
@DiscriminatorValue("C")
@NamedQueries( {
	@NamedQuery(name = "findSolicitudCodigoWithCodigo", query = "SELECT s FROM SolicitudCodigo AS s WHERE s.codigo = :codigo"),
	@NamedQuery(name = "findSolicitudesCodigoWithEstado", query = "SELECT s FROM SolicitudCodigo AS s WHERE s.estado = :estado")
})
public class SolicitudCodigo extends Solicitud {
	private String numero;
	private String clave;
	private int mesesVigencia;
	private Compania compania;

	public SolicitudCodigo() {
	}

	@Column(unique = true, nullable = false, length = 31)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(nullable = false, length = 31)
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(nullable = false)
	public int getMesesVigencia() {
		return mesesVigencia;
	}

	public void setMesesVigencia(int mesesVigencia) {
		this.mesesVigencia = mesesVigencia;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	public Compania getCompania() {
		return compania;
	}

	public void setCompania(Compania compania) {
		this.compania = compania;
	}
}
