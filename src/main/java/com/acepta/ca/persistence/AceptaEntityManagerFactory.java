package com.acepta.ca.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Fabrica de administradores de entidades persistentes de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 *
 */
public class AceptaEntityManagerFactory {
	private EntityManagerFactory entityManagerFactory;

	public AceptaEntityManagerFactory(String persistenceUnitName) {
		setEntityManagerFactory(Persistence.createEntityManagerFactory(persistenceUnitName));
	}

	public void close() {
		getEntityManagerFactory().close();
	}

	public AceptaEntityManager createEntityManager() {
		return new AceptaEntityManager(getEntityManagerFactory().createEntityManager());
	}

	private EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	private void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
}
