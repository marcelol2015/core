package com.acepta.ca.persistence;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

/**
 * Artefacto emitido por una Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-11-15
 */
@MappedSuperclass
public abstract class Artefacto {
	private Integer id;
	private Autoridad emisor;
	private BigInteger numeroSerie;
	private byte[] data;
	private int version;

	public Artefacto() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ARTEFACTO")
	@TableGenerator(name = "ARTEFACTO", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(nullable = false)
	public Autoridad getEmisor() {
		return emisor;
	}

	public void setEmisor(Autoridad emisor) {
		this.emisor = emisor;
	}

	@Column(nullable = false)
	public BigInteger getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(BigInteger numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Column(nullable = false)
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
