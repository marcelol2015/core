package com.acepta.ca.persistence;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entidad persistente que representa una Solicitud de Certificado para un Codigo de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
@PrimaryKeyJoinColumn
@DiscriminatorValue("F")
@NamedQueries({ 
	@NamedQuery(name = "findSolicitudCAFWithRutTipoFolio", query = "SELECT s FROM SolicitudCAF AS s WHERE s.cAF.rutEmpresa = :rutEmpresa AND s.cAF.tipoDTE = :tipoDTE AND s.cAF.desdeFolio <= :folio AND s.cAF.hastaFolio >= :folio"),
	@NamedQuery(name = "findSolicitudesCAFWithRutEmpresa", query = "SELECT s FROM SolicitudCAF AS s WHERE s.cAF.rutEmpresa = :rutEmpresa ORDER BY s.cAF.tipoDTE")	
})
public class SolicitudCAF extends Solicitud {
	private CAF cAF;
	
	public SolicitudCAF() {
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	public CAF getCAF() {
		return cAF;
	}

	public void setCAF(CAF cAF) {
		this.cAF = cAF;
	}
}
