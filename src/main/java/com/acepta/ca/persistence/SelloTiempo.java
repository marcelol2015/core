package com.acepta.ca.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Entidad persistente que representa un Sello de Tiempo X.509 emitido por una Autoridad de Sellado de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-28
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "EMISOR_ID", "NUMEROSERIE" }))
@NamedQueries({
	@NamedQuery(name = "findSelloTiempoWithEmisorNumeroSerie", query = "SELECT s FROM SelloTiempo AS s WHERE s.emisor = :emisor AND s.numeroSerie = :numeroSerie"),
	@NamedQuery(name = "findSellosTiemposWithEmisor", query = "SELECT s FROM SelloTiempo AS s WHERE s.emisor = :emisor")
})
public class SelloTiempo extends Artefacto {
	private Date tiempo;
	
	public SelloTiempo() {
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getTiempo() {
		return tiempo;
	}

	public void setTiempo(Date tiempo) {
		this.tiempo = tiempo;
	}
}
