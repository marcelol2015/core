package com.acepta.ca.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

/**
 * Entidad persistente con informacion de las Personas Naturales que han solicitado Certificados
 * Electronicos X.509 en la Autoridad Certificadora.  
 *  
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
@Entity
public class Persona {
	private Integer id;
	private String rut;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String profesion;
	private String correo;
	private int version;
	
	public Persona() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PERSONA")
	@TableGenerator(name = "PERSONA", allocationSize = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 10)
	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	@Column(nullable = false, length = 63)
	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	@Column(nullable = false, length = 63)
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Column(nullable = false, length = 63)
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Column(length = 63)
	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	@Column(nullable = false, length = 63)
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Version
	@Column(nullable = false)
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNombreComun() {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(getNombres());
		buffer.append(" ");
		buffer.append(getApellidoPaterno());
		buffer.append(" ");
		buffer.append(getApellidoMaterno());
		
		return buffer.toString().replaceAll(" +", " ").trim();
	}
}
