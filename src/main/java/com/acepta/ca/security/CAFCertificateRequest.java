package com.acepta.ca.security;

import java.math.BigInteger;
import java.security.PublicKey;
import java.util.Date;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.NameBuilder;

/**
 * Peticion de Certificados Electronicos X.509 para Codigos de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class CAFCertificateRequest extends CertificateRequest {
	private BigInteger serialNumber;
	private Date notBefore;
	private Date notAfter;
	private String subjectRUT;
	private String subjectRazonSocial;
	private int subjectTipoDTE;
	private long subjectDesdeFolio;
	private long subjectHastaFolio;
	private int subjectLlaveID;
	private byte[] subjectCAF;
	private PublicKey subjectPublicKey;

	public CAFCertificateRequest() {
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public String getSubjectRUT() {
		return subjectRUT;
	}

	public String getSubjectRazonSocial() {
		return subjectRazonSocial;
	}

	public int getSubjectTipoDTE() {
		return subjectTipoDTE;
	}

	public long getSubjectDesdeFolio() {
		return subjectDesdeFolio;
	}

	public long getSubjectHastaFolio() {
		return subjectHastaFolio;
	}

	public byte[] getSubjectCAF() {
		return subjectCAF;
	}

	public int getSubjectLlaveID() {
		return subjectLlaveID;
	}

	public PublicKey getSubjectPublicKey() {
		return subjectPublicKey;
	}

	public Name getSubjectName() {
		return createSubjectName(getSubjectRazonSocial(), getSubjectTipoDTE(), getSubjectDesdeFolio(), getSubjectHastaFolio(), getSubjectLlaveID());
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public void setSubjectRUT(String rut) {
		this.subjectRUT = rut;
	}

	public void setSubjectRazonSocial(String razonSocial) {
		this.subjectRazonSocial = razonSocial;
	}

	public void setSubjectTipoDTE(int tipoDTE) {
		this.subjectTipoDTE = tipoDTE;
	}

	public void setSubjectDesdeFolio(long desdeFolio) {
		this.subjectDesdeFolio = desdeFolio;
	}

	public void setSubjectHastaFolio(long hastaFolio) {
		this.subjectHastaFolio = hastaFolio;
	}

	public void setSubjectCAF(byte[] subjectCAF) {
		this.subjectCAF = subjectCAF;
	}

	public void setSubjectLlaveID(int subjectLlaveID) {
		this.subjectLlaveID = subjectLlaveID;
	}
	
	public void setSubjectPublicKey(PublicKey publicKey) {
		this.subjectPublicKey = publicKey;
	}

	private static Name createSubjectName(String razonSocial, int tipoDTE, long desdeFolio, long hastaFolio, int llaveID) {
		NameBuilder buffer = new NameBuilder();

		buffer.append(NameBuilder.COUNTRY, "CL");
		buffer.append(NameBuilder.COMMON_NAME, razonSocial + " " + formatLlaveID(llaveID) + " " + formatTipoDTE(tipoDTE) + " FOLIOS " + desdeFolio + " AL " + hastaFolio);

		return buffer.toName();
	}

	private static String formatTipoDTE(int tipoDTE) {
		switch (tipoDTE) {
		case 33:
			return "FACTURAS";
		case 34:
			return "FACTURAS EXENTAS";
		case 39:
			return "BOLETAS DE SERVICIO";
		case 41:
			return "BOLETAS EXENTAS";
		case 46:
			return "FACTURAS DE COMPRA";
		case 52:
			return "GUIAS DE DESPACHO";
		case 56:
			return "NOTAS DE DEBITO";
		case 61:
			return "NOTAS DE CREDITO";
		case 110:
			return "FACTURAS DE EXPORTACION";
		case 111:
			return "NOTAS DE DEBITO DE EXPORTACION";
		case 112:
			return "NOTAS DE CREDITO DE EXPORTACION";
		}
		return "DTE TIPO " + tipoDTE;
	}
	
	private static String formatLlaveID(int llaveID) {
		if (llaveID == 100)
			return "CERTIFICACION";
		
		if (llaveID == 300)
			return "PRODUCCION";
		
		return "AMBIENTE DESCONOCIDO";
	}
}
