package com.acepta.ca.security;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.acepta.ca.xml.XMLDocument;

/**
 * Configuracion del proveedor criptografico de las Autoridades Certificadoras de Acepta.com S.A.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaSecurityConfiguration extends XMLDocument {
	private static final String ACEPTA_SECURITY_NS_URI = "http://www.acepta.com/xml/ns/security";
	private static final String ACEPTA_SECURITY_NS_PREFIX = "s";

	private static final String ACEPTA_SECURITY_PROVIDER_XPATH = "/s:security/s:security-token[@name='%s']/s:provider";
	private static final String ACEPTA_SECURITY_AUTHORITY_PROVIDER_XPATH = "/s:security/s:security-token[@name='%s']/s:authority";
	private static final String ACEPTA_SECURITY_PROPERTY_XPATH = "/s:security/s:security-token[@name='%s']/s:properties/s:property[@name='%s']/@value";
	private static final String ACEPTA_SECURITY_PROPERTIES_XPATH = "/s:security/s:security-token[@name='%s']/s:properties/s:property/@name";

	private String securityTokenName;
	
	public AceptaSecurityConfiguration(String securityTokenName, InputStream input) throws SAXException, IOException {
		super(input);
		setSecurityTokenName(securityTokenName);
		setNamespaceURI(ACEPTA_SECURITY_NS_PREFIX, ACEPTA_SECURITY_NS_URI);
	}
	
	public String getProvider() {
		return getString(String.format(ACEPTA_SECURITY_PROVIDER_XPATH, getSecurityTokenName())).trim();
	}
	
	public String getAuthorityProvider() {
		return getString(String.format(ACEPTA_SECURITY_AUTHORITY_PROVIDER_XPATH, getSecurityTokenName())).trim();
	}

	public String getProperty(String propertyName) {
		return getString(String.format(ACEPTA_SECURITY_PROPERTY_XPATH, getSecurityTokenName(), propertyName));
	}
	
	public Collection<String> getPropertyNames() {
		NodeList nodes = getNodeSet(String.format(ACEPTA_SECURITY_PROPERTIES_XPATH, getSecurityTokenName()));

		Collection<String> propertyNames = new ArrayList<String>();
		
		for (int index = 0; index < nodes.getLength(); index++)
			propertyNames.add(nodes.item(index).getNodeValue());
		
		return propertyNames;
	}

	public String getSecurityTokenName() {
		return securityTokenName;
	}

	private void setSecurityTokenName(String securityTokenName) {
		this.securityTokenName = securityTokenName;
	}
}
