package com.acepta.ca.security;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.Date;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.NameBuilder;
import com.acepta.ca.security.util.PKCS10;

/**
 * Peticion de Certificados Electronicos X.509 para Sitios Web.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-02-05
 */
public class SitioWebCertificateRequest extends CertificateRequest {
	private BigInteger serialNumber;
	private Date notBefore;
	private Date notAfter;
	private String subjectEmailAddress;
	private String subjectCommonName;
	private String subjectOrganizationalUnit;
	private String subjectOrganization;
	private String subjectLocality;
	private String subjectState;
	private String subjectCountry;
	private PublicKey subjectPublicKey;

	public SitioWebCertificateRequest() {
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public Name getSubjectName() {
		return createSubjectName(getSubjectCountry(), getSubjectState(), getSubjectLocality(),
				getSubjectOrganization(), getSubjectOrganizationalUnit(), getSubjectCommonName(),
				getSubjectEmailAddress());
	}

	public PublicKey getSubjectPublicKey() {
		return subjectPublicKey;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public void setSubjectName(String emailAddress, String commonName, String organizationalUnit, String organization, String locality, String state, String country) {
		setSubjectEmailAddress(emailAddress);
		setSubjectCommonName(commonName);
		setSubjectOrganizationalUnit(organizationalUnit);
		setSubjectOrganization(organization);
		setSubjectLocality(locality);
		setSubjectState(state);
		setSubjectCountry(country);
	}

	public void setSubjectPublicKey(PublicKey subjectPublicKey) {
		this.subjectPublicKey = subjectPublicKey;
	}

	public String getSubjectEmailAddress() {
		return subjectEmailAddress;
	}

	public String getSubjectCommonName() {
		return subjectCommonName;
	}

	public String getSubjectOrganizationalUnit() {
		return subjectOrganizationalUnit;
	}

	public String getSubjectOrganization() {
		return subjectOrganization;
	}

	public String getSubjectLocality() {
		return subjectLocality;
	}

	public String getSubjectState() {
		return subjectState;
	}

	public String getSubjectCountry() {
		return subjectCountry;
	}

	public void setSubjectEmailAddress(String subjectEmailAddress) {
		this.subjectEmailAddress = subjectEmailAddress;
	}

	public void setSubjectCommonName(String subjectCommonName) {
		this.subjectCommonName = subjectCommonName;
	}

	public void setSubjectOrganizationalUnit(String subjectOrganizationalUnit) {
		this.subjectOrganizationalUnit = subjectOrganizationalUnit;
	}

	public void setSubjectOrganization(String subjectOrganization) {
		this.subjectOrganization = subjectOrganization;
	}

	public void setSubjectLocality(String subjectLocality) {
		this.subjectLocality = subjectLocality;
	}

	public void setSubjectState(String subjectState) {
		this.subjectState = subjectState;
	}

	public void setSubjectCountry(String subjectCountry) {
		this.subjectCountry = subjectCountry;
	}

	public void setSubjectPublicKeyFromPKCS10(String pkcs10Base64) throws AceptaSecurityException {
		try {
			setSubjectPublicKey(PKCS10.decodePublicKeyFromPKCS10CerticateRequest(pkcs10Base64));
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException(exception);
		}
	}
	
	private static Name createSubjectName(String country, String stateOrProvince, String locality, String organization, String organizationalUnit, String commonName, String emailAddress) {
		NameBuilder buffer = new NameBuilder();

		buffer.append(NameBuilder.COUNTRY, country);

		if ((stateOrProvince = normalize(stateOrProvince)) != null)
			buffer.append(NameBuilder.STATE_OR_PROVINCE, stateOrProvince);

		if ((locality = normalize(locality)) != null)
			buffer.append(NameBuilder.LOCALITY, locality);

		if ((organization = normalize(organization)) != null)
			buffer.append(NameBuilder.ORGANIZATION, organization);

		if ((organizationalUnit = normalize(organizationalUnit)) != null)
			buffer.append(NameBuilder.ORGANIZATIONAL_UNIT, organizationalUnit);

		if ((commonName = normalize(commonName)) != null)
			buffer.append(NameBuilder.COMMON_NAME, commonName);

		if ((emailAddress = normalize(emailAddress)) != null)
			buffer.append(NameBuilder.EMAIL_ADDRESS, emailAddress);

		return buffer.toName();
	}
	
	private static String normalize(String text) {
		if (text != null) {
			text = text.trim();
			if (text.length() != 0)
				return text;
		}
		return null;
	}
}
