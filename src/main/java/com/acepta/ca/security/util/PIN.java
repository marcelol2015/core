package com.acepta.ca.security.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.bouncycastle.util.encoders.Base64;

/**
 * Encripta PIN de las Solicitudes de Certificados para Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-18
 */
public class PIN {
	private static final String PIN_MESSAGE_DIGEST_ALGORITHM = "SHA1";
	private static final String PIN_CHARACTER_ENCODING = "ISO-8859-1";
	
	public static String encrypt(String pin) {
		try {
			MessageDigest digest = MessageDigest.getInstance(PIN_MESSAGE_DIGEST_ALGORITHM);
	
			digest.update(pin.getBytes(PIN_CHARACTER_ENCODING));
	
			return new String(Base64.encode(digest.digest()));
		}
		catch (NoSuchAlgorithmException exception) {
			throw new RuntimeException(exception);
		}
		catch (UnsupportedEncodingException exception) {
			throw new RuntimeException(exception);
		}
	}
}
