package com.acepta.ca.security.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.openssl.PasswordFinder;

/**
 * Utilitario para codificar y decodificar certificados X.509 y llaves RSA publicas y privadas.
 * 
 * Las llaves RSA decodificafas se instancian en el proveedor criptografico SunRsaSign y los
 * certificados X.509 en el proveedor SUN. Por lo tanto, en particular, las llaves RSA no se
 * pueden ocupar con otros proveedores para generar o validar firmas. 
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-18
 *
 */
public class PEM {
	private static final String RSA_KEY_PROVIDER_NAME = "SunRsaSign";
	private static final String X509_CERTIFICATE_PROVIDER_NAME = "SUN";
	
	private static final String RSA_KEY_ENCRYPTION_PROVIDER_NAME = "SunJCE";
	private static final String RSA_KEY_ENCRYPTION_ALGORITHM_NAME = "DES-EDE3-CBC";

	/*
	 * RSA Public Key
	 */
	public static PublicKey decodeRSAPublicKey(String rsaPublicKey) throws IOException {
		return (PublicKey) decodeObject(rsaPublicKey, RSA_KEY_PROVIDER_NAME, null);
	}
	
	public static String encodeRSAPublicKey(PublicKey rsaPublicKey) throws IOException {
		return encodeObject(rsaPublicKey, null, null, null);
	}
	
	/*
	 * RSA PrivateKey
	 */
	public static PrivateKey decodeRSAPrivateKey(String rsaPrivateKey) throws IOException {
		return decodeRSAPrivateKey(rsaPrivateKey, null);
	}

	public static PrivateKey decodeRSAPrivateKey(String rsaPrivateKey, String password) throws IOException {
		KeyPair keyPair = (KeyPair) decodeObject(rsaPrivateKey, RSA_KEY_PROVIDER_NAME, password);
		
		return (keyPair != null ? keyPair.getPrivate() : null);
	}

	public static String encodeRSAPrivateKey(PrivateKey rsaPrivateKey) throws IOException {
		return encodeRSAPrivateKey(rsaPrivateKey, null);
	}

	public static String encodeRSAPrivateKey(PrivateKey rsaPrivateKey, String password) throws IOException {
		return encodeObject(rsaPrivateKey, RSA_KEY_ENCRYPTION_PROVIDER_NAME, RSA_KEY_ENCRYPTION_ALGORITHM_NAME, password);
	}

	/*
	 * X.509 Certificate
	 */
	public static X509Certificate decodeX509Certificate(String certificate) throws IOException {
		return (X509Certificate) decodeObject(certificate, X509_CERTIFICATE_PROVIDER_NAME, null);
	}

	public static String encodeX509Certificate(X509Certificate certificate) throws IOException {
		return encodeObject(certificate, null, null, null);
	}

	
 	private static Object decodeObject(String object, String provider, final String password) throws IOException {
		PasswordFinder passwordFinder = new PasswordFinder() {
			public char[] getPassword() {
				return (password != null ? password.toCharArray() : null);
			}
		};
		
		PEMReader reader = new PEMReader(new StringReader(object), passwordFinder, provider);
		try {
			return reader.readObject();
		}
		finally {
			reader.close();
		}
	}
 	
	private static String encodeObject(Object object, String provider, String algorithm, String password) throws IOException {
		StringWriter stringWriter = new StringWriter();
		
		PEMWriter writer = new PEMWriter(stringWriter, provider);
		try {
			if (password != null)
				writer.writeObject(object, algorithm, password.toCharArray(), new SecureRandom());
			else
				writer.writeObject(object);
		}
		finally {
			writer.close();
		}
		
		return stringWriter.toString();
	}
}
