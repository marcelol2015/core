package com.acepta.ca.security.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.List;

public class X509 {
	private static final String X509_CERTIFICATE_FACTORY_TYPE = "X.509";
	
	public static X509Certificate decodeCertificate(byte[] data) throws CertificateException, IOException {
		CertificateFactory certificateFactory = CertificateFactory.getInstance(X509_CERTIFICATE_FACTORY_TYPE);
	
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		try {
			return (X509Certificate) certificateFactory.generateCertificate(input);
		}
		finally {
			input.close();
		}
	}
	
	public static String decodeSubjectRUTFromCertificate(String certificateBase64) throws CertificateException, IOException {
		return decodeSubjectRUTFromCertificate(decodeCertificate(BASE64.decode(certificateBase64)));
	}
	
	public static String decodeSubjectRUTFromCertificate(X509Certificate certificate) throws CertificateParsingException {
		for (List<?> generalName : certificate.getSubjectAlternativeNames()) {
			Integer nameType = (Integer) generalName.get(0);
			
			if (nameType.intValue() == 0) {
				byte[] otherName = (byte[]) generalName.get(1);
				
				if (otherName.length >= 14) {
					/*
					 *  OtherName ::= SEQUENCE {
	          		 *    type-id    OBJECT IDENTIFIER,
	          		 *	  value      [0] EXPLICIT ANY DEFINED BY type-id
	          		 *  }
					 */
	
					// OtherName type-id is 1.3.6.1.4.1.8321.1?
					if (otherName[ 0] == (byte) 0x30 && otherName[ 2] == (byte) 0x06 &&
						otherName[ 3] == (byte) 0x08 && otherName[ 4] == (byte) 0x2b &&
						otherName[ 5] == (byte) 0x06 && otherName[ 6] == (byte) 0x01 &&
						otherName[ 7] == (byte) 0x04 && otherName[ 8] == (byte) 0x01 &&
						otherName[ 9] == (byte) 0xc1 && otherName[10] == (byte) 0x01 &&
						otherName[11] == (byte) 0x01 && otherName[12] == (byte) 0xa0) {
	
						/*
						 * OtherName ::= SEQUENCE {
						 *   type-id    OBJECT IDENTIFIER = 1.3.6.1.4.1.8321.1
						 *   subjectRut [0] EXPLICIT IA5String
						 * }
						 */
						StringBuffer rut = new StringBuffer();
	
						int offset = 14;
	
						while (offset < otherName.length && otherName[offset] != 0x16)
							offset++;
						
						offset += 2;
						
						while (offset < otherName.length) {
							char ch = (char) otherName[offset++];
							
							if ((ch >= '0' && ch <= '9') || (ch == '-') || (ch == 'K') || (ch == 'k'))
								rut.append(ch);
						}
						
						return rut.toString();
					}
				}
			}
		}
		
		return null;
	}
}
