package com.acepta.ca.security.util;

import org.bouncycastle.util.encoders.Base64;

public class BASE64 {
	public static byte[] decode(String data) {
		return Base64.decode(data);
	}
	
	public static String encode(byte[] data) {
		return new String(Base64.encode(data));
	}
}
