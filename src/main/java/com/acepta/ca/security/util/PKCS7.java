package com.acepta.ca.security.util;

import java.io.IOException;
import java.util.Collection;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.x509.X509CertificateStructure;

public class PKCS7 {
	public static byte[] encodePKCS7SignedData(Collection<byte[]> certificatesEncoded) throws IOException {
		ASN1EncodableVector vector = new ASN1EncodableVector();
		
		for (byte[] certificateEncoded : certificatesEncoded)
			vector.add(X509CertificateStructure.getInstance(ASN1Sequence.fromByteArray(certificateEncoded)));
		
		DERSet digestAlgorithms = new DERSet();
		ContentInfo contentInfo = new ContentInfo(ContentInfo.data, null);
		DERSet certificates = new DERSet(vector);
		DERSet crls = new DERSet();
		DERSet signerInfos = new DERSet();

		SignedData signedData = new SignedData(digestAlgorithms, contentInfo, certificates, crls, signerInfos);

		return new ContentInfo(ContentInfo.signedData, signedData).getDEREncoded();
	}
}
