package com.acepta.ca.security.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;

import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.mozilla.SignedPublicKeyAndChallenge;

/**
 * Utilitario para extraer el nombre y llave RSA publica del titular desde un paquete PKCS#10.
 * 
 * La llave RSA publica se instancia en el proveedor criptografico SunRsaSign, por lo tanto,
 * no se puede ocupar para generar o validar firmas con otros proveedores.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-18
 *
 */
public class PKCS10 {
	private static final String PUBLIC_KEY_PROVIDER_NAME = "SunRsaSign";

	static {
		// HACK: Necesario para decodificar llave publica de un SPKAC con el proveedor SunRsaSign
	    Security.getProvider(PUBLIC_KEY_PROVIDER_NAME).put("Alg.Alias.KeyFactory.1.2.840.113549.1.1.1", "RSA");
  }
	
	public static String decodeSubjectFromPKCS10CertificateRequest(String pkcs10Base64) {
		PKCS10CertificationRequest request = new PKCS10CertificationRequest(BASE64.decode(pkcs10Base64));
		
		return request.getCertificationRequestInfo().getSubject().toString();
	}

	public static PublicKey decodePublicKeyFromPKCS10CerticateRequest(String pkcs10Base64) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException {
		PKCS10CertificationRequest request = new PKCS10CertificationRequest(BASE64.decode(pkcs10Base64));

		return request.getPublicKey(PUBLIC_KEY_PROVIDER_NAME);
	}
	
	public static PublicKey decodePublicKeyFromSignedPublicKeyAndChallenge(String spkacBase64) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException {
		SignedPublicKeyAndChallenge signedPublicKeyAndChallenge = new SignedPublicKeyAndChallenge(BASE64.decode(spkacBase64));

		return signedPublicKeyAndChallenge.getPublicKey(PUBLIC_KEY_PROVIDER_NAME);
	}
}
