package com.acepta.ca.security.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class PKCS12 {
	private static final String PKCS12_KEY_STORE_TYPE = "PKCS12";

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public static byte[] encodeCertificate(PrivateKey privateKey, String password, X509Certificate... certificates)
			throws GeneralSecurityException {
		try {
			KeyStore keyStore = KeyStore.getInstance(PKCS12_KEY_STORE_TYPE, BouncyCastleProvider.PROVIDER_NAME);

			keyStore.load(null, null);

			keyStore.setKeyEntry("alias", privateKey, password != null ? password.toCharArray() : new char[0],
					certificates);

			ByteArrayOutputStream output = new ByteArrayOutputStream();
			try {
				keyStore.store(output, password != null ? password.toCharArray() : new char[0]);
			} finally {
				output.close();
			}

			return output.toByteArray();
		} catch (IOException exception) {
			throw new KeyStoreException(exception);
		}
	}
}
