package com.acepta.ca.security.util;

import org.bouncycastle.crypto.engines.RC4Engine;
import org.bouncycastle.crypto.params.KeyParameter;

public class RC4 {
	public static byte[] encrypt(byte[] key, byte[] input) {
		return process(true, key, input);
	}

	public static byte[] decrypt(byte[] key, byte[] input) {
		return process(false, key, input);
	}

	private static byte[] process(boolean encrypt, byte[] key, byte[] input) {
		RC4Engine engine = new RC4Engine();

		KeyParameter keyParameter = new KeyParameter(key);

		engine.init(encrypt, keyParameter);

		byte[] output = new byte[input.length];

		engine.processBytes(input, 0, input.length, output, 0);

		return output;
	}
}
