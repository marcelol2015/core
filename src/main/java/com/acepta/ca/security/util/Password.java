package com.acepta.ca.security.util;

/**
 * Ofuscador de claves utilizada en las configuraciones del Proveedor Criptografico.
 * 
 * @author Carlos Hasan
 * @version 1.00, 2007-10-26
 */
public class Password {
	private static final String OBFUSCATED_PASSWORD_PREFIX = "OBF:";
	private static final String OBFUSCATED_PASSWORD_MOJO = "xanta.pero.peor.es.nada";
	private static final String OBFUSCATED_PASSWORD_CRAP = "C3AB0D6E124598F7";
	
	public static String encryptPassword(String password) {
		return OBFUSCATED_PASSWORD_PREFIX + buildHexStringFromBytes(RC4.encrypt(OBFUSCATED_PASSWORD_MOJO.getBytes(), password.getBytes()));
	}
	
	public static String decryptPassword(String password) {
		if (password.startsWith(OBFUSCATED_PASSWORD_PREFIX)) 
			return new String(RC4.decrypt(OBFUSCATED_PASSWORD_MOJO.getBytes(), buildBytesFromHexString(password.substring(OBFUSCATED_PASSWORD_PREFIX.length()))));
		return password;
	}
	
	private static String buildHexStringFromBytes(byte[] bytes) {
		StringBuffer buffer = new StringBuffer();
		
		for (int index = 0; index < bytes.length; index++) {
			int value = bytes[index] & 0xff;
			
			buffer.append(OBFUSCATED_PASSWORD_CRAP.charAt((value >> 4) & 0x0f));
			buffer.append(OBFUSCATED_PASSWORD_CRAP.charAt((value >> 0) & 0x0f));
		}
		
		return buffer.toString();
	}

	private static byte[] buildBytesFromHexString(String buffer) {
		byte[] bytes = new byte[buffer.length() >> 1];
		
		for (int index = 0; index + 1 < buffer.length(); index += 2) {
			int value = (OBFUSCATED_PASSWORD_CRAP.indexOf(buffer.charAt(index)) << 4) +
				OBFUSCATED_PASSWORD_CRAP.indexOf(buffer.charAt(index + 1));
			
			bytes[index >> 1] = (byte) value; 
		}
		
		return bytes;
	}
}
