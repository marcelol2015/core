package com.acepta.ca.security;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.NameBuilder;
import com.acepta.ca.security.util.PKCS10;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

/**
 * @author Angelo Estartus
 * @version 1.0, 2015-10-05
 */
public class SelloTiempoCertificateRequest extends CertificateRequest {

    private String subjectRUT;
    private BigInteger serialNumber;
    private Date notBefore;
    private Date notAfter;
    private Name subjectName;
    private PublicKey subjectPublicKey;
    private Collection<String> certificatePolicies;

    public String getSubjectRUT() {
        return subjectRUT;
    }

    public void setSubjectRUT(String subjectRUT) {
        this.subjectRUT = subjectRUT;
    }

    public void setSerialNumber(BigInteger serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public void setNotAfter(Date notAfter) {
        this.notAfter = notAfter;
    }

    public void setSubjectName(Name subjectName) {
        this.subjectName = subjectName;
    }

    public void setSubjectName(String commonName, String organizationalUnit, String organization, String locality, String stateOrProvince, String country) {
        NameBuilder buffer = new NameBuilder();

        buffer.append(NameBuilder.COUNTRY, country);

        if ((stateOrProvince = normalize(stateOrProvince)) != null)
            buffer.append(NameBuilder.STATE_OR_PROVINCE, stateOrProvince);

        if ((locality = normalize(locality)) != null)
            buffer.append(NameBuilder.LOCALITY, locality);

        if ((organization = normalize(organization)) != null)
            buffer.append(NameBuilder.ORGANIZATION, organization);

        if ((organizationalUnit = normalize(organizationalUnit)) != null)
            buffer.append(NameBuilder.ORGANIZATIONAL_UNIT, organizationalUnit);

        if ((commonName = normalize(commonName)) != null)
            buffer.append(NameBuilder.COMMON_NAME, commonName);

        setSubjectName(buffer.toName());
    }

    public void setSubjectPublicKey(PublicKey subjectPublicKey) {
        this.subjectPublicKey = subjectPublicKey;
    }

    public Collection<String> getCertificatePolicies() {
        return certificatePolicies;
    }

    public void setCertificatePolicies(Collection<String> certificatePolicies) {
        this.certificatePolicies = certificatePolicies;
    }

    public void setCertificatePolicies(String... certificatePolicies) {
        setCertificatePolicies(Arrays.asList(certificatePolicies));
    }

    public void setSubjectPublicKeyFromPKCS10(String pkcs10Base64) throws AceptaSecurityException {
        try {
            setSubjectPublicKey(PKCS10.decodePublicKeyFromPKCS10CerticateRequest(pkcs10Base64));
        }
        catch (GeneralSecurityException exception) {
            throw new AceptaSecurityException(exception);
        }
    }

    @Override
    public BigInteger getSerialNumber() {
        return null;
    }

    @Override
    public Date getNotBefore() {
        return null;
    }

    @Override
    public Date getNotAfter() {
        return null;
    }

    @Override
    public Name getSubjectName() {
        return null;
    }

    @Override
    public PublicKey getSubjectPublicKey() {
        return null;
    }

    private static String normalize(String text) {
        if (text != null) {
            text = text.trim();
            if (text.length() != 0)
                return text;
        }
        return null;
    }
}
