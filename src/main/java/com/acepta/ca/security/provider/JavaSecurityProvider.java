package com.acepta.ca.security.provider;

import java.io.File;
import java.security.KeyStoreException;

import com.acepta.ca.security.util.Password;

/**
 * Proveedor Criptografico RSA de Java.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public class JavaSecurityProvider extends SecurityProvider {
	private static final String JAVA_PROVIDER_NAME = "SunRsaSign";
	private static final String JAVA_KEY_STORE_TYPE = "JKS";

	private static final String JAVA_KEY_STORE_FILE_PROPERTY_NAME = "java.keystore.file";
	private static final String JAVA_KEY_STORE_PASSWORD_PROPERTY_NAME = "java.keystore.password";

	public JavaSecurityProvider() throws KeyStoreException {
		super(JAVA_PROVIDER_NAME, JAVA_KEY_STORE_TYPE);
	}

	public void setProperty(String name, String value) {
		if (name.equals(JAVA_KEY_STORE_FILE_PROPERTY_NAME))
			getKeyStore().setFile(new File(value));
		
		if (name.equals(JAVA_KEY_STORE_PASSWORD_PROPERTY_NAME))
			getKeyStore().setPassword(Password.decryptPassword(value).toCharArray());
	}
	
	public void login() {
	}
	
	public void logout() {
	}

	@Override
	public void config() {
		
	}
}
