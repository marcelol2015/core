package com.acepta.ca.security.provider;

import java.security.KeyStoreException;

import com.acepta.ca.security.util.Password;
import com.chrysalisits.crypto.LunaTokenManager;

/**
 * Proveedor Criptografico del modulo de seguridad por hardware (HSM) Luna SA.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */

@Deprecated
public class LunaSecurityProvider extends SecurityProvider {
	private static final String LUNA_PROVIDER_NAME = "LunaJCAProvider";
	private static final String LUNA_KEY_STORE_TYPE = "Luna";

	private static final String LUNA_PARTITION_SLOT_PROPERTY_NAME = "luna.partition.slot";
	private static final String LUNA_PARTITION_PASSWORD_PROPERTY_NAME = "luna.partition.password";

	private int partitionSlot;
	private String partitionPassword;

	public LunaSecurityProvider() throws KeyStoreException {
		super(LUNA_PROVIDER_NAME, LUNA_KEY_STORE_TYPE);
	}

	public int getPartitionSlot() {
		return partitionSlot;
	}

	public void setPartitionSlot(int partitionSlot) {
		this.partitionSlot = partitionSlot;
	}

	public String getPartitionPassword() {
		return partitionPassword;
	}

	public void setPartitionPassword(String partitionPassword) {
		this.partitionPassword = partitionPassword;
	}

	public void setProperty(String name, String value) {
		if (name.equals(LUNA_PARTITION_SLOT_PROPERTY_NAME))
			setPartitionSlot(Integer.parseInt(value));

		if (name.equals(LUNA_PARTITION_PASSWORD_PROPERTY_NAME))
			setPartitionPassword(Password.decryptPassword(value));
	}

	public void login() {
		LunaTokenManager.getInstance().Login(getPartitionSlot(), getPartitionPassword());
	}

	public void logout() {
		LunaTokenManager.getInstance().Logout();
	}

	@Override
	public void config() {
		
	}
}
