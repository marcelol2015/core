package com.acepta.ca.security.provider;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.security.KeyStoreException;
import java.security.Provider;
import java.security.Security;

import sun.security.pkcs11.SunPKCS11;

public class ThalesSecurityProvider extends SecurityProvider {
	private static String THALES_PROVIDER_NAME = "ThalesCAProvider";
	private static String THALES_KEYSTORE_TYPE = "PKCS11";
	private static String PKCS11_PREFIX ="SunPKCS11-";
	
	private static final String THALES_LIBRARY_PATH_POPERTY_NAME = "thales.library.path";	
	private static final String THALES_SLOT_ID_POPERTY_NAME = "thales.slot.id";
	private static final String THALES_SLOT_PIN_POPERTY_NAME = "thales.slot.pin";

	private int slotID;
	private File libraryPath;
	private String slotPin;
	

	public ThalesSecurityProvider() throws KeyStoreException{
		
	}

	public void setSlotID(int slotID) {
		this.slotID = slotID;
	}

	public void setLibraryPath(File libraryPath) {
		this.libraryPath = libraryPath;
	}

	public void setSlotPin(String slotPin) {
		this.slotPin = slotPin;
	}


	@Override
	public void setProperty(String name, String value) {
		if (name.equals(THALES_SLOT_ID_POPERTY_NAME))
			setSlotID(Integer.parseInt(value));

		if (name.equals(THALES_SLOT_PIN_POPERTY_NAME))
			setSlotPin(value);
		
		if (name.equals(THALES_LIBRARY_PATH_POPERTY_NAME))
			setLibraryPath(new File(value));
	}

	@Override
	public void login() {
		
	}

	@Override
	public void logout() {
		Security.removeProvider(THALES_PROVIDER_NAME);

	}
	
	
	private static InputStream createConfig(String name, File library, int slotID) {
		final ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		final PrintWriter writer = new PrintWriter(buffer);
		try {
			writer.println("name=" + name);
			writer.println("library=" + library);
			writer.println("slot=" + slotID);

			writer.println("attributes(import, CKO_PUBLIC_KEY, CKK_RSA) = {");
			writer.println("    CKA_ENCRYPT = true");
			writer.println("    CKA_VERIFY = true");
			writer.println("    CKA_WRAP = true");
			writer.println("}");

			writer.println("attributes(generate, CKO_PUBLIC_KEY, CKK_RSA) = {");
			writer.println("    CKA_ENCRYPT = true");
			writer.println("    CKA_VERIFY = true");
			writer.println("    CKA_WRAP = true");
			writer.println("}");

			writer.println("attributes(generate, CKO_PRIVATE_KEY, CKK_RSA) = {");
			writer.println("    CKA_TOKEN = true");
			writer.println("    CKA_SENSITIVE = true");
			writer.println("    CKA_EXTRACTABLE = false");
			writer.println("    CKA_DECRYPT = true");
			writer.println("    CKA_SIGN = true");
			writer.println("    CKA_UNWRAP = true");
			writer.println("}");
		} finally {
			writer.close();
		}

		return new ByteArrayInputStream(buffer.toByteArray());
	}

	public int getSlotID() {
		return slotID;
	}

	public File getLibraryPath() {
		return libraryPath;
	}

	public String getSlotPin() {
		return slotPin;
	}

	@Override
	public void config() throws KeyStoreException {
		final InputStream input = createConfig(THALES_PROVIDER_NAME, libraryPath, slotID);		
		Provider provider =new SunPKCS11(input);
		Security.addProvider(provider);	

		setUp(PKCS11_PREFIX+THALES_PROVIDER_NAME, THALES_KEYSTORE_TYPE );
	    getKeyStore().setPassword(getSlotPin().toCharArray());
	}	

}
