package com.acepta.ca.security.provider;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Provider;
import java.security.Security;

/**
 * Proveedor Criptografico y Almacen de Llaves y Certificados Electronicos X.509.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public abstract class SecurityProvider {
	private Provider provider;
	private SecurityKeyStore keyStore;
	
	protected SecurityProvider() {}
	public SecurityProvider(String providerName, String keyStoreType) throws KeyStoreException {
		setUp(providerName, keyStoreType);
	}

	protected void setUp(String providerName, String keyStoreType) throws KeyStoreException{
		setProvider(createProvider(providerName));
		if (keyStoreType.equals("PKCS11"))
			setKeyStore(createKeyStore(keyStoreType, getProvider()));
		else
			setKeyStore(createKeyStore(keyStoreType));
	}
	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public SecurityKeyStore getKeyStore() {
		return keyStore;
	}

	public void setKeyStore(SecurityKeyStore keyStore) {
		this.keyStore = keyStore;
	}

	public abstract void setProperty(String name, String value);

	public abstract void login();
	
	public abstract void logout();
	
	private static Provider createProvider(String providerName) {
		return Security.getProvider(providerName);
	}

	private static SecurityKeyStore createKeyStore(String keyStoreType) throws KeyStoreException {
		  return new SecurityKeyStore(KeyStore.getInstance(keyStoreType));
	}
	private static SecurityKeyStore createKeyStore(String keyStoreType, Provider provider) throws KeyStoreException {
		  return new SecurityKeyStore(KeyStore.getInstance(keyStoreType, provider));
	}
	
	public  void config() throws KeyStoreException {};
	
}
