package com.acepta.ca.security.provider;

import com.acepta.ca.security.util.BASE64;
import com.acepta.ca.security.util.X509;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Almacen con los Certificados de las Autoridades Confiables de Acepta.com Jerarquia V1.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-11-09
 *
 */
public class AceptaV2TrustedKeyStore {
	private static final String ACEPTA_AUTORIDAD_CLASE3_2010_CERTIFICATE =
		"MIIGDzCCBPegAwIBAgIBAjANBgkqhkiG9w0BAQUFADCBkzELMAkGA1UEBhMCQ0wx\n" +
		"GDAWBgNVBAoTD0FjZXB0YS5jb20gUy5BLjE1MDMGA1UEAxMsQWNlcHRhLmNvbSBB\n" +
		"dXRvcmlkYWQgQ2VydGlmaWNhZG9yYSBSYWl6IC0gVjIxHjAcBgkqhkiG9w0BCQEW\n" +
		"D2luZm9AYWNlcHRhLmNvbTETMBEGA1UEBRMKOTY5MTkwNTAtODAeFw0xMDA4MDky\n" +
		"MDQ1MjlaFw0zMDA4MDkyMDQ1MjlaMIGmMQswCQYDVQQGEwJDTDEYMBYGA1UEChMP\n" +
		"QWNlcHRhLmNvbSBTLkEuMUgwRgYDVQQDEz9BY2VwdGEuY29tIEF1dG9yaWRhZCBD\n" +
		"ZXJ0aWZpY2Fkb3JhIENsYXNlIDMgUGVyc29uYSBOYXR1cmFsIC0gVjIxHjAcBgkq\n" +
		"hkiG9w0BCQEWD2luZm9AYWNlcHRhLmNvbTETMBEGA1UEBRMKOTY5MTkwNTAtODCC\n" +
		"ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALBpalRvK4R5VlB8nnmt/xU0\n" +
		"Uluv4RzladCJYg/E+CjoQzXYrs43FCajVGx7NRlM3xW4eBQ5cr6AQx64tThp8pKD\n" +
		"/+hYIOJWyQTgME4w2WIp4W6JV8Qf0kqHt2oh/84N9bC9w5f0NUjkbNPMPGDHarw9\n" +
		"ILAOAGzcfQXGRp+MO4zPfW0I93y50+aPXtSmXjLzKiMPFTvzPXMnhAvLh5u/BriQ\n" +
		"3YVcIMEWVN2JN/AjCQA/ErNFaA2NXCuh/EAXIKoedjNtDATroSoKfilsZ7V8VKAP\n" +
		"bYMEanyVfAthxl4OG0miclqkt5qLEJA8htAYWaiju/5w1POwLg21u00uzw8BFKkC\n" +
		"AwEAAaOCAlcwggJTMA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAU4xS8sL2n\n" +
		"d6qCCRZFWHpmqGacBCkwHQYDVR0OBBYEFEDf6PZWEPl0Gp6EzCII0DatKSZRMAsG\n" +
		"A1UdDwQEAwIBBjCCATUGA1UdIASCASwwggEoMIIBJAYKKwYBBAG1a4dpATCCARQw\n" +
		"JwYIKwYBBQUHAgEWG2h0dHA6Ly93d3cuYWNlcHRhLmNvbS9DUFNWMjCB6AYIKwYB\n" +
		"BQUHAgIwgdswFhYPQWNlcHRhLmNvbSBTLkEuMAMCAQEagcBMYSB1dGlsaXphY2lv\n" +
		"biBkZSBlc3RlIGNlcnRpZmljYWRvIGVzdGEgc3VqZXRhIGEgbGFzIHBvbGl0aWNh\n" +
		"cyBkZSBjZXJ0aWZpY2FkbyAoQ1ApIHkgcHJhY3RpY2FzIGRlIGNlcnRpZmljYWNp\n" +
		"b24gKENQUykgZXN0YWJsZWNpZGFzIHBvciBBY2VwdGEuY29tLCB5IGRpc3Bvbmli\n" +
		"bGVzIHB1YmxpY2FtZW50ZSBlbiB3d3cuYWNlcHRhLmNvbS4wXAYDVR0SBFUwU6AY\n" +
		"BggrBgEEAcEBAqAMFgo5NjkxOTA1MC04oCYGCisGAQUFBwASCAOgGDAWDAo5Njkx\n" +
		"OTA1MC04BggrBgEEAcEBAoEPaW5mb0BhY2VwdGEuY29tMFwGA1UdEQRVMFOgGAYI\n" +
		"KwYBBAHBAQGgDBYKOTY5MTkwNTAtOKAmBgorBgEFBQcAEggDoBgwFgwKOTY5MTkw\n" +
		"NTAtOAYIKwYBBAHBAQKBD2luZm9AYWNlcHRhLmNvbTANBgkqhkiG9w0BAQUFAAOC\n" +
		"AQEATPY6DJJ+zkrxEQN0gt3Ylc7IpylCe4pAlgiTB1rXVkT3seXCq/YipIYeUi2v\n" +
		"+VmTrb13L5R2cykKoQs47WHHV3JmZZ9H2S1WkS6qJnVFvHIOM1T6DBuJO4mvtfZM\n" +
		"6viMsjxiCansIQUepQnE8Gj2qfyehLYbnbPzIeDmbaNqVq5b7EZD/05H3Yu0uvtj\n" +
		"DJZ/Oo6tfXWxuTeAbrJ13hCM4DYGdOcO9IHzZn9rfNcOQEkQGAbZTAZXmJVGkvQs\n" +
		"+VfO94XalMTPY+lQfakXbqz65jVsXjxhb5yqVoa8rER+VmWsrtni0kK/lSglosvN\n" +
		"o0H2FhUrBp9NXtCUg8+JhUwo2A==\n";

	private static final String ACEPTA_AUTORIDAD_FIRMA_AVANZADA_2010_CERTIFICATE =
		"MIIGFTCCBP2gAwIBAgIBAzANBgkqhkiG9w0BAQUFADCBkzELMAkGA1UEBhMCQ0wx\n" +
		"GDAWBgNVBAoTD0FjZXB0YS5jb20gUy5BLjE1MDMGA1UEAxMsQWNlcHRhLmNvbSBB\n" +
		"dXRvcmlkYWQgQ2VydGlmaWNhZG9yYSBSYWl6IC0gVjIxHjAcBgkqhkiG9w0BCQEW\n" +
		"D2luZm9AYWNlcHRhLmNvbTETMBEGA1UEBRMKOTY5MTkwNTAtODAeFw0xMDA4MDky\n" +
		"MDQ1MjlaFw0zMDA4MDkyMDQ1MjlaMIGsMQswCQYDVQQGEwJDTDEYMBYGA1UEChMP\n" +
		"QWNlcHRhLmNvbSBTLkEuMU4wTAYDVQQDE0VBY2VwdGEuY29tIEF1dG9yaWRhZCBD\n" +
		"ZXJ0aWZpY2Fkb3JhIGRlIEZpcm1hIEVsZWN0cm9uaWNhIEF2YW56YWRhIC0gVjIx\n" +
		"HjAcBgkqhkiG9w0BCQEWD2luZm9AYWNlcHRhLmNvbTETMBEGA1UEBRMKOTY5MTkw\n" +
		"NTAtODCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAPN1vt9DtMUhui3T\n" +
		"Fd+FmKyYh1yTkiJ3TgBo4mgzK3c595gQvzFKa13dLqfqWQzAE7atI6+Fyctak+qH\n" +
		"g2AvKzALmPI+0XmsqkVEBMfeAQGih6hfBrBffLkVYKoNzXZ7m6LOoE7nnG31Zkxk\n" +
		"0f4vxJkMwQgX8sT0jDLPHxRQ+m6QLa6rnyVGZkzdRydV8jNlD8J4E4SgIILvBAbo\n" +
		"gFLuSb06F4UUcOz+6e6UL/i+Jv53aLeLzaXxEZhq8O/YzXpkX4U1/BN+KTjIXQGX\n" +
		"E7WSaG9mSUOihGKSiJksjqabNxttILcFHyhLgZZ2CjN75M6F+Pn5Pvaq0WBQmM8x\n" +
		"kv8TS1ECAwEAAaOCAlcwggJTMA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAU\n" +
		"4xS8sL2nd6qCCRZFWHpmqGacBCkwHQYDVR0OBBYEFMnDiLKGhl+02ifQVa3sNW+5\n" +
		"mcI1MAsGA1UdDwQEAwIBBjCCATUGA1UdIASCASwwggEoMIIBJAYKKwYBBAG1a4dp\n" +
		"ATCCARQwJwYIKwYBBQUHAgEWG2h0dHA6Ly93d3cuYWNlcHRhLmNvbS9DUFNWMjCB\n" +
		"6AYIKwYBBQUHAgIwgdswFhYPQWNlcHRhLmNvbSBTLkEuMAMCAQEagcBMYSB1dGls\n" +
		"aXphY2lvbiBkZSBlc3RlIGNlcnRpZmljYWRvIGVzdGEgc3VqZXRhIGEgbGFzIHBv\n" +
		"bGl0aWNhcyBkZSBjZXJ0aWZpY2FkbyAoQ1ApIHkgcHJhY3RpY2FzIGRlIGNlcnRp\n" +
		"ZmljYWNpb24gKENQUykgZXN0YWJsZWNpZGFzIHBvciBBY2VwdGEuY29tLCB5IGRp\n" +
		"c3BvbmlibGVzIHB1YmxpY2FtZW50ZSBlbiB3d3cuYWNlcHRhLmNvbS4wXAYDVR0S\n" +
		"BFUwU6AYBggrBgEEAcEBAqAMFgo5NjkxOTA1MC04oCYGCisGAQUFBwASCAOgGDAW\n" +
		"DAo5NjkxOTA1MC04BggrBgEEAcEBAoEPaW5mb0BhY2VwdGEuY29tMFwGA1UdEQRV\n" +
		"MFOgGAYIKwYBBAHBAQGgDBYKOTY5MTkwNTAtOKAmBgorBgEFBQcAEggDoBgwFgwK\n" +
		"OTY5MTkwNTAtOAYIKwYBBAHBAQKBD2luZm9AYWNlcHRhLmNvbTANBgkqhkiG9w0B\n" +
		"AQUFAAOCAQEAs1nfiQEhHmA85yoorYnRvNugHDEmwsKVaqtoxlL2K/3yMbm4juCB\n" +
		"4u6CtnfUCKusWsgKlKStzK7NKGoFiwPeurIaye5PeC87HIxcLPpXApYrpKUEKcZ5\n" +
		"5tIliU006AoTGWypdc1/SxhA6+6qudyq15o/4HY2kCXlG5pX3PPs5HvcEhnrCbpV\n" +
		"tEulhqwUFmasDC7NV/miqfWIEUgAz1zqYTmcRqAhnm0vOQXAwgooM++kK9XFAwXn\n" +
		"Gmmik997FWGtuocOkauoH0cfO/PgRMB7I6P1AClcbejQ87DyGP9USqDBQsRJUIQK\n" +
		"fr6mjRmp4lhjqOeuqlnWSI8+lXjnK6ygvA==\n";

	public AceptaV2TrustedKeyStore() {
	}
	
	public Collection<X509Certificate> getTrustedCertificates() throws GeneralSecurityException {
		ArrayList<X509Certificate> certificates = new ArrayList<X509Certificate>();
		
		certificates.add(decodeCertificate(ACEPTA_AUTORIDAD_CLASE3_2010_CERTIFICATE));
		certificates.add(decodeCertificate(ACEPTA_AUTORIDAD_FIRMA_AVANZADA_2010_CERTIFICATE));
		
		return certificates;
	}
	
	private static X509Certificate decodeCertificate(String certificateBase64) throws CertificateException {
		try {
			return X509.decodeCertificate(BASE64.decode(certificateBase64));
		}
		catch (IOException exception) {
			throw new CertificateException(exception);
		}
	}
}
