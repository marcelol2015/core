package com.acepta.ca.security.provider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

/**
 * Almacen de Llaves Privadas y Certificados Electronicos X.509.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public class SecurityKeyStore {
	private static final String PRIVATE_KEY_ENTRY_PASSWORD = "";
	
	private KeyStore keyStore;
	private File file;
	private char[] password;

	public SecurityKeyStore(KeyStore keyStore) {
		setKeyStore(keyStore);
	}

	public KeyStore getKeyStore() {
		return keyStore;
	}

	public void setKeyStore(KeyStore keyStore) {
		this.keyStore = keyStore;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public boolean hasFile() {
		return getFile() != null;
	}
	
	public void load() throws GeneralSecurityException {
		if (hasFile()) {
			try {
				FileInputStream input = new FileInputStream(getFile());
				try {
					getKeyStore().load(input, getPassword());
				}
				finally {
					input.close();
				}
			}
			catch (IOException exception) {
				throw new KeyStoreException("Falla al cargar el almacen de llaves", exception);
			}
		}
		else {
			try {
					getKeyStore().load(null, getPassword());
			}
			catch (IOException exception) {
				throw new KeyStoreException("Falla al cargar el almacen de llaves", exception);
			}
			
		}
	}

	public void store() throws GeneralSecurityException {
		if (hasFile()) {
			try {
				FileOutputStream output = new FileOutputStream(getFile());
				try {
					getKeyStore().store(output, getPassword());
				}
				finally {
					output.close();
				}
			}
			catch (IOException exception) {
				throw new KeyStoreException("Falla al grabar el almacen de llaves", exception);
			}
		}
		else {
			try {
				getKeyStore().store(null, null);
			}
			catch (IOException exception) {
				throw new KeyStoreException("Falla al grabar el almacen de llaves", exception);
			}
		}
	}
	
	public Collection<X509Certificate> getTrustedCertificates() throws GeneralSecurityException {
		ArrayList<X509Certificate> trustedCertificates = new ArrayList<X509Certificate>();
		
		Enumeration<String> aliases = getKeyStore().aliases();
		
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			
			if (getKeyStore().isCertificateEntry(alias))
				trustedCertificates.add((X509Certificate) getKeyStore().getCertificate(alias));
			
			if (getKeyStore().isKeyEntry(alias))
				trustedCertificates.add((X509Certificate) ((KeyStore.PrivateKeyEntry) getKeyStore().getEntry(alias, getKeyProtection())).getCertificate());
		}

		return trustedCertificates;
	}

	public KeyStore.PrivateKeyEntry getKeyEntry(String alias) throws GeneralSecurityException {
		return (KeyStore.PrivateKeyEntry) getKeyStore().getEntry(alias, getKeyProtection());
	}

	public void setKeyEntry(String alias, PrivateKey privateKey, X509Certificate certificate) throws GeneralSecurityException {
		KeyStore.PrivateKeyEntry entry = new KeyStore.PrivateKeyEntry(privateKey, new X509Certificate[] { certificate });

		getKeyStore().setEntry(alias, entry, getKeyProtection());
	}
	
	public X509Certificate getCertificateEntry(String alias) throws GeneralSecurityException {
		return (X509Certificate) getKeyStore().getCertificate(alias);
	}
	
	public void setCertificateEntry(String alias, X509Certificate certificate) throws GeneralSecurityException {
		getKeyStore().setCertificateEntry(alias, certificate);
	}

	private KeyStore.ProtectionParameter getKeyProtection() {
		return new KeyStore.PasswordProtection(PRIVATE_KEY_ENTRY_PASSWORD.toCharArray());
	}
}
