package com.acepta.ca.security.provider;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

/**
 * Almacen de certificados confiables instalados en la Maquina Virtual Java (JVM).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-09
 */
public class JavaTrustedKeyStore {
	private KeyStore trustKeyStore;
	
	public JavaTrustedKeyStore() throws GeneralSecurityException {
		setTrustKeyStore(loadTrustKeyStore());
	}
	
	public KeyStore getTrustKeyStore() {
		return trustKeyStore;
	}

	public void setTrustKeyStore(KeyStore trustKeyStore) {
		this.trustKeyStore = trustKeyStore;
	}

	public Collection<X509Certificate> getTrustedCertificates() throws GeneralSecurityException {
		ArrayList<X509Certificate> certificates = new ArrayList<X509Certificate>();
		
		Enumeration<String> aliases = getTrustKeyStore().aliases();
		
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			
			if (getTrustKeyStore().isCertificateEntry(alias))
				certificates.add((X509Certificate) getTrustKeyStore().getCertificate(alias));
		}
		
		return certificates;
	}
	
	private static KeyStore loadTrustKeyStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException {
		File trustKeyStoreFile = new File(System.getProperty("java.home"), "lib/security/cacerts");
		try {
			KeyStore trustKeyStore = KeyStore.getInstance("JKS");
			
			InputStream input = new FileInputStream(trustKeyStoreFile);
			try {
				trustKeyStore.load(input, null);
			}
			finally {
				input.close();
			}
			
			return trustKeyStore;
		}
		catch (IOException exception) {
			throw new KeyStoreException("No es posible instanciar almacen criptografico confiable: " + trustKeyStoreFile.getPath(), exception);
		}
	}
}
