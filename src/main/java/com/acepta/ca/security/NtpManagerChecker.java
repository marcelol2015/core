package com.acepta.ca.security;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NtpManagerChecker {
    private String sD1;
    private String sD2;
    private SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS");
    private final int PORT = 123;

    public String getNTPDate() {
        InetAddress address;
        DatagramSocket socket;
        String sntpDateTime = null;
        try {
            final String serverName = "ntp.shoa.cl";
            socket = new DatagramSocket();
            socket.setSoTimeout(3000);
            if(socket != null){
                address = InetAddress.getByName(serverName);
                if(address != null){
                    final byte[] buffer = new NtpMessage().toByteArray();
                    if(buffer != null){
                        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, PORT);
                        if(packet != null){
                            socket.send(packet);
                            packet = new DatagramPacket(buffer, buffer.length);
                            socket.receive(packet);
                            final NtpMessage msg = new NtpMessage(packet.getData());
                            socket.close();
                            sntpDateTime = NtpMessage.timestampToString(msg.transmitTimestamp);
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return sntpDateTime;
    }
    public void dateDiff(String checkDynamic_date, String time) {
        sD1=checkDynamic_date;
        sD2=time;
    }
    public long getDifferenceInMinutes (){
        System.out.println("sD1 = " + sD1);
        System.out.println("sD2 = " + sD2);

        Date d1 = null;
        long d2Ms = 0;
        long d1Ms = 0;
        try {
            d1 = df.parse(sD1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date d2 = null;
        try {
            d2 = df.parse(sD2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(d1 != null){
            d1Ms = d1.getTime();
            System.out.println("d1Ms = " + d1Ms);
        }
        if(d2 != null){
            d2Ms = d2.getTime();
            System.out.println("d2Ms = " + d2Ms);
        }

        return Math.abs(d1Ms-d2Ms);
    }
}
