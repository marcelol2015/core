package com.acepta.ca.security;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityProvider;
import com.acepta.ca.security.provider.*;

/**
 * Fabrica de los administradores y proveedores criptograficos de las Autoridades Certificadoras de Acepta.com S.A.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaSecurityManagerFactory {
	private SecurityProvider securityProvider;
	private AuthorityProvider authorityProvider;
	private Collection<Authority> authorityHierarchy;
	
	public AceptaSecurityManagerFactory(String securityTokenName) {
		AceptaSecurityConfiguration configuration = getSecurityConfiguration(securityTokenName);

		setSecurityProvider(createSecurityProvider(configuration));
		setAuthorityProvider(createAuthorityProvider(configuration));
		
		getSecurityProvider().login();
		
		setAuthorityHierarchy(loadAuthorityHierarchy());
	}

	public void bootstrap() {
		setAuthorityHierarchy(generateAuthorityHierarchy());
	}

	public void update(){
		setAuthorityHierarchy(updateAuthorityHierarchy());
	}
	public void close() {
		getSecurityProvider().logout();
	}

	public AceptaSecurityManager createSecurityManager() {
		return new AceptaSecurityManager(getAuthorityHierarchy(), getAuthorityCertificates());
	}

	public void __addTrustedAuthority(Authority authority) {
		try {
			getSecurityProvider().getKeyStore().setCertificateEntry(authority.getAlias(), authority.getCertificate());
			
			getSecurityProvider().getKeyStore().store();
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al registrar autoridad confiable", exception);
		}
	}
	
	private SecurityProvider getSecurityProvider() {
		return securityProvider;
	}

	private void setSecurityProvider(SecurityProvider securityProvider) {
		this.securityProvider = securityProvider;
	}

	private AuthorityProvider getAuthorityProvider() {
		return authorityProvider;
	}

	private void setAuthorityProvider(AuthorityProvider authorityProvider) {
		this.authorityProvider = authorityProvider;
	}

	private Collection<Authority> getAuthorityHierarchy() {
		return authorityHierarchy;
	}

	private void setAuthorityHierarchy(Collection<Authority> authorityHierarchy) {
		this.authorityHierarchy = authorityHierarchy;
	}

    private Collection<Authority> updateAuthorityHierarchy(){
		try {
			int authoritiesAdded=0;
			getSecurityProvider().getKeyStore().load();
			Authority rootAuthority = null;
            Authority intermediateAuthorityTSA = null;
			Collection<Authority> authorityHierarchy = this.loadAuthorityHierarchy();
			
			for (Authority authority : authorityHierarchy) {
                System.out.println("Authories "+ authority.getAlias());
				X509Certificate certificate = authority.getCertificate();  
				if (certificate!=null && certificate.getIssuerX500Principal().equals(certificate.getSubjectX500Principal())	){
					rootAuthority=authority;
				}
			}
			if (rootAuthority==null)
				throw new AceptaSecurityException("Falla al cargar autoridad raiz" );			
	
			for (Authority authority : authorityHierarchy) {
				KeyStore.PrivateKeyEntry entry = getSecurityProvider().getKeyStore().getKeyEntry(authority.getAlias());
				
				if (entry != null) {
					authority.setPrivateKey(entry.getPrivateKey());
					authority.setCertificate((X509Certificate) entry.getCertificate());
				}
				else {
					if(authority.getAlias().equalsIgnoreCase("EmisorSelloTiempo-G4")){
                        System.out.println("intermediateAuthority "+ intermediateAuthorityTSA.getAlias());
                        System.out.println("Inicia generacion de final "+ authoritiesAdded);
                        Authority intermediateAuthority = getAuthorityProvider().generateFinalAuthority(getSecurityProvider().getProvider(), authority.getAlias(), intermediateAuthorityTSA);
                        System.out.println("Seteo propiedades template");
                        authority.setPrivateKey(intermediateAuthority.getPrivateKey());
                        authority.setCertificate(intermediateAuthority.getCertificate());
                        getSecurityProvider().getKeyStore().setKeyEntry(authority.getAlias(), authority.getPrivateKey(), authority.getCertificate());
                        authoritiesAdded++;
                        System.out.println("Auth added : "+ authoritiesAdded);
                    }else{
                        System.out.println("rootAuthority "+ rootAuthority.getAlias());
                        System.out.println("Inicia generacion de intermedia "+ authoritiesAdded);
                        Authority intermediateAuthority = getAuthorityProvider().generateIntermediateAuthority(getSecurityProvider().getProvider(), authority.getAlias(), rootAuthority);
                        if(intermediateAuthority.getAlias().equalsIgnoreCase("SelloTiempo-G4")){
                            intermediateAuthorityTSA = intermediateAuthority;
                        }
                        System.out.println("Seteo propiedades template");
                        authority.setPrivateKey(intermediateAuthority.getPrivateKey());
                        authority.setCertificate(intermediateAuthority.getCertificate());
                        getSecurityProvider().getKeyStore().setKeyEntry(authority.getAlias(), authority.getPrivateKey(), authority.getCertificate());
                        authoritiesAdded++;
                        System.out.println("Auth added : "+ authoritiesAdded);
                    }
				}

			}
			if (authoritiesAdded>0)
				getSecurityProvider().getKeyStore().store();
			
			return authorityHierarchy;
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al cargar las autoridades", exception);
		}		
	}
	private Collection<X509Certificate> getAuthorityCertificates() {
		try {
			ArrayList<X509Certificate> authorityCertificates = new ArrayList<X509Certificate>();
			
			authorityCertificates.addAll(getSecurityProvider().getKeyStore().getTrustedCertificates());

			authorityCertificates.addAll(new AceptaV1TrustedKeyStore().getTrustedCertificates());
			authorityCertificates.addAll(new AceptaV2TrustedKeyStore().getTrustedCertificates());
			authorityCertificates.addAll(new AceptaG3TrustedKeyStore().getTrustedCertificates());
			authorityCertificates.addAll(new JavaTrustedKeyStore().getTrustedCertificates());
			
			return authorityCertificates;
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al cargar los certificados confiables", exception);
		}
	}
	
	private Collection<Authority> loadAuthorityHierarchy() {
		try {
			getSecurityProvider().getKeyStore().load();
			
			Collection<Authority> authorityHierarchy = getAuthorityProvider().loadAuthorityHierarchy(getSecurityProvider().getProvider());
	
			for (Authority authority : authorityHierarchy) {
				KeyStore.PrivateKeyEntry entry = getSecurityProvider().getKeyStore().getKeyEntry(authority.getAlias());
				
				if (entry != null) {
					authority.setPrivateKey(entry.getPrivateKey());
					authority.setCertificate((X509Certificate) entry.getCertificate());
				}
			}
			
			return authorityHierarchy;
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al cargar las autoridades", exception);
		}
	}
	
	private Collection<Authority> generateAuthorityHierarchy() {
		try {
			 Collection<Authority> authorityHierarchy = getAuthorityProvider().generateAuthorityHierarchy(getSecurityProvider().getProvider());
			
			for (Authority authority : authorityHierarchy) {
				getSecurityProvider().getKeyStore().setKeyEntry(authority.getAlias(), authority.getPrivateKey(), authority.getCertificate());
			}
			
			getSecurityProvider().getKeyStore().store();
			
			return authorityHierarchy;
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al crear las autoridades", exception);
		}
	}
	
	
	private static SecurityProvider createSecurityProvider(AceptaSecurityConfiguration configuration) {
		String securityProviderClassName = configuration.getProvider();
		
		try {
			SecurityProvider securityProvider = (SecurityProvider) Class.forName(securityProviderClassName).newInstance();
			
			for (String propertyName : configuration.getPropertyNames())
				securityProvider.setProperty(propertyName, configuration.getProperty(propertyName));
			
			securityProvider.config();
			return securityProvider;
		}
		catch (Exception exception) {
			throw new AceptaSecurityException("Falla al instanciar el proveedor de seguridad '" + securityProviderClassName + "' del token '" + configuration.getSecurityTokenName() + "'", exception);
		}
	}
	
	private static AuthorityProvider createAuthorityProvider(AceptaSecurityConfiguration configuration) {
		String authorityProviderClassName = configuration.getAuthorityProvider();
		
		try {
			AuthorityProvider authorityProvider = (AuthorityProvider) Class.forName(authorityProviderClassName).newInstance();
			
			return authorityProvider;
		}
		catch (Exception exception) {
			throw new AceptaSecurityException("Falla al instanciar el proveedor de la jerarquia '" + authorityProviderClassName + "' del token '" + configuration.getSecurityTokenName() + "'", exception);
		}
	}
	
	private static AceptaSecurityConfiguration getSecurityConfiguration(String securityTokenName) {
		try {
			InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/security.xml");
			try {
				return new AceptaSecurityConfiguration(securityTokenName, input);
			}
			finally {
				input.close();
			}
		}
		catch (Exception exception) {
			throw new AceptaSecurityException("Falla al leer la configuracion de seguridad", exception);
		}
	}
}
