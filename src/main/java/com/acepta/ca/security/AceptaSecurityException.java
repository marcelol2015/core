package com.acepta.ca.security;

public class AceptaSecurityException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AceptaSecurityException(String message, Exception cause) {
		super(message, cause);
	}
	
	public AceptaSecurityException(Exception exception) {
		super(exception);
	}
	
	public AceptaSecurityException(String message) {
		super(message);
	}
}
