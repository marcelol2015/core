package com.acepta.ca.security;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.NameBuilder;
import com.acepta.ca.security.util.PKCS10;

/**
 * Peticion de Certificados Electronicos X.509 para Sistemas.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-03
 */
public class SistemaCertificateRequest extends CertificateRequest {
	/*
	 * Politicas de Certificados
	 */
	public static final String ACEPTA_SISTEMAS_POLICY_IDENTIFIER_VPN = "1.3.6.1.4.1.6891.1001.5.1";
	public static final String ACEPTA_SISTEMAS_POLICY_IDENTIFIER_SERVICIO_FED = "1.3.6.1.4.1.6891.1001.5.2";
	public static final String ACEPTA_SISTEMAS_POLICY_IDENTIFIER_PUNTO_FED = "1.3.6.1.4.1.6891.1001.5.3";
	
	private String subjectRUT;	
	private BigInteger serialNumber;
	private Date notBefore;
	private Date notAfter;
	private Name subjectName;
	private PublicKey subjectPublicKey;
	private Collection<String> certificatePolicies;
	
	public SistemaCertificateRequest() {
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public Name getSubjectName() {
		return subjectName;
	}
	
	public void setSubjectName(Name subjectName) {
		this.subjectName = subjectName;
	}
	
	public PublicKey getSubjectPublicKey() {
		return subjectPublicKey;
	}

	public void setSubjectPublicKey(PublicKey publicKey) {
		this.subjectPublicKey = publicKey;
	}

	public Collection<String> getCertificatePolicies() {
		return certificatePolicies;
	}

	public void setCertificatePolicies(Collection<String> certificatePolicies) {
		this.certificatePolicies = certificatePolicies;
	}

	public void setSubjectName(String rut, String nombre,String unidadOrganizacional,String organizacion,String ciudad,String estado,String pais) {
		setSubjectName(createSubjectName(rut, nombre, unidadOrganizacional, organizacion, ciudad, estado, pais));
	}

	public Name createSubjectName(String serialNumber, String commonName, String organizationalUnit, String organization, String locality, String stateOrProvince, String country) {
		NameBuilder buffer = new NameBuilder();

		buffer.append(NameBuilder.COUNTRY, country);

		if ((stateOrProvince = normalize(stateOrProvince)) != null)
			buffer.append(NameBuilder.STATE_OR_PROVINCE, stateOrProvince);

		if ((locality = normalize(locality)) != null)
			buffer.append(NameBuilder.LOCALITY, locality);

		if ((organization = normalize(organization)) != null)
			buffer.append(NameBuilder.ORGANIZATION, organization);

		if ((organizationalUnit = normalize(organizationalUnit)) != null)
			buffer.append(NameBuilder.ORGANIZATIONAL_UNIT, organizationalUnit);

		if ((commonName = normalize(commonName)) != null)
			buffer.append(NameBuilder.COMMON_NAME, commonName);

		if ((serialNumber = normalize(serialNumber)) != null)
			buffer.append(NameBuilder.SERIAL_NUMBER, serialNumber);

		return buffer.toName();
	}

	public void setCertificatePolicies(String... certificatePolicies) {
		setCertificatePolicies(Arrays.asList(certificatePolicies));
	}

	public void setSubjectPublicKeyFromPKCS10(String pkcs10Base64) throws AceptaSecurityException {
		try {
			setSubjectPublicKey(PKCS10.decodePublicKeyFromPKCS10CerticateRequest(pkcs10Base64));
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException(exception);
		}
	}
	
	public void setSubjectRUT(String rut) {
		this.subjectRUT = rut;
	}

	public String getSubjectRUT() {
		return this.subjectRUT;
	}
	
	private static String normalize(String text) {
		if (text != null) {
			text = text.trim();
			if (text.length() != 0)
				return text;
		}
		return null;
	}	
}
