package com.acepta.ca.security;

import java.security.cert.X509Certificate;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;

/**
 * Administrador de las Autoridades Certificadoras de Acepta.com S.A.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaSecurityManager {
	private Collection<Authority> authorities;
	private Collection<X509Certificate> trustedCertificates;
	
	public AceptaSecurityManager(Collection<Authority> authorities, Collection<X509Certificate> trustedCertificates) {
		setAuthorities(authorities);
		setTrustedCertificates(trustedCertificates);
	}

	public void close() {
	}
	
	public Authority getAuthority(String alias) {
		for (Authority authority : getAuthorities()) {
			if (authority.getAlias().equals(alias))
				return authority;
		}
		throw new AceptaSecurityException("No existe la Autoridad: " + alias);
	}

	public Collection<Authority> getAuthorities() {
		return authorities;
	}

	private void setAuthorities(Collection<Authority> authorities) {
		this.authorities = authorities;
	}
	
	public Collection<X509Certificate> getTrustedCertificates() {
		return trustedCertificates;
	}

	private void setTrustedCertificates(Collection<X509Certificate> trustedCertificates) {
		this.trustedCertificates = trustedCertificates;
	}
}
