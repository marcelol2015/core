package com.acepta.ca.security;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.Date;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.NameBuilder;
import com.acepta.ca.security.util.PKCS10;

/**
 * Peticion de Certificados Electronicos X.509 para Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-02-05
 */
public class PersonaCertificateRequest extends CertificateRequest {
	private BigInteger serialNumber;
	private Date notBefore;
	private Date notAfter;
	private String subjectRUT;
	private String subjectEmailAddress;
	private String subjectCommonName;
	private String subjectTitle;
	private String subjectCountry;
	private PublicKey subjectPublicKey;

	public PersonaCertificateRequest() {
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public Name getSubjectName() {
		return createSubjectName(getSubjectRUT(), getSubjectEmailAddress(), getSubjectCommonName(), getSubjectTitle(), getSubjectCountry());
	}

	public PublicKey getSubjectPublicKey() {
		return subjectPublicKey;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public void setSubjectName(String rut, String emailAddress, String commonName, String title, String country) {
		setSubjectRUT(rut);
		setSubjectEmailAddress(emailAddress);
		setSubjectCommonName(commonName);
		setSubjectTitle(title);
		setSubjectCountry(country);
	}

	public void setSubjectPublicKey(PublicKey publicKey) {
		this.subjectPublicKey = publicKey;
	}

	public void setSubjectPublicKeyFromPKCS10(String pkcs10Base64) throws AceptaSecurityException {
		try {
			setSubjectPublicKey(PKCS10.decodePublicKeyFromPKCS10CerticateRequest(pkcs10Base64));
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException(exception);
		}
	}

	public void setSubjectPublicKeyFromSPKAC(String spkacBase64) throws AceptaSecurityException {
		try {
			setSubjectPublicKey(PKCS10.decodePublicKeyFromSignedPublicKeyAndChallenge(spkacBase64));
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException(exception);
		}
	}

	public String getSubjectRUT() {
		return subjectRUT;
	}

	public String getSubjectEmailAddress() {
		return subjectEmailAddress;
	}

	public String getSubjectCommonName() {
		return subjectCommonName;
	}

	public String getSubjectTitle() {
		return subjectTitle;
	}

	public String getSubjectCountry() {
		return subjectCountry;
	}

	public void setSubjectRUT(String rut) {
		this.subjectRUT = rut;
	}

	public void setSubjectEmailAddress(String emailAddress) {
		this.subjectEmailAddress = emailAddress;
	}

	public void setSubjectCommonName(String commonName) {
		this.subjectCommonName = commonName;
	}

	public void setSubjectTitle(String title) {
		this.subjectTitle = title;
	}

	public void setSubjectCountry(String country) {
		this.subjectCountry = country;
	}

	private static Name createSubjectName(String serialNumber, String emailAddress, String commonName, String title, String country) {
		NameBuilder buffer = new NameBuilder();

		buffer.append(NameBuilder.COUNTRY, country);

		if ((title = normalize(title)) != null)
			buffer.append(NameBuilder.TITLE, title);

		if ((commonName = normalize(commonName)) != null)
			buffer.append(NameBuilder.COMMON_NAME, commonName);
		
		if ((emailAddress = normalize(emailAddress)) != null)
			buffer.append(NameBuilder.EMAIL_ADDRESS, emailAddress);

		if ((serialNumber = normalize(serialNumber)) != null)
			buffer.append(NameBuilder.SERIAL_NUMBER, serialNumber);

		return buffer.toName();
	}
	
	private static String normalize(String text) {
		if (text != null) {
			text = text.trim();
			if (text.length() != 0)
				return text;
		}
		return null;
	}
}
