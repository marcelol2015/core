package com.acepta.ca.security.crypto.cert;

import java.util.Date;

/**
 * Peticion OCSP.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public class OCSPRequest {
	private byte[] encoded;
	private Date thisUpdate;
	private OCSPQueryService queryService;
	
	public OCSPRequest() {
	}

	public byte[] getEncoded() {
		return encoded;
	}

	public void setEncoded(byte[] encoded) {
		this.encoded = encoded;
	}

	public Date getThisUpdate() {
		return thisUpdate;
	}

	public void setThisUpdate(Date thisUpdate) {
		this.thisUpdate = thisUpdate;
	}

	public OCSPQueryService getQueryService() {
		return queryService;
	}

	public void setQueryService(OCSPQueryService queryService) {
		this.queryService = queryService;
	}
}
