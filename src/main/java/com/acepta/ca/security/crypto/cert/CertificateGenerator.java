package com.acepta.ca.security.crypto.cert;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Vector;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DisplayText;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.NoticeReference;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierId;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.asn1.x509.UserNotice;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.authority.Authority;

/**
 * Generador de Certificados Electronicos X.509 de Acepta.com S.A. (RFC 3280)
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public abstract class CertificateGenerator {
	/*
	 * Key Usage
	 */
	protected final static int KEY_USAGE_DIGITAL_SIGNATURE = KeyUsage.digitalSignature;
	protected final static int KEY_USAGE_NON_REPUDIATION = KeyUsage.nonRepudiation;
	protected final static int KEY_USAGE_KEY_ENCIPHERMENT = KeyUsage.keyEncipherment;
	protected final static int KEY_USAGE_DATA_ENCIPHERMENT = KeyUsage.dataEncipherment;
	protected final static int KEY_USAGE_KEY_AGREEMENT = KeyUsage.keyAgreement;
	protected final static int KEY_USAGE_KEY_CERT_SIGN = KeyUsage.keyCertSign;
	protected final static int KEY_USAGE_CRL_SIGN = KeyUsage.cRLSign;
	protected final static int KEY_USAGE_ENCIPHER_ONLY = KeyUsage.encipherOnly;
	protected final static int KEY_USAGE_DECIPHER_ONLY = KeyUsage.decipherOnly;

	/*
	 * Extended Key Usage
	 */
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_SERVER_AUTH = KeyPurposeId.id_kp_serverAuth;
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_CLIENT_AUTH = KeyPurposeId.id_kp_clientAuth;
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_CODE_SIGNING = KeyPurposeId.id_kp_codeSigning;
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_EMAIL_PROTECTION = KeyPurposeId.id_kp_emailProtection;
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_TIME_STAMPING = KeyPurposeId.id_kp_timeStamping;
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_OCSP_SIGNING = KeyPurposeId.id_kp_OCSPSigning;

	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_MICROSOFT_INDIVIDUAL_CODE_SIGNING = new DERObjectIdentifier("1.3.6.1.4.1.311.2.1.21");
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_MICROSOFT_COMMERCIAL_CODE_SIGNING = new DERObjectIdentifier("1.3.6.1.4.1.311.2.1.22");

	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_NETSCAPE_SERVER_GATED_CRYPTO = new DERObjectIdentifier("2.16.840.1.113730.4.1");
	protected final static DERObjectIdentifier EXTENDED_KEY_USAGE_MICROSOFT_SERVER_GATED_CRYPTO = new DERObjectIdentifier("1.3.6.1.4.1.311.10.3.3");

	/*
	 * Netscape Cert Types
	 */
	protected final static int NETSCAPE_CERT_TYPE_SSL_CLIENT = 1 << 7;
	protected final static int NETSCAPE_CERT_TYPE_SSL_SERVER = 1 << 6;
	protected final static int NETSCAPE_CERT_TYPE_SMIME = 1 << 5;
	protected final static int NETSCAPE_CERT_TYPE_OBJECT_SIGNING = 1 << 4;
	protected final static int NETSCAPE_CERT_TYPE_RESERVED = 1 << 3;
	protected final static int NETSCAPE_CERT_TYPE_SSL_CA = 1 << 2;
	protected final static int NETSCAPE_CERT_TYPE_SMIME_CA = 1 << 1;
	protected final static int NETSCAPE_CERT_TYPE_OBJECT_SIGNING_CA = 1 << 0;

	/*
	 * Permanent Identifiers (RFC 4043)
	 */
	private final static String OTHER_NAME_TYPE_ID_PERMANENT_IDENTIFIER = "1.3.6.1.5.5.7.8.3";

	/*
	 * Ley No. 19.799 sobre Documentos Electronicos y Firma Electronica (Other Names)
	 */
	private final static String OTHER_NAME_TYPE_ID_LEY_19799_SUBJECT_RUT = "1.3.6.1.4.1.8321.1";
	private final static String OTHER_NAME_TYPE_ID_LEY_19799_ISSUER_RUT = "1.3.6.1.4.1.8321.2";

	/*
	 * P.I. de Acepta para identificar el RUT
	 */
	private final static String PERMANENT_IDENTIFIER_ASSIGNER_ID_ACEPTA_RUT = OTHER_NAME_TYPE_ID_LEY_19799_ISSUER_RUT;

	/*
	 * Netscape Cert Type Extension
	 */
	private final static String EXTENSION_TYPE_ID_NETSCAPE_CERT_TYPE = "2.16.840.1.113730.1.1";

	/*
	 * Netscape SSL Server Name Extension
	 */
	private final static String EXTENSION_TYPE_ID_NETSCAPE_SSL_SERVER_NAME = "2.16.840.1.113730.1.12";

	/*
	 * Acepta Extension con XML del CAF
	 */
	private final static String EXTENSION_TYPE_ID_ACEPTA_CAF = "1.3.6.1.4.1.6891.50.1";

	/*
	 * Acepta P.I. para identificar CAF (RUT|TD|D|H)
	 */
	private final static String PERMANENT_IDENTIFIER_ASSIGNER_ID_ACEPTA_CAF = "1.3.6.1.4.1.6891.50.2";
	private final static String PERMANENT_IDENTIFIER_ASSIGNER_ID_BOGUS_ACEPTA_CAF = "1.3.6.1.4.1.50.2";

	private X509V3CertificateGenerator generator = new X509V3CertificateGenerator();
	private Authority authority;

	protected CertificateGenerator(Authority authority) {
		setAuthority(authority);
	}
	
	protected Authority getAuthority() {
		return authority;
	}

	protected void setAuthority(Authority authority) {
		this.authority = authority;
	}
	
	public abstract X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException;

	/*
	 * Generation
	 */
	protected final void reset() {
		generator.reset();
	}

	protected final X509Certificate generate(PrivateKey issuerPrivateKey) throws GeneralSecurityException {
		X509Certificate certificate = generator.generate(issuerPrivateKey, getAuthority().getProvider().getName());
		
		X509Certificate authorityCertificate = getAuthority().getCertificate();
		
		if (authorityCertificate != null) {
			if (certificate.getNotBefore().before(authorityCertificate.getNotBefore()) ||
				certificate.getNotAfter().after(authorityCertificate.getNotAfter()))
				throw new GeneralSecurityException("Certificate validity date range out of bounds");
		}
		
		return certificate;
	}

	/*
	 * Attributes
	 */
	protected final void setSerialNumber(BigInteger serialNumber) {
		generator.setSerialNumber(serialNumber);
	}

	protected final void setSignatureAlgorithm(String signatureAlgorithm) {
		generator.setSignatureAlgorithm(signatureAlgorithm);
	}

	protected final void setIssuerName(Name issuerName) {
		generator.setIssuerDN(issuerName);
	}

	protected final void setNotBefore(Date notBefore) {
		generator.setNotBefore(notBefore);
	}

	protected final void setNotAfter(Date notAfter) {
		generator.setNotAfter(notAfter);
	}

	protected final void setSubjectName(Name subjectName) {
		generator.setSubjectDN(subjectName);
	}

	protected final void setSubjectPublicKey(PublicKey subjectPublicKey) {
		generator.setPublicKey(subjectPublicKey);
	}

	/*
	 * Extensions
	 */
	protected final void setBasicConstraints(boolean isCritical, BasicConstraints basicConstraints) {
		generator.addExtension(X509Extensions.BasicConstraints, isCritical, basicConstraints);
	}

	protected final void setAuthorityKeyIdentifier(boolean isCritical, AuthorityKeyIdentifierStructure authorityKeyIdentifier) {
		generator.addExtension(X509Extensions.AuthorityKeyIdentifier, isCritical, authorityKeyIdentifier);
	}

	protected final void setSubjectKeyIdentifier(boolean isCritical, SubjectKeyIdentifierStructure subjectKeyIdentifier) {
		generator.addExtension(X509Extensions.SubjectKeyIdentifier, isCritical,	subjectKeyIdentifier);
	}

	protected final void setKeyUsage(boolean isCritical, KeyUsage keyUsage) {
		generator.addExtension(X509Extensions.KeyUsage, isCritical, keyUsage);
	}

	protected final void setExtendedKeyUsage(boolean isCritical, ExtendedKeyUsage extendedKeyUsage) {
		generator.addExtension(X509Extensions.ExtendedKeyUsage, isCritical, extendedKeyUsage);
	}

	protected final void setCertificatePolicies(boolean isCritical, PolicyInformation... certificatePolicies) {
		generator.addExtension(X509Extensions.CertificatePolicies, isCritical, new DERSequence(certificatePolicies));
	}

	protected final void setIssuerAlternativeName(boolean isCritical, GeneralNames issuerAlternativeName) {
		generator.addExtension(X509Extensions.IssuerAlternativeName, isCritical, issuerAlternativeName);
	}

	protected final void setSubjectAlternativeName(boolean isCritical, GeneralNames subjectAlternativeName) {
		generator.addExtension(X509Extensions.SubjectAlternativeName, isCritical, subjectAlternativeName);
	}

	protected final void setAuthorityInformationAccess(boolean isCritical, AuthorityInformationAccess authorityInformationAccess) {
		generator.addExtension(X509Extensions.AuthorityInfoAccess, isCritical, authorityInformationAccess);
	}

	protected final void setCRLDistributionPoints(boolean isCritical, CRLDistPoint crlDistributionPoints) {
		generator.addExtension(X509Extensions.CRLDistributionPoints, isCritical, crlDistributionPoints);
	}

	protected final void setNetscapeCertType(boolean isCritical, DERBitString netscapeCertType) {
		generator.addExtension(new DERObjectIdentifier(EXTENSION_TYPE_ID_NETSCAPE_CERT_TYPE), isCritical, netscapeCertType);
	}

	protected final void setNetscapeSSLServerName(boolean isCritical, DERIA5String netscapeSSLServerName) {
		generator.addExtension(new DERObjectIdentifier(EXTENSION_TYPE_ID_NETSCAPE_SSL_SERVER_NAME), isCritical, netscapeSSLServerName);
	}

	protected final void setAceptaSubjectCAF(boolean isCritical, byte[] caf) {
		generator.addExtension(new DERObjectIdentifier(EXTENSION_TYPE_ID_ACEPTA_CAF), isCritical, new DEROctetString(caf));
	}

	/*
	 * Extension Helpers
	 */
	protected final static BasicConstraints createBasicContraints(boolean isCA) {
		BasicConstraints basicConstraints = new BasicConstraints(isCA);

		return basicConstraints;
	}

	protected final static AuthorityKeyIdentifierStructure createAuthorityKeyIdentifier(PublicKey authorityPublicKey) throws InvalidKeyException {
		AuthorityKeyIdentifierStructure authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(authorityPublicKey);

		return authorityKeyIdentifier;
	}

	protected final static AuthorityKeyIdentifierStructure createAuthorityKeyIdentifier(X509Certificate authorityCertificate) throws InvalidKeyException,
			CertificateParsingException {
		AuthorityKeyIdentifierStructure authorityKeyIdentifier = new AuthorityKeyIdentifierStructure( authorityCertificate);

		return authorityKeyIdentifier;
	}

	protected final static SubjectKeyIdentifierStructure createSubjectKeyIdentifier( PublicKey subjectPublicKey) throws CertificateParsingException, InvalidKeyException {
		SubjectKeyIdentifierStructure subjectKeyIdentifier = new SubjectKeyIdentifierStructure( subjectPublicKey);

		return subjectKeyIdentifier;
	}

	protected final static KeyUsage createKeyUsage(int... keyUsages) {
		int bits = 0;

		for (int bit : keyUsages)
			bits |= bit;

		KeyUsage keyUsage = new KeyUsage(bits);

		return keyUsage;
	}

	protected final static ExtendedKeyUsage createExtendedKeyUsage(DERObjectIdentifier... keyUsages) {
		Vector<DERObjectIdentifier> keyPurposes = new Vector<DERObjectIdentifier>();

		for (DERObjectIdentifier keyUsage : keyUsages)
			keyPurposes.add(keyUsage);

		ExtendedKeyUsage extendedKeyUsage = new ExtendedKeyUsage(keyPurposes);

		return extendedKeyUsage;
	}

	protected final static PolicyInformation createPolicyInformation(String certPolicyID, PolicyQualifierInfo... policyQualifierInfos) {
		DERObjectIdentifier policyIdentifier = new DERObjectIdentifier(certPolicyID);
		DERSequence policyQualifiers = new DERSequence(policyQualifierInfos);

		PolicyInformation policyInformation = new PolicyInformation(policyIdentifier, policyQualifiers);

		return policyInformation;
	}

	protected final static GeneralNames createIssuerAlternativeName(String issuerRUT, String issuerEmail) {
		return createGeneralNames(createIssuerRUTName(issuerRUT), createRUTPIName(issuerRUT),
				createRFC822Name(issuerEmail));
	}

	protected final static GeneralNames createSubjectAlternativeName(String subjectRUT, String subjectEmail) {
		return createGeneralNames(createSubjectRUTName(subjectRUT),
				createRUTPIName(subjectRUT), createRFC822Name(subjectEmail));
	}

	protected final static GeneralNames createSubjectAlternativeName(String subjectRUT) {
		return createGeneralNames(createSubjectRUTName(subjectRUT),
				createRUTPIName(subjectRUT));
	}
	
	protected final static GeneralNames createSubjectAlternativeNameForCAF(String subjectRUT, int subjectTipoDTE, long subjectDesdeFolio, long subjectHastaFolio) {
		return createGeneralNames(createBogusCAFPIName(subjectRUT, subjectTipoDTE, subjectDesdeFolio, subjectHastaFolio));
	}

	protected final static AuthorityInformationAccess createAuthorityInformationAccess(AccessDescription... accessDescriptions) {
		DERSequence sequence = new DERSequence(accessDescriptions);

		AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(sequence);

		return authorityInformationAccess;
	}

	protected final static CRLDistPoint createCRLDistributionPoints(DistributionPoint... distributionPoints) {
		CRLDistPoint crlDistPoints = new CRLDistPoint(distributionPoints);

		return crlDistPoints;
	}

	protected final static DERBitString createNetscapeCertType(int netscapeCertTypes) {
		int padBits = 0;

		while (padBits < 7 && (netscapeCertTypes & (1 << padBits)) == 0)
			padBits++;

		byte[] dataBits = new byte[] { (byte) netscapeCertTypes };

		return new DERBitString(dataBits, padBits);
	}

	protected final static DERIA5String createNetscapeSSLServerName(String serverName) {
		return new DERIA5String(serverName);
	}

	/*
	 * CRL Distribution Points Helpers
	 */
	protected final static DistributionPoint createDistributionPoint(GeneralName... names) {
		GeneralNames fullName = createGeneralNames(names);

		DistributionPointName distributionPointName = new DistributionPointName(DistributionPointName.FULL_NAME, fullName);

		DistributionPoint distributionPoint = new DistributionPoint(distributionPointName, null, null);

		return distributionPoint;
	}

	/*
	 * Authority Information Access Helpers
	 */
	protected final static AccessDescription createOCSPAccessDescription(String ocspURI) {
		return createAccessDescription(AccessDescription.id_ad_ocsp, createURIName(ocspURI));
	}

	protected final static AccessDescription createAccessDescription(DERObjectIdentifier accessMethodID, GeneralName accessLocation) {
		AccessDescription accessDescription = new AccessDescription(accessMethodID, accessLocation);

		return accessDescription;
	}

	/*
	 * Policy Information Helpers
	 */
	protected final static PolicyQualifierInfo createUserNoticePolicyQualifierInfo(String explicitText, String organization, int noticeNumber) {
		// Windows solo entiende esta politica cuando se usa IA5String y VisibleString
		// en la organizacion y texto explicito, respectivamente.
		NoticeReference noticeReference = new NoticeReference(
				DisplayText.CONTENT_TYPE_IA5STRING, organization, new DERSequence(new DERInteger(noticeNumber)));

		UserNotice userNotice = new UserNotice(noticeReference, new DisplayText(
				DisplayText.CONTENT_TYPE_VISIBLESTRING, explicitText));

		PolicyQualifierInfo userNoticePolicyQualifierInfo = new PolicyQualifierInfo(
				PolicyQualifierId.id_qt_unotice, userNotice);

		return userNoticePolicyQualifierInfo;
	}

	protected final static PolicyQualifierInfo createCPSPolicyQualifierInfo(String cpsURI) {
		PolicyQualifierInfo cpsPolicyQualifierInfo = new PolicyQualifierInfo(PolicyQualifierId.id_qt_cps, new DERIA5String(cpsURI));

		return cpsPolicyQualifierInfo;
	}

	/*
	 * General Name Helpers
	 */
	protected final static GeneralNames createGeneralNames(GeneralName... names) {
		GeneralNames generalNames = new GeneralNames(new DERSequence(names));

		return generalNames;
	}

	protected final static GeneralName createIssuerRUTName(String rut) {
		return createOtherName(OTHER_NAME_TYPE_ID_LEY_19799_ISSUER_RUT, new DERIA5String(rut));
	}

	protected final static GeneralName createSubjectRUTName(String rut) {
		return createOtherName(OTHER_NAME_TYPE_ID_LEY_19799_SUBJECT_RUT, new DERIA5String(rut));
	}

	protected static GeneralName createRUTPIName(String rut) {
		return createPermanentIdentifierName(rut, PERMANENT_IDENTIFIER_ASSIGNER_ID_ACEPTA_RUT);
	}

	protected final static GeneralName createCAFPIName(String rut, int tipoDTE, long desdeFolio, long hastaFolio) {
		return createPermanentIdentifierName(rut + "|" + tipoDTE + "|" + desdeFolio + "|" + hastaFolio, PERMANENT_IDENTIFIER_ASSIGNER_ID_ACEPTA_CAF);
	}

	protected final static GeneralName createBogusCAFPIName(String rut, int tipoDTE, long desdeFolio, long hastaFolio) {
		return createPermanentIdentifierName(rut + "|" + tipoDTE + "|" + desdeFolio + "|" + hastaFolio, PERMANENT_IDENTIFIER_ASSIGNER_ID_BOGUS_ACEPTA_CAF);
	}

	protected final static GeneralName createPermanentIdentifierName(String identifierValue, String assignerID) {
		DERSequence permanentIdentifier = new DERSequence(new ASN1Encodable[] {
				new DERUTF8String(identifierValue), new DERObjectIdentifier(assignerID) });

		return createOtherName(OTHER_NAME_TYPE_ID_PERMANENT_IDENTIFIER, permanentIdentifier);
	}

	protected final static GeneralName createOtherName(String typeID, ASN1Encodable value) {
		DERSequence otherName = new DERSequence(new ASN1Encodable[] {
				new DERObjectIdentifier(typeID), new DERTaggedObject(0, value) });

		GeneralName generalName = new GeneralName(GeneralName.otherName, otherName);

		return generalName;
	}

	protected final static GeneralName createRFC822Name(String email) {
		GeneralName generalName = new GeneralName(GeneralName.rfc822Name, new DERIA5String(email));

		return generalName;
	}

	protected static GeneralName createURIName(String uri) {
		GeneralName generalName = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(uri));

		return generalName;
	}
}
