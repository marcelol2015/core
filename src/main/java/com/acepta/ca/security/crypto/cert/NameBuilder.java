package com.acepta.ca.security.crypto.cert;

/**
 * Constructor de nombres X.500 a partir de cadenas de caracteres (RFC 2253).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-11-07
 */
public class NameBuilder {
	public static final String COUNTRY = "C";
	public static final String LOCALITY = "L";
	public static final String STATE_OR_PROVINCE = "ST";
	public static final String ORGANIZATION = "O";
	public static final String ORGANIZATIONAL_UNIT = "OU";
	public static final String TITLE = "T";
	public static final String COMMON_NAME = "CN";
	public static final String EMAIL_ADDRESS = "EmailAddress";
	public static final String SERIAL_NUMBER = "SerialNumber";

	private StringBuffer buffer = new StringBuffer();
	
	public NameBuilder() {
	}

	public void append(String type, String value) {
		if (buffer.length() != 0)
			buffer.append(", ");
		buffer.append(type).append("=").append(escape(value));
	}

	public Name toName() {
		return new Name(buffer.toString());
	}
	
	private static String escape(String value) {
		return value.replace("\\", "\\\\").replace(",", "\\,").replace("+", "\\+").replace("\"", "\\\"").replace("<", "\\<").replace(">", "\\>").replace(";", "\\;").replace("#", "\\#").replace("=", "\\=");
	}
}
