package com.acepta.ca.security.crypto.tsp;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStore;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Set;

import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampResponseGenerator;
import org.bouncycastle.tsp.TimeStampTokenGenerator;

import com.acepta.ca.security.authority.Authority;

/**
 * Generador de Sellos de Tiempo X.509.
 *
 * @author Carlos Hasan
 * @version 1.0, 2007-09-27
 */
public abstract class TimeStampGenerator {
	private Authority authority;
	private TimeSource timeSource;
	private String policy;
	private String digestAlgorithm;
	private Set<String> acceptedDigestAlgorithms;
	private Set<String> acceptedPolicies;
	private BigInteger serialNumber;
	private byte[] requestEncoded;

	protected TimeStampGenerator() {
	}

	protected Authority getAuthority() {
		return authority;
	}

	protected void setAuthority(Authority authority) {
		this.authority = authority;
	}

	protected TimeSource getTimeSource() {
		return timeSource;
	}

	protected void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}

	protected String getPolicy() {
		return policy;
	}

	protected void setPolicy(String policy) {
		this.policy = policy;
	}

	protected String getDigestAlgorithm() {
		return digestAlgorithm;
	}

	protected void setDigestAlgorithm(String digestAlgorithm) {
		this.digestAlgorithm = digestAlgorithm;
	}

	protected Set<String> getAcceptedDigestAlgorithms() {
		return acceptedDigestAlgorithms;
	}

	protected void setAcceptedDigestAlgorithms(Set<String> acceptedAlgorithms) {
		this.acceptedDigestAlgorithms = acceptedAlgorithms;
	}

	protected Set<String> getAcceptedPolicies() {
		return acceptedPolicies;
	}

	protected void setAcceptedPolicies(Set<String> acceptedPolicies) {
		this.acceptedPolicies = acceptedPolicies;
	}

	protected BigInteger getSerialNumber() {
		return serialNumber;
	}

	protected void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	protected byte[] getRequestEncoded() {
		return requestEncoded;
	}

	protected void setRequestEncoded(byte[] requestEncoded) {
		this.requestEncoded = requestEncoded;
	}

	public abstract X509TimeStampResponse generateTimeStamp(X509TimeStampRequest request) throws GeneralSecurityException;

	protected TimeStampResponse generateTimeStampResponse() throws TSPException, NoSuchAlgorithmException, NoSuchProviderException {
		TimeStampResponseGenerator generator = new TimeStampResponseGenerator(createTimeStampGenerator(), getAcceptedDigestAlgorithms(), getAcceptedPolicies());

		TimeStampRequest request = createTimeStampRequest();

		return generator.generate(request, getSerialNumber(), getTimeSource().generateTime(), getAuthority().getProvider().getName());
	}

	private TimeStampRequest createTimeStampRequest() throws TSPException {
		try {
			return new TimeStampRequest(getRequestEncoded());
		}
		catch (IOException exception) {
			throw new TSPException("Peticion de Sellado de Tiempo es invalida");
		}
	}

	private TimeStampTokenGenerator createTimeStampGenerator() throws TSPException {
		TimeStampTokenGenerator generator = new TimeStampTokenGenerator(getAuthority().getPrivateKey(), getAuthority().getCertificate(), getDigestAlgorithm(), getPolicy());

		generator.setAccuracyMicros(getTimeSource().getAccuracyMicroseconds());
		generator.setAccuracyMillis(getTimeSource().getAccuracyMilliseconds());
		generator.setAccuracySeconds(getTimeSource().getAccuracySeconds());

		generator.setOrdering(getTimeSource().getOrdering());

		try {
			generator.setCertificatesAndCRLs(createCertStore(getAuthority().getCertificate()));
		}
		catch (Exception exception) {
			throw new TSPException("Falla al adjuntar Certificado en el Sello de Tiempo", exception);
		}

		generator.setTSA(new GeneralName(getAuthority().getName()));

		return generator;
	}

	private static CertStore createCertStore(X509Certificate certificate) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
		return CertStore.getInstance("Collection", new CollectionCertStoreParameters(Arrays.asList(certificate)));
	}
}