package com.acepta.ca.security.crypto.cert;

import java.math.BigInteger;

public interface OCSPQueryService {
	public OCSPQueryResponse queryCertificateStatus(BigInteger serialNumber);
}
