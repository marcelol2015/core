package com.acepta.ca.security.crypto;

import java.math.BigInteger;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;

/**
 * Peticion para generar Llaves Privadas.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public class PrivateKeyRequest {
	private static final String RSA_KEY_ALGORITHM = "RSA";
	
	private String keyAlgorithm;
	private AlgorithmParameterSpec keyParameters;

	public PrivateKeyRequest() {
	}

	public String getKeyAlgorithm() {
		return keyAlgorithm;
	}

	public void setKeyAlgorithm(String keyAlgorithm) {
		this.keyAlgorithm = keyAlgorithm;
	}

	public AlgorithmParameterSpec getKeyParameters() {
		return keyParameters;
	}

	public void setKeyParameters(AlgorithmParameterSpec keyParameters) {
		this.keyParameters = keyParameters;
	}

	public void setRSAKeyParameters(int keySize, BigInteger keyPublicExponent) {
		setKeyAlgorithm(RSA_KEY_ALGORITHM);
		setKeyParameters(new RSAKeyGenParameterSpec(keySize, keyPublicExponent));
	}
}
