package com.acepta.ca.security.crypto.tsp;

import java.math.BigInteger;

/**
 * Peticion de Sellado de Tiempo X.509 (RFC 3161).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-27
 */
public class X509TimeStampRequest {
	private BigInteger serialNumber;
	private byte[] encoded;

	public X509TimeStampRequest(BigInteger serialNumber, byte[] encoded) {
		setSerialNumber(serialNumber);
		setEncoded(encoded);
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public byte[] getEncoded() {
		return encoded;
	}

	public void setEncoded(byte[] encoded) {
		this.encoded = encoded;
	}
}
