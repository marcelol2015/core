package com.acepta.ca.security.crypto;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Provider;

/**
 * Generador de Llaves Privadas.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public class PrivateKeyGenerator {
	private Provider provider;
	
	public PrivateKeyGenerator(Provider provider) {
		setProvider(provider);
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	public KeyPair generatePrivateKey(PrivateKeyRequest request) throws GeneralSecurityException {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(request.getKeyAlgorithm(), getProvider());
		
		keyPairGenerator.initialize(request.getKeyParameters());
		
		return keyPairGenerator.generateKeyPair();
	}
}
