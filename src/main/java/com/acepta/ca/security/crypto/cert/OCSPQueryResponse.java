package com.acepta.ca.security.crypto.cert;

import java.util.Date;

public class OCSPQueryResponse {
	public static enum Status {
		GOOD,
		REVOKED,
		UNKNOWN
	}

	private Status status;
	private Date revocationDate;
	private int reason;
	
	public OCSPQueryResponse(Status status) {
		this(status, null, CRLEntry.REASON_UNUSED);
	}
	
	public OCSPQueryResponse(Status status, Date revocationDate, int reason) {
		setStatus(status);
		setRevocationDate(revocationDate);
		setReason(reason);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getRevocationDate() {
		return revocationDate;
	}

	public void setRevocationDate(Date revocationDate) {
		this.revocationDate = revocationDate;
	}

	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}
}
