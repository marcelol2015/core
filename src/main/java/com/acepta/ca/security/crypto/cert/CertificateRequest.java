package com.acepta.ca.security.crypto.cert;

import java.math.BigInteger;
import java.security.PublicKey;
import java.util.Date;


/**
 * Peticion de Certificados Electronicos X.509.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-18
 */
public abstract class CertificateRequest {
	public abstract BigInteger getSerialNumber();

	public abstract Date getNotBefore();

	public abstract Date getNotAfter();

	public abstract Name getSubjectName();

	public abstract PublicKey getSubjectPublicKey();
}
