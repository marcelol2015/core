package com.acepta.ca.security.crypto.tsp;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


/**
 * Fuente de Tiempo basada en el Calendario de Java.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-27
  */
public class JavaTimeSource extends TimeSource {
	private static final String UTC_TIME_ZONE = "GMT+0";

	private static final int ACCURACY_UNSPECIFIED = 0;
	
	private Calendar calendar;
	
	public JavaTimeSource() {
		setCalendar(Calendar.getInstance(TimeZone.getTimeZone(UTC_TIME_ZONE)));
	}
	
	public Date generateTime() {
		return getCalendar().getTime();
	}
	
	public int getAccuracySeconds() {
		return ACCURACY_UNSPECIFIED;
	}

	public int getAccuracyMilliseconds() {
		return ACCURACY_UNSPECIFIED;
	}
	
	public int getAccuracyMicroseconds() {
		return ACCURACY_UNSPECIFIED;
	}

	public boolean getOrdering() {
		return false;
	}
	
	private Calendar getCalendar() {
		return calendar;
	}

	private void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
}
