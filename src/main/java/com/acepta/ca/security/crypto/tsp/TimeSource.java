package com.acepta.ca.security.crypto.tsp;

import java.util.Date;

/**
 * Fuente de Tiempo para el Generador de Sellos de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-27
 */
public abstract class TimeSource {
	public abstract Date generateTime();
	
	public abstract int getAccuracySeconds();
	
	public abstract int getAccuracyMilliseconds();
	
	public abstract int getAccuracyMicroseconds();
	
	public abstract boolean getOrdering();
}
