package com.acepta.ca.security.crypto.cert;

import java.util.Vector;

import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.asn1.x509.X509NameEntryConverter;

/**
 * X.501 Relative Distinguished Name (ITU-T Recommendation X.501)
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-01-22
 */
public class Name extends X509Name {
	/*
	 * Converter for X.509 distinguished name entries from string to ASN.1 values.
	 * Use PrintableString instead of UTF8String whenever possible.
	 */
	private static class EntryConverter extends X509NameEntryConverter {
		public DERObject getConvertedValue(DERObjectIdentifier type, String value) {
			if (type.equals(EmailAddress)) {
				return new DERIA5String(value);
			}
			else if (type.equals(C) || type.equals(SERIALNUMBER)) {
				return new DERPrintableString(value);
			}
			else if (canBePrintable(value)) {
				return new DERPrintableString(value);
			}
			return new DERUTF8String(value);
		}
	}
	
	public Name(String name) {
		super(false, name, new EntryConverter());
	}
	
	public Name(X509Name name) {
		super(name.getOIDs(), name.getValues(), new EntryConverter());
	}
	
	public String getSerialNumber() {
		return getAttribute(SERIALNUMBER);
	}

	public String getEmailAddress() {
		return getAttribute(EmailAddress);
	}

	public String getCommonName() {
		return getAttribute(CN);
	}

	public String getOrganizationalUnit() {
		return getAttribute(OU);
	}

	public String getOrganization() {
		return getAttribute(O);
	}

	public String getLocality() {
		return getAttribute(L);
	}
	
	public String getState() {
		return getAttribute(ST);
	}
	
	public String getCountry() {
		return getAttribute(C);
	}

	public String getRUT() {
		return getSerialNumber();
	}

	private String getAttribute(DERObjectIdentifier type) {
		Vector<?> types = getOIDs();
		Vector<?> values = getValues();

		for (int index = 0; index < types.size(); index++) {
			if (types.get(index).equals(type))
				return (String) values.get(index);
		}
		return null;
	}
}
