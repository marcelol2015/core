package com.acepta.ca.security.crypto.cert;

import java.math.BigInteger;
import java.util.Date;

import org.bouncycastle.asn1.x509.CRLReason;

/**
 * Certificado revocado en una CRL.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-02-07
 */
public class CRLEntry {
	/*
	 * Reason Flags
	 */
	public static final int REASON_UNUSED = CRLReason.unspecified;
	public static final int REASON_KEY_COMPROMISE = CRLReason.keyCompromise;
	public static final int REASON_CA_COMPROMISE = CRLReason.cACompromise;
	public static final int REASON_AFFILIATION_CHANGED = CRLReason.affiliationChanged;
	public static final int REASON_SUPERSEDED = CRLReason.superseded;
	public static final int REASON_CESSATION_OF_OPERATION = CRLReason.cessationOfOperation;
	public static final int REASON_CERTIFICATE_HOLD = CRLReason.certificateHold;
	public static final int REASON_PRIVILEGE_WITHDRAWN = CRLReason.privilegeWithdrawn;
	public static final int REASON_AA_COMPROMISE = CRLReason.aACompromise;

	private BigInteger userCertificate;
	private Date revocationDate;
	private int reason;

	public CRLEntry(BigInteger userCertificate, Date revocationDate, int reason) {
		setUserCertificate(userCertificate);
		setRevocationDate(revocationDate);
		setReason(reason);
	}

	public int getReason() {
		return reason;
	}

	public Date getRevocationDate() {
		return revocationDate;
	}

	public BigInteger getUserCertificate() {
		return userCertificate;
	}

	private void setReason(int reason) {
		this.reason = reason;
	}

	private void setRevocationDate(Date revocationDate) {
		this.revocationDate = revocationDate;
	}

	private void setUserCertificate(BigInteger userCertificate) {
		this.userCertificate = userCertificate;
	}
}
