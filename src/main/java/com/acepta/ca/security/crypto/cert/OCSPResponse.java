package com.acepta.ca.security.crypto.cert;

/**
 * Respuesta OCSP.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public class OCSPResponse {
	private int status;
	private byte[] encoded;

	public OCSPResponse(int status, byte[] encoded) {
		setStatus(status);
		setEncoded(encoded);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public byte[] getEncoded() {
		return encoded;
	}

	public void setEncoded(byte[] encoded) {
		this.encoded = encoded;
	}
}
