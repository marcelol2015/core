package com.acepta.ca.security.crypto.tsp;

import org.bouncycastle.tsp.GenTimeAccuracy;

import java.math.BigInteger;
import java.util.Date;

/**
 * Respuesta de Sellado de Tiempo X.509 (RFC 3161).
 * 
 * @author Carlos Hasan, Angelo Estartus
 * @version 2.0, 2015-09-21
 */
public class X509TimeStampResponse {
	private boolean granted;
	private byte[] encoded;
	private BigInteger serialNumber;
	private Date genTime;
    private GenTimeAccuracy accuracy;

	public X509TimeStampResponse(boolean granted, byte[] encoded, BigInteger serialNumber, Date genTime,GenTimeAccuracy accuracy) {
		setGranted(granted);
		setEncoded(encoded);
		setSerialNumber(serialNumber);
		setGenTime(genTime);
        setAccuracy(accuracy);
	}

	public boolean isGranted() {
		return granted;
	}

	private void setGranted(boolean granted) {
		this.granted = granted;
	}

	public byte[] getEncoded() {
		return encoded;
	}

	private void setEncoded(byte[] encoded) {
		this.encoded = encoded;
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	private void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getGenTime() {
		return genTime;
	}

	private void setGenTime(Date genTime) {
		this.genTime = genTime;
	}

    public GenTimeAccuracy getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(GenTimeAccuracy accuracy) {
        this.accuracy = accuracy;
    }
}
