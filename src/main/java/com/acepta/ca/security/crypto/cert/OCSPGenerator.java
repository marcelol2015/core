package com.acepta.ca.security.crypto.cert;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Date;

import org.bouncycastle.ocsp.BasicOCSPResp;
import org.bouncycastle.ocsp.BasicOCSPRespGenerator;
import org.bouncycastle.ocsp.CertificateID;
import org.bouncycastle.ocsp.CertificateStatus;
import org.bouncycastle.ocsp.OCSPException;
import org.bouncycastle.ocsp.OCSPReq;
import org.bouncycastle.ocsp.OCSPResp;
import org.bouncycastle.ocsp.OCSPRespGenerator;
import org.bouncycastle.ocsp.OCSPRespStatus;
import org.bouncycastle.ocsp.Req;
import org.bouncycastle.ocsp.RevokedStatus;
import org.bouncycastle.ocsp.UnknownStatus;

import com.acepta.ca.security.authority.Authority;

/**
 * Generador de respuestas OCSP (RFC 2560).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public abstract class OCSPGenerator {
	private static final String MESSAGE_DIGEST_PROVIDER_NAME = "SUN";

	static {
		// HACK: Necesario para codificar CertificateIDs con algoritmo SHA-1 y MD5.
	    Security.getProvider(MESSAGE_DIGEST_PROVIDER_NAME).put("Alg.Alias.MessageDigest.1.3.14.3.2.26", "SHA1");
	    Security.getProvider(MESSAGE_DIGEST_PROVIDER_NAME).put("Alg.Alias.MessageDigest.1.2.840.113549.2.5", "MD5");
	}
	
	private Authority authority;
	private String signingAlgorithm;
	private Date thisUpdate;
	private OCSPQueryService queryService;
	private byte[] requestEncoded;
	
	protected OCSPGenerator(Authority authority) {
		setAuthority(authority);
	}

	protected Authority getAuthority() {
		return authority;
	}

	protected void setAuthority(Authority authority) {
		this.authority = authority;
	}

	protected String getSigningAlgorithm() {
		return signingAlgorithm;
	}

	protected void setSigningAlgorithm(String signingAlgorithm) {
		this.signingAlgorithm = signingAlgorithm;
	}

	protected Date getThisUpdate() {
		return thisUpdate;
	}

	protected void setThisUpdate(Date thisUpdate) {
		this.thisUpdate = thisUpdate;
	}

	protected OCSPQueryService getQueryService() {
		return queryService;
	}

	protected void setQueryService(OCSPQueryService queryService) {
		this.queryService = queryService;
	}

	protected byte[] getRequestEncoded() {
		return requestEncoded;
	}

	protected void setRequestEncoded(byte[] requestEncoded) {
		this.requestEncoded = requestEncoded;
	}

	public OCSPResponse generateOCSP(OCSPRequest request) throws GeneralSecurityException {
		setSigningAlgorithm(getAuthority().getSignatureAlgorithm());
		setThisUpdate(request.getThisUpdate());
		setQueryService(request.getQueryService());
		setRequestEncoded(request.getEncoded());
		
		try {
			OCSPResp resp = generateOCSPResponse();
			
			return new OCSPResponse(resp.getStatus(), resp.getEncoded());
		}
		catch (Exception exception) {
			throw new GeneralSecurityException(exception);
		}
	}
	
	protected OCSPResp generateOCSPResponse() throws GeneralSecurityException, OCSPException {
		OCSPRespGenerator generator = new OCSPRespGenerator();
		
		try {
			BasicOCSPResp responseBytes = generateBasicOCSPResponse();
		
			return generator.generate(OCSPRespStatus.SUCCESSFUL, responseBytes);
		}
		catch (IOException exception) {
			return generator.generate(OCSPRespStatus.MALFORMED_REQUEST, null);
		}
		catch (OCSPException exception) {
			return generator.generate(OCSPRespStatus.INTERNAL_ERROR, null);
		}
	}
	
	private BasicOCSPResp generateBasicOCSPResponse() throws OCSPException, IOException, NoSuchProviderException, IllegalArgumentException  {
		BasicOCSPRespGenerator generator = new BasicOCSPRespGenerator(getAuthority().getCertificate().getPublicKey());
		
		OCSPReq request = createOCSPRequest();
		
		for (Req req : request.getRequestList())
			generator.addResponse(req.getCertID(), getCertificateStatus(req.getCertID()));
	
		return generator.generate(getSigningAlgorithm(), getAuthority().getPrivateKey(), new X509Certificate[] { getAuthority().getCertificate() }, getThisUpdate(), getAuthority().getProvider().getName());
	}

	private CertificateStatus getCertificateStatus(CertificateID certID) throws OCSPException {
		try {
			if (!certID.equals(createIssuerCertificateID(certID.getHashAlgOID(), certID.getSerialNumber())))
				return new UnknownStatus();
			
			OCSPQueryResponse response = getQueryService().queryCertificateStatus(certID.getSerialNumber());
			
			switch (response.getStatus()) {
			case GOOD:
				return null;
			case REVOKED:
				return new RevokedStatus(response.getRevocationDate(), response.getReason());
			}
			
			return new UnknownStatus();
		}
		catch (Exception exception) {
			throw new OCSPException("Falla al consultar estado del certificado", exception);
		}
	}
	
	private CertificateID createIssuerCertificateID(String hashAlgorithm, BigInteger serialNumber) throws OCSPException {
		return new CertificateID(hashAlgorithm, getAuthority().getCertificate(), serialNumber, MESSAGE_DIGEST_PROVIDER_NAME);
	}
	
	private OCSPReq createOCSPRequest() throws IOException {
		return new OCSPReq(getRequestEncoded());
	}
}
