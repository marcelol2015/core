package com.acepta.ca.security.crypto.cert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Peticion para generar una Lista de Revocacion de Certificados (CRL) X.509.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-02-05
 */
public class CRLRequest {
	private Date thisUpdate;
	private Date nextUpdate;
	private Collection<CRLEntry> entries;
	private BigInteger cRLNumber;

	public CRLRequest() {
		setEntries(new ArrayList<CRLEntry>());
	}

	public Date getThisUpdate() {
		return thisUpdate;
	}

	public void setThisUpdate(Date thisUpdate) {
		this.thisUpdate = thisUpdate;
	}

	public Date getNextUpdate() {
		return nextUpdate;
	}

	public void setNextUpdate(Date nextUpdate) {
		this.nextUpdate = nextUpdate;
	}

	public Collection<CRLEntry> getEntries() {
		return entries;
	}

	public void setEntries(Collection<CRLEntry> entries) {
		this.entries = entries;
	}

	public void addEntry(BigInteger userCertificate, Date revocationDate, int reason) {
		getEntries().add(new CRLEntry(userCertificate, revocationDate, reason));
	}

	public BigInteger getCRLNumber() {
		return cRLNumber;
	}

	public void setCRLNumber(BigInteger number) {
		cRLNumber = number;
	}
}
