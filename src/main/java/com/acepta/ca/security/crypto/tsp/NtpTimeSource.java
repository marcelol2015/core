package com.acepta.ca.security.crypto.tsp;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by nhakor on 04-09-15.
 */
public class NtpTimeSource extends TimeSource {

    private Calendar calendar;

    public NtpTimeSource(){

    }
    @Override
    public Date generateTime() {
        return null;
    }

    @Override
    public int getAccuracySeconds() {
        return 0;
    }

    @Override
    public int getAccuracyMilliseconds() {
        return 0;
    }

    @Override
    public int getAccuracyMicroseconds() {
        return 0;
    }

    @Override
    public boolean getOrdering() {
        return false;
    }

    private Calendar getCalendar() {
        return calendar;
    }

    private void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }
}
