package com.acepta.ca.security.crypto.cert;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Date;

import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V2CRLGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;

import com.acepta.ca.security.authority.Authority;

/**
 * Generador de Listas de Revocacion de Certificados X.509 Version 2 (RFC 3280).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-02-05
 */
public abstract class CRLGenerator {
	private X509V2CRLGenerator generator = new X509V2CRLGenerator();
	private Authority authority;

	protected CRLGenerator(Authority authority) {
		setAuthority(authority);
	}

	protected Authority getAuthority() {
		return authority;
	}

	protected void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public X509CRL generateCRL(CRLRequest request) throws GeneralSecurityException {
		setIssuerName(getAuthority().getName());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setThisUpdate(request.getThisUpdate());
		setNextUpdate(request.getNextUpdate());
		
		setAuthorityKeyIdentifier(false, createAuthorityKeyIdentifier(getAuthority().getCertificate().getPublicKey()));
		setCRLNumber(false, createCRLNumber(request.getCRLNumber()));
		
		addCRLEntries(request.getEntries());
		
		return generate(getAuthority().getPrivateKey());
	}

	/*
	 * Generation
	 */
	protected final void reset() {
		generator.reset();
	}

	protected final X509CRL generate(PrivateKey issuerPrivateKey) throws GeneralSecurityException {
		return generator.generate(issuerPrivateKey, getAuthority().getProvider().getName());
	}

	/*
	 * Attributes
	 */
	protected final void setIssuerName(Name issuerName) {
		generator.setIssuerDN(issuerName);
	}

	protected final void setSignatureAlgorithm(String signatureAlgorithm) {
		generator.setSignatureAlgorithm(signatureAlgorithm);
	}

	protected final void setThisUpdate(Date thisUpdate) {
		generator.setThisUpdate(thisUpdate);
	}

	protected final void setNextUpdate(Date nextUpdate) {
		generator.setNextUpdate(nextUpdate);
	}

	/*
	 * Entries
	 */
	protected final void addCRLEntries(Collection<CRLEntry> entries) {
		for (CRLEntry entry : entries)
			addCRLEntry(entry);
	}

	protected final void addCRLEntry(CRLEntry entry) {
		generator.addCRLEntry(entry.getUserCertificate(), entry.getRevocationDate(), entry.getReason());
	}

	/*
	 * Extensions
	 */
	protected final void setAuthorityKeyIdentifier(boolean isCritical, AuthorityKeyIdentifierStructure authorityKeyIdentifier) {
		generator.addExtension(X509Extensions.AuthorityKeyIdentifier, isCritical, authorityKeyIdentifier);
	}

	protected final void setIssuerAlternativeName(boolean isCritical, GeneralNames issuerAlternativeName) {
		generator.addExtension(X509Extensions.IssuerAlternativeName, isCritical, issuerAlternativeName);
	}

	protected final void setCRLNumber(boolean isCritical, CRLNumber cRLNumber) {
		generator.addExtension(X509Extensions.CRLNumber, isCritical, cRLNumber);
	}

	/*
	 * Extension Helpers
	 */
	protected final static AuthorityKeyIdentifierStructure createAuthorityKeyIdentifier(PublicKey authorityPublicKey) throws InvalidKeyException {
		AuthorityKeyIdentifierStructure authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(authorityPublicKey);

		return authorityKeyIdentifier;
	}

	protected final static CRLNumber createCRLNumber(BigInteger number) {
		CRLNumber cRLNumber = new CRLNumber(number);

		return cRLNumber;
	}

	/*
	 * General Name Helpers
	 */
	protected final static GeneralNames createGeneralNames(GeneralName... names) {
		GeneralNames generalNames = new GeneralNames(new DERSequence(names));

		return generalNames;
	}

	protected final static GeneralName createRFC822Name(String email) {
		GeneralName generalName = new GeneralName(GeneralName.rfc822Name, new DERIA5String(email));

		return generalName;
	}

	protected static GeneralName createDNSName(String dNSName) {
		GeneralName generalName = new GeneralName(GeneralName.dNSName, new DERIA5String(dNSName));

		return generalName;
	}

	protected static GeneralName createURIName(String uri) {
		GeneralName generalName = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(uri));

		return generalName;
	}
}
