package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Created by nhakor on 16-09-15.
 */
public class AceptaSellotiempoG4OCSPGenerator extends OCSPGenerator {
    protected AceptaSellotiempoG4OCSPGenerator(AceptaSelloTiempoG4Authority authority) {
        super(authority);
    }
}
