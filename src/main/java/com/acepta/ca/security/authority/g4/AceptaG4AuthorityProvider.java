package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.authority.AuthorityProvider;

/**
 * Proveedor de la jerarquia de Autoridades Certificadoras WebTrust de Acepta.com S.A.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaG4AuthorityProvider extends AuthorityProvider {
	public static final int ACEPTA_G4_AUTHORITY_HIERARCHY_DURATION_IN_YEARS = 20;
	
	public AceptaG4AuthorityProvider() {
		setAuthorityDurationInYears(ACEPTA_G4_AUTHORITY_HIERARCHY_DURATION_IN_YEARS);
		registerRootAuthorityFactory();
		registerIntermediateAuthorityFactories();
        registerFinalAuthorityFactories();
	}

	private void registerRootAuthorityFactory() {
		setRootAuthorityFactory(new AceptaRaizG4AuthorityFactory());
	}

	private void registerIntermediateAuthorityFactories() {
		addIntermediateAuthorityFactory(new AceptaClase3G4AuthorityFactory());
		addIntermediateAuthorityFactory(new AceptaClase2G4AuthorityFactory());
		addIntermediateAuthorityFactory(new AceptaFirmaAvanzadaG4AuthorityFactory());
        addIntermediateAuthorityFactory(new AceptaSelloTiempoG4AuthorityFactory());
	}

    private void registerFinalAuthorityFactories() {
        addFinalAuthorityFactory(new EmisorSelloTiempoG4AuthorityFactory());
    }
}
