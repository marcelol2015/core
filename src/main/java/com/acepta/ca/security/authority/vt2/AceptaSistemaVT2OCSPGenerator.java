package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Generador de respuestas OCSP de la Autoridad Certificadora de Sistema.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public class AceptaSistemaVT2OCSPGenerator extends OCSPGenerator {
	public AceptaSistemaVT2OCSPGenerator(AceptaSistemaVT2Authority authority) {
		super(authority);
	}
}
