package com.acepta.ca.security.authority.vt2;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;

import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.SistemaCertificateRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora de Sistema.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-03
 */
public class AceptaSistemaVT2CertificateGenerator extends CertificateGenerator {
	/*
	 * Certificate Policies
	 */
	private static final String ACEPTA_SISTEMA_VT2_POLICY_IDENTIFIER_ARC = "1.3.6.1.4.1.6891.1001.5.";
	private static final String ACEPTA_SISTEMA_VT2_POLICY_CPS_URI = "http://www.acepta.com/CPSVT2";

	/*
	 * OCSP Authority Information Access
	 */
	private static final String ACEPTA_SISTEMA_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI = "http://ocsp.acepta.com/SistemaVT2";

	/*
	 * CRL Distribution Points
	 */
	private static final String ACEPTA_SISTEMA_VT2_CRL_DISTRIBUTION_POINT_URI = "http://crl.acepta.com/SistemaVT2.crl";

	public AceptaSistemaVT2CertificateGenerator(AceptaSistemaVT2Authority authority) {
		super(authority);
	}

	public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {
		SistemaCertificateRequest sistemas = (SistemaCertificateRequest) request;
		
		setSerialNumber(sistemas.getSerialNumber());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setIssuerName(getAuthority().getName());
		setNotBefore(sistemas.getNotBefore());
		setNotAfter(sistemas.getNotAfter());
		setSubjectName(sistemas.getSubjectName());
		setSubjectPublicKey(sistemas.getSubjectPublicKey());

		setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier());
		setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(sistemas.getSubjectPublicKey()));

		setKeyUsage(false, getKeyUsage());
		setExtendedKeyUsage(false, getExtendedKeyUsage());

		setCertificatePolicies(false, getCertificatePolicies(sistemas.getCertificatePolicies()));
		
		setIssuerAlternativeName(false, getIssuerAlternativeName());
		setSubjectAlternativeName(false, getSubjectAlternativeName(sistemas.getSubjectRUT()));
		
		setAuthorityInformationAccess(false, getAuthorityInformationAccess());
		setCRLDistributionPoints(false, getCRLDistributionPoints());

		return generate(getAuthority().getPrivateKey());
	}

	/*
	 * Certificate Generation Attributes
	 */
	private GeneralNames getSubjectAlternativeName(String subjectRUT) {
		return CertificateGenerator.createSubjectAlternativeName(subjectRUT);
	}
	
	private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier() throws InvalidKeyException {
		return CertificateGenerator.createAuthorityKeyIdentifier(getAuthority().getCertificate().getPublicKey());
	}

	private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey) throws CertificateParsingException, InvalidKeyException {
		return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
	}
	
	private KeyUsage getKeyUsage() {
		return CertificateGenerator.createKeyUsage(CertificateGenerator.KEY_USAGE_DIGITAL_SIGNATURE);
	}

	private ExtendedKeyUsage getExtendedKeyUsage() {
		return CertificateGenerator.createExtendedKeyUsage(CertificateGenerator.EXTENDED_KEY_USAGE_CLIENT_AUTH);
	}

	private PolicyInformation[] getCertificatePolicies(Collection<String> certificatePoliciesIDs) throws GeneralSecurityException {
		ArrayList<PolicyInformation> certificatePolicies = new ArrayList<PolicyInformation>(); 
		
		for (String certificatePolicyID : certificatePoliciesIDs) {
			if (!isAllowedCertificatePolicy(certificatePolicyID))
				throw new GeneralSecurityException("Politica de Certificado de Sistemas no permitida: " + certificatePolicyID);
		
			PolicyInformation policyInformation = CertificateGenerator.createPolicyInformation(certificatePolicyID, getCPSPolicyQualifierInfo());
			
			certificatePolicies.add(policyInformation);
		}

		return certificatePolicies.toArray(new PolicyInformation[certificatePolicies.size()]);
	}

	private PolicyQualifierInfo getCPSPolicyQualifierInfo() {
		return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_SISTEMA_VT2_POLICY_CPS_URI);
	}

	private GeneralNames getIssuerAlternativeName() {
		Name issuerName = getAuthority().getName();
		
		return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
	}

	private AuthorityInformationAccess getAuthorityInformationAccess() {
		AccessDescription ocspAccessDescriptor = CertificateGenerator.createOCSPAccessDescription(ACEPTA_SISTEMA_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI);
		
		return CertificateGenerator.createAuthorityInformationAccess(ocspAccessDescriptor);
	}

	private CRLDistPoint getCRLDistributionPoints() {
		GeneralName crlURI = CertificateGenerator.createURIName(ACEPTA_SISTEMA_VT2_CRL_DISTRIBUTION_POINT_URI);
		
		return CertificateGenerator.createCRLDistributionPoints(CertificateGenerator.createDistributionPoint(crlURI));
	}
	
	private static boolean isAllowedCertificatePolicy(String certificatePolicy) {
		return certificatePolicy.startsWith(ACEPTA_SISTEMA_VT2_POLICY_IDENTIFIER_ARC);
	}
}
