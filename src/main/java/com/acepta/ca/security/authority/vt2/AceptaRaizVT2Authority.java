package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;

/**
 * Autoridad Certificadora Raiz "No WebTrust".
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaRaizVT2Authority extends Authority {
	/*
	 * Alias
	 */
	private static final String ACEPTA_RAIZ_VT2_CA_ALIAS = "RaizVT2";
	
	/*
	 * Distinguished Name
	 */
	private static final String ACEPTA_RAIZ_VT2_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Autoridad Certificadora Raiz - VT2, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

	/*
	 * Signature Algorithm
	 */
	private static final String ACEPTA_RAIZ_VT2_SIGNATURE_ALGORITHM = "SHA256WithRSA";

	/*
	 * Initial Serial Number (UNUSED)
	 */
	private static final BigInteger ACEPTA_RAIZ_VT2_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(5L);
	
	public AceptaRaizVT2Authority() {
		super(ACEPTA_RAIZ_VT2_CA_ALIAS, ACEPTA_RAIZ_VT2_CA_NAME, ACEPTA_RAIZ_VT2_INITIAL_SERIAL_NUMBER);
	}

	public LegalProtection getLegalProtection() {
		return LegalProtection.FIRMA_SIMPLE;
	}

	public Collection<KeyPurpose> getKeyUsage() {
		return Arrays.asList(KeyPurpose.KEY_CERT_SIGN);
	}

    public String getSignatureAlgorithm() {
		return ACEPTA_RAIZ_VT2_SIGNATURE_ALGORITHM;
	}	
	
	public CertificateGenerator getCertificateGenerator() {
		return new AceptaRaizVT2CertificateGenerator(this);
	}

	public CRLGenerator getCRLGenerator() {
		return new AceptaRaizVT2CRLGenerator(this);
	}
}
