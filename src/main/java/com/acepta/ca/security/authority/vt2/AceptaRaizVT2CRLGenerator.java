package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora Raiz.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaRaizVT2CRLGenerator extends CRLGenerator {
	public AceptaRaizVT2CRLGenerator(AceptaRaizVT2Authority authority) {
		super(authority);
	}
}
