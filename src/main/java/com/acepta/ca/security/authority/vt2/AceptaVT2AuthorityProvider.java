package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.authority.AuthorityProvider;

/**
 * Proveedor de la jerarquia de Autoridades Certificadoras "No WebTrust" de Acepta.com S.A.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaVT2AuthorityProvider extends AuthorityProvider {
	public static final int ACEPTA_VT2_AUTHORITY_HIERARCHY_DURATION_IN_YEARS = 20;
	
	public AceptaVT2AuthorityProvider() {
		setAuthorityDurationInYears(ACEPTA_VT2_AUTHORITY_HIERARCHY_DURATION_IN_YEARS);
		registerRootAuthorityFactory();
		registerIntermediateAuthorityFactories();
	}

	private void registerRootAuthorityFactory() {
		setRootAuthorityFactory(new AceptaRaizVT2AuthorityFactory());
	}

	private void registerIntermediateAuthorityFactories() {
		addIntermediateAuthorityFactory(new AceptaSistemaVT2AuthorityFactory());
        /**Se cambia la jerarquía para que las intermedias se creen a partir de la raíz única de Acepta**/
		//addIntermediateAuthorityFactory(new AceptaSelloTiempoG4AuthorityFactory());
		addIntermediateAuthorityFactory(new AceptaCAFVT2AuthorityFactory());
		addIntermediateAuthorityFactory(new AceptaCodigoVT2AuthorityFactory());
		addIntermediateAuthorityFactory(new AceptaSitioWebVT2AuthorityFactory());
	}
}
