package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Generador de respuestas OCSP de la Autoridad Certificadora de Sitio Web.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public class AceptaSitioWebVT2OCSPGenerator extends OCSPGenerator {
	public AceptaSitioWebVT2OCSPGenerator(AceptaSitioWebVT2Authority authority) {
		super(authority);
	}
}
