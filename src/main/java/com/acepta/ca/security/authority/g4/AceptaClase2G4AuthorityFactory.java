package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora Clase 2 de Personas Naturales.
 * 
 * @author Carlos Hasan, Alvaro Millalen
 * @version 1.0, 2012-01-11
 */
public class AceptaClase2G4AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_CLASE2_G4_CA_SERIAL_NUMBER = 9;
	
	public AceptaClase2G4AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new AceptaClase2G4Authority();
	}

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_CLASE2_G4_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
