package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora Raiz.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaRaizG4CRLGenerator extends CRLGenerator {
	public AceptaRaizG4CRLGenerator(AceptaRaizG4Authority authority) {
		super(authority);
	}
}
