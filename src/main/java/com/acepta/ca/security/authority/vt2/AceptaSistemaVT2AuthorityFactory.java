package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora de Sistema.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-03
 */
public class AceptaSistemaVT2AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_SISTEMA_VT2_CA_SERIAL_NUMBER = 5;
	
	public AceptaSistemaVT2AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new AceptaSistemaVT2Authority();
	}

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_SISTEMA_VT2_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
