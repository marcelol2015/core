package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Generador de respuestas OCSP de la Autoridad Certificadora de Firma Electronica Avanzada.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public class AceptaFirmaAvanzadaG4OCSPGenerator extends OCSPGenerator {
	public AceptaFirmaAvanzadaG4OCSPGenerator(AceptaFirmaAvanzadaG4Authority authority) {
		super(authority);
	}
}
