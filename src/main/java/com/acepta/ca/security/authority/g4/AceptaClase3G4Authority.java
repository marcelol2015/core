package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Autoridad Certificadora Clase 3 de Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaClase3G4Authority extends Authority {
	/*
	 * Alias
	 */
	private static final String ACEPTA_CLASE3_G4_CA_ALIAS = "Clase3-G4";

	/*
	 * Distinguished Name
	 */
	private static final String ACEPTA_CLASE3_G4_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Autoridad Certificadora Clase 3 Persona Natural - G4, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

	/*
	 * Signature Algorithm
	 */
	private static final String ACEPTA_CLASE3_G4_SIGNATURE_ALGORITHM = "SHA256WithRSA";

	/*
	 * Initial Serial Number
	 */
	private static final BigInteger ACEPTA_CLASE3_G4_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(1L);
	
	public AceptaClase3G4Authority() {
		super(ACEPTA_CLASE3_G4_CA_ALIAS, ACEPTA_CLASE3_G4_CA_NAME, ACEPTA_CLASE3_G4_INITIAL_SERIAL_NUMBER);
	}

	public LegalProtection getLegalProtection() {
		return LegalProtection.FIRMA_SIMPLE;
	}

	public Collection<KeyPurpose> getKeyUsage() {
		return Arrays.asList(KeyPurpose.KEY_CERT_SIGN, KeyPurpose.CRL_SIGN, KeyPurpose.OCSP_SIGNING);
	}

    public String getSignatureAlgorithm() {
		return ACEPTA_CLASE3_G4_SIGNATURE_ALGORITHM;
	}
	
	public CertificateGenerator getCertificateGenerator() {
		return new AceptaClase3G4CertificateGenerator(this);
	}
	
	public OCSPGenerator getOCSPGenerator() {
		return new AceptaClase3G4OCSPGenerator(this);
	}

	public CRLGenerator getCRLGenerator() {
		return new AceptaClase3G4CRLGenerator(this);
	}
}
