package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora Clase 3 de Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaClase3G4CRLGenerator extends CRLGenerator {
	public AceptaClase3G4CRLGenerator(AceptaClase3G4Authority authority) {
		super(authority);
	}
}
