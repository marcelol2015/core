package com.acepta.ca.security.authority;

import java.security.GeneralSecurityException;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Proveedor y/o generador de una jerarquia de Autoridades Certificadoras.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AuthorityProvider {
	private AuthorityFactory rootAuthorityFactory;
	private Collection<AuthorityFactory> intermediateAuthorityFactories;
    private Collection<AuthorityFactory> finalAuthorityFactories;
	private int authorityDurationInYears;
	
	protected AuthorityProvider() {
		setIntermediateAuthorityFactories(new ArrayList<AuthorityFactory>());
        setFinalAuthorityFactories(new ArrayList<AuthorityFactory>());
	}

	protected int getAuthorityDurationInYears() {
		return authorityDurationInYears;
	}

	protected void setAuthorityDurationInYears(int authorityDurationInYears) {
		this.authorityDurationInYears = authorityDurationInYears;
	}

	protected AuthorityFactory getRootAuthorityFactory() {
		return rootAuthorityFactory;
	}

	protected void setRootAuthorityFactory(AuthorityFactory rootAuthorityFactory) {
		this.rootAuthorityFactory = rootAuthorityFactory;
	}

	protected Collection<AuthorityFactory> getIntermediateAuthorityFactories() {
		return intermediateAuthorityFactories;
	}

	protected void setIntermediateAuthorityFactories(Collection<AuthorityFactory> intermediateAuthorityFactories) {
		this.intermediateAuthorityFactories = intermediateAuthorityFactories;
	}

	protected void addIntermediateAuthorityFactory(AuthorityFactory authorityFactory) {
		getIntermediateAuthorityFactories().add(authorityFactory);
	}

    protected Collection<AuthorityFactory> getFinalAuthorityFactories() {
        return finalAuthorityFactories;
    }

    protected void setFinalAuthorityFactories(Collection<AuthorityFactory> finalAuthorityFactories) {
        this.finalAuthorityFactories = finalAuthorityFactories;
    }

    protected void addFinalAuthorityFactory(AuthorityFactory authorityFactory) {
        getFinalAuthorityFactories().add(authorityFactory);
    }

	protected void removeAuthorityFactory(AuthorityFactory authorityFactory) {
		getIntermediateAuthorityFactories().remove(authorityFactory);
	}
	
	public Collection<Authority> generateAuthorityHierarchy(Provider provider) throws GeneralSecurityException {
		Calendar calendar = Calendar.getInstance();
		Date notBefore = calendar.getTime();
		
		calendar.add(Calendar.YEAR, getAuthorityDurationInYears());
		Date notAfter = calendar.getTime();

		Collection<Authority> authorities = new ArrayList<Authority>();

		Authority rootAuthority = getRootAuthorityFactory().generateAuthority(provider, null, notBefore, notAfter);
	
		authorities.add(rootAuthority);
        Authority intermediateAuthorityFinal=null;
		
		for (AuthorityFactory authorityFactory : getIntermediateAuthorityFactories()) {
    		Authority intermediateAuthority = authorityFactory.generateAuthority(provider, rootAuthority, notBefore, notAfter);
    		intermediateAuthority.setIssuer(rootAuthority);
			authorities.add(intermediateAuthority);

            if(intermediateAuthority.getAlias().equalsIgnoreCase("SelloTiempo-G4")){
                intermediateAuthorityFinal = intermediateAuthority;
            }
		}

        for (AuthorityFactory authorityFactoryFinal : getFinalAuthorityFactories()) {
            Authority finalAuthority = authorityFactoryFinal.generateAuthority(provider, intermediateAuthorityFinal, notBefore, notAfter);
            finalAuthority.setIssuer(intermediateAuthorityFinal);
            authorities.add(finalAuthority);
        }

		return authorities;
	}
	
	public Authority generateIntermediateAuthority(Provider provider, String alias, Authority rootAuthority) throws GeneralSecurityException{
		Authority intermediateAuthority = null;		
		Calendar calendar = Calendar.getInstance();
		Date notBefore = calendar.getTime();
		
		calendar.add(Calendar.YEAR, getAuthorityDurationInYears());
		Date notAfter = rootAuthority.getCertificate().getNotAfter();
		

		
		for (AuthorityFactory authorityFactory : getIntermediateAuthorityFactories()) {
			Authority authority = authorityFactory.createAuthorityTemplate();
			if (authority.getAlias().equals(alias)){
				intermediateAuthority = authorityFactory.generateAuthority(provider, rootAuthority, notBefore, notAfter);
				intermediateAuthority.setIssuer(rootAuthority);				
			}
		}		
		return intermediateAuthority;
	}

    public Authority generateFinalAuthority(Provider provider, String alias, Authority intermediateAuthority) throws GeneralSecurityException{
        Authority finalAuthority = null;
        Calendar calendar = Calendar.getInstance();
        Date notBefore = calendar.getTime();

        calendar.add(Calendar.YEAR, getAuthorityDurationInYears());
        Date notAfter = intermediateAuthority.getCertificate().getNotAfter();

        for (AuthorityFactory authorityFactory : getFinalAuthorityFactories()) {
            Authority authority = authorityFactory.createAuthorityTemplate();
            if (authority.getAlias().equals(alias)){
                finalAuthority = authorityFactory.generateAuthority(provider, intermediateAuthority, notBefore, notAfter);
                finalAuthority.setIssuer(intermediateAuthority);
            }
        }
        return finalAuthority;
    }

	public Collection<Authority> loadAuthorityHierarchy(Provider provider) throws GeneralSecurityException {
		Collection<Authority> authorities = new ArrayList<Authority>();
		
		Authority rootAuthority = getRootAuthorityFactory().createAuthorityTemplate();

		rootAuthority.setProvider(provider);
		
		authorities.add(rootAuthority);

        Authority intermediateAuthorityFinal= null;
		
		for (AuthorityFactory authorityFactory : getIntermediateAuthorityFactories()) {
			Authority intermediateAuthority = authorityFactory.createAuthorityTemplate();
			intermediateAuthority.setProvider(provider);
			intermediateAuthority.setIssuer(rootAuthority);
			authorities.add(intermediateAuthority);

            if(intermediateAuthority.getAlias().equalsIgnoreCase("SelloTiempo-G4")){
                intermediateAuthorityFinal=intermediateAuthority;
            }
		}

        for (AuthorityFactory authorityFactoryFinal : getFinalAuthorityFactories()) {
            Authority finalAuthority = authorityFactoryFinal.createAuthorityTemplate();
            finalAuthority.setProvider(provider);
            finalAuthority.setIssuer(intermediateAuthorityFinal);
            authorities.add(finalAuthority);
        }
		
		return authorities;
	}
}
