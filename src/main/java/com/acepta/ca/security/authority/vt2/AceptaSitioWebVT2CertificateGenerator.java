package com.acepta.ca.security.authority.vt2;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.SitioWebCertificateRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora de Sitios Web.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaSitioWebVT2CertificateGenerator extends CertificateGenerator {
	/*
	 * Certificate Policies
	 */
	private static final String ACEPTA_SITIOWEB_VT2_POLICY_IDENTIFIER = "1.3.6.1.4.1.6891.1001.4";
	private static final String ACEPTA_SITIOWEB_VT2_POLICY_CPS_URI = "http://www.acepta.com/CPSVT2";
	private static final String ACEPTA_SITIOWEB_VT2_POLICY_USER_NOTICE_EXPLICIT_TEXT = "La utilizacion de este certificado esta sujeta a las politicas de certificado (CP) y practicas de certificacion (CPS) establecidas por Acepta.com, y disponibles publicamente en www.acepta.com.";
	private static final String ACEPTA_SITIOWEB_VT2_POLICY_USER_NOTICE_ORGANIZATION = "Acepta.com S.A.";
	private static final int ACEPTA_SITIOWEB_VT2_POLICY_USER_NOTICE_NUMBER = 4;

	/*
	 * Authority Information Access
	 */
	private static final String ACEPTA_SITIOWEB_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI = "http://ocsp.acepta.com/SitioWebVT2";

	/*
	 * CRL Distribution Points
	 */
	private static final String ACEPTA_SITIOWEB_VT2_CRL_DISTRIBUTION_POINT_URI = "http://crl.acepta.com/SitioWebVT2.crl";

	public AceptaSitioWebVT2CertificateGenerator(AceptaSitioWebVT2Authority authority) {
		super(authority);
	}

	public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {
		SitioWebCertificateRequest sitioweb = (SitioWebCertificateRequest) request;

		setSerialNumber(sitioweb.getSerialNumber());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setIssuerName(getAuthority().getName());
		setNotBefore(sitioweb.getNotBefore());
		setNotAfter(sitioweb.getNotAfter());
		setSubjectName(sitioweb.getSubjectName());
		setSubjectPublicKey(sitioweb.getSubjectPublicKey());

		setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier());
		setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(sitioweb.getSubjectPublicKey()));

		setKeyUsage(false, getKeyUsage());
		setExtendedKeyUsage(false, getExtendedKeyUsage());

		setNetscapeCertType(false, getNetscapeCertType());

		setNetscapeSSLServerName(false, getNetscapeSSLServerName(sitioweb.getSubjectCommonName()));

		setCertificatePolicies(false, getCertificatePolicies());

		setIssuerAlternativeName(false, getIssuerAlternativeName());

		setAuthorityInformationAccess(false, getAuthorityInformationAccess());
		setCRLDistributionPoints(false, getCRLDistributionPoints());

		return generate(getAuthority().getPrivateKey());
	}

	/*
	 * Certificate Generation Attributes
	 */
	private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier() throws InvalidKeyException {
		return CertificateGenerator.createAuthorityKeyIdentifier(getAuthority().getCertificate().getPublicKey());
	}

	private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey)
			throws CertificateParsingException, InvalidKeyException {
		return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
	}

	private KeyUsage getKeyUsage() {
		return CertificateGenerator.createKeyUsage(CertificateGenerator.KEY_USAGE_DIGITAL_SIGNATURE,
				CertificateGenerator.KEY_USAGE_KEY_ENCIPHERMENT);
	}

	private ExtendedKeyUsage getExtendedKeyUsage() {
		return CertificateGenerator.createExtendedKeyUsage(CertificateGenerator.EXTENDED_KEY_USAGE_SERVER_AUTH,
				CertificateGenerator.EXTENDED_KEY_USAGE_CLIENT_AUTH,
				CertificateGenerator.EXTENDED_KEY_USAGE_NETSCAPE_SERVER_GATED_CRYPTO,
				CertificateGenerator.EXTENDED_KEY_USAGE_MICROSOFT_SERVER_GATED_CRYPTO);
	}

	private PolicyInformation getCertificatePolicies() {
		return CertificateGenerator.createPolicyInformation(ACEPTA_SITIOWEB_VT2_POLICY_IDENTIFIER,
				getCPSPolicyQualifierInfo(), getUserNoticePolicyQualifierInfo());
	}

	private static PolicyQualifierInfo getCPSPolicyQualifierInfo() {
		return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_SITIOWEB_VT2_POLICY_CPS_URI);
	}

	private static PolicyQualifierInfo getUserNoticePolicyQualifierInfo() {
		return CertificateGenerator.createUserNoticePolicyQualifierInfo(
				ACEPTA_SITIOWEB_VT2_POLICY_USER_NOTICE_EXPLICIT_TEXT, ACEPTA_SITIOWEB_VT2_POLICY_USER_NOTICE_ORGANIZATION,
				ACEPTA_SITIOWEB_VT2_POLICY_USER_NOTICE_NUMBER);
	}

	private GeneralNames getIssuerAlternativeName() {
		Name issuerName = getAuthority().getName();

		return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
	}

	private AuthorityInformationAccess getAuthorityInformationAccess() {
		AccessDescription ocspAccessDescription = CertificateGenerator.createOCSPAccessDescription(ACEPTA_SITIOWEB_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI);
		
		return CertificateGenerator.createAuthorityInformationAccess(ocspAccessDescription);
	}

	private CRLDistPoint getCRLDistributionPoints() {
		GeneralName crlURI = CertificateGenerator.createURIName(ACEPTA_SITIOWEB_VT2_CRL_DISTRIBUTION_POINT_URI);
		
		return CertificateGenerator.createCRLDistributionPoints(CertificateGenerator.createDistributionPoint(crlURI));
	}

	private DERBitString getNetscapeCertType() {
		return CertificateGenerator.createNetscapeCertType(CertificateGenerator.NETSCAPE_CERT_TYPE_SSL_SERVER);
	}

	private DERIA5String getNetscapeSSLServerName(String subjectHostName) {
		return CertificateGenerator.createNetscapeSSLServerName(subjectHostName);
	}
}
