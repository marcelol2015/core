package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora Raiz.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaRaizG4AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_RAIZ_G4_CA_SERIAL_NUMBER = 1;

	public AceptaRaizG4AuthorityFactory() {
	}
	
	public Authority createAuthorityTemplate() {
		return new AceptaRaizG4Authority();
	}
	
	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		if (rootAuthority != null)
			throw new GeneralSecurityException();
		
		return generateAuthority(provider, null, BigInteger.valueOf(ACEPTA_RAIZ_G4_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
