package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora Clase 3 de Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaClase3G4AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_CLASE3_G4_CA_SERIAL_NUMBER = 2;
	
	public AceptaClase3G4AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new AceptaClase3G4Authority();
	}

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_CLASE3_G4_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
