package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Created by nhakor on 16-09-15.
 */
public class AceptaSelloTiempoG4CRLGenerator extends CRLGenerator{

    protected AceptaSelloTiempoG4CRLGenerator(AceptaSelloTiempoG4Authority authority) {
        super(authority);
    }
}
