package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora de Sistema.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-03
 */
public class AceptaSistemaVT2CRLGenerator extends CRLGenerator {
	public AceptaSistemaVT2CRLGenerator(AceptaSistemaVT2Authority authority) {
		super(authority);
	}
}
