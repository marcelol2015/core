package com.acepta.ca.security.authority.vt2;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.CodigoCertificateRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora de Codigo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-06
 */
public class AceptaCodigoVT2CertificateGenerator extends CertificateGenerator {
	/*
	 * Certificate Policies
	 */
	private static final String ACEPTA_CODIGO_VT2_POLICY_IDENTIFIER = "1.3.6.1.4.1.6891.1001.8";
	private static final String ACEPTA_CODIGO_VT2_POLICY_CPS_URI = "http://www.acepta.com/CPSVT2";
	// FIXME
	private static final String ACEPTA_CODIGO_VT2_POLICY_USER_NOTICE_EXPLICIT_TEXT = "";
	private static final String ACEPTA_CODIGO_VT2_POLICY_USER_NOTICE_ORGANIZATION = "Acepta.com S.A.";
	private static final int ACEPTA_CODIGO_VT2_POLICY_USER_NOTICE_NUMBER = 8;

	/*
	 * Authority Information Access
	 */
	private static final String ACEPTA_CODIGO_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI = "http://ocsp.acepta.com/CodigoVT2";

	/*
	 * CRL Distribution Points
	 */
	private static final String ACEPTA_CODIGO_VT2_CRL_DISTRIBUTION_POINT_URI = "http://crl.acepta.com/CodigoVT2.crl";

	public AceptaCodigoVT2CertificateGenerator(AceptaCodigoVT2Authority authority) {
		super(authority);
	}

	@Override
	public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {
		CodigoCertificateRequest codigo = (CodigoCertificateRequest) request;

		assert codigo != null;
		
		setSerialNumber(request.getSerialNumber());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setIssuerName(getAuthority().getName());
		setNotBefore(request.getNotBefore());
		setNotAfter(request.getNotAfter());
		setSubjectName(request.getSubjectName());
		setSubjectPublicKey(request.getSubjectPublicKey());

		setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier());
		setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(request.getSubjectPublicKey()));
		
		setExtendedKeyUsage(false, getExtendedKeyUsage());

		setNetscapeCertType(false, getNetscapeCertType());

		setCertificatePolicies(false, getCertificatePolicies());

		setIssuerAlternativeName(false, getIssuerAlternativeName());

		setAuthorityInformationAccess(false, getAuthorityInformationAccess());
		setCRLDistributionPoints(false, getCRLDistributionPoints());

		return generate(getAuthority().getPrivateKey());
	}

	/*
	 * Certificate Generation Attributes
	 */
	private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier() throws InvalidKeyException {
		return CertificateGenerator.createAuthorityKeyIdentifier(getAuthority().getCertificate().getPublicKey());
	}

	private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey)
			throws CertificateParsingException, InvalidKeyException {
		return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
	}
	
	private ExtendedKeyUsage getExtendedKeyUsage() {
		return CertificateGenerator.createExtendedKeyUsage(
				CertificateGenerator.EXTENDED_KEY_USAGE_CODE_SIGNING,
				CertificateGenerator.EXTENDED_KEY_USAGE_MICROSOFT_COMMERCIAL_CODE_SIGNING);
	}

	private PolicyInformation getCertificatePolicies() {
		return CertificateGenerator.createPolicyInformation(ACEPTA_CODIGO_VT2_POLICY_IDENTIFIER,
				getCPSPolicyQualifierInfo(), getUserNoticePolicyQualifierInfo());
	}

	private PolicyQualifierInfo getCPSPolicyQualifierInfo() {
		return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_CODIGO_VT2_POLICY_CPS_URI);
	}

	private PolicyQualifierInfo getUserNoticePolicyQualifierInfo() {
		return CertificateGenerator.createUserNoticePolicyQualifierInfo(
				ACEPTA_CODIGO_VT2_POLICY_USER_NOTICE_EXPLICIT_TEXT,
				ACEPTA_CODIGO_VT2_POLICY_USER_NOTICE_ORGANIZATION,
				ACEPTA_CODIGO_VT2_POLICY_USER_NOTICE_NUMBER);
	}

	private GeneralNames getIssuerAlternativeName() {
		Name issuerName = getAuthority().getName();
		
		return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
	}

	private AuthorityInformationAccess getAuthorityInformationAccess() {
		return CertificateGenerator.createAuthorityInformationAccess(CertificateGenerator.createOCSPAccessDescription(ACEPTA_CODIGO_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI));
	}

	private CRLDistPoint getCRLDistributionPoints() {
		GeneralName crlURI = CertificateGenerator.createURIName(ACEPTA_CODIGO_VT2_CRL_DISTRIBUTION_POINT_URI);
		
		return CertificateGenerator.createCRLDistributionPoints(CertificateGenerator.createDistributionPoint(crlURI));
	}	
	
	private DERBitString getNetscapeCertType() {
		return CertificateGenerator.createNetscapeCertType(CertificateGenerator.NETSCAPE_CERT_TYPE_OBJECT_SIGNING);
	}
}
