package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;

/**
 * Autoridad Certificadora Raiz.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaRaizG4Authority extends Authority {
	/*
	 * Alias
	 */
	private static final String ACEPTA_RAIZ_G4_CA_ALIAS = "Raiz-G4";
	
	/*
	 * Distinguished Name
	 */
	private static final String ACEPTA_RAIZ_G4_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Autoridad Certificadora Raiz - G4, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

	/*
	 * Signature Algorithm
	 */
	private static final String ACEPTA_RAIZ_G4_SIGNATURE_ALGORITHM = "SHA256WithRSA";

	/*
	 * Initial Serial Number (UNUSED)
	 */
	private static final BigInteger ACEPTA_RAIZ_G4_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(1L);
	
	public AceptaRaizG4Authority() {
		super(ACEPTA_RAIZ_G4_CA_ALIAS, ACEPTA_RAIZ_G4_CA_NAME, ACEPTA_RAIZ_G4_INITIAL_SERIAL_NUMBER);
	}

	public LegalProtection getLegalProtection() {
		return LegalProtection.FIRMA_AVANZADA;
	}

	public Collection<KeyPurpose> getKeyUsage() {
		return Arrays.asList(KeyPurpose.KEY_CERT_SIGN);
	}

    public String getSignatureAlgorithm() {
		return ACEPTA_RAIZ_G4_SIGNATURE_ALGORITHM;
	}	
	
	public CertificateGenerator getCertificateGenerator() {
		return new AceptaRaizG4CertificateGenerator(this);
	}

	public CRLGenerator getCRLGenerator() {
		return new AceptaRaizG4CRLGenerator(this);
	}
}
