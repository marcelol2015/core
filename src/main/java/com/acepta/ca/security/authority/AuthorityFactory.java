package com.acepta.ca.security.authority;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.security.cert.X509Certificate;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Date;

import com.acepta.ca.security.crypto.PrivateKeyGenerator;
import com.acepta.ca.security.crypto.PrivateKeyRequest;
import com.acepta.ca.security.util.PEM;

/**
 * Generador de Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public abstract class AuthorityFactory {
	public static final int RSA_KEY_SIZE = 2048;
	public static final BigInteger RSA_KEY_PUBLIC_EXPONENT = RSAKeyGenParameterSpec.F4;

	public AuthorityFactory() {
	}

	public abstract Authority createAuthorityTemplate() throws GeneralSecurityException;
	
	public abstract Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException;

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter) throws GeneralSecurityException	{
		return generateAuthority(provider, rootAuthority, notBefore, notAfter, null);
	}
	
	public Authority generateAuthority(Provider provider, Authority rootAuthority, BigInteger serialNumber, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		AuthorityCertificateRequest request = new AuthorityCertificateRequest();
		
		request.setSerialNumber(serialNumber);
		request.setNotBefore(notBefore);
		request.setNotAfter(notAfter);

		Authority authority = createAuthorityTemplate();
		
		println("Creando la autoridad: " + authority.getName().getCommonName());
		
		if (rootAuthority == null)
			rootAuthority = authority;

		if (keyPair == null)
			keyPair = generateKeyPair(provider);
		
		request.setSubjectName(authority.getName());
		request.setKeyUsage(authority.getKeyUsage());

		request.setSubjectPublicKey(keyPair.getPublic());
		
		authority.setProvider(provider);
		authority.setPrivateKey(keyPair.getPrivate());
		
		authority.setCertificate(rootAuthority.generateCertificate(request));
		
		printAuthority(authority);
		
		return authority;
	}

	private KeyPair generateKeyPair(Provider provider) throws GeneralSecurityException {
		PrivateKeyGenerator generator = new PrivateKeyGenerator(provider);
		
		PrivateKeyRequest request = new PrivateKeyRequest();

		request.setRSAKeyParameters(RSA_KEY_SIZE, RSA_KEY_PUBLIC_EXPONENT);

		KeyPair keyPair = generator.generatePrivateKey(request);
		
		return keyPair;
	}
	
	private static void printAuthority(Authority authority) {
		try {
			println("Alias: " + authority.getAlias());
			println("Proveedor: " + authority.getProvider().getName());
			
			X509Certificate certificate = authority.getCertificate();

			println("Certificado:");
			println("  Numero de serie: " + certificate.getSerialNumber());
			println("  Emisor: " + certificate.getIssuerDN().getName());
			println("  Valido desde: " + certificate.getNotBefore());
			println("  Valido hasta: " + certificate.getNotAfter());
			println("  Asunto: " + certificate.getSubjectDN().getName());
			
			println(PEM.encodeX509Certificate(certificate));
		}
		catch (IOException exception) {
			println("ERROR: Falla al desplegar el certificado: " + exception.getMessage());
		}
	}

	private static void println(String message) {
		System.out.println(message);
	}
}
