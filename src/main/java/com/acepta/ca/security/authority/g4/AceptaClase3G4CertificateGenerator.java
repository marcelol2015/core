package com.acepta.ca.security.authority.g4;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.PersonaCertificateRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.CLASE3_CPS_URI;
import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.CLASE3_OCSP_URI;
import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.CLASE3_CRL_URI;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora Clase 3 de Personas Naturales.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaClase3G4CertificateGenerator extends CertificateGenerator {
	/*
	 * Certificate Policies
	 */
	private static final String ACEPTA_CLASE3_G4_POLICY_IDENTIFIER = "1.3.6.1.4.1.6891.2";
	private static final String ACEPTA_CLASE3_G4_POLICY_CPS_URI = CLASE3_CPS_URI;
	private static final String ACEPTA_CLASE3_G4_POLICY_USER_NOTICE_EXPLICIT_TEXT = "El titular ha sido validado en forma presencial, quedando habilitado el Certificado para uso tributario, pagos, comercio y otros.";
	private static final String ACEPTA_CLASE3_G4_POLICY_USER_NOTICE_ORGANIZATION = "Acepta.com S.A.";
	private static final int ACEPTA_CLASE3_G4_POLICY_USER_NOTICE_NUMBER = 2;

	/*
	 * OCSP Authority Information Access
	 */
	private static final String ACEPTA_CLASE3_G4_AUTHORITY_INFO_ACCESS_OCSP_URI = CLASE3_OCSP_URI;

	/*
	 * CRL Distribution Points
	 */
	private static final String ACEPTA_CLASE3_G4_CRL_DISTRIBUTION_POINT_URI = CLASE3_CRL_URI;

	public AceptaClase3G4CertificateGenerator(AceptaClase3G4Authority authority) {
		super(authority);
	}

	public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {
		PersonaCertificateRequest persona = (PersonaCertificateRequest) request;
		
		setSerialNumber(persona.getSerialNumber());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setIssuerName(getAuthority().getName());
		setNotBefore(persona.getNotBefore());
		setNotAfter(persona.getNotAfter());
		setSubjectName(persona.getSubjectName());
		setSubjectPublicKey(persona.getSubjectPublicKey());

		setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier());
		setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(persona.getSubjectPublicKey()));

		setKeyUsage(false, getKeyUsage());
		setExtendedKeyUsage(false, getExtendedKeyUsage());

		setNetscapeCertType(false, getNetscapeCertType());

		setCertificatePolicies(false, getCertificatePolicies());

		setIssuerAlternativeName(false, getIssuerAlternativeName());
		setSubjectAlternativeName(false, getSubjectAlternativeName(persona.getSubjectRUT(), persona.getSubjectEmailAddress()));

		setAuthorityInformationAccess(false, getAuthorityInformationAccess());
		setCRLDistributionPoints(false, getCRLDistributionPoints());

		return generate(getAuthority().getPrivateKey());
	}

	/*
	 * Certificate Generation Attributes
	 */
	private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier() throws InvalidKeyException {
		return CertificateGenerator.createAuthorityKeyIdentifier(getAuthority().getCertificate().getPublicKey());
	}

	private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey) throws CertificateParsingException, InvalidKeyException {
		return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
	}
	
	private KeyUsage getKeyUsage() {
		return CertificateGenerator.createKeyUsage(
				CertificateGenerator.KEY_USAGE_DIGITAL_SIGNATURE,
				CertificateGenerator.KEY_USAGE_NON_REPUDIATION,
				CertificateGenerator.KEY_USAGE_KEY_ENCIPHERMENT,
				CertificateGenerator.KEY_USAGE_DATA_ENCIPHERMENT);
	}

	private ExtendedKeyUsage getExtendedKeyUsage() {
		return CertificateGenerator.createExtendedKeyUsage(
				CertificateGenerator.EXTENDED_KEY_USAGE_CLIENT_AUTH,
				CertificateGenerator.EXTENDED_KEY_USAGE_EMAIL_PROTECTION);
	}

	private PolicyInformation getCertificatePolicies() {
		return CertificateGenerator.createPolicyInformation(ACEPTA_CLASE3_G4_POLICY_IDENTIFIER,
				getCPSPolicyQualifierInfo(), getUserNoticePolicyQualifierInfo());
	}

	private PolicyQualifierInfo getCPSPolicyQualifierInfo() {
		return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_CLASE3_G4_POLICY_CPS_URI);
	}

	private PolicyQualifierInfo getUserNoticePolicyQualifierInfo() {
		return CertificateGenerator.createUserNoticePolicyQualifierInfo(
				ACEPTA_CLASE3_G4_POLICY_USER_NOTICE_EXPLICIT_TEXT,
				ACEPTA_CLASE3_G4_POLICY_USER_NOTICE_ORGANIZATION,
				ACEPTA_CLASE3_G4_POLICY_USER_NOTICE_NUMBER);
	}

	private GeneralNames getIssuerAlternativeName() {
		Name issuerName = getAuthority().getName();
		
		return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
	}

	private GeneralNames getSubjectAlternativeName(String subjectRUT, String subjectEmailAddress) {
		return CertificateGenerator.createSubjectAlternativeName(subjectRUT, subjectEmailAddress);
	}

	private AuthorityInformationAccess getAuthorityInformationAccess() {
		AccessDescription ocspAccessDescriptor = CertificateGenerator.createOCSPAccessDescription(ACEPTA_CLASE3_G4_AUTHORITY_INFO_ACCESS_OCSP_URI);
		
		return CertificateGenerator.createAuthorityInformationAccess(ocspAccessDescriptor);
	}

	private CRLDistPoint getCRLDistributionPoints() {
		GeneralName crlURI = CertificateGenerator.createURIName(ACEPTA_CLASE3_G4_CRL_DISTRIBUTION_POINT_URI);
		
		return CertificateGenerator.createCRLDistributionPoints(CertificateGenerator.createDistributionPoint(crlURI));
	}

	private DERBitString getNetscapeCertType() {
		return CertificateGenerator.createNetscapeCertType(CertificateGenerator.NETSCAPE_CERT_TYPE_SSL_CLIENT | CertificateGenerator.NETSCAPE_CERT_TYPE_SMIME);
	}
}
