package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Generador de respuesta OCSP de la Autoridad Certificadora de Codigos de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-11
 */
public class AceptaCAFVT2OCSPGenerator extends OCSPGenerator {
	public AceptaCAFVT2OCSPGenerator(AceptaCAFVT2Authority authority) {
		super(authority);
	}
}
