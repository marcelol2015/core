package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora Raiz.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaRaizVT2AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_RAIZ_VT2_CA_SERIAL_NUMBER = 1;

	public AceptaRaizVT2AuthorityFactory() {
	}
	
	public Authority createAuthorityTemplate() {
		return new AceptaRaizVT2Authority();
	}
	
	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		if (rootAuthority != null)
			throw new GeneralSecurityException();
		
		return generateAuthority(provider, null, BigInteger.valueOf(ACEPTA_RAIZ_VT2_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
