package com.acepta.ca.security.authority;

import java.math.BigInteger;
import java.security.PublicKey;
import java.util.Collection;
import java.util.Date;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

/**
 * Peticion de Certificados Electronicos X.509 para Autoridades Certificadoras intermedias.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AuthorityCertificateRequest extends CertificateRequest {
	private BigInteger serialNumber;
	private Date notBefore;
	private Date notAfter;
	private Name subjectName;
	private PublicKey subjectPublicKey;
	private Collection<Authority.KeyPurpose> keyUsage;
    private Collection<Authority.KeyPurpose> extendedKeyUsage;
	
	public AuthorityCertificateRequest() {
	}
	
	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public Name getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(Name subjectName) {
		this.subjectName = subjectName;
	}

	public PublicKey getSubjectPublicKey() {
		return subjectPublicKey;
	}

	public void setSubjectPublicKey(PublicKey subjectPublicKey) {
		this.subjectPublicKey = subjectPublicKey;
	}

	public Collection<Authority.KeyPurpose> getKeyUsage() {
		return keyUsage;
	}

	public void setKeyUsage(Collection<Authority.KeyPurpose> keyUsage) {
		this.keyUsage = keyUsage;
	}

    public Collection<Authority.KeyPurpose> getExtendedKeyUsage() {
        return extendedKeyUsage;
    }

    public void setExtendedKeyUsage(Collection<Authority.KeyPurpose> extendedKeyUsage) {
        this.extendedKeyUsage = extendedKeyUsage;
    }
}
