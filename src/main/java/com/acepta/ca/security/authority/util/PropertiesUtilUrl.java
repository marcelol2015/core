package com.acepta.ca.security.authority.util;

import com.acepta.ca.security.AceptaSecurityException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * Created by Angelo Estartus on 07-04-14.
 */
public class PropertiesUtilUrl {

    private static final String PROPERTIES_LOCATION = "/urlcertificategenerator.properties";

    private PropertiesUtilUrl() {
    }

    public static final String RAIZ_CPS_URI = getPropertiesAutoridad("acepta.raiz.g4.policy.cps.uri");

    public static final String CLASE2_CPS_URI = getPropertiesAutoridad("acepta.clase2.g4.policy.cps.uri");
    public static final String CLASE2_CRL_URI = getPropertiesAutoridad("acepta.clase2.g4.crl.distribution.point.uri");
    public static final String CLASE2_OCSP_URI = getPropertiesAutoridad("acepta.clase2.g4.authority.info.access.ocsp.uri");

    public static final String CLASE3_CPS_URI = getPropertiesAutoridad("acepta.clase3.g4.policy.cps.uri");
    public static final String CLASE3_CRL_URI = getPropertiesAutoridad("acepta.clase3.g4.crl.distribution.point.uri");
    public static final String CLASE3_OCSP_URI = getPropertiesAutoridad("acepta.clase3.g4.authority.info.access.ocsp.uri");

    public static final String FEA_CPS_URI = getPropertiesAutoridad("acepta.fa.g4.policy.cps.uri");
    public static final String FEA_CRL_URI = getPropertiesAutoridad("acepta.fa.g4.crl.distribution.point.uri");
    public static final String FEA_OCSP_URI = getPropertiesAutoridad("acepta.fa.g4.authority.info.access.ocsp.uri");

    public static final String SELLOTIEMPO_CPS_URI = getPropertiesAutoridad("acepta.tsa.g4.policy.cps.uri");
    public static final String SELLOTIEMPO_CRL_URI = getPropertiesAutoridad("acepta.tsa.g4.crl.distribution.point.uri");
    public static final String SELLOTIEMPO_OCSP_URI = getPropertiesAutoridad("acepta.tsa.g4.authority.info.access.ocsp.uri");

    public static final String CRL_EXPIRATION_HOUR = getPropertiesAutoridad("acepta.crl.duration");

    public static final String ACEPTA_TEMPLATES = getPropertiesAutoridad("acepta.email.templates");

    private static Properties loadResource() throws IOException {
        Properties prop = new Properties();

        final InputStream input = PropertiesUtilUrl.class.getResourceAsStream(PROPERTIES_LOCATION);
        prop.load(input);

        return prop;
    }

    private static String getPropertiesAutoridad(String property) {
        String propertyFile;
        try{
            propertyFile = loadResource().getProperty(property);
        }catch(IOException e){
            throw new AceptaSecurityException("No se pudo obtener la propiedad ["+property+"]: "+e.getMessage());
        }
        return propertyFile;
    }
}
