package com.acepta.ca.security.authority.g4;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.authority.AuthorityCertificateRequest;
import com.acepta.ca.security.authority.Authority.KeyPurpose;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.RAIZ_CPS_URI;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora Raiz.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaRaizG4CertificateGenerator extends CertificateGenerator {
	/*
	 * Certificate Policies
	 */
	private static final String ACEPTA_RAIZ_G4_POLICY_IDENTIFIER = "1.3.6.1.4.1.6891.1002.1";

	private static final String ACEPTA_RAIZ_G4_POLICY_CPS_URI = RAIZ_CPS_URI;

	private static final String ACEPTA_RAIZ_G4_POLICY_USER_NOTICE_EXPLICIT_TEXT = "La utilizacion de este certificado esta sujeta a las politicas de certificado (CP) y practicas de certificacion (CPS) establecidas por Acepta.com, y disponibles publicamente en www.acepta.com.";
	private static final String ACEPTA_RAIZ_G4_POLICY_USER_NOTICE_ORGANIZATION = "Acepta.com S.A.";
	private static final int ACEPTA_RAIZ_G4_POLICY_USER_NOTICE_NUMBER = 1;

	public AceptaRaizG4CertificateGenerator(AceptaRaizG4Authority authority) {
		super(authority);
	}

	public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {
		AuthorityCertificateRequest intermediate = (AuthorityCertificateRequest) request;

		setSerialNumber(intermediate.getSerialNumber());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setIssuerName(getAuthority().getName());
		setNotBefore(intermediate.getNotBefore());
		setNotAfter(intermediate.getNotAfter());
		setSubjectName(intermediate.getSubjectName());
		setSubjectPublicKey(intermediate.getSubjectPublicKey());

		BasicConstraints basicConstraints = getBasicConstraints(intermediate);
		if (basicConstraints != null)
			setBasicConstraints(true, basicConstraints);

		setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier(getAuthorityPublicKey(intermediate)));
		setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(intermediate.getSubjectPublicKey()));

		KeyUsage keyUsage = getKeyUsage(intermediate);
		if (keyUsage != null)
			setKeyUsage(false, keyUsage);

		ExtendedKeyUsage extendedKeyUsage = getExtendedKeyUsage(intermediate);
		if (extendedKeyUsage != null)
			setExtendedKeyUsage(true, extendedKeyUsage);

		setCertificatePolicies(false, getCertificatePolicies());

		setIssuerAlternativeName(false, getIssuerAlternativeName(getAuthority().getName()));
		setSubjectAlternativeName(false, getSubjectAlternativeName(intermediate.getSubjectName()));

		return generate(getAuthority().getPrivateKey());
	}

	private PublicKey getAuthorityPublicKey(AuthorityCertificateRequest request) {
		if (getAuthority().getCertificate() != null)
			return getAuthority().getCertificate().getPublicKey();
		else
			return request.getSubjectPublicKey();
	}
	
	private BasicConstraints getBasicConstraints(AuthorityCertificateRequest request) {
		if (request.getKeyUsage().contains(KeyPurpose.KEY_CERT_SIGN))
			return CertificateGenerator.createBasicContraints(true);

		return null;
	}

	private KeyUsage getKeyUsage(AuthorityCertificateRequest request) {
		int keyPurposes = 0;
		
		if (request.getKeyUsage().contains(KeyPurpose.KEY_CERT_SIGN))
			keyPurposes |= CertificateGenerator.KEY_USAGE_KEY_CERT_SIGN;
		
		if (request.getKeyUsage().contains(KeyPurpose.CRL_SIGN))
			keyPurposes |= CertificateGenerator.KEY_USAGE_CRL_SIGN;
		
		if (keyPurposes != 0)
			return CertificateGenerator.createKeyUsage(keyPurposes);
			
		return null;
	}

	private ExtendedKeyUsage getExtendedKeyUsage(AuthorityCertificateRequest request) {
		if (request.getKeyUsage().contains(KeyPurpose.OCSP_SIGNING))
			// Windows advierte que no puede comprobar los usos si se agrega OCSP SIGNING.
			// return CertificateGenerator.createExtendedKeyUsage(CertificateGenerator.EXTENDED_KEY_USAGE_OCSP_SIGNING);
			return null;
			
		if (request.getKeyUsage().contains(KeyPurpose.TIME_STAMPING))
			return CertificateGenerator.createExtendedKeyUsage(CertificateGenerator.EXTENDED_KEY_USAGE_TIME_STAMPING);
		
		return null;
	}

	private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier(PublicKey authorityPublicKey) throws InvalidKeyException {
		return CertificateGenerator.createAuthorityKeyIdentifier(authorityPublicKey);
	}

	private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey)
			throws CertificateParsingException, InvalidKeyException {
		return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
	}

	private GeneralNames getIssuerAlternativeName(Name issuerName) {
		return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
	}

	private GeneralNames getSubjectAlternativeName(Name subjectName) {
		return CertificateGenerator.createSubjectAlternativeName(subjectName.getRUT(), subjectName.getEmailAddress());
	}
	
	private PolicyInformation getCertificatePolicies() {
		return CertificateGenerator.createPolicyInformation(ACEPTA_RAIZ_G4_POLICY_IDENTIFIER,
				getCPSPolicyQualifierInfo(), getUserNoticeQualifierInfo());
	}

	private PolicyQualifierInfo getCPSPolicyQualifierInfo() {
		return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_RAIZ_G4_POLICY_CPS_URI);
	}

	private PolicyQualifierInfo getUserNoticeQualifierInfo() {
		return CertificateGenerator.createUserNoticePolicyQualifierInfo(ACEPTA_RAIZ_G4_POLICY_USER_NOTICE_EXPLICIT_TEXT,
				ACEPTA_RAIZ_G4_POLICY_USER_NOTICE_ORGANIZATION, ACEPTA_RAIZ_G4_POLICY_USER_NOTICE_NUMBER);
	}
}
