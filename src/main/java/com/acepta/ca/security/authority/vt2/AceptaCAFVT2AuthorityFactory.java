package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora de Codigos de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaCAFVT2AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_CAF_VT2_CA_SERIAL_NUMBER = 7;

	public AceptaCAFVT2AuthorityFactory() {
	}
	
	public Authority createAuthorityTemplate() {
		return new AceptaCAFVT2Authority();
	}
	
	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_CAF_VT2_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
