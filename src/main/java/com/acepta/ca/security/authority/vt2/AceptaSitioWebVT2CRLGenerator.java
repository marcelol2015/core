package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora de Sitios Web.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaSitioWebVT2CRLGenerator extends CRLGenerator {
	public AceptaSitioWebVT2CRLGenerator(AceptaSitioWebVT2Authority authority) {
		super(authority);
	}
}
