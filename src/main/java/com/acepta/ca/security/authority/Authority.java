package com.acepta.ca.security.authority;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;

import com.acepta.ca.security.AceptaSecurityException;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CRLRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.OCSPGenerator;
import com.acepta.ca.security.crypto.cert.OCSPRequest;
import com.acepta.ca.security.crypto.cert.OCSPResponse;
import com.acepta.ca.security.crypto.tsp.TimeStampGenerator;
import com.acepta.ca.security.crypto.tsp.X509TimeStampRequest;
import com.acepta.ca.security.crypto.tsp.X509TimeStampResponse;

/**
 * Autoridad Certificadora o de Sellado de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public abstract class Authority {
	public static enum KeyPurpose {
		KEY_CERT_SIGN,
		CRL_SIGN,
		OCSP_SIGNING,
		TIME_STAMPING
	}
	
	public static enum LegalProtection {
		FIRMA_SIMPLE,
		FIRMA_AVANZADA
	}
	
	private String alias;
	private Name name;
	private Provider provider;
	private PrivateKey privateKey;
	private X509Certificate certificate;
	private BigInteger initialSerialNumber;
	private Authority issuer;
	
	public Authority(String alias, String name, BigInteger initialSerialNumber) {
		setAlias(alias);
		setName(new Name(name));
		setInitialSerialNumber(initialSerialNumber);
	}

	public String getAlias() {
		return alias;
	}

	protected void setAlias(String alias) {
		this.alias = alias;
	}

	public Name getName() {
		return name;
	}

	protected void setName(Name name) {
		this.name = name;
	}

	public Provider getProvider() {
		return provider;
	}

	protected void setProvider(Provider provider) {
		this.provider = provider;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public X509Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(X509Certificate certificate) {
		this.certificate = certificate;
	}
	
	public BigInteger getInitialSerialNumber() {
		return initialSerialNumber;
	}

	public void setInitialSerialNumber(BigInteger initialSerialNumber) {
		this.initialSerialNumber = initialSerialNumber;
	}

	public Authority getIssuer() {
		return issuer;
	}

	public void setIssuer(Authority issuer) {
		this.issuer = issuer;
	}

	public abstract Collection<KeyPurpose> getKeyUsage();

	public abstract LegalProtection getLegalProtection();

	public abstract String getSignatureAlgorithm();
	
	public X509Certificate generateCertificate(CertificateRequest request) throws AceptaSecurityException {
		try {
			return getCertificateGenerator().generateCertificate(request);
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al generar Certificado X.509", exception);
		}
	}

	public X509CRL generateCRL(CRLRequest request) throws AceptaSecurityException {
		try {
			return getCRLGenerator().generateCRL(request);
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al generar CRL X.509", exception);
		}
	}
	
	public OCSPResponse generateOCSP(OCSPRequest request) throws AceptaSecurityException {
		try {
			return getOCSPGenerator().generateOCSP(request);
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al generar respuesta OCSP X.509", exception);
		}
	}
	
	public X509TimeStampResponse generateTimeStamp(X509TimeStampRequest request) throws AceptaSecurityException {
		try {
			return getTimeStampGenerator().generateTimeStamp(request);
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException("Falla al generar Marca de Tiempo X.509", exception);
		}
	}
	
	protected CertificateGenerator getCertificateGenerator() {
		throw new AceptaSecurityException("Autoridad no emite Certificados X.509");
	}

	protected CRLGenerator getCRLGenerator() {
		throw new AceptaSecurityException("Autoridad no emite Listas de Revocacion X.509");
	}
	
	protected OCSPGenerator getOCSPGenerator() {
		throw new AceptaSecurityException("Autoridad no emite respuestas OCSP");
	}
	
	protected TimeStampGenerator getTimeStampGenerator() {
		throw new AceptaSecurityException("Autoridad no emite Marcas de Tiempo X.509");
	}
}
