package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora de Firma Electronica Avanzada.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaFirmaAvanzadaG4CRLGenerator extends CRLGenerator {
	public AceptaFirmaAvanzadaG4CRLGenerator(AceptaFirmaAvanzadaG4Authority authority) {
		super(authority);
	}
}
