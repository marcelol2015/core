package com.acepta.ca.security.authority.g4;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashSet;

import org.bouncycastle.asn1.cmp.PKIStatus;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampTokenInfo;

import com.acepta.ca.security.crypto.tsp.JavaTimeSource;
import com.acepta.ca.security.crypto.tsp.TimeSource;
import com.acepta.ca.security.crypto.tsp.TimeStampGenerator;
import com.acepta.ca.security.crypto.tsp.X509TimeStampRequest;
import com.acepta.ca.security.crypto.tsp.X509TimeStampResponse;

/**
 * Generador de Sellos de Tiempo X.509 de la Autoridad de Sellado de Tiempo de Acepta.com S.A.
 * 
 * @author Carlos Hasan, Angelo Estartus
 * @version 2.0, 2015-09-21
 */
public class EmisorSelloTiempoG4Generator extends TimeStampGenerator{
	/*
	 * SHA-256, SHA-1 and MD5 Digest Algorithms
	 */
    private static final String ALGORITHM_IDENTIFIER_SHA256 = "2.16.840.1.101.3.4.2.1";
    private static final String ALGORITHM_IDENTIFIER_SHA1 = "1.3.14.3.2.26";
	private static final String ALGORITHM_IDENTIFIER_MD5 = "1.2.840.113549.2.5";

	/*
	 * TSA Policy Identifier
	 */
	private static final String ACEPTA_SELLO_TIEMPO_G4_POLICY_IDENTIFIER = "1.3.6.1.4.1.6891.200";

	/*
	 * TSA Digest Algorithm
	 */
	private static final String ACEPTA_SELLO_TIEMPO_G4_DIGEST_ALGORITHM = ALGORITHM_IDENTIFIER_SHA256;
	
	/*
	 * TSA Accepted Digest Algorithms and Policies
	 */
	private static final String[] ACEPTA_SELLO_TIEMPO_G4_ACCEPTED_DIGEST_ALGORITHMS = { ALGORITHM_IDENTIFIER_SHA256,ALGORITHM_IDENTIFIER_SHA1, ALGORITHM_IDENTIFIER_MD5 };
	private static final String[] ACEPTA_SELLO_TIEMPO_G4_ACCEPTED_POLICIES = { ACEPTA_SELLO_TIEMPO_G4_POLICY_IDENTIFIER };
	
	public EmisorSelloTiempoG4Generator(EmisorSelloTiempoG4 authority) {
		setAuthority(authority);
		setTimeSource(createTimeSource());
	}

	public X509TimeStampResponse generateTimeStamp(X509TimeStampRequest request) throws GeneralSecurityException {
		setPolicy(ACEPTA_SELLO_TIEMPO_G4_POLICY_IDENTIFIER);
		setDigestAlgorithm(ACEPTA_SELLO_TIEMPO_G4_DIGEST_ALGORITHM);

		setAcceptedDigestAlgorithms(createAcceptedDigestAlgorithms());
		setAcceptedPolicies(createAcceptedPolicies());
		
		setSerialNumber(request.getSerialNumber());
		setRequestEncoded(request.getEncoded());

		try {
			TimeStampResponse response = generateTimeStampResponse();
			
			return createTimeStampResponse(response);
		}
		catch (Exception exception) {
			throw new GeneralSecurityException(exception);
		}
	}

	private static X509TimeStampResponse createTimeStampResponse(TimeStampResponse response) throws IOException {
		if (response.getStatus() == PKIStatus.GRANTED || response.getStatus() == PKIStatus.GRANTED_WITH_MODS)
			return createGrantedTimeStampResponse(response);
		else
			return createRejectedTimeStampResponse(response);
	}

	private static X509TimeStampResponse createGrantedTimeStampResponse(TimeStampResponse response) throws IOException {
		TimeStampTokenInfo timeStampInfo = response.getTimeStampToken().getTimeStampInfo();

		return new X509TimeStampResponse(true, response.getEncoded(), timeStampInfo.getSerialNumber(), timeStampInfo.getGenTime(), timeStampInfo.getGenTimeAccuracy());
	}

	private static X509TimeStampResponse createRejectedTimeStampResponse(TimeStampResponse response) throws IOException {
		return new X509TimeStampResponse(false, response.getEncoded(), null, null,null);
	}

	private static TimeSource createTimeSource() {
		return new JavaTimeSource();
	}

	private static HashSet<String> createAcceptedDigestAlgorithms() {
		return new HashSet<String>(Arrays.asList(ACEPTA_SELLO_TIEMPO_G4_ACCEPTED_DIGEST_ALGORITHMS));
	}
	
	private static HashSet<String> createAcceptedPolicies() {
		return new HashSet<String>(Arrays.asList(ACEPTA_SELLO_TIEMPO_G4_ACCEPTED_POLICIES));
	}


}
