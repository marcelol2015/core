package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora Clase 3 Biometrica de Personas Naturales.
 * 
 * @author Carlos Hasan, Alvaro Millalen
 * @version 1.0, 2012-01-11
 */
public class AceptaClase2G4CRLGenerator extends CRLGenerator {
	public AceptaClase2G4CRLGenerator(AceptaClase2G4Authority authority) {
		super(authority);
	}
}
