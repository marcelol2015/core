package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad de Sellado de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-27
 */
public class AceptaSelloTiempoG4AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_SELLO_TIEMPO_G4_CA_SERIAL_NUMBER = 200;
	
	public AceptaSelloTiempoG4AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new AceptaSelloTiempoG4Authority();
	}

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_SELLO_TIEMPO_G4_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
