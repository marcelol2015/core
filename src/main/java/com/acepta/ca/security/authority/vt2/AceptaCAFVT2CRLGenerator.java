package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.CRLGenerator;

/**
 * Generador de Listas de Revocacion (CRL) de la Autoridad Certificadora de Codigos de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaCAFVT2CRLGenerator extends CRLGenerator {
	public AceptaCAFVT2CRLGenerator(AceptaCAFVT2Authority authority) {
		super(authority);
	}
}
