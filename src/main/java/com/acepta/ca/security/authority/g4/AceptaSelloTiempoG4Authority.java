package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Autoridad de Sellado de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaSelloTiempoG4Authority extends Authority {
	/*
	 * Alias
	 */
	private static final String ACEPTA_SELLO_TIEMPO_G4_CA_ALIAS = "SelloTiempo-G4";

	/*
	 * Distinguished Name
	 */
	private static final String ACEPTA_SELLO_TIEMPO_G4_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Autoridad de Sellado de Tiempo - G4, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

	/*
	 * Signature Algorithm
	 */
	private static final String ACEPTA_SELLO_TIEMPO_G4_SIGNATURE_ALGORITHM = "SHA256WithRSA";
	
	/*
	 * Initial Serial Number
	 */
	private static final BigInteger ACEPTA_SELLO_TIEMPO_G4_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(1L);

	public AceptaSelloTiempoG4Authority() {
		super(ACEPTA_SELLO_TIEMPO_G4_CA_ALIAS, ACEPTA_SELLO_TIEMPO_G4_CA_NAME, ACEPTA_SELLO_TIEMPO_G4_INITIAL_SERIAL_NUMBER);
	}
	
	public LegalProtection getLegalProtection() {
		return LegalProtection.FIRMA_AVANZADA;
	}

	public Collection<KeyPurpose> getKeyUsage() {
		return Arrays.asList(KeyPurpose.KEY_CERT_SIGN, KeyPurpose.CRL_SIGN, KeyPurpose.OCSP_SIGNING);
	}

	public String getSignatureAlgorithm() {
		return ACEPTA_SELLO_TIEMPO_G4_SIGNATURE_ALGORITHM;
	}
	
    public CertificateGenerator getCertificateGenerator() {
        return new AceptaSelloTiempoG4CertificateGenerator(this);
    }

    public OCSPGenerator getOCSPGenerator() {
        return new AceptaSellotiempoG4OCSPGenerator(this);
    }

    public CRLGenerator getCRLGenerator() {
        return new AceptaSelloTiempoG4CRLGenerator(this);
    }
}
