package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.tsp.TimeStampGenerator;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by nhakor on 07-10-15.
 */
public class EmisorSelloTiempoG4 extends Authority {

    /*
	 * Alias
	 */
    private static final String ACEPTA_SELLO_TIEMPO_VT1_CA_ALIAS = "EmisorSelloTiempo-G4";

    /*
     * Distinguished Name
     */
    private static final String ACEPTA_SELLO_TIEMPO_VT1_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Emisor Sello de Tiempo - G4, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

    /*
     * Signature Algorithm
     */
    private static final String ACEPTA_SELLO_TIEMPO_VT1_SIGNATURE_ALGORITHM = "SHA256WithRSA";

    /*
     * Initial Serial Number
     */
    private static final BigInteger ACEPTA_SELLO_TIEMPO_VT1_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(1L);

    public EmisorSelloTiempoG4() {
        super(ACEPTA_SELLO_TIEMPO_VT1_CA_ALIAS, ACEPTA_SELLO_TIEMPO_VT1_CA_NAME, ACEPTA_SELLO_TIEMPO_VT1_INITIAL_SERIAL_NUMBER);
    }


    public LegalProtection getLegalProtection() {
        return LegalProtection.FIRMA_SIMPLE;
    }

    public Collection<KeyPurpose> getKeyUsage() {
        return Arrays.asList(KeyPurpose.TIME_STAMPING);
    }

    public String getSignatureAlgorithm() {
        return ACEPTA_SELLO_TIEMPO_VT1_SIGNATURE_ALGORITHM;
    }

    public TimeStampGenerator getTimeStampGenerator() {
        return new EmisorSelloTiempoG4Generator(this);
    }
}
