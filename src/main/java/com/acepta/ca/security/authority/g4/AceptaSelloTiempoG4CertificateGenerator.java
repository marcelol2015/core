package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityCertificateRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.SELLOTIEMPO_CPS_URI;
import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.SELLOTIEMPO_OCSP_URI;
import static com.acepta.ca.security.authority.util.PropertiesUtilUrl.SELLOTIEMPO_CRL_URI;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora de Sello de Tiempo.
 *
 * @author Angelo Estartus
 * @version 1.0, 2015-10-05
 */
public class AceptaSelloTiempoG4CertificateGenerator extends CertificateGenerator {

    /*
	 * Certificate Policies
	 */
    private static final String ACEPTA_SELLOTIEMPO_G4_POLICY_IDENTIFIER= "1.3.6.1.4.1.6891.200";
    private static final String ACEPTA_SELLOTIEMPO_G4_POLICY_CPS_URI = SELLOTIEMPO_CPS_URI;
    private static final String ACEPTA_SELLOTIEMPO_G4_POLICY_USER_NOTICE_EXPLICIT_TEXT = "Certificado emisor de sello de tiempo. Resolucion exenta 3556 del 09 de noviembre de 2016, Subsecretaria de Economia, Fomento y Reconstruccion.";
    private static final String ACEPTA_SELLOTIEMPO_G4_POLICY_USER_NOTICE_ORGANIZATION = "Acepta.com S.A.";
    private static final int ACEPTA_SELLOTIEMPO_G4_POLICY_USER_NOTICE_NUMBER = 3;

    /*
     * OCSP Authority Information Access
     */
    private static final String ACEPTA_SELLOTIEMPO_G4_AUTHORITY_INFO_ACCESS_OCSP_URI = SELLOTIEMPO_OCSP_URI;
    /*
     * CRL Distribution Points
     */
    private static final String ACEPTA_SELLOTIEMPO_G4_CRL_DISTRIBUTION_POINT_URI = SELLOTIEMPO_CRL_URI;

    protected AceptaSelloTiempoG4CertificateGenerator(AceptaSelloTiempoG4Authority authority) {
        super(authority);
    }

    @Override
    public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {

        AuthorityCertificateRequest intermediate = (AuthorityCertificateRequest) request;

        setSerialNumber(intermediate.getSerialNumber());
        setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
        setIssuerName(getAuthority().getName());
        setNotBefore(intermediate.getNotBefore());
        setNotAfter(intermediate.getNotAfter());
        setSubjectName(intermediate.getSubjectName());
        setSubjectPublicKey(intermediate.getSubjectPublicKey());

        BasicConstraints basicConstraints = getBasicConstraints(intermediate);
        if (basicConstraints != null)
            setBasicConstraints(true, basicConstraints);

        setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier(getAuthorityPublicKey(intermediate)));
        setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(intermediate.getSubjectPublicKey()));

        org.bouncycastle.asn1.x509.KeyUsage keyUsage = getKeyUsage(intermediate);
        if (keyUsage != null)
            setKeyUsage(false, keyUsage);

        ExtendedKeyUsage extendedKeyUsage = getExtendedKeyUsage(intermediate);
        if (extendedKeyUsage != null)
            setExtendedKeyUsage(true, extendedKeyUsage);

        setCertificatePolicies(false, getCertificatePolicies());

        setIssuerAlternativeName(false, getIssuerAlternativeName(getAuthority().getName()));
        setSubjectAlternativeName(false, getSubjectAlternativeName(intermediate.getSubjectName()));

        setAuthorityInformationAccess(false, getAuthorityInformationAccess());
        setCRLDistributionPoints(false, getCRLDistributionPoints());

        return generate(getAuthority().getPrivateKey());

    }

    private PublicKey getAuthorityPublicKey(AuthorityCertificateRequest request) {
        if (getAuthority().getCertificate() != null)
            return getAuthority().getCertificate().getPublicKey();
        else
            return request.getSubjectPublicKey();
    }

    private BasicConstraints getBasicConstraints(AuthorityCertificateRequest request) {
        if (request.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN))
            return CertificateGenerator.createBasicContraints(true);

        return null;
    }

    private org.bouncycastle.asn1.x509.KeyUsage getKeyUsage(AuthorityCertificateRequest request) {
        int keyPurposes = 0;

        if (request.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN))
            keyPurposes |= CertificateGenerator.KEY_USAGE_KEY_CERT_SIGN;

        if (request.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN))
            keyPurposes |= CertificateGenerator.KEY_USAGE_CRL_SIGN;

        if (keyPurposes != 0)
            return CertificateGenerator.createKeyUsage(keyPurposes);

        return null;
    }

    private ExtendedKeyUsage getExtendedKeyUsage(AuthorityCertificateRequest request) {
        if (request.getKeyUsage().contains(Authority.KeyPurpose.OCSP_SIGNING))
            // Windows advierte que no puede comprobar los usos si se agrega OCSP SIGNING.
            // return CertificateGenerator.createExtendedKeyUsage(CertificateGenerator.EXTENDED_KEY_USAGE_OCSP_SIGNING);
            return null;

        if (request.getKeyUsage().contains(Authority.KeyPurpose.TIME_STAMPING))
            return CertificateGenerator.createExtendedKeyUsage(CertificateGenerator.EXTENDED_KEY_USAGE_TIME_STAMPING);

        return null;
    }

    private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier(PublicKey authorityPublicKey) throws InvalidKeyException {
        return CertificateGenerator.createAuthorityKeyIdentifier(authorityPublicKey);
    }

    private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey)
            throws CertificateParsingException, InvalidKeyException {
        return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
    }

    private GeneralNames getIssuerAlternativeName(Name issuerName) {
        return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
    }

    private GeneralNames getSubjectAlternativeName(Name subjectName) {
        return CertificateGenerator.createSubjectAlternativeName(subjectName.getRUT(), subjectName.getEmailAddress());
    }

    private PolicyInformation getCertificatePolicies() {
        return CertificateGenerator.createPolicyInformation(ACEPTA_SELLOTIEMPO_G4_POLICY_IDENTIFIER,
                getCPSPolicyQualifierInfo(),getUserNoticePolicyQualifierInfo());
    }

    private PolicyQualifierInfo getUserNoticePolicyQualifierInfo() {
        return CertificateGenerator.createUserNoticePolicyQualifierInfo(
                ACEPTA_SELLOTIEMPO_G4_POLICY_USER_NOTICE_EXPLICIT_TEXT,
                ACEPTA_SELLOTIEMPO_G4_POLICY_USER_NOTICE_ORGANIZATION, ACEPTA_SELLOTIEMPO_G4_POLICY_USER_NOTICE_NUMBER);
    }

    private PolicyQualifierInfo getCPSPolicyQualifierInfo() {
        return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_SELLOTIEMPO_G4_POLICY_CPS_URI);
    }

    private AuthorityInformationAccess getAuthorityInformationAccess() {
        AccessDescription ocspAccessDescriptor = CertificateGenerator.createOCSPAccessDescription(ACEPTA_SELLOTIEMPO_G4_AUTHORITY_INFO_ACCESS_OCSP_URI);

        return CertificateGenerator.createAuthorityInformationAccess(ocspAccessDescriptor);
    }

    private CRLDistPoint getCRLDistributionPoints() {
        GeneralName crlURI = CertificateGenerator.createURIName(ACEPTA_SELLOTIEMPO_G4_CRL_DISTRIBUTION_POINT_URI);

        return CertificateGenerator.createCRLDistributionPoints(CertificateGenerator.createDistributionPoint(crlURI));
    }
}
