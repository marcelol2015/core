package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora de Sitios Web.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaSitioWebVT2AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_SITIOWEB_VT2_CA_SERIAL_NUMBER = 4;

	public AceptaSitioWebVT2AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new AceptaSitioWebVT2Authority();
	}

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_SITIOWEB_VT2_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
