package com.acepta.ca.security.authority.vt2;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Generador de respuesta OCSP de la Autoridad Certificadora de Codigo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-06
 */
public class AceptaCodigoVT2OCSPGenerator extends OCSPGenerator {
	public AceptaCodigoVT2OCSPGenerator(AceptaCodigoVT2Authority authority) {
		super(authority);
	}
}
