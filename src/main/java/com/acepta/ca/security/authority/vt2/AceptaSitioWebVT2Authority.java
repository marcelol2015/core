package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Autoridad Certificadora de Sitios Web.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaSitioWebVT2Authority extends Authority {
	/*
	 * Alias
	 */
	private static final String ACEPTA_SITIOWEB_VT2_CA_ALIAS = "SitioWebVT2";
	
	/*
	 * Distinguished Name
	 */
	private static final String ACEPTA_SITIOWEB_VT2_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Autoridad Certificadora de Sitio Web - VT2, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

	/*
	 * Signature Algorithm
	 */
	private static final String ACEPTA_SITIOWEB_VT2_SIGNATURE_ALGORITHM = "SHA256WithRSA";

	/*
	 * Initial Serial Number
	 */
	private static final BigInteger ACEPTA_SITIOWEB_VT2_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(1L);

	public AceptaSitioWebVT2Authority() {
		super(ACEPTA_SITIOWEB_VT2_CA_ALIAS, ACEPTA_SITIOWEB_VT2_CA_NAME, ACEPTA_SITIOWEB_VT2_INITIAL_SERIAL_NUMBER);
	}

	public LegalProtection getLegalProtection() {
		return LegalProtection.FIRMA_SIMPLE;
	}

	public Collection<KeyPurpose> getKeyUsage() {
		return Arrays.asList(KeyPurpose.KEY_CERT_SIGN, KeyPurpose.CRL_SIGN, KeyPurpose.OCSP_SIGNING);
	}

    public String getSignatureAlgorithm() {
		return ACEPTA_SITIOWEB_VT2_SIGNATURE_ALGORITHM;
	}
	
	public CertificateGenerator getCertificateGenerator() {
		return new AceptaSitioWebVT2CertificateGenerator(this);
	}

	public OCSPGenerator getOCSPGenerator() {
		return new AceptaSitioWebVT2OCSPGenerator(this);
	}
	
	public CRLGenerator getCRLGenerator() {
		return new AceptaSitioWebVT2CRLGenerator(this);
	}
}
