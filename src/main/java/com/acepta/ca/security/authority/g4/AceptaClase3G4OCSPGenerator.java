package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

public class AceptaClase3G4OCSPGenerator extends OCSPGenerator {
	public AceptaClase3G4OCSPGenerator(AceptaClase3G4Authority authority) {
		super(authority);
	}
}
