package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.crypto.cert.OCSPGenerator;

public class AceptaClase2G4OCSPGenerator extends OCSPGenerator {
	public AceptaClase2G4OCSPGenerator(AceptaClase2G4Authority authority) {
		super(authority);
	}
}
