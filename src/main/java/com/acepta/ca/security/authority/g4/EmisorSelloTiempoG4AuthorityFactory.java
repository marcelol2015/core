package com.acepta.ca.security.authority.g4;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

/**
 * Generador de la Autoridad de Sellado de Tiempo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-27
 */
public class EmisorSelloTiempoG4AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_EMISOR_SELLO_TIEMPO_G4_CA_SERIAL_NUMBER = 201;

	public EmisorSelloTiempoG4AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new EmisorSelloTiempoG4();
	}

	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_EMISOR_SELLO_TIEMPO_G4_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
