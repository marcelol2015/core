package com.acepta.ca.security.authority.g4;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Provider;
import java.util.Date;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;

/**
 * Generador de la Autoridad Certificadora de Firma Electronica Avanzada.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaFirmaAvanzadaG4AuthorityFactory extends AuthorityFactory {
	/*
	 * Serial Number
	 */
	private static final long ACEPTA_FA_G4_CA_SERIAL_NUMBER = 3;

	public AceptaFirmaAvanzadaG4AuthorityFactory() {
	}

	public Authority createAuthorityTemplate() {
		return new AceptaFirmaAvanzadaG4Authority();
	}
	
	public Authority generateAuthority(Provider provider, Authority rootAuthority, Date notBefore, Date notAfter, KeyPair keyPair) throws GeneralSecurityException {
		return generateAuthority(provider, rootAuthority, BigInteger.valueOf(ACEPTA_FA_G4_CA_SERIAL_NUMBER), notBefore, notAfter, keyPair);
	}
}
