package com.acepta.ca.security.authority.vt2;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;

import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLGenerator;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.OCSPGenerator;

/**
 * Autoridad Certificadora de Codigos de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaCAFVT2Authority extends Authority {
	/*
	 * Alias
	 */
	private static final String ACEPTA_CAF_VT2_CA_ALIAS = "CAFVT2";
	
	/*
	 * Distinguished Name
	 */
	private static final String ACEPTA_CAF_VT2_CA_NAME = "C=CL, O=Acepta.com S.A., CN=Acepta.com Autoridad Certificadora de Codigos de Asignacion de Folios - VT2, EmailAddress=info@acepta.com, SerialNumber=96919050-8";

	/*
	 * Signature Algorithm
	 */
	private static final String ACEPTA_CAF_VT2_SIGNATURE_ALGORITHM = "SHA256WithRSA";
	
	/*
	 * Initial Serial Number
	 */
	private static final BigInteger ACEPTA_CAF_VT2_INITIAL_SERIAL_NUMBER = BigInteger.valueOf(1L);
	
	public AceptaCAFVT2Authority() {
		super(ACEPTA_CAF_VT2_CA_ALIAS, ACEPTA_CAF_VT2_CA_NAME, ACEPTA_CAF_VT2_INITIAL_SERIAL_NUMBER);
	}

	public LegalProtection getLegalProtection() {
		return LegalProtection.FIRMA_SIMPLE;
	}

	public Collection<KeyPurpose> getKeyUsage() {
		return Arrays.asList(KeyPurpose.KEY_CERT_SIGN, KeyPurpose.CRL_SIGN, KeyPurpose.OCSP_SIGNING);
	}

    public String getSignatureAlgorithm() {
		return ACEPTA_CAF_VT2_SIGNATURE_ALGORITHM;
	}

	public CertificateGenerator getCertificateGenerator() {
		return new AceptaCAFVT2CertificateGenerator(this);
	}

	public OCSPGenerator getOCSPGenerator() {
		return new AceptaCAFVT2OCSPGenerator(this);
	}
	
	public CRLGenerator getCRLGenerator() {
		return new AceptaCAFVT2CRLGenerator(this);
	}
}
