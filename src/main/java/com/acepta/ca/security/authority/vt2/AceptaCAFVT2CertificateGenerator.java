package com.acepta.ca.security.authority.vt2;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import com.acepta.ca.security.CAFCertificateRequest;
import com.acepta.ca.security.crypto.cert.CertificateGenerator;
import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;

/**
 * Generador de Certificados X.509 de la Autoridad Certificadora de Codigos de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AceptaCAFVT2CertificateGenerator extends CertificateGenerator {
	/*
	 * Certificate Policies
	 */
	private static final String ACEPTA_CAF_VT2_POLICY_IDENTIFIER = "1.3.6.1.4.1.6891.1001.14";
	private static final String ACEPTA_CAF_VT2_POLICY_CPS_URI = "http://www.acepta.com/CPSVT2";
	private static final String ACEPTA_CAF_VT2_POLICY_USER_NOTICE_EXPLICIT_TEXT = "Para la emision de este tipo de certificados, Acepta.com solo cambia el formato de las claves entregadas por el SII. No realiza validaciones adicionales.";
	private static final String ACEPTA_CAF_VT2_POLICY_USER_NOTICE_ORGANIZATION = "Acepta.com S.A.";
	private static final int ACEPTA_CAF_VT2_POLICY_USER_NOTICE_NUMBER = 14;

	/*
	 * Authority Information Access
	 */
	private static final String ACEPTA_CAF_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI = "http://ocsp.acepta.com/CAFVT2";

	/*
	 * CRL Distribution Points
	 */
	private static final String ACEPTA_CAF_VT2_CRL_DISTRIBUTION_POINT_URI = "http://crl.acepta.com/CAFVT2.crl";

	public AceptaCAFVT2CertificateGenerator(AceptaCAFVT2Authority authority) {
		super(authority);
	}

	public X509Certificate generateCertificate(CertificateRequest request) throws GeneralSecurityException {
		CAFCertificateRequest caf = (CAFCertificateRequest) request;

		setSerialNumber(request.getSerialNumber());
		setSignatureAlgorithm(getAuthority().getSignatureAlgorithm());
		setIssuerName(getAuthority().getName());
		setNotBefore(request.getNotBefore());
		setNotAfter(request.getNotAfter());
		setSubjectName(request.getSubjectName());
		setSubjectPublicKey(request.getSubjectPublicKey());

		setAuthorityKeyIdentifier(false, getAuthorityKeyIdentifier());
		setSubjectKeyIdentifier(false, getSubjectKeyIdentifier(request.getSubjectPublicKey()));

		setKeyUsage(false, getKeyUsage());

		setCertificatePolicies(false, getCertificatePolicies());

		setIssuerAlternativeName(false, getIssuerAlternativeName());
		setSubjectAlternativeName(false, getSubjectAlternativeName(caf.getSubjectRUT(),
				caf.getSubjectTipoDTE(), caf.getSubjectDesdeFolio(), caf.getSubjectHastaFolio()));

		setAuthorityInformationAccess(false, getAuthorityInformationAccess());
		setCRLDistributionPoints(false, getCRLDistributionPoints());

		setAceptaSubjectCAF(true, caf.getSubjectCAF());

		return generate(getAuthority().getPrivateKey());
	}
	
	/*
	 * Certificate Generation Attributes
	 */
	private AuthorityKeyIdentifierStructure getAuthorityKeyIdentifier() throws InvalidKeyException {
		return CertificateGenerator.createAuthorityKeyIdentifier(getAuthority().getCertificate().getPublicKey());
	}

	private SubjectKeyIdentifierStructure getSubjectKeyIdentifier(PublicKey subjectPublicKey)
			throws CertificateParsingException, InvalidKeyException {
		return CertificateGenerator.createSubjectKeyIdentifier(subjectPublicKey);
	}

	private KeyUsage getKeyUsage() {
		return CertificateGenerator.createKeyUsage(
				CertificateGenerator.KEY_USAGE_DIGITAL_SIGNATURE,
				CertificateGenerator.KEY_USAGE_NON_REPUDIATION);
	}

	private PolicyInformation getCertificatePolicies() {
		return CertificateGenerator.createPolicyInformation(ACEPTA_CAF_VT2_POLICY_IDENTIFIER,
				getCPSPolicyQualifierInfo(), getUserNoticePolicyQualifierInfo());
	}

	private PolicyQualifierInfo getCPSPolicyQualifierInfo() {
		return CertificateGenerator.createCPSPolicyQualifierInfo(ACEPTA_CAF_VT2_POLICY_CPS_URI);
	}

	private PolicyQualifierInfo getUserNoticePolicyQualifierInfo() {
		return CertificateGenerator.createUserNoticePolicyQualifierInfo(
				ACEPTA_CAF_VT2_POLICY_USER_NOTICE_EXPLICIT_TEXT,
				ACEPTA_CAF_VT2_POLICY_USER_NOTICE_ORGANIZATION,
				ACEPTA_CAF_VT2_POLICY_USER_NOTICE_NUMBER);
	}

	private GeneralNames getIssuerAlternativeName() {
		Name issuerName = getAuthority().getName();
		
		return CertificateGenerator.createIssuerAlternativeName(issuerName.getRUT(), issuerName.getEmailAddress());
	}

	private GeneralNames getSubjectAlternativeName(String subjectRUT, int subjectTipoDTE, long subjectDesdeFolio, long subjectHastaFolio) {
		return CertificateGenerator.createSubjectAlternativeNameForCAF(subjectRUT, subjectTipoDTE, subjectDesdeFolio, subjectHastaFolio);
	}

	private AuthorityInformationAccess getAuthorityInformationAccess() {
		return CertificateGenerator.createAuthorityInformationAccess(CertificateGenerator.createOCSPAccessDescription(ACEPTA_CAF_VT2_AUTHORITY_INFO_ACCESS_OCSP_URI));
	}

	private CRLDistPoint getCRLDistributionPoints() {
		GeneralName crlURI = CertificateGenerator.createURIName(ACEPTA_CAF_VT2_CRL_DISTRIBUTION_POINT_URI);
		
		return CertificateGenerator.createCRLDistributionPoints(CertificateGenerator.createDistributionPoint(crlURI));
	}
}
