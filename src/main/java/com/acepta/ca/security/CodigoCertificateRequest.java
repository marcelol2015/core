package com.acepta.ca.security;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.Date;

import com.acepta.ca.security.crypto.cert.CertificateRequest;
import com.acepta.ca.security.crypto.cert.Name;
import com.acepta.ca.security.crypto.cert.NameBuilder;
import com.acepta.ca.security.util.PKCS10;

/**
 * Peticion de Certificados Electronicos X.509 para Firma de Codigo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-06
 */
public class CodigoCertificateRequest extends CertificateRequest {
	private BigInteger serialNumber;
	private Date notBefore;
	private Date notAfter;
	private PublicKey subjectPublicKey;
	public String subjectRut;
	private String subjectCountry;
	private String subjectStateOrProvince;
	private String subjectLocality;
	private String subjectOrganization;
	private String subjectOrganizationalUnit;
	private String subjectCommonName;
	
	public CodigoCertificateRequest() {
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public PublicKey getSubjectPublicKey() {
		return subjectPublicKey;
	}

	public void setSubjectPublicKey(PublicKey subjectPublicKey) {
		this.subjectPublicKey = subjectPublicKey;
	}

	public String getSubjectRut() {
		return subjectRut;
	}

	public void setSubjectRut(String subjectRut) {
		this.subjectRut = subjectRut;
	}

	public String getSubjectCountry() {
		return subjectCountry;
	}

	public void setSubjectCountry(String subjectCountry) {
		this.subjectCountry = subjectCountry;
	}

	public String getSubjectStateOrProvince() {
		return subjectStateOrProvince;
	}

	public void setSubjectStateOrProvince(String subjectStateOrProvince) {
		this.subjectStateOrProvince = subjectStateOrProvince;
	}

	public String getSubjectLocality() {
		return subjectLocality;
	}

	public void setSubjectLocality(String subjectLocality) {
		this.subjectLocality = subjectLocality;
	}

	public String getSubjectOrganization() {
		return subjectOrganization;
	}

	public void setSubjectOrganization(String subjectOrganization) {
		this.subjectOrganization = subjectOrganization;
	}

	public String getSubjectOrganizationalUnit() {
		return subjectOrganizationalUnit;
	}

	public void setSubjectOrganizationalUnit(String subjectOrganizationalUnit) {
		this.subjectOrganizationalUnit = subjectOrganizationalUnit;
	}

	public String getSubjectCommonName() {
		return subjectCommonName;
	}

	public void setSubjectCommonName(String subjectCommonName) {
		this.subjectCommonName = subjectCommonName;
	}

	public void setSubjectName(String subjectRut, String subjectCountry, String subjectStateOrProvince, String subjectLocality, String subjectOrganization, String subjectOrganizationalUnit, String subjectCommonName) {
		setSubjectRut(subjectRut);
		setSubjectCountry(subjectCountry);
		setSubjectStateOrProvince(subjectStateOrProvince);
		setSubjectLocality(subjectLocality);
		setSubjectOrganization(subjectOrganization);
		setSubjectOrganizationalUnit(subjectOrganizationalUnit);
		setSubjectCommonName(subjectCommonName);
	}
	
	public void setSubjectPublicKeyFromPKCS10(String pkcs10Base64) throws AceptaSecurityException {
		try {
			setSubjectPublicKey(PKCS10.decodePublicKeyFromPKCS10CerticateRequest(pkcs10Base64));
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaSecurityException(exception);
		}
	}
	
	public Name getSubjectName() {
		NameBuilder buffer = new NameBuilder();

		String country = normalize(getSubjectCountry());
		if (country != null)
			buffer.append(NameBuilder.COUNTRY, country);
		
		String stateOrProvice = normalize(getSubjectStateOrProvince());
		if (stateOrProvice != null)
			buffer.append(NameBuilder.STATE_OR_PROVINCE, stateOrProvice);

		String locality = normalize(getSubjectLocality());
		if (locality != null)
			buffer.append(NameBuilder.LOCALITY, locality);

		String organization = normalize(getSubjectOrganization());
		if (organization != null)
			buffer.append(NameBuilder.ORGANIZATION, organization);

		String organizationalUnit = normalize(getSubjectOrganizationalUnit());
		if (organizationalUnit != null)
			buffer.append(NameBuilder.ORGANIZATIONAL_UNIT, organizationalUnit);
		
		String commonName = normalize(getSubjectCommonName());
		if (commonName != null)
			buffer.append(NameBuilder.COMMON_NAME, commonName);
		
		String rut = normalize(getSubjectRut());
		if (rut != null)
			buffer.append(NameBuilder.SERIAL_NUMBER, rut);

		return buffer.toName();
	}
	
	private static String normalize(String text) {
		if (text != null) {
			text = text.trim();
			if (text.length() != 0)
				return text;
		}
		return null;
	}
}
