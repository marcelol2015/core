package com.acepta.ca.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aestartus on 29-11-16.
 */
public class MigracionSolicitudesAprobadas {

    private final String USER_AGENT = "ACEPTACA-G4/1.0";
    private final String URL_G3 ="https://acg3.acepta.com/acg3";

    public String getValidezSolicitud(String codigo,String pin) throws Exception {
        StringBuffer respuestaHTML = sendPost(URL_G3+"/servlet/instalacioncertificado","CodigoSolicitud="+codigo+"&Pin="+pin);
        String respuestaInstalacionG3 = htmlGetTagValue(respuestaHTML);
        return respuestaInstalacionG3;
    }

    public StringBuffer getSolicitudLegacy(String codigo) throws Exception {
        StringBuffer respuestaXMLG3 = sendGet(URL_G3+"/servlet/getxml","?FileName="+codigo);
        return respuestaXMLG3;
    }

    public String getCorreoFromXML(StringBuffer solicitudXML){
        String correo = xmlGetTagValue("CorreoElectronico",solicitudXML);
        return correo;
    }

    // HTTP GET request
    private StringBuffer sendGet(String url, String mensaje) throws Exception {

        URL obj = new URL(url+mensaje);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nEnviando 'GET' request a la URL : " + url);
        System.out.println("Codigo de Respuesta : " + responseCode);
        System.out.println("Mensaje de Respuesta : " + con.getResponseMessage());

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        return response;

    }

    // HTTP POST request
    private StringBuffer sendPost(String url, String mensaje) throws Exception {

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        //con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(mensaje);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nEnviando 'POST' request a la URL : " + url);
        System.out.println("Parametros del POST : " + mensaje);
        System.out.println("Codigo de Respuesta : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;

    }

    private String htmlGetTagValue(StringBuffer html){
        Document doc;
        doc = Jsoup.parse(html.toString());

        Elements divs = doc.select("div#ErrorInstalacion");
        if(divs.size()>0){
            for (Element div : divs) {
                return div.text();
            }
        }else{
            return "OK";
        }
        return null;
    }

    private String xmlGetTagValue(String tagName,StringBuffer xml) {
        Document doc = Jsoup.parse(xml.toString(), "", Parser.xmlParser());
        for (Element e : doc.select(tagName)) {
            return e.text();
        }
        return null;
    }

}
