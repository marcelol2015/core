package com.acepta.ca.bean;

/**
 * Excepcion lanzada por las componentes de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaBeanException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AceptaBeanException(String message) {
		super(message);
	}
	
	public AceptaBeanException(String message, Exception exception) {
		super(message, exception);
	}
}
