package com.acepta.ca.bean;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.security.AceptaSecurityManager;

/**
 * Componente que provee parte de los servicios y/o funcionalidades de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public abstract class AceptaBean {
	/*
	 * Nombres de las Autoridades
	 */
	protected static final String ACEPTA_AUTORIDAD_CLASE3 = "Clase3-G4";
	protected static final String ACEPTA_AUTORIDAD_CLASE2 = "Clase2-G4";
	protected static final String ACEPTA_AUTORIDAD_FIRMA_AVANZADA = "FirmaAvanzada-G4";
    protected static final String ACEPTA_AUTORIDAD_SELLO_TIEMPO = "SelloTiempo-G4";

	protected static final String ACEPTA_AUTORIDAD_SITIO_WEB = "SitioWebVT2";
	protected static final String ACEPTA_AUTORIDAD_CAF = "CAFVT2";
	protected static final String ACEPTA_AUTORIDAD_SISTEMA = "SistemaVT2";
	protected static final String ACEPTA_AUTORIDAD_CODIGO = "CodigoVT2";

    /*
     * Nombres del alias del emisor de Sello de Tiempo
     */
    protected static final String ACEPTA_EMISOR_SELLO_TIEMPO = "EmisorSelloTiempo-G4";

	/*
	 * Nombres de los Grupos
	 */
	protected static final String ACEPTA_GRUPO_PERSONA_OPERADOR_REGISTRO = "AceptaOperadorRegistro";
	protected static final String ACEPTA_GRUPO_PERSONA_OPERADOR_VALIDACION = "AceptaOperadorValidacion";
	
	protected static final String ACEPTA_GRUPO_SITIO_WEB_OPERADOR_REGISTRO = "AceptaSitioWebOperadorRegistro";
	protected static final String ACEPTA_GRUPO_SITIO_WEB_OPERADOR_VALIDACION = "AceptaSitioWebOperadorValidacion";

	protected static final String ACEPTA_GRUPO_SISTEMA_OPERADOR_REGISTRO = "AceptaSistemaOperadorRegistro";
	protected static final String ACEPTA_GRUPO_SISTEMA_OPERADOR_VALIDACION = "AceptaSistemaOperadorValidacion";

	protected static final String ACEPTA_GRUPO_CODIGO_OPERADOR_REGISTRO = "AceptaCodigoOperadorRegistro";
	protected static final String ACEPTA_GRUPO_CODIGO_OPERADOR_VALIDACION = "AceptaCodigoOperadorValidacion";
	
	protected static final String ACEPTA_GRUPO_PERSONA_OPERADOR_REGISTRO_AUTOMATA = "AceptaOperadorAutomata";

	/*
	 * Nombres de los Tipos de Registro
	 */
	protected static final String ACEPTA_TIPO_REGISTRO_PRESENCIAL_MANUAL ="PRESENCIAL_MANUAL";
	protected static final String ACEPTA_TIPO_REGISTRO_PRESENCIAL_CEDULANUEVA ="PRESENCIAL_CEDULANUEVA";
	protected static final String ACEPTA_TIPO_REGISTRO_ONLINE_CLAVEUNICA ="ONLINE_CLAVEUNICA";
	protected static final String ACEPTA_TIPO_REGISTRO_ONLINE_CLASE2 ="ONLINE_CLASE2";

	private AceptaEntityManager entityManager;
	private AceptaSecurityManager securityManager;

	public AceptaBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		setEntityManager(entityManager);
		setSecurityManager(securityManager);
	}

	public void close() {
		getEntityManager().close();
		getSecurityManager().close();
	}
	
	protected AceptaEntityManager getEntityManager() {
		return entityManager;
	}

	protected void setEntityManager(AceptaEntityManager entityManager) {
		this.entityManager = entityManager;
	}

	protected AceptaSecurityManager getSecurityManager() {
		return securityManager;
	}

	protected void setSecurityManager(AceptaSecurityManager securityManager) {
		this.securityManager = securityManager;
	}
}
