package com.acepta.ca.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.NoResultException;

import org.xml.sax.SAXException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.SitioWeb;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudSitioWeb;
import com.acepta.ca.persistence.Usuario;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.SitioWebCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.xml.XMLSolicitudSitioWeb;

/**
 * Componente para consultar e ingresar Solicitudes de Certificados para Sitios Web,
 * y para emitir los Certificados Electronicos X.509 correspondientes.
 *  
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class SolicitudSitioWebBean extends AceptaBean {
	public SolicitudSitioWebBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Busca una Solicitud de Certificado por codigo.
	 * 
	 * @param codigo	Codigo de la Solicitud
	 * 
	 * @return	Solicitud de Certificado
	 */
	public SolicitudSitioWeb buscarSolicitudSitioWebConCodigo(String codigo) {
		try {
			return getEntityManager().findSolicitudSitioWebWithCodigo(codigo);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe la Solicitud No. " + codigo, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitud No. " + codigo, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes de Certificados pendientes, aprobadas, rechazadas o cerradas.
	 * 
	 * @param estado	Estado de las Solicitudes
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudSitioWeb> buscarSolicitudesSitioWebConEstado(Solicitud.Estado estado) {
		try {
			return getEntityManager().findSolicitudesSitioWebWithEstado(estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes en estado " + estado, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes de Certificados para los Sitios Web de un dominio.
	 * 
	 * @param dominio	Nombre de Dominio
	 * 
	 * @return	Listado de Solicitudes del dominio
	 */
	public Collection<SolicitudSitioWeb> buscarSolicitudesSitioWebConDominio(String dominio) {
		try {
			return getEntityManager().findSolicitudesSitioWebWithDominio(dominio);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes con dominio " + dominio, exception);
		}
	}

	/**
	 * Ingresar una nueva Solicitud y emitir el Certificado Electronico X.509.
	 * 
	 * @param data	Documento XML con la Solicitud
	 * 
	 * @return	Solicitud con su Certificado Electronico X.509
	 */
	public SolicitudSitioWeb ingresarSolicitudSitioWeb(byte[] data) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudSitioWeb peticion = createPeticionFromData(data);
			
			validateEsquemaPeticion(peticion);
			
			validateFirmaOperadorValidacionDePeticion(peticion);

			SolicitudSitioWeb solicitud = createSolicitudFromPeticion(peticion);

			Certificado certificado = generateCertificadoForSolicitud(solicitud, peticion.getPKCS10());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);

			getEntityManager().persist(solicitud);
			
			getEntityManager().persist(new Documento(solicitud, data));

			getEntityManager().persist(new Cambio("GENERAR", solicitud));
			
			getEntityManager().commitTransaction();

			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al ingresar Solicitud: " + exception.getMessage(), exception);
		}
	}

	private Certificado generateCertificadoForSolicitud(SolicitudSitioWeb solicitud, String pkcs10Base64) {
		try {
			Authority authority = getSecurityManager().getAuthority(solicitud.getAutoridad().getNombre());

			SitioWebCertificateRequest request = generateCertificateRequestForSolicitud(solicitud, pkcs10Base64);

			X509Certificate certificate = authority.generateCertificate(request);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(certificate.getSerialNumber());
			certificado.setValidoDesde(certificate.getNotBefore());
			certificado.setValidoHasta(certificate.getNotAfter());
			certificado.setSujeto(request.getSubjectCommonName());
			certificado.setData(certificate.getEncoded());

			certificado.changeEstado(Certificado.Estado.VALIDO);

			return certificado;
		}
		catch (CertificateEncodingException exception) {
			throw new AceptaBeanException("Falla al serializar Certificado X.509", exception);
		}
	}

	private SitioWebCertificateRequest generateCertificateRequestForSolicitud(SolicitudSitioWeb solicitud, String pkcs10Base64) {
		Calendar calendar = Calendar.getInstance();
		Date notBefore = calendar.getTime();

		calendar.add(Calendar.MONTH, solicitud.getMesesVigencia());
		Date notAfter = calendar.getTime();

		SitioWebCertificateRequest request = new SitioWebCertificateRequest();

		request.setSerialNumber(solicitud.getAutoridad().generarNumeroSerieCertificado());
		request.setNotBefore(notBefore);
		request.setNotAfter(notAfter);

		request.setSubjectCommonName(solicitud.getSitioWeb().getDominio());
		request.setSubjectOrganizationalUnit(solicitud.getSitioWeb().getUnidadOrganizacional());
		request.setSubjectOrganization(solicitud.getSitioWeb().getOrganizacion());
		request.setSubjectState(solicitud.getSitioWeb().getEstado());
		request.setSubjectLocality(solicitud.getSitioWeb().getCiudad());
		request.setSubjectCountry(solicitud.getSitioWeb().getPais());

		request.setSubjectPublicKeyFromPKCS10(pkcs10Base64);

		return request;
	}

	private SolicitudSitioWeb createSolicitudFromPeticion(XMLSolicitudSitioWeb peticion) {
		SolicitudSitioWeb solicitud = new SolicitudSitioWeb();

		solicitud.setNumero(peticion.getNumeroSolicitud());
		solicitud.setClave(peticion.getClave());
		solicitud.setMesesVigencia(peticion.getMesesVigencia());

		SitioWeb sitioWeb = createSitioWebFromPeticion(peticion);

		solicitud.setSitioWeb(sitioWeb);

		solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_SITIO_WEB));

		return solicitud;
	}

	private SitioWeb createSitioWebFromPeticion(XMLSolicitudSitioWeb peticion) {
		SitioWeb sitioWeb = new SitioWeb();

		sitioWeb.setDominio(peticion.getDominio());

		sitioWeb.setRut(peticion.getRut());
		sitioWeb.setOrganizacion(peticion.getOrganizacion());
		sitioWeb.setUnidadOrganizacional(peticion.getUnidadOrganizacional());
		
		sitioWeb.setEstado(peticion.getEstado());
		sitioWeb.setCiudad(peticion.getCiudad());
		sitioWeb.setPais(peticion.getPais());

		return sitioWeb;
	}

	private void validateEsquemaPeticion(XMLSolicitudSitioWeb peticion) {
		try {
			if (peticion.getDominio().equals(""))
				throw new AceptaBeanException("Falta el dominio del sitioweb");
			
			if (peticion.getOrganizacion().equals(""))
				throw new AceptaBeanException("Falta nombre de la organizacion");
			
			if (peticion.getPais().equals(""))
				throw new AceptaBeanException("Falta el pais");
			
			if (peticion.getPKCS10().equals(""))
				throw new AceptaBeanException("Falta la peticion PKCS#10");
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Solicitud con esquema invalido: " + exception.getMessage());
		}
	}
	
	private void validateFirmaOperadorValidacionDePeticion(XMLSolicitudSitioWeb peticion) {
		validatePeticionSignature(peticion, getRUTsDeGrupoUsuarios(ACEPTA_GRUPO_SITIO_WEB_OPERADOR_VALIDACION));
	}

	private Collection<String> getRUTsDeGrupoUsuarios(String nombreGrupo) {
		Collection<String> ruts = new ArrayList<String>();
		
		for (Usuario usuario : getEntityManager().findUsuariosWithGrupo(nombreGrupo))
			ruts.add(usuario.getRut());
		
		return ruts;
	}

	private void validatePeticionSignature(XMLSolicitudSitioWeb peticion, Collection<String> trustedRUTs) {
		try {
			if (!peticion.validate(getSecurityManager().getTrustedCertificates(), trustedRUTs))
				throw new AceptaBeanException("Firma invalida");
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al validar la firma de la Solicitud: " + exception.getMessage(), exception);
		}
	}

	private XMLSolicitudSitioWeb createPeticionFromData(byte[] data) {
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(data);
			try {
				return new XMLSolicitudSitioWeb(input);
			}
			finally {
				input.close();
			}
		}
		catch (SAXException exception) {
			throw new AceptaBeanException("Falla al decodificar XML", exception);
		}
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al leer XML", exception);
		}
	}
}
