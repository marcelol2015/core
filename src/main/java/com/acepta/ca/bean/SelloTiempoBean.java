package com.acepta.ca.bean;

import java.math.BigInteger;
import java.util.Collection;

import javax.persistence.NoResultException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.SelloTiempo;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.tsp.X509TimeStampRequest;
import com.acepta.ca.security.crypto.tsp.X509TimeStampResponse;

/**
 * Componente para consular y procesar Sellos de Tiempo de la Autoridad Certificadora.  
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-28
  */
public class SelloTiempoBean extends AceptaBean {
	public SelloTiempoBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Devuelve todos las Sellos de Tiempo emitidas por la Autoridad.
	 * 
	 * @return	Listado de sellos de tiempo
	 */
	public Collection<SelloTiempo> buscarSellosTiempos() {
		try {
			Autoridad autoridad = getEntityManager().findAutoridadWithNombre(ACEPTA_EMISOR_SELLO_TIEMPO);
		
			return getEntityManager().findSellosTiemposWithEmisor(autoridad);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar los Sellos de Tiempo", exception);
		}
	}
	
	/**
	 * Buscar un Sello de Tiempo por Numero de Serie.
	 * 
	 */
	public SelloTiempo buscarSelloTiempoConNumeroSerie(BigInteger numeroSerie) {
		try {
			Autoridad autoridad = getEntityManager().findAutoridadWithNombre(ACEPTA_EMISOR_SELLO_TIEMPO);
			
			return getEntityManager().findSelloTiempoWithEmisorNumeroSerie(autoridad, numeroSerie);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe el Sello de Tiempo con Numero de Serie " + numeroSerie, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Sello de Tiempo con Numero de Serie " + numeroSerie, exception);
		}
	}
	
	/**
	 * Emitir un Sello de Tiempo para una peticion en formato TSP (RFC 3161).
	 * 
	 * @param	peticion	Peticion de sellado de tiempo en formato TSP
	 * 
	 * @return	Respuesta con el sello de tiempo (o informacion de rechazo) en formato TSP
	 */
	public byte[] emitirSelloTiempo(byte[] peticion) {
		getEntityManager().beginTransaction();
		try {
			Autoridad autoridad = getEntityManager().findAutoridadWithNombre(ACEPTA_EMISOR_SELLO_TIEMPO);
			
			X509TimeStampResponse response = generateSelloTiempoForEmisorPeticion(autoridad, peticion);
			
			if (response.isGranted()) {
				SelloTiempo selloTiempo = createSelloTiempoFromEmisorResponse(autoridad, response);
			
				getEntityManager().persist(selloTiempo);
			}
			
			getEntityManager().commitTransaction();
			
			return response.getEncoded();
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al emitir Sello de Tiempo", exception);
		}
	}

	private X509TimeStampResponse generateSelloTiempoForEmisorPeticion(Autoridad autoridad, byte[] peticion) {
		X509TimeStampRequest request = new X509TimeStampRequest(autoridad.generarNumeroSerieSelloTiempo(), peticion);
		
		Authority authority = getSecurityManager().getAuthority(autoridad.getNombre());
		
		return authority.generateTimeStamp(request);
	}

	private SelloTiempo createSelloTiempoFromEmisorResponse(Autoridad autoridad, X509TimeStampResponse response) {
		SelloTiempo selloTiempo = new SelloTiempo();
		
		selloTiempo.setEmisor(autoridad);
		selloTiempo.setNumeroSerie(response.getSerialNumber());
		selloTiempo.setTiempo(response.getGenTime());
		selloTiempo.setData(response.getEncoded());
		
		return selloTiempo;
	}
}
