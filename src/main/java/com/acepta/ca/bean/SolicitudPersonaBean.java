package com.acepta.ca.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import org.xml.sax.SAXException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Persona;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudPersona;
import com.acepta.ca.persistence.Usuario;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.PersonaCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.util.BASE64;
import com.acepta.ca.xml.XMLSolicitudPersona;

/**
 * Componente para consultar, ingresar, modificar, rechazar y aprobar Solicitudes de Certificados
 * para Personas Naturales, y para emitir los Certificados Electronicos X.509 correspondientes.
 *  
 * @author Carlos Hasan, Alvaro Millalen
 * @version 1.1, 2011-01-14
 */
public class SolicitudPersonaBean extends AceptaBean {
	private static final String RUT_REGULAR_EXPRESSION = "[0-9]{1,9}-[0-9K]";
	private static final String EMAIL_ADDRESS_REGULAR_EXPRESSION = "[A-Za-z0-9z!#$%*/?|^{}`~&'+=_.-]{1,63}@[A-Za-z0-9\\.-]{1,255}";
	private static final String APROBACION_AUTOMATICA_CN ="Aprobado automáticamente por tipo de cédula";
	private static final String APROBACION_AUTOMATICA_CU ="Aprobado automáticamente por registro con clave única";



	public SolicitudPersonaBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Devuelve las Solicitudes de Certificado de un titular identificado por RUT.
	 * 
	 * @param rutTitular	RUT del Titular
	 * 
	 * @return	Listado de Solicitudes del titular
	 */
	public Collection<SolicitudPersona> buscarSolicitudesPersonaConRutTitular(String rutTitular) {
		try {
			return getEntityManager().findSolicitudesPersonaWithRutTitular(rutTitular);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes del titular con RUT " + rutTitular, exception);
		}
	}
	
	/**
	 * Devuelve las Solicitudes de Certificado de un titular identificado por direccion de correo.
	 * 
	 * @param correoTitular		Direccion de correo del Titular
	 * 
	 * @return	Listado de Solicitudes del titular
	 */
	public Collection<SolicitudPersona> buscarSolicitudesPersonaConCorreoTitular(String correoTitular) {
		try {
			return getEntityManager().findSolicitudesPersonaWithCorreoTitular(correoTitular);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes del titular con correo " + correoTitular, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes de Certificado pendientes, aprobadas, rechazadas o cerradas.
	 * 
	 * @param estado	Estado de las Solicitudes
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudPersona> buscarSolicitudesPersonaConEstado(Solicitud.Estado estado) {
		try {
			return getEntityManager().findSolicitudesPersonaWithEstado(estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes en estado " + estado, exception);
		}
	}

	public Collection<SolicitudPersona> buscarSolicitudesPersonaConRutTitularyEstado(String rutTitular, Solicitud.Estado estado) {
		try {
			return getEntityManager().findSolicitudesPersonaWithRutTitularAndEstado(rutTitular, estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes del titular con RUT "+rutTitular+" en estado " + estado, exception);
		}
	}

	public Collection<SolicitudPersona> buscarSolicitudesPersonaConRutTitularyEstadoyTipoCert(String rutTitular, Solicitud.Estado estado, String tipoCert) {
		try {
			return getEntityManager().findSolicitudesPersonaWithRutTitularAndEstadoAndTipoCert(rutTitular, estado, tipoCert);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes del titular con RUT "+rutTitular+" en estado " + estado+" y de tipo "+tipoCert, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes de Certificado correspondientes a numero de serie de certificado.
	 * 
	 * @param numeroSerie	Numero de Serie del Certificado
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudPersona> buscarSolicitudesPersonaConNumeroSerie(BigInteger numeroSerie) {
		try {
			return getEntityManager().findSolicitudesPersonaWithNumeroSerie(numeroSerie);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes con numero de serie " + numeroSerie, exception);
		}
	}
	
	/**
	 * Devuelve las Solicitudes de Certificado que expiran en un periodo determinado.
	 * 
	 * @param desdeFecha	Comienzo del periodo
	 * @param hastaFecha	Fin del periodo
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudPersona> buscarSolicitudesPersonaConExpiracionEntre(Date desdeFecha, Date hastaFecha) {
		try {
			return getEntityManager().findSolicitudesPersonaWithExpiracionEntre(desdeFecha, hastaFecha);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes con expiracion entre " + desdeFecha + " y " + hastaFecha, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes cerradas con Certificados validos o revocados.
	 * 
	 * @param estado		Estado del Certificado
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudPersona> buscarSolicitudesPersonaConEstadoCertificado(Certificado.Estado estado) {
		try {
			return getEntityManager().findSolicitudesPersonaWithEstadoCertificado(estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes con Certificados en estado " + estado, exception);
		}
	}

	/**
	 * Busca una Solicitud de Certificado por codigo. 
	 * 
	 * @param codigo	Codigo de la Solicitud
	 * 
	 * @return	Solicitud
	 */
	public SolicitudPersona buscarSolicitudPersonaConCodigo(String codigo) {
		try {
			return getEntityManager().findSolicitudPersonaWithCodigo(codigo);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe la Solicitud No. " + codigo, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitud No. " + codigo, exception);
		}
	}
	
	/**
	 * Busca una Solicitud de Certificado por numero. 
	 * 
	 * @param numero	Numero de la Solicitud
	 * 
	 * @return	Listado de Solicitudes
	 */
	public SolicitudPersona buscarSolicitudPersonaConNumero(String numero) {
		try {
			return getEntityManager().findSolicitudPersonaWithNumero(numero);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe la Solicitud con numero " + numero, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitud con numero " + numero, exception);
		}
	}

	/**
	 * Devuelve el documento mas reciente de una Solicitud.
	 * 
	 * @param solicitud	Solicitud persona
	 * 
	 * @return	Ultimo documento registrado de la Solicitud
	 */
	public Documento buscarDocumentoDeSolicitudPersona(SolicitudPersona solicitud) {
		try {
			for (Documento documento : getEntityManager().findDocumentosWithSolicitud(solicitud))
				return documento;
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar el documento de la Solicitud No. " + solicitud.getCodigo(), exception);
		}
		
		throw new AceptaBeanException("Solicitud No. " + solicitud.getCodigo() + " no tiene documentos");
	}
	
	/**
	 * Devuelve la fotografia del titular de una Solicitud.
	 * 
	 *  @param	solicitud	Solicitud persona
	 *  
	 *  @return	Fotografia codificada en formato JPEG.
	 */
	public byte[] buscarFotografiaDeSolicitudPersona(SolicitudPersona solicitud) {
		try {
			Documento documento = buscarDocumentoDeSolicitudPersona(solicitud);
	
			XMLSolicitudPersona peticion = createPeticionFromData(documento.getData());
			
			return BASE64.decode(peticion.getPersonaFotografia());
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar la Fotografia de la Solicitud No. " + solicitud.getCodigo(), exception);
		}
	}
	
	/**
	 * Ingresa una nueva Solicitud de Certificado.
	 * 
	 * @param data	Documento XML con la Solicitud
	 * 
	 * @return	Solicitud ingresada
	 */
	public SolicitudPersona ingresarSolicitudPersona(byte[] data) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudPersona peticion = createPeticionFromData(data);
			
			validateFirmaOperadorRegistroDePeticion(peticion);
			
			SolicitudPersona solicitud = createSolicitudFromPeticion(peticion);

			String tipoRegistro = peticion.getTipoRegistro();
			if (tipoRegistro.equals(ACEPTA_TIPO_REGISTRO_PRESENCIAL_MANUAL)) {
				solicitud.changeEstado(Solicitud.Estado.PENDIENTE);
				getEntityManager().persist(solicitud);
				getEntityManager().persist(new Documento(solicitud, data));
				getEntityManager().persist(new Cambio("INGRESAR", solicitud));
			} else if (tipoRegistro.equals(ACEPTA_TIPO_REGISTRO_PRESENCIAL_CEDULANUEVA)) {
				solicitud.changeEstado(Solicitud.Estado.APROBADA);
				getEntityManager().persist(solicitud);
				getEntityManager().persist(new Documento(solicitud, data));
				getEntityManager().persist(new Cambio("APROBAR", solicitud, APROBACION_AUTOMATICA_CN));
			} else if (tipoRegistro.equals(ACEPTA_TIPO_REGISTRO_ONLINE_CLAVEUNICA)) {
				solicitud.changeEstado(Solicitud.Estado.APROBADA);
				getEntityManager().persist(solicitud);
				getEntityManager().persist(new Documento(solicitud, data));
				getEntityManager().persist(new Cambio("APROBAR", solicitud, APROBACION_AUTOMATICA_CU));
			} else if (tipoRegistro.equals(ACEPTA_TIPO_REGISTRO_ONLINE_CLASE2)) {
				solicitud.changeEstado(Solicitud.Estado.PENDIENTE);
				getEntityManager().persist(solicitud);
				getEntityManager().persist(new Documento(solicitud, data));
				getEntityManager().persist(new Cambio("INGRESAR", solicitud));
			} else {
				solicitud.changeEstado(Solicitud.Estado.PENDIENTE);
				getEntityManager().persist(solicitud);
				getEntityManager().persist(new Documento(solicitud, data));
				getEntityManager().persist(new Cambio("INGRESAR", solicitud));
			}

			getEntityManager().commitTransaction();
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			if (exception instanceof PersistenceException)
				throw new AceptaBeanException("Falla al ingresar la Solicitud en la base de datos", exception);
			else
				throw new AceptaBeanException("Falla al ingresar Solicitud: " + exception.getMessage(), exception);
		}
	}

	/**
	 * Rechaza una Solicitud de Certificado pendiente.
	 * 
	 * @param codigo		Codigo de la Solicitud
	 * @param data			Documento XML con el Rechazo
	 * @param comentario	Comentarios adicionales (opcional)
	 * 
	 * @return	Solicitud rechazada
	 */
	public SolicitudPersona rechazarSolicitudPersona(String codigo, byte[] data, String comentario) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudPersona peticion = createPeticionFromData(data);

			validateFirmaOperadorValidacionDePeticion(peticion);

			SolicitudPersona solicitud = getEntityManager().findSolicitudPersonaWithCodigo(codigo);
			
			if (!solicitud.getNumero().equals(peticion.getNumeroSolicitud()))
				throw new AceptaBeanException("Solicitud no corresponde");

			if (!solicitud.isPendiente())
				throw new AceptaBeanException("Solicitud no esta pendiente");
			
			solicitud.changeEstado(Solicitud.Estado.RECHAZADA);

			getEntityManager().persist(new Documento(solicitud, data));
			
			getEntityManager().persist(new Cambio("RECHAZAR", solicitud, comentario));
			
			getEntityManager().commitTransaction();
			
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al rechazar Solicitud: " + exception.getMessage(), exception);
		}
	}

	/**
	 * Anular una Solicitud de Certificado aprobada.
	 * 
	 * @param codigo		Codigo de la Solicitud
	 * @param data			Documento XML con el Anulado
	 * @param comentario	Comentarios adicionales (opcional)
	 * 
	 * @return	Solicitud anulada
	 */
	public SolicitudPersona anularSolicitudPersona(String codigo, byte[] data, String comentario) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudPersona peticion = createPeticionFromData(data);

			validateFirmaOperadorValidacionDePeticion(peticion);

			SolicitudPersona solicitud = getEntityManager().findSolicitudPersonaWithCodigo(codigo);
			
			if (!solicitud.getNumero().equals(peticion.getNumeroSolicitud()))
				throw new AceptaBeanException("Solicitud no corresponde");

			if (!solicitud.isAprobada())
				throw new AceptaBeanException("Solicitud no esta aprobada");
			
			solicitud.changeEstado(Solicitud.Estado.ANULADA);

			getEntityManager().persist(new Documento(solicitud, data));
			
			getEntityManager().persist(new Cambio("ANULAR", solicitud, comentario));
			
			getEntityManager().commitTransaction();
			
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al anular Solicitud: " + exception.getMessage(), exception);
		}
	}
	
	/**
	 * Modifica el estado de pago, clase y vigencia de una Solicitud de Certificado pendiente, aprobada o rechazada.
	 * 
	 * @param codigo			Codigo de la Solicitud
	 * @param pagada			Estado de pago
	 * @param montoPagado		Monto pagado
	 * @param isFirmaAvanzada	Clase del Certificado (Avanzada o Simple)
	 * @param mesesVigencia		Vigencia del Certificado
	 * 
	 * @return	Solicitud modificada
	 */
	public SolicitudPersona pagarSolicitudPersona(String codigo, boolean pagada, long montoPagado, boolean isFirmaAvanzada, int mesesVigencia) {
		getEntityManager().beginTransaction();
		try {
			SolicitudPersona solicitud = getEntityManager().findSolicitudPersonaWithCodigo(codigo);
			
			if (!solicitud.isPendiente() && !solicitud.isAprobada() && !solicitud.isRechazada())
				throw new AceptaBeanException("Solicitud no esta pendiente, aprobada o rechazada");


			
			if (isFirmaAvanzada)
				solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_FIRMA_AVANZADA));
			else
				if(solicitud.getAutoridad().getNombre().equalsIgnoreCase(ACEPTA_AUTORIDAD_CLASE2))
					solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_CLASE2));
				else
					solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_CLASE3));
			
			solicitud.setPagada(pagada);
			solicitud.setMontoPagado(montoPagado);			
			solicitud.setMesesVigencia(mesesVigencia);
			
			getEntityManager().persist(new Cambio("PAGAR", solicitud));
			
			getEntityManager().commitTransaction();
			
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al pagar Solicitud: " + exception.getMessage(), exception);
		}
	}

	/**
	 * Corrige los datos del titular de una Solicitud de Certificado pendiente, aprobada o rechazada.
	 * 
	 * @param codigo	Codigo de la Solicitud
	 * @param titular	Datos del Titular
	 * 
	 * @return	Solicitud corregida
	 */
	public SolicitudPersona corregirSolicitudPersona(String codigo, Persona titular) {
		getEntityManager().beginTransaction();
		try {
			SolicitudPersona solicitud = getEntityManager().findSolicitudPersonaWithCodigo(codigo);
			
			if (!solicitud.isPendiente() && !solicitud.isAprobada() && !solicitud.isRechazada())
				throw new AceptaBeanException("Solicitud no esta pendiente, aprobada, o rechazada");
			
			solicitud.getTitular().setNombres(normalizeSubjectNombre(titular.getNombres()));
			solicitud.getTitular().setApellidoPaterno(normalizeSubjectNombre(titular.getApellidoPaterno()));
			solicitud.getTitular().setApellidoMaterno(normalizeSubjectNombre(titular.getApellidoMaterno()));
			solicitud.getTitular().setProfesion(normalizeSubjectNombre(titular.getProfesion()));
			solicitud.getTitular().setCorreo(normalizeSubjectCorreo(titular.getCorreo()));

			getEntityManager().persist(new Cambio("CORREGIR", solicitud));

			getEntityManager().commitTransaction();
			
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al corregir Solicitud: " + exception.getMessage(), exception);
		}
	}

	/**
	 * Aprobar una Solicitud de Certificado pendiente o rechazada.
	 * 
	 * @param codigo		Codigo de la Solicitud
	 * @param data			Documento XML con la Aprobacion
	 * @param comentario	Comentarios adicionales (opcional)
	 * 
	 * @return	Solicitud aprobada
	 */
	public SolicitudPersona aprobarSolicitudPersona(String codigo, byte[] data, String comentario) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudPersona peticion = createPeticionFromData(data);

			validateFirmaOperadorValidacionDePeticion(peticion);
			
			SolicitudPersona solicitud = getEntityManager().findSolicitudPersonaWithCodigo(codigo);
			
			if (!solicitud.getNumero().equals(peticion.getNumeroSolicitud()))
				throw new AceptaBeanException("Solicitud no corresponde");
				
			if (!solicitud.isPendiente() && !solicitud.isRechazada())
				throw new AceptaBeanException("Solicitud no esta pendiente o rechazada");
			
			if (!solicitud.isPagada())
				throw new AceptaBeanException("Solicitud no esta pagada");

			validateTitularDeSolicitud(solicitud);
			
			solicitud.changeEstado(Solicitud.Estado.APROBADA);

			getEntityManager().persist(new Documento(solicitud, data));
			
			getEntityManager().persist(new Cambio("APROBAR", solicitud, comentario));
			
			getEntityManager().commitTransaction();
			
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al aprobar Solicitud: " + exception.getMessage(), exception);
		}
	}

	/**
	 * Emite el Certificado Electronico X.509 de una Solicitud aprobada.
	 * 
	 * @param codigo		Codigo de la Solicitud
	 * @param clave			PIN de la Solicitud
	 * @param pkcs10Base64	Peticion de Certificado en formato PKCS#10
	 * @param spkacBase64	Peticion de Certificado en formato SPKAC
	 * 
	 * @return	Solicitud con el Certificado emitido
	 */
	public SolicitudPersona emitirCertificadoPersona(String codigo, String clave, String pkcs10Base64, String spkacBase64) {
		getEntityManager().beginTransaction();
		try {
			SolicitudPersona solicitud = getEntityManager().findSolicitudPersonaWithCodigo(codigo);
			
			if (!solicitud.getClave().equals(clave))
				throw new AceptaBeanException("Clave incorrecta");
		
			if (solicitud.isRechazada())
				throw new AceptaBeanException("Solicitud fue rechazada");

			if (solicitud.isCerrada())
				throw new AceptaBeanException("Solicitud esta cerrada");

			if (solicitud.isAnulada())
				throw new AceptaBeanException("Solicitud esta anulada");
			
			if (!solicitud.isAprobada())
				throw new AceptaBeanException("Solicitud no esta aprobada");

			Certificado certificado = generateCertificateForSolicitud(solicitud, pkcs10Base64, spkacBase64);

			solicitud.setCertificado(certificado);
			
			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			getEntityManager().persist(new Cambio("GENERAR", solicitud));

			getEntityManager().commitTransaction();
			
			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al emitir Certificado de la Solicitud: " + exception.getMessage(), exception);
		}
	}

	private SolicitudPersona createSolicitudFromPeticion(XMLSolicitudPersona peticion) {
		SolicitudPersona solicitud = new SolicitudPersona();
		
		solicitud.setNumero(peticion.getNumeroSolicitud());
		solicitud.setClave(peticion.getClaveActivacion());
		solicitud.setRecordatorio(peticion.getRecordatorio());
		
		solicitud.setTitular(createTitularFromPeticion(peticion));
		
		solicitud.setMesesVigencia(peticion.getMesesVigencia());
		
		if (peticion.isFirmaAvanzada())
			solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_FIRMA_AVANZADA));
		else
            if(peticion.getTipoRegistro()!= null && peticion.getTipoRegistro().contains("CLASE2"))
			    solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_CLASE2));
            else
                solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_CLASE3));

		
		validateSubjectRut(solicitud.getTitular().getRut());

		return solicitud;
	}

	private Persona createTitularFromPeticion(XMLSolicitudPersona peticion) {
		Persona titular = new Persona();
		
		titular.setRut(normalizeSubjectRut(peticion.getPersonaRut()));
		titular.setNombres(normalizeSubjectNombre(peticion.getPersonaNombres()));
		titular.setApellidoPaterno(normalizeSubjectNombre(peticion.getPersonaApellidoPaterno()));
		titular.setApellidoMaterno(normalizeSubjectNombre(peticion.getPersonaApellidoMaterno()));
		titular.setProfesion(normalizeSubjectNombre(peticion.getPersonaProfesion()));
		titular.setCorreo(normalizeSubjectCorreo(peticion.getPersonaCorreoElectronico()));
		
		return titular;
	}

	private void validateTitularDeSolicitud(SolicitudPersona solicitud) {
		Persona titular = solicitud.getTitular();
		
		validateSubjectRut(titular.getRut());
		validateSubjectCorreo(titular.getCorreo());
		validateSubjectNombreComun(titular.getNombreComun());
	}

	private void validateFirmaOperadorRegistroDePeticion(XMLSolicitudPersona peticion) {
		if (peticion.getTipoRegistro().equals(ACEPTA_TIPO_REGISTRO_ONLINE_CLAVEUNICA) || peticion.getTipoRegistro().equals(ACEPTA_TIPO_REGISTRO_ONLINE_CLASE2))
			validatePeticionSignature(peticion, getRUTsDeGrupoUsuarios(ACEPTA_GRUPO_PERSONA_OPERADOR_REGISTRO_AUTOMATA));
		else
			validatePeticionSignature(peticion, getRUTsDeGrupoUsuarios(ACEPTA_GRUPO_PERSONA_OPERADOR_REGISTRO));
	}

	private void validateFirmaOperadorValidacionDePeticion(XMLSolicitudPersona peticion) {
		validatePeticionSignature(peticion, getRUTsDeGrupoUsuarios(ACEPTA_GRUPO_PERSONA_OPERADOR_VALIDACION));
	}

	private Collection<String> getRUTsDeGrupoUsuarios(String nombreGrupo) {
		Collection<String> ruts = new ArrayList<String>();
		
		for (Usuario usuario : getEntityManager().findUsuariosWithGrupo(nombreGrupo))
			ruts.add(usuario.getRut());
		
		return ruts;
	}
	
	private void validatePeticionSignature(XMLSolicitudPersona peticion, Collection<String> trustedRUTs) {
		try {
			if (!peticion.validate(getSecurityManager().getTrustedCertificates(), trustedRUTs))
				throw new AceptaBeanException("Firma invalida");
		}
		catch (XMLSignatureException exception) {
			throw new AceptaBeanException("Falla al validar la firma de la Solicitud: " + exception.getMessage(), exception);
		}
		catch (CertificateParsingException exception) {
			throw new AceptaBeanException("Falla al validar la firma de la Solicitud: " + exception.getMessage(), exception);
		}
		catch (MarshalException exception) {
			throw new AceptaBeanException("Falla al analizar la firma de la Solicitud", exception);
		}
	}

	private XMLSolicitudPersona createPeticionFromData(byte[] data) {
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(data);
			try {
				XMLSolicitudPersona peticion = new XMLSolicitudPersona(input);

				return peticion;
			}
			finally {
				input.close();
			}
		}
		catch (SAXException exception) {
			throw new AceptaBeanException("Falla al decodificar XML", exception);
		}
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al leer XML", exception);
		}
	}
	
	private Certificado generateCertificateForSolicitud(SolicitudPersona solicitud, String pkcs10Base64, String spkacBase64) {
		try {
			Authority authority = getSecurityManager().getAuthority(solicitud.getAutoridad().getNombre());
			
			PersonaCertificateRequest request = generateCertificateRequestForSolicitud(solicitud, pkcs10Base64, spkacBase64);
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			Certificado certificado = new Certificado();
			
			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(certificate.getSerialNumber());
			certificado.setValidoDesde(certificate.getNotBefore());
			certificado.setValidoHasta(certificate.getNotAfter());
			certificado.setSujeto(request.getSubjectCommonName());
			certificado.setData(certificate.getEncoded());
			
			certificado.changeEstado(Certificado.Estado.VALIDO);
			
			return certificado;
		}
		catch (CertificateEncodingException exception) {
			throw new AceptaBeanException("Falla al serializar Certificado X.509", exception);
		}
	}

	private PersonaCertificateRequest generateCertificateRequestForSolicitud(SolicitudPersona solicitud, String pkcs10Base64, String spkacBase64) {
		Calendar calendar = Calendar.getInstance();
		Date notBefore = calendar.getTime();
		
		calendar.add(Calendar.MONTH, solicitud.getMesesVigencia());
		Date notAfter = calendar.getTime();
		
		PersonaCertificateRequest request = new PersonaCertificateRequest();
		
		request.setSerialNumber(solicitud.getAutoridad().generarNumeroSerieCertificado());
		request.setNotBefore(notBefore);
		request.setNotAfter(notAfter);
		request.setSubjectRUT(solicitud.getTitular().getRut());
		request.setSubjectEmailAddress(solicitud.getTitular().getCorreo());
		request.setSubjectCommonName(solicitud.getTitular().getNombreComun());
		request.setSubjectTitle(solicitud.getTitular().getProfesion());
		request.setSubjectCountry("CL");
		
		if (pkcs10Base64 != null && pkcs10Base64.length() != 0) {
			request.setSubjectPublicKeyFromPKCS10(pkcs10Base64);
		}
		else if (spkacBase64 != null && spkacBase64.length() != 0) {
			request.setSubjectPublicKeyFromSPKAC(spkacBase64);
		}
		else {
			throw new AceptaBeanException("No se especifico PKCS#10 o SPKAC con la llave publica");
		}
		
		return request;
	}
	
	private static void validateSubjectRut(String rut) {
		if (!rut.matches(RUT_REGULAR_EXPRESSION))
			throw new AceptaBeanException("RUT del titular es invalido: " + rut);
	}
	
	private static void validateSubjectCorreo(String correo) {
		if (!correo.matches(EMAIL_ADDRESS_REGULAR_EXPRESSION))
			throw new AceptaBeanException("Direccion de correo del titular es invalida: " + correo);
	}
	
	private static void validateSubjectNombreComun(String nombreComun) {
		if (nombreComun == null || nombreComun.length() == 0)
			throw new AceptaBeanException("Nombre del titular en blanco");
	}
	
	private static String normalizeSubjectRut(String rut) {
		if (rut != null)
			rut = rut.trim().replace(".", "").toUpperCase();
		return rut;
	}
	
	private static String normalizeSubjectCorreo(String correo) {
		if (correo != null)
			correo = correo.trim();
		return correo;
	}

	private static String normalizeSubjectNombre(String nombre) {
		if (nombre != null) {
			// quitar caracteres internacionales (acentos)
			nombre = nombre.replace('\u00E1', 'a').replace('\u00C1', 'A');
			nombre = nombre.replace('\u00E9', 'e').replace('\u00C9', 'E');
			nombre = nombre.replace('\u00ED', 'i').replace('\u00CD', 'I');
			nombre = nombre.replace('\u00F3', 'o').replace('\u00D3', 'O');
			nombre = nombre.replace('\u00FA', 'u').replace('\u00DA', 'U');
			nombre = nombre.replace('\u00F1', 'n').replace('\u00D1', 'N');
			
			nombre = nombre.trim().toUpperCase();
		}
		
		return nombre;
	}
}
