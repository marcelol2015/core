package com.acepta.ca.bean;

import java.math.BigInteger;
import java.security.cert.X509CRL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.NoResultException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.CRL;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLRequest;
import com.acepta.ca.security.crypto.cert.OCSPQueryResponse;
import com.acepta.ca.security.crypto.cert.OCSPQueryService;
import com.acepta.ca.security.crypto.cert.OCSPRequest;
import com.acepta.ca.security.crypto.cert.OCSPResponse;

/**
 * Componente para consultar, revocar, suspender y restablecer Certificados Electronicos X.509
 * emitidos por la Autoridad Certificadora. 
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class CertificadoBean extends AceptaBean {
	public CertificadoBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Devuelve listado con los nombres de las Autoridades emisoras de Certificados Electronicos X.509.
	 * (Se asume que las Autoridades emisoras de Certificados tambien deben firman CRLs y OCSPs)
	 * 
	 * @return	Listado de Nombres de las Autoridades Certificadoras
	 */
	public Collection<String> getNombresAutoridadesCertificadoras() {
		ArrayList<String> nombres = new ArrayList<String>();
		
		for (Authority authority : getSecurityManager().getAuthorities()) {
			if ((authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN) &&
				authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN) &&
				authority.getKeyUsage().contains(Authority.KeyPurpose.OCSP_SIGNING)))
				nombres.add(authority.getAlias());
		}
		
		return nombres;
	}

	/**
	 * Devuelve listado con los nombres de las Autoridades emisoras de Sellos de Tiempo.
	 * 
	 * @return Listado de Nombres de las Autoridades Certificadoras
	 */
	public Collection<String> getNombresAutoridadesSelladoTiempo() {
		ArrayList<String> nombres = new ArrayList<String>();
		
		for (Authority authority : getSecurityManager().getAuthorities()) {
			if (authority.getKeyUsage().contains(Authority.KeyPurpose.TIME_STAMPING))
				nombres.add(authority.getAlias());
		}
		
		return nombres;
	}
	
	/**
	 * Devuelve los Certificados Electronicos X.509 emitidos por una Autoridad Certificadora.
	 * 
	 * @param nombreAutoridad	Nombre de la Autoridad Certificadora
	 * 
	 * @return	Listado de Certificados de la autoridad
	 */
	public Collection<Certificado> buscarCertificadosConEmisor(String nombreAutoridad) {
		try {
			Autoridad emisor = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
			
			return getEntityManager().findCertificadosWithEmisor(emisor);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Certificados X.509 de la autoridad " + nombreAutoridad, exception);
		}
	}

	/**
	 * Devuelve los Certificados Electronico X.509 validos o revocados de una Autoridad Certificadora.
	 * 
	 * @param nombreAutoridad	Nombre de la Autoridad Certificadora
	 * @param estado			Estado de los Certificados (valido o revocado)
	 * 
	 * @return	Listado de Certificados validos o revocados
	 */
	public Collection<Certificado> buscarCertificadosConEmisorEstado(String nombreAutoridad, Certificado.Estado estado) {
		try {
			Autoridad emisor = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
			
			return getEntityManager().findCertificadosWithEmisorEstado(emisor, estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Certificados X.509 de la autoridad " + nombreAutoridad + " en estado " + estado, exception);
		}
	}
	
	/**
	 * Construye cadena de certificados con las autoridades intermedias y raiz.
	 * 
	 * @parm certificado	Certificado inicial.
	 * 
	 * @return Cadena de certificados en formato binario X.509.
	 */
	public Collection<byte[]> construirCadenaParaCertificado(Certificado certificado) {
		try {
			ArrayList<byte[]> certificados = new ArrayList<byte[]>();
			
			certificados.add(certificado.getData());
			
			Authority authority = getSecurityManager().getAuthority(certificado.getEmisor().getNombre());
				
			while (authority != null) {
				certificados.add(authority.getCertificate().getEncoded());
				
				authority = authority.getIssuer();
			}
			
			return certificados;
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar cadena de certificados", exception);
		}
	}

	/**
	 * Revoca o suspende un Certificado Electronico X.509 de una Autoridad Certificadora.
	 * 
	 * @param nombreAutoridad	Emisor del Certificado
	 * @param numeroSerie		Numero de Serie del Certificado
	 * @param razon				Razon de la revocacion
	 * @param comentario		Comentarios adicionales (opcional)
	 */
	public void revocarCertificadoConEmisorNumeroSerie(String nombreAutoridad, BigInteger numeroSerie, Certificado.Razon razon, String comentario) {
		getEntityManager().beginTransaction();
		try {
			Autoridad emisor = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
			
			Certificado certificado = getEntityManager().findCertificadoWithEmisorNumeroSerie(emisor, numeroSerie);
			
			if (!certificado.isValido() && !certificado.isSuspendido())
				throw new AceptaBeanException("Certificado no esta vigente o suspendido");
			
			certificado.changeEstado(Certificado.Estado.REVOCADO, razon);

			getEntityManager().persist(new Cambio("REVOCAR", certificado, comentario));
			
			getEntityManager().commitTransaction();
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();

			throw new AceptaBeanException("Falla al revocar o suspender Certificado X.509: " + exception.getMessage(), exception);
		}
	}
	
	/**
	 * Revoca un Certificado Electronico X.509.
	 * 
	 * @param nombreAutoridad	Emisor del Certificado
	 * @param numeroSerie		Numero de Serie del Certificado
	 * @param comentario		Comentarios adicionales (Opcional)
	 */
	public void revocarCertificadoConEmisorNumeroSerie(String nombreAutoridad, BigInteger numeroSerie, String comentario) {
		revocarCertificadoConEmisorNumeroSerie(nombreAutoridad, numeroSerie, Certificado.Razon.UNUSED, comentario);
	}
	
	/**
	 * Suspende un Certificado Electronico X.509.
	 * 
	 * @param nombreAutoridad	Emisor del Certificado
	 * @param numeroSerie		Numero de Serie del Certificado
	 * @param comentario		Comentarios adicionales (Opcional)
	 */
	public void suspenderCertificadoConEmisorNumeroSerie(String nombreAutoridad, BigInteger numeroSerie, String comentario) {
		revocarCertificadoConEmisorNumeroSerie(nombreAutoridad, numeroSerie, Certificado.Razon.CERTIFICATE_HOLD, comentario);
	}

	/**
	 * Restablece un Certificado Electronico X.509 suspendido.
	 * 
	 * @param nombreAutoridad	Emisor del Certificado
	 * @param numeroSerie		Numero de serie del Certificado
	 * @param comentario		Comentarios adicionales (Opcional)
	 */
	public void restablecerCertificadoConEmisorNumeroSerie(String nombreAutoridad, BigInteger numeroSerie, String comentario) {
		getEntityManager().beginTransaction();
		try {
			Autoridad emisor = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
			
			Certificado certificado = getEntityManager().findCertificadoWithEmisorNumeroSerie(emisor, numeroSerie);
			
			if (!certificado.isSuspendido())
				throw new AceptaBeanException("Certificado no esta suspendido");
			
			certificado.changeEstado(Certificado.Estado.VALIDO);
			
			getEntityManager().persist(new Cambio("RESTABLECER", certificado, comentario));

			getEntityManager().commitTransaction();
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al restablecer Certificado X.509: " + exception.getMessage(), exception);
		}
	}

	/**
	 * Buscar ultimo Listado de Revocaciones de Certificados (CRL) de una Autoridad Certificadora.
	 * 
	 * @param nombreAutoridad	Nombre de la Autoridad
	 * 
	 * @return Listado de Revocaciones de Certificados
	 */
	public CRL buscarUltimaCRLConEmisor(String nombreAutoridad) {
		try {
			Autoridad emisor = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
			
			return getEntityManager().findLatestCRLWithEmisor(emisor);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar CRL X.509: " + exception.getMessage(), exception);
		}
	}
	
	/**
	 * Genera Listado de Revocaciones de Certificados (CRL) de una Autoridad Certificadora.
	 * 
	 * @param nombreAutoridad	Nombre de la Autoridad Certificadora
	 * @param horasVigencia		Horas de vigencia de la CRL
	 * 
	 * @return	Listado de Revocaciones de Certificados
	 */
	public CRL generarCRLConEmisorVigencia(String nombreAutoridad, int horasVigencia) {
		getEntityManager().beginTransaction();
		try {
			Autoridad emisor = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
			
			BigInteger numeroSerie = emisor.generarNumeroSerieCRL();
			
			CRLRequest request = generateCRLRequest(emisor, numeroSerie, horasVigencia);
			
			X509CRL x509CRL = generateCRL(emisor, request);
			
			CRL crl = new CRL(emisor, numeroSerie, x509CRL.getThisUpdate(), x509CRL.getNextUpdate(), x509CRL.getEncoded());
			
			getEntityManager().persist(crl);
			
			getEntityManager().commitTransaction();
			
			return crl;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al generar CRL X.509: " + exception.getMessage(), exception);
		}
	}
	
	/**
	 * Generar respuesta OCSP de una Autoridad Certificadora.
	 *
	 * @param nombreAutoridad	Nombre de la Autoridad Certificadora
	 * @param peticion			Peticion OCSP
	 * 
	 * @return	Respuesta OCSP
	 */
	public byte[] generarOCSP(String nombreAutoridad, byte[] peticion) {
		Autoridad autoridad = getEntityManager().findAutoridadWithNombre(nombreAutoridad);
		
		OCSPRequest request = generateOCSPRequest(autoridad, peticion);
		
		OCSPResponse response = generateOCSPResponse(autoridad, request);
		
		return response.getEncoded();
	}

	private OCSPRequest generateOCSPRequest(final Autoridad autoridad, byte[] peticion) {
		Date thisUpdate = new Date();

		OCSPRequest request = new OCSPRequest();
		
		request.setThisUpdate(thisUpdate);
		request.setEncoded(peticion);
		
		request.setQueryService(new OCSPQueryService() {
			public OCSPQueryResponse queryCertificateStatus(BigInteger serialNumber) {
				try {
					Certificado certificado = getEntityManager().findCertificadoWithEmisorNumeroSerie(autoridad, serialNumber);
					
					switch (certificado.getEstado()) {
					case VALIDO:
						return new OCSPQueryResponse(OCSPQueryResponse.Status.GOOD);
					
					case REVOCADO:
						return new OCSPQueryResponse(OCSPQueryResponse.Status.REVOKED, certificado.getFecha(), certificado.getRazon().ordinal());
					}
					
					throw new AceptaBeanException("Certificado en estado desconocido");
				}
				catch (NoResultException exception) {
					return new OCSPQueryResponse(OCSPQueryResponse.Status.UNKNOWN);
				}
			}
		});
		
		return request;
	}
	
	private OCSPResponse generateOCSPResponse(Autoridad autoridad, OCSPRequest request) {
		Authority authority = getSecurityManager().getAuthority(autoridad.getNombre());
		
		return authority.generateOCSP(request);
	}
	
	private CRLRequest generateCRLRequest(Autoridad autoridad, BigInteger numeroSerie, int horasVigencia) {
		Calendar calendar = Calendar.getInstance();
		Date thisUpdate = calendar.getTime();
		
		calendar.add(Calendar.HOUR, horasVigencia);
		Date nextUpdate = calendar.getTime();
		
		CRLRequest request = new CRLRequest();
		
		request.setThisUpdate(thisUpdate);
		request.setNextUpdate(nextUpdate);
	
		request.setCRLNumber(numeroSerie);
		
		for (Certificado certificado : getEntityManager().findCertificadosWithEmisorEstado(autoridad, Certificado.Estado.REVOCADO))
			request.addEntry(certificado.getNumeroSerie(), certificado.getFecha(), certificado.getRazon().ordinal());
		
		return request;
	}

	private X509CRL generateCRL(Autoridad autoridad, CRLRequest request) {
		Authority authority = getSecurityManager().getAuthority(autoridad.getNombre());

		return authority.generateCRL(request);
	}
}
