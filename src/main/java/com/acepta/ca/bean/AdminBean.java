package com.acepta.ca.bean;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Grupo;
import com.acepta.ca.persistence.NumeroSerie;
import com.acepta.ca.persistence.Usuario;
import com.acepta.ca.security.AceptaSecurityException;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;
import com.acepta.ca.security.authority.Authority;

/**
 * Componente para inicializar y administrar los usuarios de la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.1, 2008-10-06
 */
public class AdminBean extends AceptaBean {
	private AceptaSecurityManagerFactory securityManagerFactory;
	
	public AdminBean(AceptaSecurityManagerFactory securityManagerFactory, AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
		setSecurityManagerFactory(securityManagerFactory);
	}
	
	protected AceptaSecurityManagerFactory getSecurityManagerFactory() {
		return securityManagerFactory;
	}

	protected void setSecurityManagerFactory(AceptaSecurityManagerFactory securityManagerFactory) {
		this.securityManagerFactory = securityManagerFactory;
	}

	/**
	 * Inicializa las Llaves Privadas RSA, Certificados Electronicos X.509, y la Unidad de Persistencia
	 * de la Autoridad Certificadora de Acepta.com S.A.
	 * 
	 * @param initializeSecurity	Verdadero para inicializar el Proveedor Criptografico
	 * @param initializePersistence	Verdadero para inicializar la Unidad de Persistencia
	 */
	public void bootstrap(boolean initializeSecurity, boolean initializePersistence) {
		if (initializeSecurity)
			bootstrapSecurityManager();
		
		if (initializePersistence)
			bootstrapEntityManager();
	}

	/**
	 * Actualiza las Llaves Privadas RSA, Certificados Electronicos X.509, y la Unidad de Persistencia
	 * de la Autoridad Certificadora de Acepta.com S.A.
	 * 
	 * @param updateSecurity	Verdadero para actualizar el Proveedor Criptografico
	 * @param updatePersistence	Verdadero para actualizar la Unidad de Persistencia
	 */
	public void update(boolean updateSecurity, boolean updatePersistence) {
		if (updateSecurity)
			updateSecurityManager();
		
		if (updatePersistence)
			updateEntityManager();
	}
	
	
	/**
	 * Devuelve los Certificados Electronicos X.509 de todas las Autoridades Certificadoras.
	 * 
	 * @return	Listado de Certificados Electronicos X.509
	 */
	public Collection<X509Certificate> findCertificadosAutoridades() {
		ArrayList<X509Certificate> autoridades = new ArrayList<X509Certificate>();
		
		for (Authority authority : getSecurityManager().getAuthorities())
			autoridades.add(authority.getCertificate());
		
		return autoridades;
	}
	
	/**
	 * Devuelve los Certificados Electronicos X.509 dado el nombre de la autoridad
	 * 
	 * @return	Certificados Electronico X.509 de la autoridad especificada
	 */
	public X509Certificate findCertificadoAutiridad(String nombreAutoridad) {
		return getSecurityManager().getAuthority(nombreAutoridad).getCertificate();
	}

	/**
	 * Agrega un nuevo Usuario a la Autoridad Certificadora.
	 * 
	 * @param rut		RUT del Usuario
	 * @param nombre	Nombre del Usuario
	 * @param grupos	Grupos a los que pertenece el Usuario
	 */
	public void addUsuario(String rut, String nombre, String... grupos) {
		getEntityManager().beginTransaction();
		try {
			Usuario usuario = new Usuario(rut, nombre);
			
			if (grupos != null)
				usuario.setGrupos(findGruposWithNombres(grupos));
			
			getEntityManager().persist(usuario);
			
			getEntityManager().commitTransaction();
		}
		catch (PersistenceException exception) {
			getEntityManager().rollbackTransaction();

			throw new AceptaBeanException("Falla al agregar usuario", exception);
		}
	}
	
	/**
	 * Elimina un Usuario de la Autoridad Certificadora.
	 * 
	 * @param rut	RUT del Usuario
	 */
	public void removeUsuario(String rut) {
		getEntityManager().beginTransaction();
		try {
			Usuario usuario = getEntityManager().findUsuarioWithRut(rut);
			
			getEntityManager().remove(usuario);
			
			getEntityManager().commitTransaction();
		}
		catch (PersistenceException exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al eliminar usuario", exception);
		}
	}

	/**
	 * Modifica el nombre y grupo de un Usuario de la Autoridad Certificadora.
	 * 
	 * @param rut		RUT del Usuario
	 * @param nombre	Nuevo nombre del Usuario
	 * @param grupos	Nuevo listado de Grupos del Usuario
	 */
	public void modifyUsuario(String rut, String nombre, String... grupos) {
		getEntityManager().beginTransaction();
		try {
			Usuario usuario = getEntityManager().findUsuarioWithRut(rut);
			
			if (nombre != null)
				usuario.setNombre(nombre);
			
			if (grupos != null)
				usuario.setGrupos(findGruposWithNombres(grupos));
				
			getEntityManager().commitTransaction();
		}
		catch (PersistenceException exception) {
			getEntityManager().rollbackTransaction();
	
			throw new AceptaBeanException("Falla al modificar usuario", exception);
		}
	}

	/**
	 * Devuelve listado de los Usuarios de la Autoridad Certificadora.
	 * 
	 * @return	Listado de Usuarios
	 */
	public Collection<Usuario> findUsuarios() {
		try {
			return getEntityManager().findUsuarios();
		}
		catch (PersistenceException exception) {
			throw new AceptaBeanException("Falla al buscar usuarios", exception);
		}
	}

	/**
	 * Devuelve listado de los Grupos de la Autoridad Certificadora.
	 * 
	 * @return	Listado de Grupos
	 */
	public Collection<Grupo> findGrupos() {
		try {
			return getEntityManager().findGrupos();
		}
		catch (PersistenceException exception) {
			throw new AceptaBeanException("Falla al buscar grupos", exception);
		}
	}
	
	/**
	 * HACK: Agregar autoridad a la lista de confianza del token (solo se ocupa en las unidades de prueba).
	 * 
	 * @param authority	Nueva autoridad confiable
	 */
	public void __addTrustedAuthority(Authority authority) {
		getSecurityManagerFactory().__addTrustedAuthority(authority);
	}
	
	private Collection<Grupo> findGruposWithNombres(String... nombres) {
		Collection<Grupo> grupos = new ArrayList<Grupo>();
		
		for (String nombre : nombres)
			grupos.add(getEntityManager().findGrupoWithNombre(nombre));
		
		return grupos;
	}
	
	private void bootstrapSecurityManager() {
		try {
			getSecurityManagerFactory().bootstrap();
		}
		catch (AceptaSecurityException exception) {
			throw new AceptaBeanException("Falla al inicializar el proveedor criptografico", exception);
		}
	}

	private void updateSecurityManager() {
		try {
			getSecurityManagerFactory().update();
		}
		catch (AceptaSecurityException exception) {
			throw new AceptaBeanException("Falla al actualizar el proveedor criptografico", exception);
		}
	}
	
	private void bootstrapEntityManager() {
		getEntityManager().beginTransaction();
		try {
			for (Authority authority : getSecurityManager().getAuthorities()) {
				getEntityManager().persist(createAutoridad(authority));
				
				getEntityManager().persist(new Cambio("BOOTSTRAP", null, null, "Creacion de la Autoridad: " + authority.getAlias()));
			}
			
			for (String nombreGrupo : getNombresGrupos()) {
				getEntityManager().persist(new Grupo(nombreGrupo));
				
				getEntityManager().persist(new Cambio("BOOTSTRAP", null, null, "Creacion del Grupo: " + nombreGrupo));
			}
			
			getEntityManager().commitTransaction();
		}
		catch (PersistenceException exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al inicializar la unidad de persistencia", exception);
		}
	}

	private void updateEntityManager() {
		getEntityManager().beginTransaction();
		try {
			for (Authority authority : getSecurityManager().getAuthorities()) {
				try{
					getEntityManager().findAutoridadWithNombre(authority.getAlias());
				}catch(NoResultException exception){
					getEntityManager().persist(createAutoridad(authority));
					getEntityManager().persist(new Cambio("UPDATE", null, null, "Creacion de la Autoridad: " + authority.getAlias()));					
				}	
			}
			
			for (String nombreGrupo : getNombresGrupos()) {
				try{
					getEntityManager().findGrupoWithNombre(nombreGrupo);
				}catch (NoResultException e) {				
					getEntityManager().persist(new Grupo(nombreGrupo));
					getEntityManager().persist(new Cambio("UPDATE", null, null, "Creacion del Grupo: " + nombreGrupo));
				}
			}
			
			getEntityManager().commitTransaction();
		}
		catch (PersistenceException exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al actualizar la unidad de persistencia", exception);
		}
	}

	private Autoridad createAutoridad(Authority authority) {
		Autoridad autoridad = new Autoridad(authority.getAlias(), authority.getLegalProtection() == Authority.LegalProtection.FIRMA_AVANZADA);
		
		autoridad.setNumerosSeries(new ArrayList<NumeroSerie>());
		
		if (authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN))
			autoridad.getNumerosSeries().add(new NumeroSerie(autoridad, NumeroSerie.Proposito.CERTIFICADO, authority.getInitialSerialNumber()));
		
		if (authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN))
			autoridad.getNumerosSeries().add(new NumeroSerie(autoridad, NumeroSerie.Proposito.CRL, authority.getInitialSerialNumber()));

		if (authority.getKeyUsage().contains(Authority.KeyPurpose.TIME_STAMPING))
			autoridad.getNumerosSeries().add(new NumeroSerie(autoridad, NumeroSerie.Proposito.SELLOTIEMPO, authority.getInitialSerialNumber()));

		return autoridad;
	}

	private Collection<String> getNombresGrupos() {
		return Arrays.asList(
			ACEPTA_GRUPO_PERSONA_OPERADOR_REGISTRO, ACEPTA_GRUPO_PERSONA_OPERADOR_VALIDACION,
			ACEPTA_GRUPO_SITIO_WEB_OPERADOR_REGISTRO, ACEPTA_GRUPO_SITIO_WEB_OPERADOR_VALIDACION,
			ACEPTA_GRUPO_SISTEMA_OPERADOR_REGISTRO, ACEPTA_GRUPO_SISTEMA_OPERADOR_VALIDACION,
			ACEPTA_GRUPO_CODIGO_OPERADOR_REGISTRO, ACEPTA_GRUPO_CODIGO_OPERADOR_VALIDACION,
				ACEPTA_GRUPO_PERSONA_OPERADOR_REGISTRO_AUTOMATA);
	}

	private Collection<String> getNombresTipoRegistro() {
		return Arrays.asList(
				ACEPTA_TIPO_REGISTRO_ONLINE_CLASE2,ACEPTA_TIPO_REGISTRO_ONLINE_CLAVEUNICA,
				ACEPTA_TIPO_REGISTRO_PRESENCIAL_CEDULANUEVA,ACEPTA_TIPO_REGISTRO_PRESENCIAL_MANUAL);
	}
}
