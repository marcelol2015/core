package com.acepta.ca.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import javax.persistence.NoResultException;

import org.xml.sax.SAXException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.CAF;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCAF;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.CAFCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.util.PEM;
import com.acepta.ca.security.util.PKCS12;
import com.acepta.ca.xml.XMLAutorizacion;

/**
 * Componente para consultar y emitir Certificados Electronicos de CAF en la Autoridad Certificadora.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class SolicitudCAFBean extends AceptaBean {
	public SolicitudCAFBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Devuelve la Solicitud correspondiente al CAF de un DTE determinado.  
	 * 
	 * @param rutEmpresa	RUT del emisor del DTE
	 * @param tipoDTE		Tipo de DTE
	 * @param folioDTE		Folio del DTE
	 * 
	 * @return	Solicitud con el CAF del DTE
	 */
	public SolicitudCAF buscarSolicitudCAFConRutTipoFolio(String rutEmpresa, int tipoDTE, long folioDTE) {
		try {
			return getEntityManager().findSolicitudCAFWithRutTipoFolio(rutEmpresa, tipoDTE, folioDTE);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe la Solicitud CAF del DTE " + rutEmpresa + " " + tipoDTE + " " + folioDTE, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitud CAF del DTE " + rutEmpresa + " " + tipoDTE + " " + folioDTE, exception);
		}
	}
	
	/**
	 * Devuelve las Solicitudes con los CAFs de una empresa.
	 * 
	 * @param rutEmpresa	RUT de la empresa
	 * 
	 * @return	Listado de Solicitudes con los CAFs de una empresa
	 */
	public Collection<SolicitudCAF> buscarSolicitudesCAFConRutEmpresa(String rutEmpresa) {
		try {
			return getEntityManager().findSolicitudesCAFWithRutEmpresa(rutEmpresa);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes CAF de la empresa con RUT " + rutEmpresa, exception);
		}
	}

	/**
	 * Emite el Certificado Electronico X.509 correspondiente a una Autorizacion de CAF. 
	 * 
	 * @param data		Documento XML con la Autorizacion de CAF		
	 * @param clave		Contrasena para proteger la llave privada del CAF (opcional)
	 * 
	 * @return	Certificado X.509 y llave privada RSA del CAF en formato PEM
	 */
	public CAFCertificateBag emitirCertificadoCAFEnFormatoPEM(byte[] data, String clave) {
		return emitirCertificadoCAF(data, clave, CAFCertificateBag.Format.PEM);
	}
	
	/**
	 * Emite el Certificado Electronico X.509 correspondiente a una Autorizacion de CAF.
	 * 
	 * @param data		Documento XML con la Autorizacion de CAF
	 * @param clave		Contrasena para proteger la llave privada del CAF
	 * 
	 * @return	Certificado X.509 y llave privada RSA del CAF en formato PKCS#12
	 */
	public CAFCertificateBag emitirCertificadoCAFEnFormatoPKCS12(byte[] data, String clave) {
		return emitirCertificadoCAF(data, clave, CAFCertificateBag.Format.PKCS12);
	}

	private CAFCertificateBag emitirCertificadoCAF(byte[] data, String clave, CAFCertificateBag.Format format) {
		getEntityManager().beginTransaction();
		try {
			XMLAutorizacion autorizacion = createAutorizacionFromData(data);

			KeyPair keyPair = createKeyPairFromAutorizacion(autorizacion);

			SolicitudCAF solicitud = createSolicitudFromAutorizacion(autorizacion);
			
			X509Certificate[] certificates = generateCertificateForSolicitud(solicitud, keyPair.getPublic(), autorizacion.getCAF());
			
			CAFCertificateBag bag = createCAFBagForPrivateKeyCertificate(solicitud.getCAF(), format, keyPair.getPrivate(), clave, certificates);
			
			getEntityManager().persist(solicitud);

			getEntityManager().persist(new Documento(solicitud, autorizacion.getCAF()));
			
			getEntityManager().persist(new Cambio("GENERAR", solicitud));
			
			getEntityManager().commitTransaction();
			
			return bag;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();

			throw new AceptaBeanException("Falla al emitir Certificado CAF: " + exception.getMessage(), exception);
		}
	}

	private CAFCertificateBag createCAFBagForPrivateKeyCertificate(CAF caf, CAFCertificateBag.Format format, PrivateKey privateKey, String clave, X509Certificate[] certificates) {
		CAFCertificateBag bag = new CAFCertificateBag(caf, format);
		
		if (bag.isPEM())
			bag.setData(generatePEMForCertificate(privateKey, clave, certificates));
		else
			bag.setData(generatePKCS12ForCertificate(privateKey, clave, certificates));
		
		return bag;
	}

	private byte[] generatePKCS12ForCertificate(PrivateKey privateKey, String clave, X509Certificate[] certificates) {
		try {
			if (clave == null || clave.length() == 0)
				throw new AceptaBeanException("Clave del Certificado PKCS#12 es obligatoria");
			
			return PKCS12.encodeCertificate(privateKey, clave, certificates);
		}
		catch (GeneralSecurityException exception) {
			throw new AceptaBeanException("Falla al codificar Certificado PKCS#12", exception);
		}
	}

	private byte[] generatePEMForCertificate(PrivateKey privateKey, String clave, X509Certificate[] certificates) {
		try {
			StringBuffer buffer = new StringBuffer();
			
			buffer.append(PEM.encodeRSAPrivateKey(privateKey, clave));
			
			for (X509Certificate certificate : certificates)
				buffer.append(PEM.encodeX509Certificate(certificate));
			
			return buffer.toString().getBytes();
		}
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al codificar Certificado PEM", exception);
		}
	}
	
	private SolicitudCAF createSolicitudFromAutorizacion(XMLAutorizacion autorizacion) {
		SolicitudCAF solicitud = new SolicitudCAF();
		
		solicitud.setCAF(createCAFFromAutorizacion(autorizacion));
		
		solicitud.changeEstado(Solicitud.Estado.PENDIENTE);
		
		solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_CAF));
		
		return solicitud;
	}

	private CAF createCAFFromAutorizacion(XMLAutorizacion autorizacion) {
		try {
			CAF caf = new CAF();
	
			caf.setRutEmpresa(autorizacion.getRutEmpresa());
			caf.setRazonSocial(autorizacion.getRazonSocial());
			caf.setTipoDTE(autorizacion.getTipoDTE());
			caf.setDesdeFolio(autorizacion.getDesdeFolio());
			caf.setHastaFolio(autorizacion.getHastaFolio());
			caf.setFechaAutorizacion(autorizacion.getFechaAutorizacion());
			caf.setLlaveID(autorizacion.getLlaveID());
			
			return caf;
		}
		catch (ParseException exception) {
			throw new AceptaBeanException("Falla al decodificar Fecha de Autorizacion", exception);
		}
		catch (NumberFormatException exception) {
			throw new AceptaBeanException("Falla al decodificar tipo, folios o llave de la Autorizacion", exception);
		}
	}
	
	private X509Certificate[] generateCertificateForSolicitud(SolicitudCAF solicitud, PublicKey publicKey, byte[] caf) {
		try {
			Authority authority = getSecurityManager().getAuthority(solicitud.getAutoridad().getNombre());

			Date notBefore = new Date();
			Date notAfter = authority.getCertificate().getNotAfter();

			CAFCertificateRequest request = generateCertificateRequestForSolicitud(solicitud, publicKey, caf, notBefore, notAfter);
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			Certificado certificado = new Certificado();
			
			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(certificate.getSerialNumber());
			certificado.setValidoDesde(certificate.getNotBefore());
			certificado.setValidoHasta(certificate.getNotAfter());
			certificado.setSujeto(request.getSubjectName().getCommonName());
			certificado.setData(certificate.getEncoded());
			
			certificado.changeEstado(Certificado.Estado.VALIDO);

			solicitud.setCertificado(certificado);
			
			solicitud.changeEstado(Solicitud.Estado.CERRADA);

			return new X509Certificate[] { certificate, authority.getCertificate(), authority.getIssuer().getCertificate() };
		}
		catch (CertificateEncodingException exception) {
			throw new AceptaBeanException("Falla al serializar Certificado X.509", exception);
		}
	}
	
	private CAFCertificateRequest generateCertificateRequestForSolicitud(SolicitudCAF solicitud, PublicKey publicKey, byte[] caf, Date notBefore, Date notAfter) {
		CAFCertificateRequest request = new CAFCertificateRequest();

		request.setSerialNumber(solicitud.getAutoridad().generarNumeroSerieCertificado());
		request.setNotBefore(notBefore);
		request.setNotAfter(notAfter);
		
		request.setSubjectRUT(solicitud.getCAF().getRutEmpresa());
		request.setSubjectRazonSocial(solicitud.getCAF().getRazonSocial());
		request.setSubjectTipoDTE(solicitud.getCAF().getTipoDTE());
		request.setSubjectDesdeFolio(solicitud.getCAF().getDesdeFolio());
		request.setSubjectHastaFolio(solicitud.getCAF().getHastaFolio());
		request.setSubjectLlaveID(solicitud.getCAF().getLlaveID());
		request.setSubjectCAF(caf);
		request.setSubjectPublicKey(publicKey);
		
		return request;
	}

	private KeyPair createKeyPairFromAutorizacion(XMLAutorizacion autorizacion) {
		try {
			PublicKey publicKey = PEM.decodeRSAPublicKey(autorizacion.getRSAPublicKey());
			PrivateKey privateKey = PEM.decodeRSAPrivateKey(autorizacion.getRSAPrivateKey());
			
			if (publicKey == null || privateKey == null)
				throw new IOException();
			
			return new KeyPair(publicKey, privateKey);
		}		
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al decodificar llaves RSA", exception);
		}
	}
	
	private XMLAutorizacion createAutorizacionFromData(byte[] data) {
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(data);
			try {
				XMLAutorizacion autorizacion = new XMLAutorizacion(input);
				
				if (!autorizacion.getVersion().equals(XMLAutorizacion.VERSION))
					throw new AceptaBeanException("Version del CAF no soportada: " + autorizacion.getVersion());
				
				return autorizacion;
			}
			finally {
				input.close();
			}
		}
		catch (SAXException exception) {
			throw new AceptaBeanException("Falla al decodificar XML", exception);
		}
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al leer XML", exception);
		}
	}
}
