package com.acepta.ca.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.NoResultException;

import org.xml.sax.SAXException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Sistema;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudSistema;
import com.acepta.ca.persistence.Usuario;
import com.acepta.ca.security.AceptaSecurityException;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.SistemaCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.xml.XMLSolicitudSistema;

/**
 * Componente ahueonada para emitir cualquier hueavada que le pidan. Copia de SitioWebBean.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-05
 */
public class SolicitudSistemaBean extends AceptaBean {
	public SolicitudSistemaBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Busca una Solicitud de Certificado por codigo.
	 * 
	 * @param codigo	Codigo de la Solicitud
	 * 
	 * @return	Solicitud de Certificado
	 */
	public SolicitudSistema buscarSolicitudSistemaConCodigo(String codigo) {
		try {
			return getEntityManager().findSolicitudSistemaWithCodigo(codigo);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe la Solicitud No. " + codigo, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitud No. " + codigo, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes de Certificados pendientes, aprobadas, rechazadas o cerradas.
	 * 
	 * @param estado	Estado de las Solicitudes
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudSistema> buscarSolicitudesSistemaConEstado(Solicitud.Estado estado) {
		try {
			return getEntityManager().findSolicitudesSistemaWithEstado(estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes en estado " + estado, exception);
		}
	}
	
	/**
	 * Devuelve las Solicitudes de Certificados para los Sistemas con un nombre.
	 * 
	 * @param nombre	Nombre del Sistema
	 * 
	 * @return	Listado de Solicitudes del sistema
	 */
	public Collection<SolicitudSistema> buscarSolicitudesSistemaConNombre(String nombre) {
		try {
			return getEntityManager().findSolicitudesSistemaWithNombre(nombre);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes con nombre " + nombre, exception);
		}
	}
	
	/**
	 * Ingresar una nueva Solicitud y emitir el Certificado Electronico X.509 correspondiente.
	 * 
	 * @param data	Documento XML con la Solicitud
	 * 
	 * @return	Solicitud de Sistema con su Certificado Electronico X.509
	 */
	public SolicitudSistema ingresarSolicitudSistema(byte[] data) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudSistema peticion = createPeticionFromData(data);
			
			validateEsquemaPeticion(peticion);
			validateFirmaOperadorValidacionDePeticion(peticion);

			SolicitudSistema solicitud = createSolicitudFromPeticion(peticion);

			Certificado certificado = generateCertificadoForSolicitud(solicitud, peticion.getPKCS10());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			getEntityManager().persist(solicitud);
			
			getEntityManager().persist(new Documento(solicitud, data));

			getEntityManager().persist(new Cambio("GENERAR", solicitud));
			
			getEntityManager().commitTransaction();

			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al ingresar Solicitud: " + exception.getMessage(), exception);
		}
	}
	
	private Certificado generateCertificadoForSolicitud(SolicitudSistema solicitud, String pkcs10Base64) {
		try {
			Authority authority = getSecurityManager().getAuthority(solicitud.getAutoridad().getNombre());

			SistemaCertificateRequest request = generateCertificateRequestForSolicitud(solicitud, pkcs10Base64);

			X509Certificate certificate = authority.generateCertificate(request);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(certificate.getSerialNumber());
			certificado.setValidoDesde(certificate.getNotBefore());
			certificado.setValidoHasta(certificate.getNotAfter());
			certificado.setSujeto(request.getSubjectName().getCommonName());
			certificado.setData(certificate.getEncoded());

			certificado.changeEstado(Certificado.Estado.VALIDO);

			return certificado;
		}
		catch (CertificateEncodingException exception) {
			throw new AceptaBeanException("Falla al serializar Certificado X.509", exception);
		}
		catch (AceptaSecurityException exception) {
			throw new AceptaBeanException("Falla al generar Certificado X.509", exception);
		}
	}
	
	private SistemaCertificateRequest generateCertificateRequestForSolicitud(SolicitudSistema solicitud, String pkcs10Base64) {
		Calendar calendar = Calendar.getInstance();
		Date notBefore = calendar.getTime();

		calendar.add(Calendar.MONTH, solicitud.getMesesVigencia());
		Date notAfter = calendar.getTime();

		SistemaCertificateRequest request = new SistemaCertificateRequest();

		request.setSerialNumber(solicitud.getAutoridad().generarNumeroSerieCertificado());
		request.setNotBefore(notBefore);
		request.setNotAfter(notAfter);
		request.setSubjectRUT(solicitud.getSistema().getRut());

		// NOTA: No se esta agregando el RUT de la empresa en la peticion.
		// NOTA2: se agrega RUT de la empresa a la petición.
		request.setSubjectName(
				solicitud.getSistema().getRut(),
				solicitud.getSistema().getNombre(),
				solicitud.getSistema().getUnidadOrganizacional(),
				solicitud.getSistema().getOrganizacion(),
				solicitud.getSistema().getCiudad(),
				solicitud.getSistema().getEstado(),
				solicitud.getSistema().getPais());

		request.setSubjectPublicKeyFromPKCS10(pkcs10Base64);

		request.setCertificatePolicies(solicitud.getSistema().getPoliticas().replace(" ", "").split(","));
		
		return request;
	}

	private SolicitudSistema createSolicitudFromPeticion(XMLSolicitudSistema peticion) {
		SolicitudSistema solicitud = new SolicitudSistema();

		solicitud.setNumero(peticion.getNumeroSolicitud());
		solicitud.setMesesVigencia(peticion.getMesesVigencia());

		Sistema sistema = createSistemaFromPeticion(peticion);

		solicitud.setSistema(sistema);

		solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_SISTEMA));

		return solicitud;
	}

	private Sistema createSistemaFromPeticion(XMLSolicitudSistema peticion) {
		Sistema sistema = new Sistema();

		sistema.setNombre(peticion.getNombre());
		
		sistema.setRut(peticion.getRut());
		sistema.setUnidadOrganizacional(peticion.getUnidadOrganizacional());
		sistema.setOrganizacion(peticion.getOrganizacion());
		
		sistema.setEstado(peticion.getEstado());
		sistema.setCiudad(peticion.getCiudad());
		sistema.setPais(peticion.getPais());

		sistema.setPoliticas(peticion.getPoliticasCertificacion());
		
		return sistema;
	}	

	private void validateEsquemaPeticion(XMLSolicitudSistema peticion) {
		try {
			if (peticion.getNombre().equals(""))
				throw new AceptaBeanException("Falta el nombre");
			
			if (peticion.getRut().equals(""))
				throw new AceptaBeanException("Falta el RUT");
			
			if (peticion.getOrganizacion().equals(""))
				throw new AceptaBeanException("Falta el nombre de la organizacion");
			
			if (peticion.getPais().equals(""))
				throw new AceptaBeanException("Falta el pais");
			
			if (peticion.getPoliticasCertificacion().equals(""))
				throw new AceptaBeanException("Faltan las politicas de certificacion");
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Solicitud con esquema invalido: " + exception.getMessage());
		}
	}
	private void validateFirmaOperadorValidacionDePeticion(XMLSolicitudSistema peticion) {
		validatePeticionSignature(peticion, getRUTsDeGrupoUsuarios(ACEPTA_GRUPO_SISTEMA_OPERADOR_VALIDACION));
	}

	private Collection<String> getRUTsDeGrupoUsuarios(String nombreGrupo) {
		Collection<String> ruts = new ArrayList<String>();
		
		for (Usuario usuario : getEntityManager().findUsuariosWithGrupo(nombreGrupo))
			ruts.add(usuario.getRut());
		
		return ruts;
	}

	private void validatePeticionSignature(XMLSolicitudSistema peticion, Collection<String> trustedRUTs) {
		try {
			if (!peticion.validate(getSecurityManager().getTrustedCertificates(), trustedRUTs))
				throw new AceptaBeanException("Firma invalida");
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al validar la firma de la Solicitud: " + exception.getMessage(), exception);
		}
	}
	
	private XMLSolicitudSistema createPeticionFromData(byte[] data) {
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(data);
			try {
				return new XMLSolicitudSistema(input);
			}
			finally {
				input.close();
			}
		}
		catch (SAXException exception) {
			throw new AceptaBeanException("Falla al decodificar XML", exception);
		}
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al leer XML", exception);
		}
	}
}
