package com.acepta.ca.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.NoResultException;

import org.xml.sax.SAXException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Cambio;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Compania;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigo;
import com.acepta.ca.persistence.Usuario;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.CodigoCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.xml.XMLSolicitudCodigo;

/**
 * Componente para consultar e ingresar Solicitudes de Certificados para Firma de Codigo,
 * y para emitir los Certificados Electronicos X.509 correspondientes.
 *  
 * @author Carlos Hasan
 * @version 1.0, 2008-10-09
 */
public class SolicitudCodigoBean extends AceptaBean {
	public SolicitudCodigoBean(AceptaEntityManager entityManager, AceptaSecurityManager securityManager) {
		super(entityManager, securityManager);
	}

	/**
	 * Busca una Solicitud de Certificado por codigo.
	 * 
	 * @param codigo	Codigo de la Solicitud
	 * 
	 * @return	Solicitud de Certificado
	 */
	public SolicitudCodigo buscarSolicitudCodigoConCodigo(String codigo) {
		try {
			return getEntityManager().findSolicitudCodigoWithCodigo(codigo);
		}
		catch (NoResultException exception) {
			throw new AceptaBeanException("No existe la Solicitud No. " + codigo, exception);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitud No. " + codigo, exception);
		}
	}

	/**
	 * Devuelve las Solicitudes de Certificados pendientes, aprobadas, rechazadas o cerradas.
	 * 
	 * @param estado	Estado de las Solicitudes
	 * 
	 * @return	Listado de Solicitudes
	 */
	public Collection<SolicitudCodigo> buscarSolicitudesCodigoConEstado(Solicitud.Estado estado) {
		try {
			return getEntityManager().findSolicitudesCodigoWithEstado(estado);
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al buscar Solicitudes en estado " + estado, exception);
		}
	}

	/**
	 * Ingresar una nueva Solicitud y emitir el Certificado Electronico X.509.
	 * 
	 * @param data	Documento XML con la Solicitud
	 * 
	 * @return	Solicitud con su Certificado Electronico X.509
	 */
	public SolicitudCodigo ingresarSolicitudCodigo(byte[] data) {
		getEntityManager().beginTransaction();
		try {
			XMLSolicitudCodigo peticion = createPeticionFromData(data);
			
			validateEsquemaPeticion(peticion);
			validateFirmaOperadorValidacionDePeticion(peticion);

			SolicitudCodigo solicitud = createSolicitudFromPeticion(peticion);

			Certificado certificado = generateCertificadoForSolicitud(solicitud, peticion.getPKCS10());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);

			getEntityManager().persist(solicitud);
			
			getEntityManager().persist(new Documento(solicitud, data));

			getEntityManager().persist(new Cambio("GENERAR", solicitud));
			
			getEntityManager().commitTransaction();

			return solicitud;
		}
		catch (Exception exception) {
			getEntityManager().rollbackTransaction();
			
			throw new AceptaBeanException("Falla al ingresar Solicitud: " + exception.getMessage(), exception);
		}
	}

	private Certificado generateCertificadoForSolicitud(SolicitudCodigo solicitud, String pkcs10Base64) {
		try {
			Authority authority = getSecurityManager().getAuthority(solicitud.getAutoridad().getNombre());

			CodigoCertificateRequest request = generateCertificateRequestForSolicitud(solicitud, pkcs10Base64);

			X509Certificate certificate = authority.generateCertificate(request);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(certificate.getSerialNumber());
			certificado.setValidoDesde(certificate.getNotBefore());
			certificado.setValidoHasta(certificate.getNotAfter());
			certificado.setSujeto(request.getSubjectCommonName());
			certificado.setData(certificate.getEncoded());

			certificado.changeEstado(Certificado.Estado.VALIDO);

			return certificado;
		}
		catch (CertificateEncodingException exception) {
			throw new AceptaBeanException("Falla al serializar Certificado X.509", exception);
		}
	}

	private CodigoCertificateRequest generateCertificateRequestForSolicitud(SolicitudCodigo solicitud, String pkcs10Base64) {
		Calendar calendar = Calendar.getInstance();
		Date notBefore = calendar.getTime();

		calendar.add(Calendar.MONTH, solicitud.getMesesVigencia());
		Date notAfter = calendar.getTime();

		CodigoCertificateRequest request = new CodigoCertificateRequest();

		request.setSerialNumber(solicitud.getAutoridad().generarNumeroSerieCertificado());
		request.setNotBefore(notBefore);
		request.setNotAfter(notAfter);

		request.setSubjectRut(solicitud.getCompania().getRut());
		request.setSubjectCommonName(solicitud.getCompania().getNombre());
		request.setSubjectOrganizationalUnit(solicitud.getCompania().getUnidadOrganizacional());
		request.setSubjectOrganization(solicitud.getCompania().getOrganizacion());
		request.setSubjectStateOrProvince(solicitud.getCompania().getEstado());
		request.setSubjectLocality(solicitud.getCompania().getCiudad());
		request.setSubjectCountry(solicitud.getCompania().getPais());

		request.setSubjectPublicKeyFromPKCS10(pkcs10Base64);

		return request;
	}

	private SolicitudCodigo createSolicitudFromPeticion(XMLSolicitudCodigo peticion) {
		SolicitudCodigo solicitud = new SolicitudCodigo();

		solicitud.setNumero(peticion.getNumeroSolicitud());
		solicitud.setClave(peticion.getClave());
		solicitud.setMesesVigencia(peticion.getMesesVigencia());

		Compania compania = createCompaniaFromPeticion(peticion);

		solicitud.setCompania(compania);

		solicitud.setAutoridad(getEntityManager().findAutoridadWithNombre(ACEPTA_AUTORIDAD_CODIGO));

		return solicitud;
	}

	private Compania createCompaniaFromPeticion(XMLSolicitudCodigo peticion) {
		Compania compania = new Compania();
		
		compania.setNombre(peticion.getNombre());

		compania.setRut(peticion.getRut());
		compania.setOrganizacion(peticion.getOrganizacion());
		compania.setUnidadOrganizacional(peticion.getUnidadOrganizacional());
		
		compania.setEstado(peticion.getEstado());
		compania.setCiudad(peticion.getCiudad());
		compania.setPais(peticion.getPais());

		return compania;
	}

	private void validateEsquemaPeticion(XMLSolicitudCodigo peticion) {
		try {
			if (peticion.getRut().equals(""))
				throw new AceptaBeanException("Falta el RUT");
			
			if (peticion.getNombre().equals(""))
				throw new AceptaBeanException("Falta el nombre");
			
			if (peticion.getOrganizacion().equals(""))
				throw new AceptaBeanException("Falta la organizacion");
			
			if (peticion.getPais().equals(""))
				throw new AceptaBeanException("Falta el pais");
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Solicitud con esquema invalido: " + exception.getMessage());
		}
	}
	
	private void validateFirmaOperadorValidacionDePeticion(XMLSolicitudCodigo peticion) {
		validatePeticionSignature(peticion, getRUTsDeGrupoUsuarios(ACEPTA_GRUPO_CODIGO_OPERADOR_VALIDACION));
	}

	private Collection<String> getRUTsDeGrupoUsuarios(String nombreGrupo) {
		Collection<String> ruts = new ArrayList<String>();
		
		for (Usuario usuario : getEntityManager().findUsuariosWithGrupo(nombreGrupo))
			ruts.add(usuario.getRut());
		
		return ruts;
	}
	
	private void validatePeticionSignature(XMLSolicitudCodigo peticion, Collection<String> trustedRUTs) {
		try {
			if (!peticion.validate(getSecurityManager().getTrustedCertificates(), trustedRUTs))
				throw new AceptaBeanException("Firma invalida");
		}
		catch (Exception exception) {
			throw new AceptaBeanException("Falla al validar la firma de la Solicitud", exception);
		}
	}

	private XMLSolicitudCodigo createPeticionFromData(byte[] data) {
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(data);
			try {
				return new XMLSolicitudCodigo(input);
			}
			finally {
				input.close();
			}
		}
		catch (SAXException exception) {
			throw new AceptaBeanException("Falla al decodificar XML", exception);
		}
		catch (IOException exception) {
			throw new AceptaBeanException("Falla al leer XML", exception);
		}
	}
}
