package com.acepta.ca.bean;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.AceptaEntityManagerFactory;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;

/**
 * Fabrica de componentes que proveen los servicios y/o funcionalidades de la Autoridad Certificadora. 
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class AceptaBeanFactory {	
	private AceptaEntityManagerFactory entityManagerFactory;
	private AceptaSecurityManagerFactory securityManagerFactory;
	
	private AceptaSecurityManager securityManager;
	
	public AceptaBeanFactory(String persistenceUnitName, String securityTokenName) {
		setEntityManagerFactory(new AceptaEntityManagerFactory(persistenceUnitName));
		setSecurityManagerFactory(new AceptaSecurityManagerFactory(securityTokenName));
		
		setSecurityManager(getSecurityManagerFactory().createSecurityManager());
	}
	
	public void close() {
		getEntityManagerFactory().close();
		getSecurityManagerFactory().close();
	}

	public AdminBean createAdminBean() {
		return new AdminBean(getSecurityManagerFactory(), createEntityManager(), createSecurityManager());
	}
	
	public CertificadoBean createCertificadoBean() {
		return new CertificadoBean(createEntityManager(), createSecurityManager());
	}

	public SelloTiempoBean createSelloTiempoBean() { 
		return new SelloTiempoBean(createEntityManager(), createSecurityManager());
	}
	
	public SolicitudPersonaBean createSolicitudPersonaBean() {
		return new SolicitudPersonaBean(createEntityManager(), createSecurityManager());
	}
	
	
	public SolicitudCAFBean createSolicitudCAFBean() {
		return new SolicitudCAFBean(createEntityManager(), createSecurityManager());
	}

	public SolicitudSitioWebBean createSolicitudSitioWebBean() {
		return new SolicitudSitioWebBean(createEntityManager(), createSecurityManager());
	}
	
	public SolicitudSistemaBean createSolicitudSistemaBean() { 
		return new SolicitudSistemaBean(createEntityManager(), createSecurityManager());
	}

    public SolicitudSelloTiempoBean createSolicitudSelloTiempoBean() {
        return new SolicitudSelloTiempoBean(createEntityManager(), createSecurityManager());
    }

	public SolicitudCodigoBean createSolicitudCodigoBean() { 
		return new SolicitudCodigoBean(createEntityManager(), createSecurityManager());
	}

	private AceptaEntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	private void setEntityManagerFactory(AceptaEntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	private AceptaSecurityManagerFactory getSecurityManagerFactory() {
		return securityManagerFactory;
	}

	private void setSecurityManagerFactory(AceptaSecurityManagerFactory securityManagerFactory) {
		this.securityManagerFactory = securityManagerFactory;
	}

	private void setSecurityManager(AceptaSecurityManager securityManager){
		this.securityManager= securityManager;
	}
	private AceptaEntityManager createEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}
	
	private AceptaSecurityManager createSecurityManager() {
		return this.securityManager;

	}
}
