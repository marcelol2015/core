package com.acepta.ca.bean;

import com.acepta.ca.persistence.CAF;

/**
 * Bolsa con Certificado Electronico X.509 y Llave Privada RSA de un Codigo de Asignacion de Folios (CAF). 
 *  
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public final class CAFCertificateBag {
	public static enum Format {
		PEM, PKCS12
	}
	
	private CAF cAF;
	private CAFCertificateBag.Format format;
	private byte[] data;
	
	public CAFCertificateBag(CAF caf, CAFCertificateBag.Format format) {
		setCAF(caf);
		setFormat(format);
	}

	public CAF getCAF() {
		return cAF;
	}

	private void setCAF(CAF caf) {
		cAF = caf;
	}

	public CAFCertificateBag.Format getFormat() {
		return format;
	}

	private void setFormat(CAFCertificateBag.Format format) {
		this.format = format;
	}

	public byte[] getData() {
		return data;
	}

	void setData(byte[] data) {
		this.data = data;
	}

	public boolean isPEM() {
		return getFormat() == Format.PEM;
	}
	
	public boolean isPKCS12() {
		return getFormat() == Format.PKCS12;
	}
}