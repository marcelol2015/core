package com.acepta.ca.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.SAXException;

import com.acepta.ca.security.util.PIN;

/**
 * Documento XML con Solicitud de Certificado para Persona Natural. 
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class XMLSolicitudPersona extends XMLSolicitud {
	public static final String SOLICITUD_CLASE2 = "CLASE2";

	public XMLSolicitudPersona(InputStream input) throws SAXException, IOException {
		super(input);
		
		setNamespaceURI("s", SRCEI_NAMESPACE_URI);
		setNamespaceURI("ds", XMLDSIG_NAMESPACE_URI);
	}

	public String getNumeroAtencion() {
		return getStringFromContenidos("s:NumeroAtencion");
	}

	public String getNumeroSolicitud() {
		return getStringFromContenidos("s:NumeroSolicitud");
	}

	public String getTipoRegistro() {
		return getStringFromContenidos("s:TipoRegistro");
	}

	public Date getFechaSolicitud() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.parse(getStringFromContenidos("s:FechaSolicitud"));
	}

	public Date getFechaVencimiento() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.parse(getStringFromContenidos("s:FechaVencimiento"));
	}

	public String getInscripcion() {
		return getStringFromContenidos("s:Inscripcion");
	}

	public String getClaveActivacion() {
		if (getDespachador().equals(DESPACHADOR_SRCEI))
			return PIN.encrypt(getStringFromContenidos("s:sha1ClaveActivacion"));
		else
			return getStringFromContenidos("s:Sha1ClaveActivacion");
	}

	public String getRecordatorio() {
		return getStringFromContenidos("s:Recordatorio");
	}

	public String getPersonaRut() {
		return getStringFromDatosPersona("s:PI_RUT");
	}

	public String getPersonaNombres() {
		return getStringFromDatosPersona("s:Nombres");
	}

	public String getPersonaApellidoPaterno() {
		return getStringFromDatosPersona("s:ApellidoPaterno");
	}

	public String getPersonaApellidoMaterno() {
		return getStringFromDatosPersona("s:ApellidoMaterno");
	}

	public String getPersonaProfesion() {
		return getStringFromDatosPersona("s:Profesion");
	}

	public String getPersonaSexo() {
		return getStringFromDatosPersona("s:Sexo");
	}

	public Date getPersonaFechaNacimiento() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.parse(getStringFromDatosPersona("s:FechaNacimiento"));
	}

	public String getPersonaPaisNacimiento() {
		return getStringFromDatosPersona("s:PaisNacimiento");
	}

	public String getPersonaCorreoElectronico() {
		return getStringFromDatosPersona("s:CorreoElectronico");
	}

	public String getPersonaNombreDedo() {
		return getStringFromDatosPersona("s:NombreDedo");
	}

	public String getPersonaImpresionDactilar() {
		return getStringFromDatosPersona("s:ImpresionDactilar");
	}

	public String getPersonaFirmaManuscrita() {
		return getStringFromDatosPersona("s:FirmaManuscrita");
	}

	public String getPersonaFotografia() {
		return getStringFromDatosPersona("s:Fotografia");
	}

	public String getPersonaRUTAnverso() {
		return getStringFromDatosPersona("s:RutAnverso");
	}

	public String getPersonaRUTReverso() {
		return getStringFromDatosPersona("s:RutReverso");
	}

	public String getPersonaTelefono() {
		return getStringFromDatosPersona("s:Telefono");
	}

	public String getPersonaTelefonoMovil() {
		return getStringFromDatosPersona("s:TelefonoMovil");
	}

	public String getPersonaDireccion() {
		return getStringFromDatosPersona("s:Direccion");
	}

	public String getPersonaComuna() {
		return getStringFromDatosPersona("s:Comuna");
	}

	public String getPersonaCiudad() {
		return getStringFromDatosPersona("s:Ciudad");
	}

	public String getPersonaPais() {
		return getStringFromDatosPersona("s:Pais");
	}

	public String getPersonaCodigoPostal() {
		return getStringFromDatosPersona("s:CodigoPostal");
	}

	public String getEmpresaRut() {
		return getStringFromDatosPersona("s:RutEmpresa");
	}

	public String getEmpresaRazonSocial() {
		return getStringFromDatosPersona("s:RazonSocial");
	}

	public int getMesesVigencia() {
		return getNumberFromDatosPersona("s:MesesVigencia").intValue();
	}

	public int getMinutosVigencia() {
		return getNumberFromDatosPersona("s:MinutosVigencia").intValue();
	}

	public String getPKCS10() {
		return getStringFromDatosPersona("s:PKCS10");
	}
	
	public boolean isFirmaAvanzada() {
		return getStringFromDatosPersona("s:FirmaAvanzada").equals("TRUE");
	}

	public String getContrato() {
		return getStringFromContenidos("s:Contrato");
	}
	
	private String getStringFromDatosPersona(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos//s:DatosPersona/" + expression);
	}
	
	private Double getNumberFromDatosPersona(String expression) {
		return getNumber("/s:EnvioDE/s:DATA/s:Contenidos//s:DatosPersona/" + expression);
	}

	private String getStringFromContenidos(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos//" + expression);
	}


}
