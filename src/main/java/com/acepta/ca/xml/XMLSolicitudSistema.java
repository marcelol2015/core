package com.acepta.ca.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.SAXException;

/**
 * Documento XML con Solicitud de Certificado para Sistemas.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-08
 */
public class XMLSolicitudSistema extends XMLSolicitud {
	public XMLSolicitudSistema(InputStream input) throws SAXException, IOException {
		super(input);
	}

	public String getNumeroSolicitud() {
		return getStringFromContenidos("s:NumeroSolicitud");
	}
	
	public Date getFechaSolicitud() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(getStringFromContenidos("s:FechaSolicitud"));
	}

	public String getNombre() {
		return getStringFromDatosSistema("s:Nombre");
	}
	
	public String getRut() {
		return getStringFromDatosSistema("s:Compania/s:Rut");
	}

	public String getOrganizacion() {
		return getStringFromDatosSistema("s:Compania/s:Organizacion");
	}

	public String getUnidadOrganizacional() {
		return getStringFromDatosSistema("s:Compania/s:UnidadOrganizacional");
	}
	
	public String getEstado() {
		return getStringFromDatosSistema("s:Compania/s:Estado");
	}
	
	public String getCiudad() {
		return getStringFromDatosSistema("s:Compania/s:Ciudad");
	}

	public String getPais() {
		return getStringFromDatosSistema("s:Compania/s:Pais");
	}

	public String getRutRepresentanteLegal() {
		return getStringFromDatosSistema("s:Compania/s:RepresentanteLegal/s:Rut");
	}
	
	public String getNombreRepresentanteLegal() {
		return getStringFromDatosSistema("s:Compania/s:RepresentanteLegal/s:Nombre");
	}
	
	public String getTelefonoRepresentanteLegal() {
		return getStringFromDatosSistema("s:Compania/s:RepresentanteLegal/s:Telefono");
	}

	public String getCorreoRepresentanteLegal() {
		return getStringFromDatosSistema("s:Compania/s:RepresentanteLegal/s:Correo");
	}
	
	public String getPoliticasCertificacion() {
		return getStringFromDatosSistema("s:PoliticasCertificacion");
	}

	public int getMesesVigencia() throws NumberFormatException {
		return getNumberFromDatosSistema("s:MesesVigencia").intValue();
	}

	public String getPKCS10() {
		return getStringFromDatosSistema("s:PKCS10");
	}

	private String getStringFromDatosSistema(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/s:DatosSistema/" + expression);
	}
	
	private Double getNumberFromDatosSistema(String expression) {
		return getNumber("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/s:DatosSistema/" + expression);
	}

	private String getStringFromContenidos(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/" + expression);
	}
}
