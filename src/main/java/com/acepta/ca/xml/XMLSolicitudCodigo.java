package com.acepta.ca.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.SAXException;

/**
 * Documento XML con Solicitud de Certificado para Firma de Codigo.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-09
 */
public class XMLSolicitudCodigo extends XMLSolicitud {
	public XMLSolicitudCodigo(InputStream input) throws SAXException, IOException {
		super(input);
	}

	public String getNumeroSolicitud() {
		return getStringFromContenidos("s:NumeroSolicitud");
	}
	
	public Date getFechaSolicitud() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(getStringFromContenidos("s:FechaSolicitud"));
	}

	public String getClave() {
		return getStringFromContenidos("s:Sha1ClaveActivacion");
	}

	public String getNombre() {
		return getStringFromDatosCodigo("s:Nombre");
	}
	
	public String getRut() {
		return getStringFromDatosCodigo("s:Compania/s:Rut");
	}
	
	public String getOrganizacion() {
		return getStringFromDatosCodigo("s:Compania/s:Organizacion");
	}

	public String getUnidadOrganizacional() {
		return getStringFromDatosCodigo("s:Compania/s:UnidadOrganizacional");
	}
	
	public String getEstado() {
		return getStringFromDatosCodigo("s:Compania/s:Estado");
	}

	public String getCiudad() {
		return getStringFromDatosCodigo("s:Compania/s:Ciudad");
	}

	public String getPais() {
		return getStringFromDatosCodigo("s:Compania/s:Pais");
	}
	
	public String getRutRepresentanteLegal() {
		return getStringFromDatosCodigo("s:Compania/s:RepresentanteLegal/s:Rut");
	}
	
	public String getNombreRepresentanteLegal() {
		return getStringFromDatosCodigo("s:Compania/s:RepresentanteLegal/s:Nombre");
	}
	
	public String getTelefonoRepresentanteLegal() {
		return getStringFromDatosCodigo("s:Compania/s:RepresentanteLegal/s:Telefono");
	}

	public String getCorreoRepresentanteLegal() {
		return getStringFromDatosCodigo("s:Compania/s:RepresentanteLegal/s:Correo");
	}

	public int getMesesVigencia() throws NumberFormatException {
		return getNumberFromDatosCodigo("s:MesesVigencia").intValue();
	}

	public String getPKCS10() {
		return getStringFromDatosCodigo("s:PKCS10");
	}

	public String getContrato() {
		return getStringFromContenidos("s:Contrato");
	}

	private String getStringFromDatosCodigo(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/s:DatosCodigo/" + expression);
	}
	
	private Double getNumberFromDatosCodigo(String expression) {
		return getNumber("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/s:DatosCodigo/" + expression);
	}

	private String getStringFromContenidos(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/" + expression);
	}
}
