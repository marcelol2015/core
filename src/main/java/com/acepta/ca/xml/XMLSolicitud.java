package com.acepta.ca.xml;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.acepta.ca.security.util.X509;
import com.acepta.ca.xml.crypto.XMLDocumentSignature;

/**
 * Documento XML base para las Solicitudes de Certificado para Personas, Sitios Web y Sistemas. 
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-08
 */
public abstract class XMLSolicitud extends XMLDocument {
	/*
	 * Namespaces
	 */
	public static final String SRCEI_NAMESPACE_URI = "http://www.srcei.cl/ns/draft2003-06-12";
	public static final String XMLDSIG_NAMESPACE_URI = "http://www.w3.org/2000/09/xmldsig#";
	
	/*
	 * Despachadores
	 */
	public static final String DESPACHADOR_ACEPTA = "http://www.acepta.com/AutoridadRegistro";
	public static final String DESPACHADOR_SRCEI = "http://www.registrocivil.cl/";

	/*
	 * Envio de Mensajes (SRCeI)
	 */
	public static final String SRCEI_ID_ENVIO_PI_APROBACION = "http://www.registrocivil.cl/mensaje/aprobacion";
	public static final String SRCEI_ID_ENVIO_PI_REVOCACION = "http://www.registrocivil.cl/mensaje/revocaciondeaprobacion";
	public static final String SRCEI_ID_ENVIO_PI_RECHAZO_BIOMETRICO = "http://www.registrocivil.cl/mensaje/rechazobiometrico";
	public static final String SRCEI_ID_ENVIO_PI_RECHAZO_DATOS = "http://www.registrocivil.cl/mensaje/rechazodatos";
	public static final String SRCEI_ID_ENVIO_PI_RECHAZO_SRCEI = "http://www.registrocivil.cl/mensaje/rechazosrcei";

	/*
	 * Recepcion de Mensajes (SRCeI)
	 */
	public static final String SRCEI_ID_ENVIO_PI_RECEPCION = "http://www.registrocivil.cl/mensaje/recepcion";

	/*
	 * Estados Recepcion (SRCeI)
	 */
	public static final String SRCEI_ESTADO_RECEPCION_CONFORME = "http://www.registrocivil.cl/mensaje/Recepcion/Conforme";
	public static final String SRCEI_ESTADO_RECEPCION_ERROR = "http://www.registrocivil.cl/mensaje/Recepcion/Error";
	public static final String SRCEI_ESTADO_RECEPCION_DATOS_INVALIDOS = "http://www.registrocivil.cl/mensaje/Recepcion/DatosInvalidos";
	public static final String SRCEI_ESTADO_RECEPCION_DATOS_FALTANTES = "http://www.registrocivil.cl/mensaje/Recepcion/DatosFaltantes";
	public static final String SRCEI_ESTADO_RECEPCION_FIRMA_INVALIDA = "http://www.registrocivil.cl/mensaje/Recepcion/FirmaInvalida";

	public XMLSolicitud(InputStream input) throws SAXException, IOException {
		super(input);
		
		setNamespaceURI("s", SRCEI_NAMESPACE_URI);
		setNamespaceURI("ds", XMLDSIG_NAMESPACE_URI);
	}

	public final String getEmisor() {
		return getString("/s:EnvioDE/s:DATA/s:Emisor");
	}

	public final String getDespachador() {
		return getString("/s:EnvioDE/s:DATA/s:Despachador");
	}

	public final String getDestinatario() {
		return getString("/s:EnvioDE/s:DATA/s:Destinatario");
	}

	public final String getIDEnvioPI() {
		return getString("/s:EnvioDE/s:DATA/s:IDEnvio/@uriPI");
	}

	public final String getIDEnvio() {
		return getString("/s:EnvioDE/s:DATA/s:IDEnvio");
	}

	public final Date getTimeStamp() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		return dateFormat.parse(getString("/s:EnvioDE/s:DATA/s:TimeStamp"));
	}

    public final String getDataURI() {
		String dataURI = getString("/s:EnvioDE/s:DATA/@ID");
		if (dataURI != null && !dataURI.isEmpty() && !dataURI.equals("null")){
			return "#" + dataURI;
		}else{
			return "#" + getString("/s:EnvioDE/s:DATA/@Id");
		}
	}

	public final NodeList getSignatures() {
		return getNodeSet("/s:EnvioDE/ds:Signature");
	}

	public final NodeList getSignatureReferenceURIs() {
		return getNodeSet("/s:EnvioDE/ds:Signature/ds:SignedInfo/ds:Reference/@URI");
	}
	
	public final boolean validate(Collection<X509Certificate> trustedCertificates, Collection<String> trustedRUTs) throws MarshalException, XMLSignatureException, CertificateParsingException {
		String dataURI = getDataURI();
		NodeList signatureReferenceURIs = getSignatureReferenceURIs();
		if (signatureReferenceURIs.getLength() <= 0)
			throw new XMLSignatureException("Sin firmas");

		for (int index = 0; index < signatureReferenceURIs.getLength(); index++) {
			Node referenceURI = signatureReferenceURIs.item(index);
			if (!referenceURI.getTextContent().equals(dataURI))
				throw new XMLSignatureException("Firma no hace referencia a los datos de la Solicitud: " + referenceURI.getTextContent());
		}
		XMLDocumentSignature signature = new XMLDocumentSignature();

		NodeList signatures = getSignatures();

		ArrayList<String> untrustedSignerRUTs = new ArrayList<String>();

		for (int index = 0; index < signatures.getLength(); index++) {
            try {
                setIdReferenceLikeId(signatures.item(index));
            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            X509Certificate signerCertificate = signature.validate(signatures.item(index), trustedCertificates);
			
			if (signerCertificate == null)
				return false;

			String signerRUT = X509.decodeSubjectRUTFromCertificate(signerCertificate);

			if (trustedRUTs.contains(signerRUT))
				return true;

			untrustedSignerRUTs.add(signerRUT);
		}
		
		throw new XMLSignatureException("Firmantes no autorizados: " + Arrays.deepToString(untrustedSignerRUTs.toArray()));
	}
	
	public final void sign(Key privateKey, X509Certificate certificate) throws GeneralSecurityException, MarshalException, XMLSignatureException {
		XMLDocumentSignature signature = new XMLDocumentSignature();

		Node parentNode = getNode("/s:EnvioDE");
		
		String referenceURI = getDataURI();
		
		signature.sign(parentNode, referenceURI, privateKey, certificate);
	}
}
