package com.acepta.ca.xml.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * @author Angelo Estartus on 10-11-16.
 */
public class CompressXML{

    public static byte[] compress(byte[] src){

        Deflater compressor = new Deflater();
        compressor.setInput(src);
        compressor.finish();

        ByteArrayOutputStream bos = new ByteArrayOutputStream(src.length);

        byte[] buf = new byte[1024];
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
        }
        try {
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bos.toByteArray();
    }

    public static byte[] decompress(byte[] src){

        Inflater decompressor = new Inflater();
        decompressor.setInput(src);

        ByteArrayOutputStream bos = new ByteArrayOutputStream(src.length);

        byte[] buf = new byte[1024];
        int count;

        while (!decompressor.finished()) {
            try{
                count = decompressor.inflate(buf);
            }catch (DataFormatException e) {
                /**se asume que la solicitud
                 * en base de datos no esta comprimida**/
                return src;
            }

            bos.write(buf, 0, count);
        }
        try {
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bos.toByteArray();
    }
}