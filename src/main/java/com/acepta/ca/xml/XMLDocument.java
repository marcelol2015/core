package com.acepta.ca.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import com.acepta.ca.xml.namespace.XMLDocumentNamespaces;

/**
 * Documento XML.
 * 
 * @author Angelo Estartus
 * @version 1.0, 2014-09-01
 */
public class XMLDocument {
	private Document document;
	private XPath xpath;
	private XMLDocumentNamespaces namespaces = new XMLDocumentNamespaces();
	private static final String FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";

	public XMLDocument(InputStream input) throws SAXException, IOException {
		setDocument(createDocument(input));
		setXPath(createXPath());
	}

	/*
	 * Namespaces
	 */
	public void setNamespaceURI(String prefix, String namespaceURI) {
		getNamespaces().setNamespaceURI(prefix, namespaceURI);
	}
	
	/*
	 * XPath
	 */
	public Double getNumber(String expression) {
		return (Double) evaluate(expression, XPathConstants.NUMBER);
	}

	public String getString(String expression) {
		return (String) evaluate(expression, XPathConstants.STRING);
	}

	public NodeList getNodeSet(String expression) {
		return (NodeList) evaluate(expression, XPathConstants.NODESET);
	}
	
	public Node getNode(String expression) {
		return (Node) evaluate(expression, XPathConstants.NODE);
	}

    public Node selectNode(Node contextNode, String expression)
            throws XPathExpressionException {

        if (contextNode == null)
            contextNode = getDocument();
        return (Node) getXPath().evaluate(expression, contextNode,
                XPathConstants.NODE);
    }

    public NodeList selectNodeSet(Node contextNode, String expression)
            throws XPathExpressionException {

        if (contextNode == null) contextNode = getDocument();
        return (NodeList) getXPath().evaluate(expression, contextNode,
                XPathConstants.NODESET);
    }

	private Object evaluate(String expression, QName returnType) {
		try {
			return getXPath().evaluate(expression, getDocument(), returnType);
		}
		catch (XPathExpressionException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	/*
	 * Transforms
	 */
	public byte[] transformNode(Node node, String encoding) {
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();

			Transformer transformer = transformerFactory.newTransformer();

			ByteArrayOutputStream output = new ByteArrayOutputStream();
			
			try {
				transformer.setOutputProperty("encoding", encoding);
			
				transformer.transform(new DOMSource(node), new StreamResult(output));
			}
			finally {
				output.close();
			}
			
			return output.toByteArray();
		}
		catch (TransformerException exception) {
			throw new RuntimeException(exception);
		}
		catch (TransformerFactoryConfigurationError exception) {
			throw new RuntimeException(exception);
		}
		catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	public byte[] toXML(String encoding) {
		return transformNode(getDocument(), encoding);
	}
	
	/*
	 * Properties
	 */
	private Document getDocument() {
		return document;
	}

	private void setDocument(Document document) {
		this.document = document;
	}

	private XPath getXPath() {
		return xpath;
	}

	private void setXPath(XPath xpath) {
		this.xpath = xpath;
	}
	
	private XMLDocumentNamespaces getNamespaces() {
		return namespaces;
	}

	/*
	 * Factories
	 */
	private Document createDocument(InputStream input) throws SAXException, IOException {
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			builderFactory.setFeature(FEATURE, true);
			builderFactory.setXIncludeAware(false);
			builderFactory.setExpandEntityReferences(false);
			builderFactory.setNamespaceAware(true);

			DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();

			return documentBuilder.parse(input);
		}
		catch (ParserConfigurationException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	private XPath createXPath() {
		XPathFactory xpathFactory = XPathFactory.newInstance();
		
		XPath xpath = xpathFactory.newXPath();

		xpath.setNamespaceContext(getNamespaces());
		
		return xpath;
	}


	public void setIdReferenceLikeId(Node signatureNode) throws XPathExpressionException {

		String[] references = getReferences(signatureNode);
		for (String reference : references) {
			if (reference.startsWith("#")) {
				String idValue = reference.substring(1);
				String searchNodeByAttributeValue = "//*[@* = '" + idValue + "']";
				Element attributeOwner = (Element) selectNode(null, searchNodeByAttributeValue);
				if (attributeOwner != null) {
					Node idAttribute = getAttributeByValue(attributeOwner, idValue);
					attributeOwner.setIdAttribute(idAttribute.getLocalName(), true);
				}
			}
		}
	}

	private String[] getReferences(Node signatureNode) throws XPathExpressionException {
		NodeList nodeList = selectNodeSet(signatureNode, ".//ds:Reference/@URI");
		String[] result = new String[nodeList.getLength()];
		for (int n = 0; n < nodeList.getLength(); n++) {
			result[n] = nodeList.item(n).getTextContent();
		}
		return result;
	}

	private Node getAttributeByValue(Element attributeOwner, String idValue) {
		NamedNodeMap attributes = attributeOwner.getAttributes();
		for (int n = 0; n < attributes.getLength(); n++) {
			Node anAttribute = attributes.item(n);
			if (idValue.equals(anAttribute.getTextContent()))
				return anAttribute;
		}
		return null;
	}

}
