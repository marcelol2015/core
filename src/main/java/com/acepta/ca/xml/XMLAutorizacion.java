package com.acepta.ca.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.SAXException;

/**
 * Documento XML con la Autorizacion de un Codigo de Asignacion de Folios (CAF).
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-09-01
 */
public class XMLAutorizacion extends XMLDocument {
	public static final String VERSION = "1.0";
	
	private static final String CAF_CHARACTER_ENCODING = "ISO-8859-1";
	
	public XMLAutorizacion(InputStream input) throws SAXException, IOException {
		super(input);
	}

	public String getVersion() {
		return getString("/AUTORIZACION/CAF/@version");
	}

	public String getRutEmpresa() {
		return getString("/AUTORIZACION/CAF/DA/RE");
	}

	public String getRazonSocial() {
		return getString("/AUTORIZACION/CAF/DA/RS");
	}

	public int getTipoDTE() throws NumberFormatException {
		return getNumber("/AUTORIZACION/CAF/DA/TD").intValue();
	}

	public long getDesdeFolio() throws NumberFormatException {
		return getNumber("/AUTORIZACION/CAF/DA/RNG/D").longValue();
	}

	public long getHastaFolio() throws NumberFormatException {
		return getNumber("/AUTORIZACION/CAF/DA/RNG/H").longValue();
	}

	public Date getFechaAutorizacion() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.parse(getString("/AUTORIZACION/CAF/DA/FA"));
	}

	public String getRSAPublicKeyModulus() {
		return getString("/AUTORIZACION/CAF/DA/RSAPK/M");
	}

	public String getRSAPublicKeyExponent() {
		return getString("/AUTORIZACION/CAF/DA/RSAPK/E");
	}

	public int getLlaveID() throws NumberFormatException {
		return getNumber("/AUTORIZACION/CAF/DA/IDK").intValue();
	}

	public String getFirmaAlgoritmo() {
		return getString("/AUTORIZACION/CAF/FRMA/@algoritmo");
	}

	public String getFirma() {
		return getString("/AUTORIZACION/CAF/FRMA");
	}

	public String getRSAPublicKey() {
		return getString("/AUTORIZACION/RSAPUBK");
	}

	public String getRSAPrivateKey() {
		return getString("/AUTORIZACION/RSASK");
	}

	public byte[] getCAF() {
		return transformNode(getNode("/AUTORIZACION/CAF"), CAF_CHARACTER_ENCODING);
	}
}
