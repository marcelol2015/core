package com.acepta.ca.xml.crypto;

import org.apache.jcp.xml.dsig.internal.dom.XMLDSigRI;
import org.w3c.dom.Node;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;


public class XMLDocumentSignature {
	private XMLSignatureFactory signatureFactory;
	
	static {
		Security.addProvider(new XMLDSigRI());
	}

	public XMLDocumentSignature() {
		setSignatureFactory(XMLSignatureFactory.getInstance());
	}

	public X509Certificate validate(Node signatureNode, Collection<X509Certificate> trustedCertificates) throws MarshalException, XMLSignatureException {
		DOMValidateContext validateContext = new DOMValidateContext(new XMLDocumentKeySelector(trustedCertificates), signatureNode);
		
		XMLSignature signature = getSignatureFactory().unmarshalXMLSignature(validateContext);

		if (!signature.validate(validateContext))
			return null;
		
		return ((XMLDocumentKeySelector.Result) signature.getKeySelectorResult()).getCertificate();
	}
	
	public void sign(Node signatureParentNode, String signatureReferenceURI, Key privateKey, X509Certificate certificate) throws GeneralSecurityException, MarshalException, XMLSignatureException {
		SignedInfo signedInfo = createSignedInfo(getSignatureFactory(), signatureReferenceURI);
		
		KeyInfo keyInfo = createKeyInfo(getSignatureFactory().getKeyInfoFactory(), certificate);
		
		XMLSignature signature = getSignatureFactory().newXMLSignature(signedInfo, keyInfo);

		DOMSignContext signContext = new DOMSignContext(privateKey, signatureParentNode);
		
		signature.sign(signContext);
	}
	
	private static SignedInfo createSignedInfo(XMLSignatureFactory signatureFactory, String referenceURI) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
	 	CanonicalizationMethod canonicalizationMethod = signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, (C14NMethodParameterSpec) null);
		
		SignatureMethod signatureMethod = signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null);
		
		DigestMethod digestMethod = signatureFactory.newDigestMethod(DigestMethod.SHA1, null);
		
		Reference reference = signatureFactory.newReference(referenceURI, digestMethod);
		
		SignedInfo signedInfo = signatureFactory.newSignedInfo(canonicalizationMethod, signatureMethod, Arrays.asList(reference));
		
		return signedInfo;
	}

	private static KeyInfo createKeyInfo(KeyInfoFactory keyInfoFactory, X509Certificate certificate) throws KeyException {
		KeyValue keyValue = keyInfoFactory.newKeyValue(certificate.getPublicKey());
		
		X509Data x509Data = keyInfoFactory.newX509Data(Arrays.asList(certificate));
		
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Arrays.asList(keyValue, x509Data));
		
		return keyInfo;
	}
	
	private XMLSignatureFactory getSignatureFactory() {
		return signatureFactory;
	}

	private void setSignatureFactory(XMLSignatureFactory signatureFactory) {
		this.signatureFactory = signatureFactory;
	}
}
