package com.acepta.ca.xml.crypto;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;

public class XMLDocumentKeySelector extends KeySelector {
	public static final class Result implements KeySelectorResult {
		private Key key;
		private X509Certificate certificate;
		
		public Result(Key key, X509Certificate certificate) {
			setKey(key);
			setCertificate(certificate);
		}
			
		public Key getKey() {
			return key;
		}

		private void setKey(Key key) {
			this.key = key;
		}

		public X509Certificate getCertificate() {
			return certificate;
		}

		public void setCertificate(X509Certificate certificate) {
			this.certificate = certificate;
		}
	}
	
	private Collection<X509Certificate> trustedCertificates;
	
	public XMLDocumentKeySelector(Collection<X509Certificate> trustedCertificates) {
		setTrustedCertificates(trustedCertificates);
	}

	public KeySelectorResult select(KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method, XMLCryptoContext context) throws KeySelectorException {
		PublicKey signerPublicKey = getKeyValueFrom(keyInfo);
	
		X509Certificate signerCertificate = getX509CertificateFrom(keyInfo, signerPublicKey);
		
		if (!isTrustedCertificate(signerCertificate))
			throw new KeySelectorException("Certificado emitido por autoridad no confiable: " + signerCertificate.getIssuerDN().getName());
		
		return new Result(signerPublicKey, signerCertificate);
	}

	private boolean isTrustedCertificate(X509Certificate signerCertificate) {
		for (X509Certificate trustedCertificate : getTrustedCertificates()) {
			if (signerCertificate.getIssuerDN().equals(trustedCertificate.getSubjectDN())) {
				try {
					signerCertificate.verify(trustedCertificate.getPublicKey());
					return true;
				}
				catch (GeneralSecurityException exception) {
					// ignorar errores de validacion para soportar certificados confiables con el mismo nombre
				}
			}
		}
		return false;
	}
	
	private PublicKey getKeyValueFrom(KeyInfo keyInfo) throws KeySelectorException {
		for (Object info : keyInfo.getContent()) {
			if (!(info instanceof KeyValue))
				continue;
			
			KeyValue keyValue = (KeyValue) info;
			
			try {
				PublicKey publicKey = keyValue.getPublicKey();
				
				if (publicKey.getAlgorithm().equals("RSA"))
					return publicKey;
			}
			catch (KeyException exception) {
				throw new KeySelectorException("Falla al extraer llave publica: " + exception.getMessage(), exception);
			}
		}
		
		throw new KeySelectorException("Firma sin llave publica");
	}
	
	private X509Certificate getX509CertificateFrom(KeyInfo keyInfo, PublicKey signerPublicKey) throws KeySelectorException {
		for (Object info : keyInfo.getContent()) {
			if (!(info instanceof X509Data))
				continue;
			
			X509Data x509Data = (X509Data) info;
			
			for (Object data : x509Data.getContent()) {
				if (!(data instanceof X509Certificate))
					continue;
				
				X509Certificate certificate = (X509Certificate) data;

				if (certificate.getPublicKey().equals(signerPublicKey))
					return certificate;
			}
		}
		
		throw new KeySelectorException("Firma sin certificado electronico");
	}

	private Collection<X509Certificate> getTrustedCertificates() {
		return trustedCertificates;
	}

	private void setTrustedCertificates(Collection<X509Certificate> trustedCertificates) {
		this.trustedCertificates = trustedCertificates;
	}
}
