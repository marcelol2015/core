package com.acepta.ca.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.SAXException;

/**
 * Documento XML con Solicitud de Certificado para Sitio Web.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-08
 */
public class XMLSolicitudSitioWeb extends XMLSolicitud {
	public XMLSolicitudSitioWeb(InputStream input) throws SAXException, IOException {
		super(input);
	}

	public String getNumeroSolicitud() {
		return getStringFromContenidos("s:NumeroSolicitud");
	}
	
	public Date getFechaSolicitud() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(getStringFromContenidos("s:FechaSolicitud"));
	}

	public String getClave() {
		return getStringFromContenidos("s:Sha1ClaveActivacion");
	}

	public String getDominio() {
		return getStringFromDatosSitioWeb("s:Dominio");
	}
	
	public String getRut() {
		return getStringFromDatosSitioWeb("s:Compania/s:Rut");
	}
	
	public String getOrganizacion() {
		return getStringFromDatosSitioWeb("s:Compania/s:Organizacion");
	}

	public String getUnidadOrganizacional() {
		return getStringFromDatosSitioWeb("s:Compania/s:UnidadOrganizacional");
	}
	
	public String getEstado() {
		return getStringFromDatosSitioWeb("s:Compania/s:Estado");
	}

	public String getCiudad() {
		return getStringFromDatosSitioWeb("s:Compania/s:Ciudad");
	}

	public String getPais() {
		return getStringFromDatosSitioWeb("s:Compania/s:Pais");
	}
	
	public String getRutContactoAdministrativo() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoAdministrativo/s:Rut");
	}
	
	public String getNombreContactoAdministrativo() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoAdministrativo/s:Nombre");
	}
	
	public String getTelefonoContactoAdministrativo() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoAdministrativo/s:Telefono");
	}

	public String getCorreoContactoAdministrativo() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoAdministrativo/s:Correo");
	}
	
	public String getRutContactoTecnico() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoTecnico/s:Rut");
	}
	
	public String getNombreContactoTecnico() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoTecnico/s:Nombre");
	}
	
	public String getTelefonoContactoTecnico() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoTecnico/s:Telefono");
	}

	public String getCorreoContactoTecnico() {
		return getStringFromDatosSitioWeb("s:Compania/s:ContactoTecnico/s:Correo");
	}
	
	public int getMesesVigencia() throws NumberFormatException {
		return getNumberFromDatosSitioWeb("s:MesesVigencia").intValue();
	}

	public String getPKCS10() {
		return getStringFromDatosSitioWeb("s:PKCS10");
	}

	private String getStringFromDatosSitioWeb(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/s:DatosSitioWeb/" + expression);
	}
	
	private Double getNumberFromDatosSitioWeb(String expression) {
		return getNumber("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/s:DatosSitioWeb/" + expression);
	}

	private String getStringFromContenidos(String expression) {
		return getString("/s:EnvioDE/s:DATA/s:Contenidos/s:DE/" + expression);
	}
}
