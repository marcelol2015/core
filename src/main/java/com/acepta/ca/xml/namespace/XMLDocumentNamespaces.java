package com.acepta.ca.xml.namespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.namespace.NamespaceContext;

public class XMLDocumentNamespaces implements NamespaceContext {
	private Map<String, String> namespaces = new HashMap<String, String>();

	public XMLDocumentNamespaces() {
	}

	public Map<String, String> getNamespaces() {
		return namespaces;
	}

	public String getNamespaceURI(String prefix) {
		return getNamespaces().get(prefix);
	}

	public String getPrefix(String namespaceURI) {
		for (Entry<String, String> entry : getNamespaces().entrySet()) {
			if (entry.getValue().equals(namespaceURI))
				return entry.getKey();
		}
		return null;
	}

	public Iterator<String> getPrefixes(String namespaceURI) {
		ArrayList<String> prefixes = new ArrayList<String>();

		for (Entry<String, String> entry : getNamespaces().entrySet()) {
			if (entry.getValue().equals(namespaceURI))
				prefixes.add(entry.getKey());
		}

		return prefixes.iterator();
	}

	public void setNamespaceURI(String prefix, String namespaceURI) {
		getNamespaces().put(prefix, namespaceURI);
	}
}
