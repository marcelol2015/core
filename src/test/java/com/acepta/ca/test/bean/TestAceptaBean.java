package com.acepta.ca.test.bean;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import junit.framework.TestCase;

import com.acepta.ca.bean.AceptaBeanFactory;
import com.acepta.ca.bean.AdminBean;
import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;
import com.acepta.ca.security.PersonaCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.authority.AuthorityFactory;
import com.acepta.ca.security.authority.AuthorityProvider;

public abstract class TestAceptaBean extends TestCase {
	private static final String TEST_ACEPTA_G4_PERSISTENCE_UNIT = "aceptaca-g4-development";
	private static final String TEST_ACEPTA_G4_SECURITY_TOKEN = "aceptaca-g4-development";
	
	private static final String TEST_ACEPTA_VT2_PERSISTENCE_UNIT = "aceptaca-vt2-development";
	private static final String TEST_ACEPTA_VT2_SECURITY_TOKEN = "aceptaca-vt2-development";
	
	protected static final class Credentials {
		private PrivateKey privateKey;
		private X509Certificate certificate;
		
		public Credentials(PrivateKey privateKey, X509Certificate certificate) {
			setPrivateKey(privateKey);
			setCertificate(certificate);
		}

		public PrivateKey getPrivateKey() {
			return privateKey;
		}

		public void setPrivateKey(PrivateKey privateKey) {
			this.privateKey = privateKey;
		}

		public X509Certificate getCertificate() {
			return certificate;
		}

		public void setCertificate(X509Certificate certificate) {
			this.certificate = certificate;
		}
	}
	
	private AceptaBeanFactory beanFactoryG4;
	private AceptaBeanFactory beanFactoryVT2;
	
	public TestAceptaBean() {
	}
	
	public AceptaBeanFactory getBeanFactoryG4() {
		return beanFactoryG4;
	}

	public void setBeanFactoryG4(AceptaBeanFactory serviceFactoryG4) {
		this.beanFactoryG4 = serviceFactoryG4;
	}

	public AceptaBeanFactory getBeanFactoryVT2() {
		return beanFactoryVT2;
	}

	public void setBeanFactoryVT2(AceptaBeanFactory serviceFactoryVT2) {
		this.beanFactoryVT2 = serviceFactoryVT2;
	}

	public void setUp() {
		setBeanFactoryG4(new AceptaBeanFactory(TEST_ACEPTA_G4_PERSISTENCE_UNIT, TEST_ACEPTA_G4_SECURITY_TOKEN));
		setBeanFactoryVT2(new AceptaBeanFactory(TEST_ACEPTA_VT2_PERSISTENCE_UNIT, TEST_ACEPTA_VT2_SECURITY_TOKEN));
	}
	
	public void tearDown() {
		getBeanFactoryG4().close();
	}
	
	protected void assertExistenAutoridadesG4() {
		AdminBean adminBeanG4 = getBeanFactoryG4().createAdminBean();
		try {
			if (adminBeanG4.findGrupos().isEmpty())
				throw new RuntimeException("Autoridad Certificadora G4 no inicializada");
		}
		finally {
			adminBeanG4.close();
		}
	}
	
	protected void assertExistenAutoridadesVT2() {
		AdminBean adminBeanVT2 = getBeanFactoryVT2().createAdminBean();
		try {
			if (adminBeanVT2.findGrupos().isEmpty())
				throw new RuntimeException("Autoridad Certificadora VT2 no inicializada");
		}
		finally {
			adminBeanVT2.close();
		}
	}

	protected Credentials createFulanitoTahlCredentials() throws GeneralSecurityException, IOException {
		AceptaSecurityManager managerG4 = new AceptaSecurityManagerFactory(TEST_ACEPTA_G4_SECURITY_TOKEN).createSecurityManager();
		try {
			Authority authority = managerG4.getAuthority("Clase3-G4");
		
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(512);
			
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			
			assertNotNull(keyPair);
			
			PersonaCertificateRequest request = new PersonaCertificateRequest();
			
			request.setSerialNumber(BigInteger.ONE);
			request.setSubjectName("1-9", "fulanito.tahl@acepta.com", "FULANITO DE TAHL", "CONEJILLO DE INDIAS", "CL");
			request.setNotBefore(authority.getCertificate().getNotBefore());
			request.setNotAfter(authority.getCertificate().getNotAfter());
			request.setSubjectPublicKey(keyPair.getPublic());
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			assertNotNull(certificate);
			
			// HACK: agregar la A.C. Clase3-G4 como confiable en las jerarquia VT2.
			AceptaSecurityManager managerVT2 = new AceptaSecurityManagerFactory(TEST_ACEPTA_VT2_SECURITY_TOKEN).createSecurityManager();
			if (!managerVT2.getTrustedCertificates().contains(authority.getCertificate())){
				AdminBean adminBeanVT2 = getBeanFactoryVT2().createAdminBean();
				try {
					adminBeanVT2.__addTrustedAuthority(authority);
				}
				finally { 
					adminBeanVT2.close();
				}
				this.setUp();				
			}
			
			
			return new Credentials(keyPair.getPrivate(), certificate);
		}
		finally {
			managerG4.close();
		}
	}
}
