package com.acepta.ca.test.bean;

import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampResponse;

import com.acepta.ca.bean.SelloTiempoBean;
import com.acepta.ca.persistence.SelloTiempo;
import com.acepta.ca.security.util.BASE64;

public class TestSelloTiempoBean extends TestAceptaBean {
	private static final String TSP_REQUEST = "MCYCAQEwITAJBgUrDgMCGgUABBQAAQIDBAUGBwgJCgsMDQ4PEBESEw==";	
	
	public void testPrecondiciones() {
		assertExistenAutoridadesVT2();
	}

	public void testEmitirSelloTiempo() throws Exception {
		SelloTiempoBean selloTiempoBean = getBeanFactoryVT2().createSelloTiempoBean();
		try {
			TimeStampRequest req = new TimeStampRequest(BASE64.decode(TSP_REQUEST));
			
			TimeStampResponse resp = new TimeStampResponse(selloTiempoBean.emitirSelloTiempo(req.getEncoded()));
			
			resp.validate(req);
			
			assertNotNull(selloTiempoBean.buscarSelloTiempoConNumeroSerie(resp.getTimeStampToken().getTimeStampInfo().getSerialNumber()));
		}
		finally {
			selloTiempoBean.close();
		}
	}
	
	public void testBuscarSelloTiempo() {
		SelloTiempoBean selloTiempoBean = getBeanFactoryVT2().createSelloTiempoBean();
		try {
			int counter = 0;
			
			for (SelloTiempo selloTiempo : selloTiempoBean.buscarSellosTiempos()) {
				assertNotNull(selloTiempo);
				
				assertNotNull(selloTiempo.getEmisor());
				assertNotNull(selloTiempo.getNumeroSerie());
				assertNotNull(selloTiempo.getTiempo());
				assertNotNull(selloTiempo.getData());
				
				assertTrue(selloTiempo.getTiempo().getTime() <= System.currentTimeMillis());
				
				SelloTiempo e = selloTiempoBean.buscarSelloTiempoConNumeroSerie(selloTiempo.getNumeroSerie());
				
				assertNotNull(e);
				
				assertEquals(selloTiempo.getEmisor(), e.getEmisor());
				assertEquals(selloTiempo.getNumeroSerie(), e.getNumeroSerie());
				assertEquals(selloTiempo.getTiempo(), e.getTiempo());
				
				assertEquals(selloTiempo.getData().length, e.getData().length);

				for (int index = 0; index < selloTiempo.getData().length; index++)
					assertEquals(selloTiempo.getData()[index], e.getData()[index]);
				
				counter++;
			}
			
			assertTrue(counter != 0);
		}
		finally {
			selloTiempoBean.close();
		}
	}
}
