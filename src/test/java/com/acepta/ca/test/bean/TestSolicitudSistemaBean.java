package com.acepta.ca.test.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.acepta.ca.bean.SolicitudSistemaBean;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudSistema;
import com.acepta.ca.security.crypto.cert.NameBuilder;
import com.acepta.ca.xml.XMLSolicitudSistema;

import javax.security.auth.x500.X500Principal;

public class TestSolicitudSistemaBean extends TestAceptaBean {
	public void testPrecondiciones() {
		assertExistenAutoridadesVT2();
	}
	
	public void testCicloVidaSolicitud() throws IOException, ParseException, GeneralSecurityException, SAXException, MarshalException, XMLSignatureException {
		Credentials credentials = createFulanitoTahlCredentials();
		SolicitudSistemaBean sistemaBean = getBeanFactoryVT2().createSolicitudSistemaBean();
		try {
			// Ingresar Solicitud
			byte[] data = newSolicitudSistema(credentials);
			
			// FIXME: esto falla porque la solicitud esta firmada con certificado Clase3V2, que no es confiable en la jerarquia VT2.
			SolicitudSistema solicitud = sistemaBean.ingresarSolicitudSistema(data);
			
			checkSolicitudCerrada(sistemaBean, solicitud);
			
			X509Certificate certificate = loadCertificateFromSolicitud(solicitud);
			
			checkCertificate(certificate);
			
			saveCertificate(certificate);
		}
		finally {
			sistemaBean.close();
		}
	}

	private void checkSolicitudCerrada(SolicitudSistemaBean sistemaBean, SolicitudSistema solicitud) {
		assertEquals(12, solicitud.getMesesVigencia());

		assertEquals("96919050-8", solicitud.getSistema().getRut());
		assertEquals("Punto de Firma Electronica Biometrica ID UareU\\12345", solicitud.getSistema().getNombre());
		assertEquals("Sistema de Firma Electronica Biometrica", solicitud.getSistema().getUnidadOrganizacional());
		assertEquals("Acepta.com S.A.", solicitud.getSistema().getOrganizacion());
		
		assertEquals("Metropolitana", solicitud.getSistema().getEstado());
		assertEquals("Santiago", solicitud.getSistema().getCiudad());
		assertEquals("CL", solicitud.getSistema().getPais());
		
		assertEquals("1.3.6.1.4.1.6891.1001.5.3, 1.3.6.1.4.1.6891.1001.5.4", solicitud.getSistema().getPoliticas());
		
		assertTrue(solicitud.isCerrada());
		
		assertEquals(solicitud.getCodigo(), sistemaBean.buscarSolicitudSistemaConCodigo(solicitud.getCodigo()).getCodigo());
		
		boolean found = false;
		for (SolicitudSistema s : sistemaBean.buscarSolicitudesSistemaConNombre(solicitud.getSistema().getNombre())) {
			if (s.getCodigo().equals(solicitud.getCodigo()))
				found = true;
		}
		assertTrue(found);

		found = false;
		for (SolicitudSistema s : sistemaBean.buscarSolicitudesSistemaConEstado(Solicitud.Estado.CERRADA)) {
			if (s.getCodigo().equals(solicitud.getCodigo()))
				found = true;
			
			assertTrue(s.isCerrada());
		}
		assertTrue(found);
	}

	private X509Certificate loadCertificateFromSolicitud(SolicitudSistema solicitud) throws CertificateException {
		CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
		
		X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(solicitud.getCertificado().getData())); 

		assertNotNull(certificate);
		
		return certificate;
	}

	private void checkCertificate(X509Certificate certificate) {
		assertTrue(System.currentTimeMillis() - certificate.getNotBefore().getTime() < 5000);
		NameBuilder nameBuilder = new NameBuilder();
		nameBuilder.append(NameBuilder.COMMON_NAME,  "Punto de Firma Electronica Biometrica ID UareU\\12345");
		nameBuilder.append(NameBuilder.ORGANIZATIONAL_UNIT, "Sistema de Firma Electronica Biometrica");
		nameBuilder.append(NameBuilder.ORGANIZATION, "Acepta.com S.A.");
		nameBuilder.append(NameBuilder.LOCALITY,"Santiago");
		nameBuilder.append(NameBuilder.STATE_OR_PROVINCE, "Metropolitana");
		nameBuilder.append(NameBuilder.COUNTRY, "CL");		
		assertTrue(certificate.getSubjectX500Principal().equals(new X500Principal(nameBuilder.toName().toString())) );
	}

	private void saveCertificate(X509Certificate certificate) throws IOException, GeneralSecurityException {
		File file = new File("target/tmp/TestSistemaBean.cer");
		
		file.getParentFile().mkdirs();
		
		FileOutputStream output = new FileOutputStream(file);
		try {
			output.write(certificate.getEncoded());
		}
		finally {
			output.close();
		}
	}
	
	private byte[] newSolicitudSistema(Credentials credentials) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		return signSolicitudSistema(loadSolicitudSistema(), credentials);
	}

	private byte[] signSolicitudSistema(byte[] data, Credentials credentials) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		try {
			XMLSolicitudSistema solicitud = new XMLSolicitudSistema(input);
	
			((Element) solicitud.getNode("/s:EnvioDE/s:DATA/s:Contenidos//s:NumeroSolicitud")).setTextContent(SolicitudCodigoGenerator.generateCodigo());
			
			solicitud.sign(credentials.getPrivateKey(), credentials.getCertificate());
			
			return solicitud.toXML("ISO-8859-1");
		}
		finally {
			input.close();
		}
	}
	
	private byte[] loadSolicitudSistema() throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/samples/SolicitudSistema.xml");
			try {
				byte[] buf = new byte[1024];
				int len;
				
				while ((len = input.read(buf)) > 0)
					output.write(buf, 0, len);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
}
