package com.acepta.ca.test.bean;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by nhakor on 04-09-15.
 */
public class TestNtpSelloTiempo {

    public static final String TIME_SERVER = "0.pool.ntp.org";
    private static final String UTC_TIME_ZONE = "GMT+0";

    public static void main(String[] args) throws Exception {
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);

        TimeZone timeZone = TimeZone.getTimeZone(UTC_TIME_ZONE);

        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getReturnTime();
        Date time = new Date(returnTime);
        System.out.println("Time from " + TIME_SERVER + ": " + time);
    }
}
