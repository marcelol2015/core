package com.acepta.ca.test.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.acepta.ca.bean.SolicitudPersonaBean;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudPersona;
import com.acepta.ca.security.crypto.PrivateKeyGenerator;
import com.acepta.ca.security.crypto.PrivateKeyRequest;
import com.acepta.ca.security.util.PKCS12;
import com.acepta.ca.xml.XMLSolicitudPersona;

public class TestSolicitudPersonaBean extends TestAceptaBean {
	public void testPrecondiciones() {
		assertExistenAutoridadesG4();
	}

	public void testCicloVidaSolicitud() throws Exception {
		SolicitudPersonaBean personaBean = getBeanFactoryG4().createSolicitudPersonaBean();
		try {
			// Ingresar Solicitud
			byte[] data = newSolicitudPersona();
			
			SolicitudPersona solicitud = personaBean.ingresarSolicitudPersona(data);
			
			checkSolicitudRegistrada(personaBean, solicitud, data);
	
			// Pagar y corregir Solicitud
			assertEquals(solicitud.getCodigo(), personaBean.pagarSolicitudPersona(solicitud.getCodigo(), true, 10000L, solicitud.getAutoridad().isAvanzada(), 12).getCodigo());
	
			solicitud.getTitular().setNombres("RAULcito CHRISTIANcito");
			
			assertEquals(solicitud.getCodigo(), personaBean.corregirSolicitudPersona(solicitud.getCodigo(), solicitud.getTitular()).getCodigo());
			
			// Aprobar Solicitud
			assertEquals(solicitud.getCodigo(), personaBean.aprobarSolicitudPersona(solicitud.getCodigo(), data, null).getCodigo());
			
			solicitud = personaBean.buscarSolicitudPersonaConCodigo(solicitud.getCodigo());
			
			checkSolicitudAprobada(solicitud);
	
			// Generar Llave y Certificado
			KeyPair keyPair = generateKeyPair();
			
			solicitud = generateCertificate(personaBean, solicitud, keyPair);
			
			// Verificar y Grabar Certificado
			X509Certificate certificate = loadCertificateFromSolicitud(solicitud);
			
			checkCertificate(keyPair, certificate);
			
			saveCertificate(keyPair, certificate);
			
			checkSolicitudCerrada(personaBean, solicitud, certificate);
		}
		finally {
			personaBean.close();
		}
	}

	private void checkSolicitudCerrada(SolicitudPersonaBean personaBean, SolicitudPersona solicitud, X509Certificate certificate) {
		boolean foundPorNumeroSerie = false;
		for (Solicitud s : personaBean.buscarSolicitudesPersonaConNumeroSerie(certificate.getSerialNumber())) 
			if (solicitud.getId() == s.getId())
				foundPorNumeroSerie = true;
		assertTrue(foundPorNumeroSerie);
		
		boolean foundPorEstado = false;
		for (Solicitud s : personaBean.buscarSolicitudesPersonaConEstadoCertificado(Certificado.Estado.VALIDO)) 
			if (solicitud.getId() == s.getId())
				foundPorEstado = true;
		assertTrue(foundPorEstado);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(certificate.getNotAfter());
		calendar.add(Calendar.MONTH, 1);
		Date fechaFuturo = calendar.getTime();
		
		assertTrue(fechaFuturo.after(certificate.getNotAfter()));
		
		boolean foundPorExpiracion = false;
		for (Solicitud s : personaBean.buscarSolicitudesPersonaConExpiracionEntre(certificate.getNotAfter(), fechaFuturo)) {
			if (solicitud.getId() == s.getId())
				foundPorExpiracion = true;
		}
		assertTrue(foundPorExpiracion);

		calendar.setTime(certificate.getNotAfter());
		calendar.add(Calendar.SECOND, 1);
		Date fechaPostExpiracion = calendar.getTime();
		
		assertTrue(fechaFuturo.after(fechaPostExpiracion));
		assertTrue(fechaPostExpiracion.after(certificate.getNotAfter()));
		
		boolean foundPorNoExpiracion = false;
		for (Solicitud s : personaBean.buscarSolicitudesPersonaConExpiracionEntre(fechaPostExpiracion, fechaFuturo)) {
			if (solicitud.getId() == s.getId())
				foundPorNoExpiracion = true;
		}
		assertFalse(foundPorNoExpiracion);
	}

	private void checkSolicitudRegistrada(SolicitudPersonaBean personaBean, SolicitudPersona solicitud, byte[] data) {
		assertEquals("Clase3-G4", solicitud.getAutoridad().getNombre());
		assertEquals("10680294-7", solicitud.getTitular().getRut());
		
		assertTrue(solicitud.isPendiente());
		
		assertEquals(solicitud.getId(), personaBean.buscarSolicitudPersonaConCodigo(solicitud.getCodigo()).getId());
		
		assertEquals(solicitud.getId(), personaBean.buscarSolicitudPersonaConNumero(solicitud.getNumero()).getId());

		assertNotNull(personaBean.buscarFotografiaDeSolicitudPersona(solicitud));
		
		boolean existe = false;
		for (SolicitudPersona s : personaBean.buscarSolicitudesPersonaConRutTitular(solicitud.getTitular().getRut())) {
			if (solicitud.getNumero().equals(s.getNumero()))
				existe = true;
		}
		assertTrue(existe);
		
		existe = false;
		for (SolicitudPersona s : personaBean.buscarSolicitudesPersonaConCorreoTitular(solicitud.getTitular().getCorreo())) {
			if (solicitud.getNumero().equals(s.getNumero()))
				existe = true;
		}
		assertTrue(existe);

		boolean solicitudEstaPendiente = false;
		for (SolicitudPersona s : personaBean.buscarSolicitudesPersonaConEstado(Solicitud.Estado.PENDIENTE)) {
			if (solicitud.getNumero() == s.getNumero())
				solicitudEstaPendiente = true;
		}
		
		assertTrue(solicitudEstaPendiente);
		
		Documento documento = personaBean.buscarDocumentoDeSolicitudPersona(solicitud);
		
		assertNotNull(documento);
		
		byte[] documentoData = documento.getData();
		
		assertEquals(data.length, documentoData.length);
		
		for (int index = 0; index < data.length; index++)
			assertEquals(data[index], documentoData[index]);
		
		// verificar caracteres internacionales ASCII'ficados y en mayusculas 
		assertEquals("RAUL CHRISTIAN", solicitud.getTitular().getNombres());
		assertEquals("RAMIREZ", solicitud.getTitular().getApellidoPaterno());
		assertEquals("VALDES", solicitud.getTitular().getApellidoMaterno());
		assertEquals("PERSONA NATURAL AEIOUN AEIOUN", solicitud.getTitular().getProfesion());
		assertEquals("ramirezvaldes2001@yahoo.com", solicitud.getTitular().getCorreo());
	}

	private void checkSolicitudAprobada(SolicitudPersona solicitud) {
		assertEquals("Clase3-G4", solicitud.getAutoridad().getNombre());
		assertEquals("10680294-7", solicitud.getTitular().getRut());

		assertTrue(solicitud.isAprobada());
	}

	private KeyPair generateKeyPair() throws GeneralSecurityException {
		PrivateKeyGenerator privateKeyGenerator = new PrivateKeyGenerator(Security.getProvider("SunRsaSign"));
		
		PrivateKeyRequest privateKeyRequest = new PrivateKeyRequest();
		
		privateKeyRequest.setRSAKeyParameters(512, BigInteger.valueOf(0x10001));
		
		return privateKeyGenerator.generatePrivateKey(privateKeyRequest);
	}

	private SolicitudPersona generateCertificate(SolicitudPersonaBean personaBean, SolicitudPersona solicitud, KeyPair keyPair) throws GeneralSecurityException {
		PKCS10CertificationRequest pkcs10Request = new PKCS10CertificationRequest("SHA256WithRSA", new X509Name("C=Ignorado"), keyPair.getPublic(), null, keyPair.getPrivate(), "SunRsaSign");
		
		String pkcs10Base64 = new String(Base64.encode(pkcs10Request.getEncoded()));

		return personaBean.emitirCertificadoPersona(solicitud.getCodigo(), solicitud.getClave(), pkcs10Base64, null);
	}

	private X509Certificate loadCertificateFromSolicitud( SolicitudPersona solicitud) throws CertificateException {
		CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
		
		X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(solicitud.getCertificado().getData())); 

		assertNotNull(certificate);
		
		return certificate;
	}

	private void checkCertificate(KeyPair keyPair, X509Certificate certificate) {
		assertEquals(certificate.getPublicKey(), keyPair.getPublic());
		
		assertTrue(System.currentTimeMillis() - certificate.getNotBefore().getTime() < 5000);
	}

	private void saveCertificate(KeyPair keyPair, X509Certificate certificate) throws IOException, GeneralSecurityException {
		File file = new File("target/tmp/TestPersonaBean.cer");
		
		file.getParentFile().mkdirs();
		
		FileOutputStream output = new FileOutputStream(file);
		try {
			output.write(certificate.getEncoded());
		}
		finally {
			output.close();
		}

		file = new File("target/tmp/TestPersonaBean.p12");

		output = new FileOutputStream(file);
		try {
			output.write(PKCS12.encodeCertificate(keyPair.getPrivate(), "test", certificate));
		}
		finally {
			output.close();
		}
	}
	
	private byte[] newSolicitudPersona() throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		return signSolicitudPersona(loadSolicitudPersona());
	}

	private byte[] signSolicitudPersona(byte[] data) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		try {
			XMLSolicitudPersona solicitud = new XMLSolicitudPersona(input);
	
			((Element) solicitud.getNode("/s:EnvioDE/s:DATA/s:Contenidos//s:NumeroSolicitud")).setTextContent(SolicitudCodigoGenerator.generateCodigo());

			Credentials credentials = createFulanitoTahlCredentials();
			
			solicitud.sign(credentials.getPrivateKey(), credentials.getCertificate());
			
			return solicitud.toXML("ISO-8859-1");
		}
		finally {
			input.close();
		}
	}
	
	private byte[] loadSolicitudPersona() throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/samples/SolicitudPersona.xml");
			try {
				byte[] buf = new byte[1024];
				int len;
				
				while ((len = input.read(buf)) > 0)
					output.write(buf, 0, len);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
}
