package com.acepta.ca.test.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.acepta.ca.bean.SolicitudSitioWebBean;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudSitioWeb;
import com.acepta.ca.xml.XMLSolicitudSitioWeb;

public class TestSolicitudSitioWebBean extends TestAceptaBean {
	public void testPrecondiciones() {
		assertExistenAutoridadesVT2();
	}

	public void testCicloVidaSolicitud() throws IOException, ParseException, GeneralSecurityException, SAXException, MarshalException, XMLSignatureException {
		Credentials credentials = createFulanitoTahlCredentials();
		
		SolicitudSitioWebBean sitioWebBean = getBeanFactoryVT2().createSolicitudSitioWebBean();
		try {
			// Ingresar Solicitud
			byte[] data = newSolicitudSitioWeb(credentials);
			
			SolicitudSitioWeb solicitud = sitioWebBean.ingresarSolicitudSitioWeb(data);
			
			checkSolicitudCerrada(sitioWebBean, solicitud);
			
			X509Certificate certificate = loadCertificateFromSolicitud(solicitud);
			
			checkCertificate(certificate);
			
			saveCertificate(certificate);
		}
		finally {
			sitioWebBean.close();
		}
	}

	private void checkSolicitudCerrada(SolicitudSitioWebBean sitioWebBean, SolicitudSitioWeb solicitud) {
		assertEquals(12, solicitud.getMesesVigencia());

		assertEquals("www.prueba.cl", solicitud.getSitioWeb().getDominio());
		
		assertEquals("1-9", solicitud.getSitioWeb().getRut());
		assertEquals("PRUEBA S.A.", solicitud.getSitioWeb().getOrganizacion());
		
		assertEquals("LA FLORIDA", solicitud.getSitioWeb().getEstado());
		assertEquals("SANTIAGO", solicitud.getSitioWeb().getCiudad());
		
		assertTrue(solicitud.isCerrada());
		
		assertEquals(solicitud.getCodigo(), sitioWebBean.buscarSolicitudSitioWebConCodigo(solicitud.getCodigo()).getCodigo());
		
		boolean found = false;
		for (SolicitudSitioWeb s : sitioWebBean.buscarSolicitudesSitioWebConDominio(solicitud.getSitioWeb().getDominio())) {
			if (s.getCodigo().equals(solicitud.getCodigo()))
				found = true;
		}
		assertTrue(found);

		found = false;
		for (SolicitudSitioWeb s : sitioWebBean.buscarSolicitudesSitioWebConEstado(Solicitud.Estado.CERRADA)) {
			if (s.getCodigo().equals(solicitud.getCodigo()))
				found = true;
			
			assertTrue(s.isCerrada());
		}
		assertTrue(found);
	}

	private X509Certificate loadCertificateFromSolicitud(SolicitudSitioWeb solicitud) throws CertificateException {
		CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
		
		X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(solicitud.getCertificado().getData())); 

		assertNotNull(certificate);
		
		return certificate;
	}

	private void checkCertificate(X509Certificate certificate) {
		assertTrue(System.currentTimeMillis() - certificate.getNotBefore().getTime() < 5000);
		
		assertEquals("CN=www.prueba.cl, OU=DESARROLLO, O=PRUEBA S.A., L=SANTIAGO, ST=LA FLORIDA, C=CL", certificate.getSubjectDN().getName());
	}

	private void saveCertificate(X509Certificate certificate) throws IOException, GeneralSecurityException {
		File file = new File("target/tmp/TestSitioWebBean.cer");
		
		file.getParentFile().mkdirs();
		
		FileOutputStream output = new FileOutputStream(file);
		try {
			output.write(certificate.getEncoded());
		}
		finally {
			output.close();
		}
	}

	private byte[] newSolicitudSitioWeb(Credentials credentials) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		return signSolicitudSitioWeb(loadSolicitudSitioWeb(), credentials);
	}

	private byte[] signSolicitudSitioWeb(byte[] data, Credentials credentials) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		try {
			XMLSolicitudSitioWeb solicitud = new XMLSolicitudSitioWeb(input);
	
			((Element) solicitud.getNode("/s:EnvioDE/s:DATA/s:Contenidos//s:NumeroSolicitud")).setTextContent(SolicitudCodigoGenerator.generateCodigo());
			
			solicitud.sign(credentials.getPrivateKey(), credentials.getCertificate());
			
			return solicitud.toXML("ISO-8859-1");
		}
		finally {
			input.close();
		}
	}
	
	private byte[] loadSolicitudSitioWeb() throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/samples/SolicitudSitioWeb.xml");
			try {
				byte[] buf = new byte[1024];
				int len;
				
				while ((len = input.read(buf)) > 0)
					output.write(buf, 0, len);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
}
