package com.acepta.ca.test.bean;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.acepta.ca.bean.CAFCertificateBag;
import com.acepta.ca.bean.SolicitudCAFBean;
import com.acepta.ca.persistence.CAF;
import com.acepta.ca.persistence.SolicitudCAF;

public class TestSolicitudCAFBean extends TestAceptaBean {
	public void testPrecondiciones() {
		assertExistenAutoridadesVT2();
	}
	
	public void testEmitirCertificadoCAF() throws IOException {
		SolicitudCAFBean cafBean = getBeanFactoryVT2().createSolicitudCAFBean();
		try {
			byte[] data = loadAutorizacionCAF();
			
			CAFCertificateBag bag = cafBean.emitirCertificadoCAFEnFormatoPKCS12(data, "test");
			
			assertNotNull(bag.getData());
			
			File file = new File("target/tmp/TestCAFBean.p12");
			
			file.getParentFile().mkdirs();
			
			FileOutputStream output = new FileOutputStream(file);
			try {
				output.write(bag.getData());
			}
			finally {
				output.close();
			}
		}
		finally {
			cafBean.close();
		}
	}

	public void testBuscarSolicitudCAF() {
		SolicitudCAFBean cafBean = getBeanFactoryVT2().createSolicitudCAFBean();
		try {
			SolicitudCAF solicitud = cafBean.buscarSolicitudCAFConRutTipoFolio("96919050-8", 61, 5000);
			
			CAF caf = solicitud.getCAF();
			
			assertEquals("96919050-8", caf.getRutEmpresa());
			assertEquals("ACEPTA COM S A", caf.getRazonSocial());
			assertEquals(61, caf.getTipoDTE());
			assertEquals(1, caf.getDesdeFolio());
			assertEquals(100000, caf.getHastaFolio());
			assertEquals(100, caf.getLlaveID());
			
			assertNotNull(solicitud.getCertificado().getData());
			
			boolean found = false;
			
			for (SolicitudCAF s : cafBean.buscarSolicitudesCAFConRutEmpresa("96919050-8")) {
				assertEquals("96919050-8", s.getCAF().getRutEmpresa());
				assertEquals("ACEPTA COM S A", s.getCAF().getRazonSocial());
				
				if (s.getCAF().getTipoDTE() == 61 && s.getCAF().getDesdeFolio() == 1 && s.getCAF().getHastaFolio() == 100000 && s.getCAF().getLlaveID() == 100)
					found = true;
			}
			
			assertTrue(found);
		}
		finally {
			cafBean.close();
		}
	}
	
	private byte[] loadAutorizacionCAF() throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/samples/Autorizacion.xml");
			try {
				byte[] buf = new byte[1024];
				int len;
				while ((len = input.read(buf)) > 0)
					output.write(buf, 0, len);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
}
