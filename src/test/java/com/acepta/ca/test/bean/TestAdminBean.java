package com.acepta.ca.test.bean;

import java.security.cert.X509Certificate;

import com.acepta.ca.bean.AceptaBeanFactory;
import com.acepta.ca.bean.AdminBean;
import com.acepta.ca.persistence.Usuario;

public class TestAdminBean extends TestAceptaBean {
	public void testBootstrap() {
		doBootstrap(getBeanFactoryG4());
		doBootstrap(getBeanFactoryVT2());
	}

	private void doBootstrap(AceptaBeanFactory beanFactory) {
		AdminBean adminBean = beanFactory.createAdminBean();
		try {
			if (adminBean.findGrupos().isEmpty())
				adminBean.bootstrap(true, true);
		}
		finally {
			adminBean.close();
		}
	}

	public void testFindAutoridadesV2() {
		doFindAutoridades(getBeanFactoryG4());
		doFindAutoridades(getBeanFactoryVT2());
	}

	private void doFindAutoridades(AceptaBeanFactory beanFactory) {
		AdminBean adminBean = beanFactory.createAdminBean();
		try {
			for (X509Certificate certificate : adminBean.findCertificadosAutoridades())
				assertNotNull(certificate);
		}
		finally {
			adminBean.close();
		}
	}

	public void testRemoveUsuarios() {
		doRemoveUsuarios(getBeanFactoryG4());
		doRemoveUsuarios(getBeanFactoryVT2());
	}

	private void doRemoveUsuarios(AceptaBeanFactory beanFactory) {
		AdminBean adminBean = beanFactory.createAdminBean();
		try {
			for (Usuario usuario : adminBean.findUsuarios())
				adminBean.removeUsuario(usuario.getRut());
		}
		finally {
			adminBean.close();
		}
	}
	
	public void testAddUsuarios() {
		doAddUsuarios(getBeanFactoryG4());
		doAddUsuarios(getBeanFactoryVT2());
	}

	private void doAddUsuarios(AceptaBeanFactory beanFactory) {
		AdminBean adminBean = beanFactory.createAdminBean();
		try {
			adminBean.addUsuario("15411724-5", "JOSE HERNAN SALINAS ARREDONDO", "AceptaOperadorRegistro");
			adminBean.addUsuario("9832793-2", "ENRIQUE EDUARDO VELASCO AGUIRRE", "AceptaOperadorValidacion");
			
			adminBean.addUsuario("14154242-7", "ALEX ALEJANDRO LORCA ROZAS",
					"AceptaOperadorRegistro", "AceptaOperadorValidacion",
					"AceptaSitioWebOperadorRegistro", "AceptaSitioWebOperadorValidacion",
					"AceptaSistemaOperadorRegistro", "AceptaSistemaOperadorValidacion",
					"AceptaCodigoOperadorRegistro", "AceptaCodigoOperadorValidacion");

			adminBean.addUsuario("1-9", "FULANITO DE TAHL",
					"AceptaOperadorRegistro", "AceptaOperadorValidacion",
					"AceptaSitioWebOperadorRegistro", "AceptaSitioWebOperadorValidacion",
					"AceptaSistemaOperadorRegistro", "AceptaSistemaOperadorValidacion",
					"AceptaCodigoOperadorRegistro", "AceptaCodigoOperadorValidacion", "AceptaOperadorRegistroClase2");
		}
		finally {
			adminBean.close();
		}
	}
	
	public void testFindUsuarios() {
		doFindUsuarios(getBeanFactoryG4());
		doFindUsuarios(getBeanFactoryVT2());
	}

	private void doFindUsuarios(AceptaBeanFactory beanFactory) throws AssertionError {
		AdminBean adminBean = beanFactory.createAdminBean();
		try {
			assertFalse(adminBean.findUsuarios().isEmpty());
			
			for (Usuario usuario : adminBean.findUsuarios()) {
				if ("15411724-5".equals(usuario.getRut())) {
					assertEquals("JOSE HERNAN SALINAS ARREDONDO", usuario.getNombre());
				}
				else if ("9832793-2".equals(usuario.getRut())) {
					assertEquals("ENRIQUE EDUARDO VELASCO AGUIRRE", usuario.getNombre());
				}
				else if ("14154242-7".equals(usuario.getRut())) {
					assertEquals("ALEX ALEJANDRO LORCA ROZAS", usuario.getNombre());
				}
				else if ("1-9".equals(usuario.getRut())) {
					assertEquals("FULANITO DE TAHL", usuario.getNombre());
				}
				else {
					throw new AssertionError();
				}
			}
		}
		finally {
			adminBean.close();
		}
	}
}