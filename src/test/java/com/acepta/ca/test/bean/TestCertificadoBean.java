package com.acepta.ca.test.bean;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.util.Collection;

import com.acepta.ca.bean.CertificadoBean;
import com.acepta.ca.persistence.CRL;
import com.acepta.ca.persistence.Certificado;

/**
 * Deben existir certificados CAF validos (ie. vigentes o expirados).
 */
public class TestCertificadoBean extends TestAceptaBean {
	public void testPrecondiciones() {
		assertExistenAutoridadesG4();
		assertExistenAutoridadesVT2();
	}

	public void testNombresAutoridades() {
		CertificadoBean certificadoBeanG4 = getBeanFactoryG4().createCertificadoBean();
		try {
			Collection<String> nombresAutoridadesCertificadoras = certificadoBeanG4.getNombresAutoridadesCertificadoras();
			
			assertFalse(nombresAutoridadesCertificadoras.contains("Raiz-G4"));
			assertTrue(nombresAutoridadesCertificadoras.contains("Clase3-G4"));
			assertTrue(nombresAutoridadesCertificadoras.contains("Clase2-G4"));
			assertTrue(nombresAutoridadesCertificadoras.contains("FirmaAvanzada-G4"));
		}
		finally {
			certificadoBeanG4.close();
		}
		
		CertificadoBean certificadoBeanVT2 = getBeanFactoryVT2().createCertificadoBean();
		try {
			Collection<String> nombresAutoridadesCertificadoras = certificadoBeanVT2.getNombresAutoridadesCertificadoras();
			
			assertFalse(nombresAutoridadesCertificadoras.contains("RaizVT2"));
			assertTrue(nombresAutoridadesCertificadoras.contains("SistemaVT2"));
			assertTrue(nombresAutoridadesCertificadoras.contains("CAFVT2"));
			assertFalse(nombresAutoridadesCertificadoras.contains("SelloTiempoVT2"));
			assertTrue(nombresAutoridadesCertificadoras.contains("CodigoVT2"));
			assertTrue(nombresAutoridadesCertificadoras.contains("SitioWebVT2"));
		}
		finally {
			certificadoBeanVT2.close();
		}
	}
	
	public void testBuscarRevocarRestablecerCertificados() throws CRLException, IOException, CertificateException {
		CertificadoBean certificadoBean = getBeanFactoryVT2().createCertificadoBean();
		try {
			int validos = 0;
			
			for (Certificado certificado : certificadoBean.buscarCertificadosConEmisorEstado("CAFVT2", Certificado.Estado.VALIDO)) {
				assertEquals("CAFVT2", certificado.getEmisor().getNombre());
				assertTrue(certificado.isValido());
				
				certificadoBean.suspenderCertificadoConEmisorNumeroSerie(certificado.getEmisor().getNombre(), certificado.getNumeroSerie(), "Suspension de prueba");
				
				validos++;
			}
	
			
			CRL crl = certificadoBean.generarCRLConEmisorVigencia("CAFVT2", 10);
			
			assertNotNull(crl);
			
			assertEquals(crl.getId(), certificadoBean.buscarUltimaCRLConEmisor("CAFVT2").getId());
			
			int suspendidos = 0;
			
			for (Certificado certificado : certificadoBean.buscarCertificadosConEmisorEstado("CAFVT2", Certificado.Estado.REVOCADO)) {
				assertEquals("CAFVT2", certificado.getEmisor().getNombre());
				assertTrue(certificado.isRevocado());
				
				if (certificado.isSuspendido()) {
					certificadoBean.restablecerCertificadoConEmisorNumeroSerie(certificado.getEmisor().getNombre(), certificado.getNumeroSerie(), "Restablecimiento de prueba");
					
					suspendidos++;
				}
				
				
				boolean certificadoInCRL = false;
				
				CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
				
				X509CRL x509crl = (X509CRL) certificateFactory.generateCRL(new ByteArrayInputStream(crl.getData()));
				
				for (X509CRLEntry crlEntry : x509crl.getRevokedCertificates()) {
					if (crlEntry.getSerialNumber().equals(certificado.getNumeroSerie()))
						certificadoInCRL = true;
				}
				
				assertTrue(certificadoInCRL);
				
			}
	
			assertEquals(validos, suspendidos);
			
			assertTrue(validos != 0 && suspendidos != 0);
			
			File file = new File("target/tmp/TestCertificadoBean.crl");
			
			file.getParentFile().mkdirs();
			
			FileOutputStream output = new FileOutputStream(file);
			try {
				output.write(crl.getData());
			}
			finally {
				output.close();
			}
		}
		finally {
			certificadoBean.close();
		}
	}
}
