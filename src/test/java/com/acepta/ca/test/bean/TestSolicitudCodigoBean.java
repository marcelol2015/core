package com.acepta.ca.test.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.acepta.ca.bean.SolicitudCodigoBean;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudCodigo;
import com.acepta.ca.xml.XMLSolicitudCodigo;

public class TestSolicitudCodigoBean extends TestAceptaBean {
	public void testPrecondiciones() {
		assertExistenAutoridadesG4();
	}

	public void testCicloVidaSolicitud() throws IOException, ParseException, GeneralSecurityException, SAXException, MarshalException, XMLSignatureException {
		Credentials credentials = createFulanitoTahlCredentials();
		
		SolicitudCodigoBean codigoBean = getBeanFactoryVT2().createSolicitudCodigoBean();
		try {
			// Ingresar Solicitud
			byte[] data = newSolicitudCodigo(credentials);
			
			SolicitudCodigo solicitud = codigoBean.ingresarSolicitudCodigo(data);
			
			checkSolicitudCerrada(codigoBean, solicitud);
			
			X509Certificate certificate = loadCertificateFromSolicitud(solicitud);
			
			checkCertificate(certificate);
			
			saveCertificate(certificate);
		}
		finally {
			codigoBean.close();
		}
	}

	private void checkSolicitudCerrada(SolicitudCodigoBean sitioWebBean, SolicitudCodigo solicitud) {
		assertEquals(12, solicitud.getMesesVigencia());

		assertEquals("EQUIPO DESARROLLO ABC", solicitud.getCompania().getNombre());
		
		assertEquals("1-9", solicitud.getCompania().getRut());
		assertEquals("PRUEBA S.A.", solicitud.getCompania().getOrganizacion());
		assertEquals("DESARROLLO", solicitud.getCompania().getUnidadOrganizacional());
		
		assertEquals("LA FLORIDA", solicitud.getCompania().getEstado());
		assertEquals("SANTIAGO", solicitud.getCompania().getCiudad());
		
		assertTrue(solicitud.isCerrada());
		
		assertEquals(solicitud.getCodigo(), sitioWebBean.buscarSolicitudCodigoConCodigo(solicitud.getCodigo()).getCodigo());
		
		boolean found = false;
		for (SolicitudCodigo s : sitioWebBean.buscarSolicitudesCodigoConEstado(Solicitud.Estado.CERRADA)) {
			if (s.getCodigo().equals(solicitud.getCodigo()))
				found = true;
			
			assertTrue(s.isCerrada());
		}
		assertTrue(found);
	}

	private X509Certificate loadCertificateFromSolicitud(SolicitudCodigo solicitud) throws CertificateException {
		CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
		
		X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(solicitud.getCertificado().getData())); 

		assertNotNull(certificate);
		
		return certificate;
	}

	private void checkCertificate(X509Certificate certificate) {
		assertTrue(System.currentTimeMillis() - certificate.getNotBefore().getTime() < 5000);
		
		assertEquals("SERIALNUMBER=1-9, CN=EQUIPO DESARROLLO ABC, OU=DESARROLLO, O=PRUEBA S.A., L=SANTIAGO, ST=LA FLORIDA, C=CL", certificate.getSubjectDN().getName());
	}

	private void saveCertificate(X509Certificate certificate) throws IOException, GeneralSecurityException {
		File file = new File("target/tmp/TestCodigoBean.cer");
		
		file.getParentFile().mkdirs();
		
		FileOutputStream output = new FileOutputStream(file);
		try {
			output.write(certificate.getEncoded());
		}
		finally {
			output.close();
		}
	}

	private byte[] newSolicitudCodigo(Credentials credentials) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		return signSolicitudCodigo(loadSolicitudCodigo(), credentials);
	}

	private byte[] signSolicitudCodigo(byte[] data, Credentials credentials) throws IOException, SAXException, GeneralSecurityException, MarshalException, XMLSignatureException {
		ByteArrayInputStream input = new ByteArrayInputStream(data);
		try {
			XMLSolicitudCodigo solicitud = new XMLSolicitudCodigo(input);
	
			((Element) solicitud.getNode("/s:EnvioDE/s:DATA/s:Contenidos//s:NumeroSolicitud")).setTextContent(SolicitudCodigoGenerator.generateCodigo());
			
			solicitud.sign(credentials.getPrivateKey(), credentials.getCertificate());
			
			return solicitud.toXML("ISO-8859-1");
		}
		finally {
			input.close();
		}
	}
	
	private byte[] loadSolicitudCodigo() throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/samples/SolicitudCodigo.xml");
			try {
				byte[] buf = new byte[1024];
				int len;
				
				while ((len = input.read(buf)) > 0)
					output.write(buf, 0, len);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
}
