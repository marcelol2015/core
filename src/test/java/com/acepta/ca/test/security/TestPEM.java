package com.acepta.ca.test.security;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;

import junit.framework.TestCase;

import com.acepta.ca.security.util.PEM;

public class TestPEM extends TestCase {
	private static final String RSA_PRIVATE_KEY =
		"-----BEGIN RSA PRIVATE KEY-----\n" +
		"MIIBOgIBAAJBANR5c4blUI8KkmqvQ+4G5o5iMTEtbxdv5ogx+kgTsAWH7MTtWvLC\n" +
		"C4o5asGWyO4YsnSqVZwdF2GK0ikEZ2VkvK0CAQMCQQCNpk0EmOBfXGGcdNf0BJm0\n" +
		"QXYgyPS6SpmwIVGFYnVZBBDFXNHqjatdGxpx6M+fQzqVG8808w+ClJUGEo4DdMkr\n" +
		"AiEA+kxnUGjP5ZqBtxmsz4NVtFB7F3ye0NrGSq2Ljy3sMBECIQDZUHrPqh2k5A8L\n" +
		"/QzB+94mRIWHUBGvQuWn8l0DMkle3QIhAKbdmjWbNUO8VnoRHd+s482K/LpTFIs8\n" +
		"hDHJB7TJSCALAiEAkOBR38a+be1fXVNd1qfpbthZBOALyiyZGqGTV3bblJMCIGGr\n" +
		"YIbjOngpph4pxjV1acp6fzN4UlegmItDT2PF0vL/\n" +
		"-----END RSA PRIVATE KEY-----\n";
		
	private static final String RSA_PUBLIC_KEY = 
		"-----BEGIN PUBLIC KEY-----\n" +
		"MFowDQYJKoZIhvcNAQEBBQADSQAwRgJBANR5c4blUI8KkmqvQ+4G5o5iMTEtbxdv\n" +
		"5ogx+kgTsAWH7MTtWvLCC4o5asGWyO4YsnSqVZwdF2GK0ikEZ2VkvK0CAQM=\n" +
		"-----END PUBLIC KEY-----\n";
		
	private static final String X509_CERTIFICATE = 
		"-----BEGIN CERTIFICATE-----\n" +
		"MIIFxTCCBK2gAwIBAgIBATANBgkqhkiG9w0BAQUFADCBsjELMAkGA1UEBhMCQ0wx\n" +
		"GDAWBgNVBAoMD0FjZXB0YS5jb20gUy5BLjEgMB4GA1UECwwXQXV0b3JpZGFkIENl\n" +
		"cnRpZmljYWRvcmExMjAwBgNVBAMMKUFjZXB0YS5jb20gQ29kaWdvIGRlIEFzaWdu\n" +
		"YWNpb24gZGUgRm9saW9zMR4wHAYJKoZIhvcNAQkBFg9pbmZvQGFjZXB0YS5jb20x\n" +
		"EzARBgNVBAUTCjk2OTE5MDUwLTgwHhcNMDcwODI0MTUxNDQ2WhcNMDcwODI0MTUx\n" +
		"NDQ2WjBLMQswCQYDVQQGEwJDTDE8MDoGA1UEAwwzQ0VSVElGSUNBQ0lPTiBBQ01F\n" +
		"IFMgQSBGQUNUVVJBUyBGT0xJT1MgMTAwIEFMIDEwMDAwMIGeMA0GCSqGSIb3DQEB\n" +
		"AQUAA4GMADCBiAKBgDM75de9UDvVGGtVb8Qu18AQdRmo0ZMhKYFuAi0sgWtGTElZ\n" +
		"duOyjBF+uAjtINVgL2DT5t11iPMZ/sFTcZtX1b5YiffoO8sJINjqYP+zfE87yImV\n" +
		"Ev12pwHY+jO+He1A8vxUc9TBX0MMGyDqKvZHjDhyZaWkKFPh5pz+TSXAvP5/AgMB\n" +
		"AAGjggLPMIICyzAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFOo9NzB8wf/viTaR\n" +
		"DjujqGgujMPwMB0GA1UdDgQWBBQB+3tBUAGZ1NolfPFXLMKzSkH51jALBgNVHQ8E\n" +
		"BAMCBsAwggEZBgNVHSAEggEQMIIBDDCCAQgGCCsGAQQBtWsOMIH7MDUGCCsGAQUF\n" +
		"BwIBFilodHRwOi8vd3d3LmFjZXB0YS5jb20vQ1BTL3YxLjAvQ1BDQUYuaHRtbDCB\n" +
		"wQYIKwYBBQUHAgIwgbQwFhYPQWNlcHRhLmNvbSBTLkEuMAMCAQEagZlQYXJhIGxh\n" +
		"IGVtaXNp824gZGUgZXN0ZSB0aXBvIGRlIGNlcnRpZmljYWRvcywgQWNlcHRhLmNv\n" +
		"bSBz82xvIGNhbWJpYSBlbCBmb3JtYXRvIGRlIGxhcyBjbGF2ZXMgZW50cmVnYWRh\n" +
		"cyBwb3IgZWwgU0lJLiBObyByZWFsaXphIHZhbGlkYWNpb25lcyBhZGljaW9uYWxl\n" +
		"cy4wXAYDVR0SBFUwU6AYBggrBgEEAcEBAqAMFgo5NjkxOTA1MC04oCYGCisGAQUF\n" +
		"BwASCAOgGDAWDAo5NjkxOTA1MC04BggrBgEEAcEBAoEPaW5mb0BhY2VwdGEuY29t\n" +
		"MDwGA1UdEQQ1MDOgMQYKKwYBBQUHABIIA6AjMCEMFjk5OTk5OTktOXwzM3wxMDB8\n" +
		"MTAwMDAGBysGAQQBMgIwMwYIKwYBBQUHAQEEJzAlMCMGCCsGAQUFBzABhhdodHRw\n" +
		"Oi8vb2NzcC5hY2VwdGEuY29tLzA0BgNVHR8ELTArMCmgJ6AlhiNodHRwOi8vY3Js\n" +
		"LmFjZXB0YS5jb20vQWNlcHRhQ0FGLmNybDBKBgkrBgEEAbVrMgEBAf8EOgQ4PENB\n" +
		"Rj4uLi45OTk5OTk5LTkuLi5BQ01FIFMgQS4uLjMzLi4uMTAwLi4uMTAwMDAuLi48\n" +
		"L0NBRj4wDQYJKoZIhvcNAQEFBQADggEBAJjfDvhwiRJ4eiwEYnTU1Suf4k+7oqFE\n" +
		"uZDBClzugI+Oh8bbOuhUq8WhUOjcSZ4OGFiTEgABOxk70VCgwNlcU6Lm32cEEKtR\n" +
		"BKEeZSbE8g1tCKUDEtLbqg7r6hizVNG260+hZqvX2msP5kJ6GiUQD5TU2Tpg1bFH\n" +
		"rnFTS/cRwv5IK94iMXFlAjpQhFI9ICjpTnsKmvGf49+FNyK2E0FfzaBgKabbipgf\n" +
		"78v9SR7fXw9fy68BPeRv+lBvpakkeGDAo+4h2cVKhiYrSGbXTXjNFxfbmJcbPaLw\n" +
		"LLkvXSyWAg2rXdiIZboGAFIt+byoxKnQkzP9tOgGw3s/RE24C6IwcDs=\n" +
		"-----END CERTIFICATE-----\n";

	public void testPEM() throws IOException, GeneralSecurityException {
		PrivateKey privateKey = PEM.decodeRSAPrivateKey(RSA_PRIVATE_KEY);
		
		assertNotNull(privateKey);
		
		PublicKey publicKey = PEM.decodeRSAPublicKey(RSA_PUBLIC_KEY);
		
		assertNotNull(publicKey);
		
		X509Certificate certificate = PEM.decodeX509Certificate(X509_CERTIFICATE);
		
		assertNotNull(certificate);
		
		String pemPublicKey = PEM.encodeRSAPublicKey(publicKey);
		
		assertNotNull(pemPublicKey);
		
		String pemPrivateKey = PEM.encodeRSAPrivateKey(privateKey, "test");
		
		assertNotNull(pemPrivateKey);
		
		String pemCertificate = PEM.encodeX509Certificate(certificate);
		
		assertNotNull(pemCertificate);
		
		
		checkKeyPair(publicKey, privateKey);
		
		
		new File("target/tmp").mkdirs();
		
		FileWriter writer = new FileWriter(new File("target/tmp/testpem.pem"));
		try {
			writer.write(pemPrivateKey);
			writer.write(pemCertificate);
		}
		finally {
			writer.close();
		}
	}

	private void checkKeyPair(PublicKey publicKey, PrivateKey privateKey) throws GeneralSecurityException {
		byte[] sig;
		
		{
			Signature signature = Signature.getInstance("SHA1WithRSA");
		
			signature.initSign(privateKey);
			
			signature.update("Hola".getBytes());
			
			sig = signature.sign();
		}
		
		{
			Signature signature = Signature.getInstance("SHA1WithRSA");
		
			signature.initVerify(publicKey);
			
			signature.update("Hola".getBytes());

			assertTrue(signature.verify(sig));
		}
	}
}
