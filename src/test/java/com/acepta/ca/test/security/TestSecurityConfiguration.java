package com.acepta.ca.test.security;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.xml.xpath.XPathExpressionException;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.security.AceptaSecurityConfiguration;

public class TestSecurityConfiguration extends TestCase {
	public void testSecurityConfiguration() throws SAXException, IOException, XPathExpressionException {
		InputStream input = getClass().getResourceAsStream("/META-INF/security.xml");
		try {
			AceptaSecurityConfiguration configuration = new AceptaSecurityConfiguration("aceptaca-g4-development", input);

			assertEquals("aceptaca-g4-development", configuration.getSecurityTokenName());
			
			assertEquals("com.acepta.ca.security.provider.JavaSecurityProvider", configuration.getProvider());
			assertEquals("com.acepta.ca.security.authority.g4.AceptaG4AuthorityProvider", configuration.getAuthorityProvider());
			
			assertEquals("target/tmp/keystore/aceptaca-g4-development.jks", configuration.getProperty("java.keystore.file"));
			assertEquals("OBF:9046B183CBCA", configuration.getProperty("java.keystore.password"));
			
			Collection<String> propertyNames = configuration.getPropertyNames();
			
			assertTrue(propertyNames.contains("java.keystore.file"));
			assertTrue(propertyNames.contains("java.keystore.password"));
			
			assertEquals(2, propertyNames.size());
		}
		finally {
			input.close();
		}
	}
}
