package com.acepta.ca.test.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.cert.X509Certificate;

import junit.framework.TestCase;

import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;
import com.acepta.ca.security.PersonaCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.util.PKCS12;

public class TestBootstrap extends TestCase {
	private AceptaSecurityManagerFactory securityManagerFactoryG4;
	private AceptaSecurityManagerFactory securityManagerFactoryVT2;
	
	public void setUp() {
		createEmptyTestKeyStore("target/tmp/keystore/aceptaca-g4-development.jks");
		createEmptyTestKeyStore("target/tmp/keystore/aceptaca-vt2-development.jks");
		
		setSecurityManagerFactoryG4(new AceptaSecurityManagerFactory("aceptaca-g4-development"));
		setSecurityManagerFactoryVT2(new AceptaSecurityManagerFactory("aceptaca-vt2-development"));
	}
	
	public void tearDown() {
		getSecurityManagerFactoryG4().close();
		getSecurityManagerFactoryVT2().close();
	}
	
	public void testBootstrap() throws GeneralSecurityException {
		getSecurityManagerFactoryG4().bootstrap();
		getSecurityManagerFactoryVT2().bootstrap();
	}
	
	public void testAutoridadRaiz() {
		AceptaSecurityManager manager = createSecurityManagerG4();
		try {
			Authority authority = manager.getAuthority("Raiz-G4");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());
			
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}

	public void testAutoridadClase2() {
		AceptaSecurityManager manager = createSecurityManagerG4();
		try {
			Authority authority = manager.getAuthority("Clase2-G4");

			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));

			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());

			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}
	
	public void testAutoridadClase3() {
		AceptaSecurityManager manager = createSecurityManagerG4();
		try {
			Authority authority = manager.getAuthority("Clase3-G4");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));
			
			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}
	
	public void testAutoridadFirmaAvanzada() {
		AceptaSecurityManager manager = createSecurityManagerG4();
		try {
			Authority authority = manager.getAuthority("FirmaAvanzada-G4");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));
			
			assertEquals(Authority.LegalProtection.FIRMA_AVANZADA, authority.getLegalProtection());
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}

	public void testAutoridadSelloTiempo() {
		AceptaSecurityManager manager = createSecurityManagerG4();
		try {
			Authority authority = manager.getAuthority("SelloTiempo-G4");

			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.TIME_STAMPING));

			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());

			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}

	public void testAutoridadRaizVT2() {
		AceptaSecurityManager manager = createSecurityManagerVT2();
		try {
			Authority authority = manager.getAuthority("RaizVT2");

			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));

			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}
	
	public void testAutoridadSitioWeb() {
		AceptaSecurityManager manager = createSecurityManagerVT2();
		try {
			Authority authority = manager.getAuthority("SitioWebVT2");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));
			
			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}

	public void testAutoridadCAF() {
		AceptaSecurityManager manager = createSecurityManagerVT2();
		try {
			Authority authority = manager.getAuthority("CAFVT2");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));
			
			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}

	public void testAutoridadSistema() {
		AceptaSecurityManager manager = createSecurityManagerVT2();
		try {
			Authority authority = manager.getAuthority("SistemaVT2");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));
			
			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}
	
	public void testAutoridadCodigo() {
		AceptaSecurityManager manager = createSecurityManagerVT2();
		try {
			Authority authority = manager.getAuthority("CodigoVT2");
			
			assertNotNull(authority);

			assertNotNull(authority.getPrivateKey());
			assertNotNull(authority.getCertificate());

			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.KEY_CERT_SIGN));
			assertTrue(authority.getKeyUsage().contains(Authority.KeyPurpose.CRL_SIGN));
			
			assertEquals(Authority.LegalProtection.FIRMA_SIMPLE, authority.getLegalProtection());
			
			saveAuthorityCertificate(authority);
		}
		finally {
			manager.close();
		}
	}
	
	public void testCreateFulanitoTahlPKCS12() throws Exception {
		AceptaSecurityManager manager = createSecurityManagerG4();
		try {
			Authority authority = manager.getAuthority("Clase3-G4");

			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(2048);
			
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			
			assertNotNull(keyPair);

			PersonaCertificateRequest request = new PersonaCertificateRequest();

			request.setSerialNumber(BigInteger.ONE);
			request.setSubjectName("1-9", "fulanito.tahl@acepta.com", "FULANITO DE TAHL", "CONEJILLO DE INDIAS", "CL");
			request.setNotBefore(authority.getCertificate().getNotBefore());
			request.setNotAfter(authority.getCertificate().getNotAfter());
			request.setSubjectPublicKey(keyPair.getPublic());

			X509Certificate certificate = authority.generateCertificate(request);
			
			assertNotNull(certificate);
			
			byte[] pkcs12 = PKCS12.encodeCertificate(keyPair.getPrivate(), "test", certificate);
			
			assertNotNull(pkcs12);
			
			File file = new File("target/tmp/FulanitoTahl.p12");

			FileOutputStream output = new FileOutputStream(file);
			try {
				output.write(pkcs12);
			}
			finally {
				output.close();
			}
		}
		finally {
			manager.close();
		}
	}
	

	public AceptaSecurityManager createSecurityManagerG4() {
		return getSecurityManagerFactoryG4().createSecurityManager();
	}
	
	public AceptaSecurityManager createSecurityManagerVT2() {
		return getSecurityManagerFactoryVT2().createSecurityManager();
	}

	public AceptaSecurityManagerFactory getSecurityManagerFactoryG4() {
		return securityManagerFactoryG4;
	}

	public void setSecurityManagerFactoryG4(AceptaSecurityManagerFactory securityManagerFactoryG4) {
		this.securityManagerFactoryG4 = securityManagerFactoryG4;
	}

	public AceptaSecurityManagerFactory getSecurityManagerFactoryVT2() {
		return securityManagerFactoryVT2;
	}

	public void setSecurityManagerFactoryVT2(AceptaSecurityManagerFactory securityManagerFactoryVT2) {
		this.securityManagerFactoryVT2 = securityManagerFactoryVT2;
	}

	private void createEmptyTestKeyStore(String name) {
		try {
			File testKeyStoreFile = new File(name);
			
			if (!testKeyStoreFile.exists()) {
				testKeyStoreFile.getParentFile().mkdirs();
				
				OutputStream output = new FileOutputStream(testKeyStoreFile);
				try {
					InputStream input = getClass().getResourceAsStream("/META-INF/keystore/aceptaca.jks");
					try {
						byte[] buffer = new byte[1024];
						int length;
						
						while ((length = input.read(buffer)) > 0)
							output.write(buffer, 0, length);
					}
					finally {
						input.close();
					}
				}
				finally {
					output.close();
				}
			}
		}
		catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}	
	
	private void saveAuthorityCertificate(Authority authority) {
		File tempDir = new File("target/tmp");
		
		tempDir.mkdirs();
		
		File file = new File(tempDir, authority.getAlias() + ".cer");

		try {
			FileOutputStream output = new FileOutputStream(file);
			try {
				output.write(authority.getCertificate().getEncoded());
			}
			finally {
				output.close();
			}
		}
		catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}
}
