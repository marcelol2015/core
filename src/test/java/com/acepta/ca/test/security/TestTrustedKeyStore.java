package com.acepta.ca.test.security;

import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import junit.framework.TestCase;

import com.acepta.ca.security.provider.AceptaV1TrustedKeyStore;
import com.acepta.ca.security.provider.JavaTrustedKeyStore;

public class TestTrustedKeyStore extends TestCase {
	public void testJavaTrustedKeyStore() throws GeneralSecurityException {
		JavaTrustedKeyStore keyStore = new JavaTrustedKeyStore();
		
		for (X509Certificate certificate : keyStore.getTrustedCertificates()) {
			assertNotNull(certificate);
		
			// assertTrue(isCA(certificate));
		}
	}
	
	public void testAceptaTrustedKeyStore() throws GeneralSecurityException {
		AceptaV1TrustedKeyStore keyStore = new AceptaV1TrustedKeyStore();
		
		for (X509Certificate certificate : keyStore.getTrustedCertificates()) {
			assertNotNull(certificate);
			
			assertTrue(isCA(certificate));
		}
	}

	private static boolean isCA(X509Certificate certificate) {
		return certificate.getBasicConstraints() >= 0;
	}	
}
