package com.acepta.ca.test.security;

import java.util.Random;

import com.acepta.ca.security.util.Password;

import junit.framework.TestCase;

public class TestPassword extends TestCase {
	public void testObfuscator() {
		Random random = new Random(System.currentTimeMillis());
		
		for (int times = 0; times < 1000; times++) {
			StringBuffer buffer = new StringBuffer();
			
			for (int length = 0; length < 1 + random.nextInt(64); length++) {
				buffer.append((char) (32 + random.nextInt(96)));
			}
			
			String password = buffer.toString();

			String encryptedPassword = Password.encryptPassword(password);
			
			String decryptedPassword = Password.decryptPassword(encryptedPassword);

			assertEquals(password, decryptedPassword);
		}
	}
}
