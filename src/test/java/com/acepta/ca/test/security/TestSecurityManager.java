package com.acepta.ca.test.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CRLException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import junit.framework.TestCase;

import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;
import com.acepta.ca.security.CAFCertificateRequest;
import com.acepta.ca.security.CodigoCertificateRequest;
import com.acepta.ca.security.PersonaCertificateRequest;
import com.acepta.ca.security.SistemaCertificateRequest;
import com.acepta.ca.security.SitioWebCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.cert.CRLEntry;
import com.acepta.ca.security.crypto.cert.CRLRequest;
import com.acepta.ca.security.util.X509;

public class TestSecurityManager extends TestCase {
	AceptaSecurityManagerFactory securityManagerFactoryG4;
	AceptaSecurityManagerFactory securityManagerFactoryVT2;
	
	public void setUp() {
		setSecurityManagerFactoryG4(new AceptaSecurityManagerFactory("aceptaca-g4-development"));
		setSecurityManagerFactoryVT2(new AceptaSecurityManagerFactory("aceptaca-vt2-development"));
	}
	
	public void tearDown() {
		getSecurityManagerFactoryG4().close();
		getSecurityManagerFactoryVT2().close();
	}
	
	public void testPersonaCertificateGeneration() throws GeneralSecurityException, IOException {
		AceptaSecurityManager manager = getSecurityManagerFactoryG4().createSecurityManager();
		try {
			for (String alias : new String[] { "Clase3-G4", "FirmaAvanzada-G4" }) {
				Authority authority = manager.getAuthority(alias);
				
				PersonaCertificateRequest request = new PersonaCertificateRequest();
	
				request.setSerialNumber(BigInteger.ONE);
				request.setSubjectName("1-9", "juan@perez.cl", "Juan Perez", "Conejillo de Indias", "CL");
				request.setNotBefore(createDate());
				request.setNotAfter(createDate());
				request.setSubjectPublicKey(createTestPublicKey());
				
				X509Certificate certificate = authority.generateCertificate(request);
				
				assertNotNull(certificate);
				
				assertEquals(request.getSerialNumber(), certificate.getSerialNumber());
				assertEquals(request.getNotBefore(), certificate.getNotBefore());
				assertEquals(request.getNotAfter(), certificate.getNotAfter());
				assertEquals(request.getSubjectName(), certificate.getSubjectDN());
				assertEquals(request.getSubjectPublicKey(), certificate.getPublicKey());
				
				certificate.verify(authority.getCertificate().getPublicKey());
				
				assertEquals(request.getSubjectRUT(), X509.decodeSubjectRUTFromCertificate(certificate));
				
				saveCertificate("target/tmp/" + alias + "-prueba.cer", certificate);
			}
		}
		finally {
			manager.close();
		}
	}

	public void testSitioWebCertificateGeneration() throws GeneralSecurityException, IOException {
		AceptaSecurityManager manager = getSecurityManagerFactoryVT2().createSecurityManager();
		try {
			Authority authority = manager.getAuthority("SitioWebVT2");
			
			SitioWebCertificateRequest request = new SitioWebCertificateRequest();

			request.setSerialNumber(BigInteger.ONE);
			request.setSubjectName("webmaster@prueba.cl", "www.prueba.cl", "Desarrollo", "Acepta.com S.A.", "Santiago", "Metropolitana", "CL");
			request.setNotBefore(createDate());
			request.setNotAfter(createDate());
			request.setSubjectPublicKey(createTestPublicKey());
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			assertNotNull(certificate);
			
			assertEquals(request.getSerialNumber(), certificate.getSerialNumber());
			assertEquals(request.getNotBefore(), certificate.getNotBefore());
			assertEquals(request.getNotAfter(), certificate.getNotAfter());
			assertEquals(request.getSubjectName(), certificate.getSubjectDN());
			assertEquals(request.getSubjectPublicKey(), certificate.getPublicKey());
			
			certificate.verify(authority.getCertificate().getPublicKey());
			
			saveCertificate("target/tmp/SitioWebVT2-prueba.cer", certificate);
		}
		finally {
			manager.close();
		}
	}

	public void testCAFCertificateGeneration() throws GeneralSecurityException, IOException {
		AceptaSecurityManager manager = getSecurityManagerFactoryVT2().createSecurityManager();
		try {
			Authority authority = manager.getAuthority("CAFVT2");
			
			CAFCertificateRequest request = new CAFCertificateRequest();

			request.setSerialNumber(BigInteger.ONE);
			request.setSubjectRUT("9999999-9");
			request.setSubjectRazonSocial("ACME S A");
			request.setSubjectTipoDTE(33);
			request.setSubjectDesdeFolio(100);
			request.setSubjectHastaFolio(10000);
			request.setSubjectLlaveID(100);
			request.setSubjectCAF("<CAF>...9999999-9...ACME S A...33...100...10000...</CAF>".getBytes());
			request.setNotBefore(createDate());
			request.setNotAfter(createDate());
			request.setSubjectPublicKey(createTestPublicKey());
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			assertNotNull(certificate);
			
			assertEquals(request.getSerialNumber(), certificate.getSerialNumber());
			assertEquals(request.getNotBefore(), certificate.getNotBefore());
			assertEquals(request.getNotAfter(), certificate.getNotAfter());
			assertEquals(request.getSubjectName(), certificate.getSubjectDN());
			assertEquals(request.getSubjectPublicKey(), certificate.getPublicKey());
			
			certificate.verify(authority.getCertificate().getPublicKey());
			
			saveCertificate("target/tmp/CAFVT2-prueba.cer", certificate);
		}
		finally {
			manager.close();
		}
	}

	public void testSistemaCertificateGeneration() throws InvalidKeySpecException, NoSuchAlgorithmException, CertificateEncodingException, IOException {
		AceptaSecurityManager manager = getSecurityManagerFactoryVT2().createSecurityManager();
		try {
			Authority authority = manager.getAuthority("SistemaVT2");
		
			SistemaCertificateRequest request = new SistemaCertificateRequest();
			
			request.setSerialNumber(BigInteger.ONE);
			request.setNotBefore(createDate());
			request.setNotAfter(createDate());
			request.setSubjectRUT("96919050-8");
			//request.setSubjectName("Punto de Firma Electronica Biometrica ID UareU 12345", "Sistema de Firma Electronica Biometrica", "Acepta.com S.A.", null, null, "CL");
			request.setSubjectPublicKey(createTestPublicKey());
		
			request.setCertificatePolicies(SistemaCertificateRequest.ACEPTA_SISTEMAS_POLICY_IDENTIFIER_PUNTO_FED);
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			assertNotNull(certificate);
			
			assertEquals(request.getSerialNumber(), certificate.getSerialNumber());
			assertEquals(request.getNotBefore(), certificate.getNotBefore());
			assertEquals(request.getNotAfter(), certificate.getNotAfter());
			assertEquals(request.getSubjectName(), certificate.getSubjectDN());
			assertEquals(request.getSubjectPublicKey(), certificate.getPublicKey());

			saveCertificate("target/tmp/SistemaVT2-prueba.cer", certificate);
		}
		finally {
			manager.close();
		}
	}
	
	public void testCodigoCertificateGeneration() throws InvalidKeySpecException, NoSuchAlgorithmException, CertificateEncodingException, IOException {
		AceptaSecurityManager manager = getSecurityManagerFactoryVT2().createSecurityManager();
		try {
			Authority authority = manager.getAuthority("CodigoVT2");
		
			CodigoCertificateRequest request = new CodigoCertificateRequest();
			
			request.setSerialNumber(BigInteger.ONE);
			request.setNotBefore(createDate());
			request.setNotAfter(createDate());
			request.setSubjectPublicKey(createTestPublicKey());
			
			request.setSubjectName("1-9", "CL", "Region Metropolitana", "Santiago", "Acepta.com S.A.", "Desarrollo", "Prueba Firma de Codigo");
			
			X509Certificate certificate = authority.generateCertificate(request);
			
			assertNotNull(certificate);
			
			assertEquals(request.getSerialNumber(), certificate.getSerialNumber());
			assertEquals(request.getNotBefore(), certificate.getNotBefore());
			assertEquals(request.getNotAfter(), certificate.getNotAfter());
			assertEquals(request.getSubjectName(), certificate.getSubjectDN());
			assertEquals(request.getSubjectPublicKey(), certificate.getPublicKey());

			saveCertificate("target/tmp/CodigoVT2-prueba.cer", certificate);
		}
		finally {
			manager.close();
		}
	}

	public void testCRLGeneration() throws GeneralSecurityException, IOException {
		generateCRLsFor(getSecurityManagerFactoryG4(), new String[] { "Clase3-G4", "FirmaAvanzada-G4" });
		generateCRLsFor(getSecurityManagerFactoryVT2(), new String[] { "CAFVT2", "SistemaVT2", "CodigoVT2", "SitioWebVT2" });
	}

	private void generateCRLsFor(AceptaSecurityManagerFactory securityManagerFactory, String[] authorityAliases) throws IOException, CRLException {
		AceptaSecurityManager manager = securityManagerFactory.createSecurityManager();
		try {
			for (String alias : authorityAliases) {
				Authority authority = manager.getAuthority(alias);
				
				CRLRequest request = new CRLRequest();
				
				request.setCRLNumber(BigInteger.ONE);
				
				request.setThisUpdate(createDate());
				request.setNextUpdate(createDate());
				
				request.addEntry(BigInteger.ONE, createDate(), CRLEntry.REASON_KEY_COMPROMISE);
				
				X509CRL crl = authority.generateCRL(request);
				
				assertEquals(authority.getName(), crl.getIssuerDN());
				
				assertEquals(request.getThisUpdate(), crl.getThisUpdate());
				assertEquals(request.getNextUpdate(), crl.getNextUpdate());
				
				assertNotNull(crl.getRevokedCertificate(BigInteger.ONE));
				
				saveCRL("target/tmp/" + alias + "-prueba.crl", crl);
			}
		}
		finally {
			manager.close();
		}
	}

	private void saveCertificate(String filename, X509Certificate certificate) throws IOException, CertificateEncodingException {
		FileOutputStream output = new FileOutputStream(new File(filename));
		try {
			output.write(certificate.getEncoded());
		}
		finally {
			output.close();
		}
	}
	
	private void saveCRL(String filename, X509CRL crl) throws IOException, CRLException {
		FileOutputStream output = new FileOutputStream(new File(filename));
		try {
			output.write(crl.getEncoded());
		}
		finally {
			output.close();
		}
	}

	private PublicKey createTestPublicKey() throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		byte[] modulusBytes = new byte[1024/8];
		
		Random random = new Random();
		
		random.nextBytes(modulusBytes);
		
		modulusBytes[0] &= 0x7F; // 1024-bit signed positive integer
		
		BigInteger modulus = new BigInteger(modulusBytes);
		
		BigInteger exponent = new BigInteger("10001", 16);
		
		RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(modulus, exponent);
		
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		
		return keyFactory.generatePublic(publicKeySpec);
	}
	
	private Date createDate() {
		Calendar calendar = Calendar.getInstance();
		
		calendar.clear(Calendar.MILLISECOND);
		
		return calendar.getTime();
	}
	
	private AceptaSecurityManagerFactory getSecurityManagerFactoryG4() {
		return securityManagerFactoryG4;
	}

	private void setSecurityManagerFactoryG4(AceptaSecurityManagerFactory securityManagerFactoryG4) {
		this.securityManagerFactoryG4 = securityManagerFactoryG4;
	}
	
	private AceptaSecurityManagerFactory getSecurityManagerFactoryVT2() {
		return securityManagerFactoryVT2;
	}

	private void setSecurityManagerFactoryVT2(AceptaSecurityManagerFactory securityManagerFactoryVT2) {
		this.securityManagerFactoryVT2 = securityManagerFactoryVT2;
	}
}
