package com.acepta.ca.test.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Date;

import com.acepta.ca.security.CAFCertificateRequest;
import junit.framework.TestCase;

import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;
import com.acepta.ca.security.PersonaCertificateRequest;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.util.PEM;
import com.acepta.ca.security.util.PKCS12;

public class TestPKCS12 extends TestCase {
	private static final String RSA_PUBLIC_KEY =
		"-----BEGIN PUBLIC KEY-----\n" +
		"MFowDQYJKoZIhvcNAQEBBQADSQAwRgJBAMuTb7dY22+Lo1+2oSEzO/jE86PRnoT3\n" +
		"AZHUlNCUWVtDVZxK1UmSUenNgGT0VLYSLXmxcneOPXagVtrxG4awrNECAQM=\n" +
		"-----END PUBLIC KEY-----\n";
	
	private static final String RSA_PRIVATE_KEY =
		"-----BEGIN RSA PRIVATE KEY-----\n" +
		"MIIBOgIBAAJBAMuTb7dY22+Lo1+2oSEzO/jE86PRnoT3AZHUlNCUWVtDVZxK1UmS\n" +
		"UenNgGT0VLYSLXmxcneOPXagVtrxG4awrNECAQMCQQCHt5/PkJJKXReVJGtrd31Q\n" +
		"g00X4RRYpKu2jbiLDZDngQiwWlsDwNsg+mbHAunnEyjpHZfAdg+gvtddipRKvIZD\n" +
		"AiEA5Mls7pvp2scnAUm+r7a+vYi2830ygoQo7RnkjdK79lcCIQDjylZeKAcucS7k\n" +
		"8LFGJLayk04bWaqjgVkmtLyvQ9ntFwIhAJiGSJ8Sm+cvb1YxKcp51H5bJKJTdwGt\n" +
		"cJ4RQwk3J/mPAiEAl9w5lBqvdEt0mKB2LsMkdwzevOZxwlY7byModNfmng8CIG2m\n" +
		"5TPltKdxN7OfKsCB3n8v9BgQDu24/NTg7LzRGmTg\n" +
		"-----END RSA PRIVATE KEY-----\n";

	public void testPKCS12Encoder() throws GeneralSecurityException, IOException {
		PublicKey publicKey = PEM.decodeRSAPublicKey(RSA_PUBLIC_KEY);
		
		PrivateKey privateKey = PEM.decodeRSAPrivateKey(RSA_PRIVATE_KEY);
		
		AceptaSecurityManagerFactory securityManagerFactory = new AceptaSecurityManagerFactory("aceptaca-vt2-development");
		try {
			AceptaSecurityManager securityManager = securityManagerFactory.createSecurityManager();
			try {
				Authority authority = securityManager.getAuthority("CAFVT2");
				CAFCertificateRequest request = new CAFCertificateRequest();


				request.setSerialNumber(BigInteger.ONE);
				request.setNotBefore(authority.getCertificate().getNotBefore());
				request.setNotAfter(authority.getCertificate().getNotAfter());

				request.setSubjectRUT("1-9");
				request.setSubjectRazonSocial("Fulanito");
				request.setSubjectTipoDTE(33);
				request.setSubjectDesdeFolio(10L);
				request.setSubjectHastaFolio(1000L);
				request.setSubjectLlaveID(1);
				request.setSubjectCAF(new byte[]{1,2,3,4});
				request.setSubjectPublicKey(publicKey);

 				X509Certificate certificate = authority.generateCertificate(request);
				
				byte[] pkcs12 = PKCS12.encodeCertificate(privateKey, null, certificate, authority.getCertificate());
				
				File file = new File("target/tmp/TestPKCS12.p12");
				
				file.getParentFile().mkdirs();
				
				FileOutputStream output = new FileOutputStream(file);
				try {
					output.write(pkcs12);
				}
				finally {
					output.close();
				}
			}
			finally {
				securityManager.close();
			}
		}
		finally {
			securityManagerFactory.close();
		}
	}
}
