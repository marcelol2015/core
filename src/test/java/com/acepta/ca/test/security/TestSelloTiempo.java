package com.acepta.ca.test.security;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import org.bouncycastle.asn1.cmp.PKIStatus;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampResponse;

import com.acepta.ca.security.AceptaSecurityManager;
import com.acepta.ca.security.AceptaSecurityManagerFactory;
import com.acepta.ca.security.authority.Authority;
import com.acepta.ca.security.crypto.tsp.X509TimeStampRequest;
import com.acepta.ca.security.crypto.tsp.X509TimeStampResponse;

public class TestSelloTiempo extends TestCase {
	AceptaSecurityManagerFactory securityManagerFactory;
	
	public void setUp() {
		setSecurityManagerFactory(new AceptaSecurityManagerFactory("aceptaca-vt2-development"));
	}
	
	public void tearDown() {
		getSecurityManagerFactory().close();
	}


	public void testAceptaSelloTiempo() throws Exception {
		AceptaSecurityManager manager = getSecurityManagerFactory().createSecurityManager();
		try {
			Authority authority = manager.getAuthority("SelloTiempoVT2");

			for (int iter = 0; iter < 2; iter++) {
				TimeStampRequestGenerator generator = new TimeStampRequestGenerator();
				
				String messageImprintDigestAlgorithm = OIWObjectIdentifiers.idSHA1.getId();
	
				byte[] messageImprintDigest = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
						                         		   0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
						                         		   0x10, 0x11, 0x12, 0x13 };
	
				if ((iter & 1) == 0)
					generator.setCertReq(true);
				
				TimeStampRequest req = generator.generate(messageImprintDigestAlgorithm, messageImprintDigest);
	
				Date beforeTimeStamping = new Date();
	
				X509TimeStampResponse response = authority.generateTimeStamp(new X509TimeStampRequest(BigInteger.ONE, req.getEncoded()));
				
				Date afterTimeStamping = new Date();
	
				TimeStampResponse resp = new TimeStampResponse(response.getEncoded());
				
				resp.validate(req);
				
				assertEquals(PKIStatus.GRANTED, resp.getStatus());
				
				
				// check message imprint
				assertEquals(messageImprintDigestAlgorithm, resp.getTimeStampToken().getTimeStampInfo().getMessageImprintAlgOID());
				
				for (int index = 0; index < messageImprintDigest.length; index++)
					assertEquals(messageImprintDigest[index], resp.getTimeStampToken().getTimeStampInfo().getMessageImprintDigest()[index]);
				
				assertEquals(BigInteger.ONE, resp.getTimeStampToken().getTimeStampInfo().getSerialNumber());
				
				
				// check gen time
				Date genTime = resp.getTimeStampToken().getTimeStampInfo().getGenTime();
			
				Calendar calendar = Calendar.getInstance();
				
				calendar.setTime(beforeTimeStamping);
				calendar.clear(Calendar.MILLISECOND);
				beforeTimeStamping = calendar.getTime();
				
				calendar.setTime(afterTimeStamping);
				calendar.clear(Calendar.MILLISECOND);
				afterTimeStamping = calendar.getTime();
				
				assertTrue(genTime.getTime() >= beforeTimeStamping.getTime());
				assertTrue(genTime.getTime() <= afterTimeStamping.getTime());
				
				// check response signature
				resp.getTimeStampToken().validate(authority.getCertificate(), authority.getProvider().getName());
			}
		}
		finally {
			manager.close();
		}
	}
	
	private AceptaSecurityManagerFactory getSecurityManagerFactory() {
		return securityManagerFactory;
	}

	private void setSecurityManagerFactory(AceptaSecurityManagerFactory securityManagerFactory) {
		this.securityManagerFactory = securityManagerFactory;
	}
}
