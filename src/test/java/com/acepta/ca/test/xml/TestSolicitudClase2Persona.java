package com.acepta.ca.test.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.xml.XMLSolicitudPersona;

public class TestSolicitudClase2Persona extends TestCase {
	
	public void testSolicitudClase2() {
		try {
			XMLSolicitudPersona solicitud = loadSolicitudResource("/META-INF/samples/SolicitudClase2Persona.xml");
		
			assertEquals("96919050-8", solicitud.getEmisor());
			assertEquals(XMLSolicitudPersona.DESPACHADOR_ACEPTA, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			assertEquals(createDateTime("2012-03-12T14:55:05"), solicitud.getTimeStamp());

			assertEquals(createDate("2012-03-12"), solicitud.getFechaSolicitud());
			
			assertEquals("10680294-7", solicitud.getPersonaRut());
			assertEquals("RAúL ChrISTIÁN", solicitud.getPersonaNombres());
			assertEquals("RAMÍREZ", solicitud.getPersonaApellidoPaterno());
			assertEquals("Valdés", solicitud.getPersonaApellidoMaterno());
			
			assertEquals("persona natural áéíóúñ ÁÉÍÓÚÑ", solicitud.getPersonaProfesion());
			assertEquals("ramirezvaldes2001@yahoo.com", solicitud.getPersonaCorreoElectronico());
			
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}
	
	private XMLSolicitudPersona loadSolicitudResource(String name) throws SAXException, IOException {
		InputStream input = getClass().getResourceAsStream(name);
		try {
			return new XMLSolicitudPersona(input);
		}
		finally {
			input.close();
		}
	}
	
	private Date createDateTime(String text) throws ParseException
	{
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		return dateTimeFormat.parse(text);
	}
	
	private Date createDate(String text) throws ParseException
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(text);
	}
}
