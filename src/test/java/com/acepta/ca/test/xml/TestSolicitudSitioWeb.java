package com.acepta.ca.test.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.security.provider.AceptaV1TrustedKeyStore;
import com.acepta.ca.xml.XMLSolicitudSitioWeb;

public class TestSolicitudSitioWeb extends TestCase {
	public void testSolicitudSitioWeb4() throws SAXException, IOException, CertificateParsingException, MarshalException, XMLSignatureException, GeneralSecurityException {
		XMLSolicitudSitioWeb solicitud = loadSolicitudResource("/META-INF/samples/Solicitud4.xml");

		assertNotNull(solicitud);
		
		assertTrue(solicitud.validate(new AceptaV1TrustedKeyStore().getTrustedCertificates(), Arrays.asList("14154242-7")));
	}
	
	public void testSolicitudSitioWeb() throws MarshalException, XMLSignatureException {
		try {
			XMLSolicitudSitioWeb solicitud = loadSolicitudResource("/META-INF/samples/SolicitudSitioWeb.xml");

			assertEquals("96919050-8", solicitud.getEmisor());
			assertEquals(XMLSolicitudSitioWeb.DESPACHADOR_ACEPTA, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			assertEquals(createDateTime("2007-07-18T15:41:27"), solicitud.getTimeStamp());
			
			assertEquals("011-39281-65379014-1", solicitud.getNumeroSolicitud());
			assertEquals(createDate("2007-07-18"), solicitud.getFechaSolicitud());
			assertEquals("dy1bc0QeYarSppF+zyym8e+tJyE=", solicitud.getClave());

			assertEquals("www.prueba.cl", solicitud.getDominio());
			
			assertEquals("1-9", solicitud.getRut());
			assertEquals("PRUEBA S.A.", solicitud.getOrganizacion());
			assertEquals("DESARROLLO", solicitud.getUnidadOrganizacional());
			
			assertEquals("LA FLORIDA", solicitud.getEstado());
			assertEquals("SANTIAGO", solicitud.getCiudad());
			assertEquals("CL", solicitud.getPais());
			
			assertEquals("1-1", solicitud.getRutContactoAdministrativo());
			assertEquals("CONTACTO ADMINISTRATIVO PRUEBA", solicitud.getNombreContactoAdministrativo());
			assertEquals("5551111", solicitud.getTelefonoContactoAdministrativo());
			assertEquals("administrativo@prueba.cl", solicitud.getCorreoContactoAdministrativo());
			
			assertEquals("2-2", solicitud.getRutContactoTecnico());
			assertEquals("CONTACTO TECNICO PRUEBA", solicitud.getNombreContactoTecnico());
			assertEquals("5552222", solicitud.getTelefonoContactoTecnico());
			assertEquals("tecnico@prueba.cl", solicitud.getCorreoContactoTecnico());
			
			assertEquals(12, solicitud.getMesesVigencia());
			
			assertNotNull(solicitud.getPKCS10());
			
			// FIXME: firmar la solicitud de sitio web de ejemplo con un certificado confiable
			// assertTrue(solicitud.validate(loadTrustedCertificates()));
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}

	public void testFirmarSolicitudSitioWeb() throws SAXException, IOException, GeneralSecurityException, MarshalException, XMLSignatureException {
		XMLSolicitudSitioWeb solicitud = loadSolicitudResource("/META-INF/samples/SolicitudSitioWeb.xml");

		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		
		keyStore.load(new FileInputStream(new File("target/tmp/FulanitoTahl.p12")), "test".toCharArray());
		
		Key privateKey = null;
		X509Certificate certificate = null;
		
		Enumeration<String> aliases = keyStore.aliases();
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			privateKey = keyStore.getKey(alias, "test".toCharArray());
			certificate = (X509Certificate) keyStore.getCertificate(alias);
		}

		solicitud.sign(privateKey, certificate);
		
		new File("target/tmp").mkdirs();
		
		FileOutputStream output = new FileOutputStream(new File("target/tmp/TestSolicitudSitioWeb.xml"));
		try {
			output.write(solicitud.toXML("UTF-8"));
		}
		finally {
			output.close();
		}
	}
	
	private XMLSolicitudSitioWeb loadSolicitudResource(String name) throws SAXException, IOException {
		InputStream input = getClass().getResourceAsStream(name);
		try {
			return new XMLSolicitudSitioWeb(input);
		}
		finally {
			input.close();
		}
	}
	
	private Date createDateTime(String text) throws ParseException
	{
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		return dateTimeFormat.parse(text);
	}
	
	private Date createDate(String text) throws ParseException
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(text);
	}
}
