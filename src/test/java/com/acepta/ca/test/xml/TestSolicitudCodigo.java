package com.acepta.ca.test.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.xml.XMLSolicitudCodigo;

public class TestSolicitudCodigo extends TestCase {
	public void testSolicitudCodigo() throws MarshalException, XMLSignatureException {
		try {
			XMLSolicitudCodigo solicitud = loadSolicitudResource("/META-INF/samples/SolicitudCodigo.xml");

			assertEquals("96919050-8", solicitud.getEmisor());
			assertEquals(XMLSolicitudCodigo.DESPACHADOR_ACEPTA, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			assertEquals(createDateTime("2007-07-18T15:41:27"), solicitud.getTimeStamp());
			
			assertEquals("011-39281-65379014-1", solicitud.getNumeroSolicitud());
			assertEquals(createDate("2007-07-18"), solicitud.getFechaSolicitud());
			assertEquals("dy1bc0QeYarSppF+zyym8e+tJyE=", solicitud.getClave());

			assertEquals("EQUIPO DESARROLLO ABC", solicitud.getNombre());
			
			assertEquals("1-9", solicitud.getRut());
			assertEquals("PRUEBA S.A.", solicitud.getOrganizacion());
			assertEquals("DESARROLLO", solicitud.getUnidadOrganizacional());
			
			assertEquals("LA FLORIDA", solicitud.getEstado());
			assertEquals("SANTIAGO", solicitud.getCiudad());
			assertEquals("CL", solicitud.getPais());
			
			assertEquals("1-1", solicitud.getRutRepresentanteLegal());
			assertEquals("CONTACTO REPRESENTANTE LEGAL", solicitud.getNombreRepresentanteLegal());
			assertEquals("5551111", solicitud.getTelefonoRepresentanteLegal());
			assertEquals("representante@prueba.cl", solicitud.getCorreoRepresentanteLegal());
			
			assertEquals(12, solicitud.getMesesVigencia());
			
			assertNotNull(solicitud.getPKCS10());
			
			assertNotNull(solicitud.getContrato());
			
			// FIXME: firmar la solicitud de sitio web de ejemplo con un certificado confiable
			// assertTrue(solicitud.validate(loadTrustedCertificates()));
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}

	public void testFirmarSolicitudCodigo() throws SAXException, IOException, GeneralSecurityException, MarshalException, XMLSignatureException {
		XMLSolicitudCodigo solicitud = loadSolicitudResource("/META-INF/samples/SolicitudCodigo.xml");

		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		
		keyStore.load(new FileInputStream(new File("target/tmp/FulanitoTahl.p12")), "test".toCharArray());
		
		Key privateKey = null;
		X509Certificate certificate = null;
		
		Enumeration<String> aliases = keyStore.aliases();
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			privateKey = keyStore.getKey(alias, "test".toCharArray());
			certificate = (X509Certificate) keyStore.getCertificate(alias);
		}

		solicitud.sign(privateKey, certificate);
		
		new File("target/tmp").mkdirs();
		
		FileOutputStream output = new FileOutputStream(new File("target/tmp/TestSolicitudCodigo.xml"));
		try {
			output.write(solicitud.toXML("UTF-8"));
		}
		finally {
			output.close();
		}
	}
	
	private XMLSolicitudCodigo loadSolicitudResource(String name) throws SAXException, IOException {
		InputStream input = getClass().getResourceAsStream(name);
		try {
			return new XMLSolicitudCodigo(input);
		}
		finally {
			input.close();
		}
	}
	
	private Date createDateTime(String text) throws ParseException
	{
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		return dateTimeFormat.parse(text);
	}
	
	private Date createDate(String text) throws ParseException
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(text);
	}
}
