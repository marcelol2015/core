package com.acepta.ca.test.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.security.util.PIN;
import com.acepta.ca.xml.XMLSolicitudPersona;

public class TestSolicitudPersona extends TestCase {
	
	public void testSolicitudAceptaAntigua() {
		try {
			XMLSolicitudPersona solicitud = loadSolicitudResource("/META-INF/samples/Solicitud1.xml");
		
			assertEquals("96919050-8", solicitud.getEmisor());
			assertEquals(XMLSolicitudPersona.DESPACHADOR_ACEPTA, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			assertEquals(createDateTime("2004-09-27T14:55:05"), solicitud.getTimeStamp());

			assertEquals("011-38257-62159071-0", solicitud.getNumeroSolicitud());
			assertEquals(createDate("2004-09-27"), solicitud.getFechaSolicitud());
			assertEquals("dy1bc0QeYarSppF+zyym8e+tJyE=", solicitud.getClaveActivacion());
			
			assertEquals("10680294-7", solicitud.getPersonaRut());
			assertEquals("RAUL CHRISTIAN", solicitud.getPersonaNombres());
			assertEquals("RAMIREZ", solicitud.getPersonaApellidoPaterno());
			assertEquals("VALDES", solicitud.getPersonaApellidoMaterno());
			
			assertEquals("persona natural", solicitud.getPersonaProfesion());
			assertEquals("ramirezvaldes2001@yahoo.com", solicitud.getPersonaCorreoElectronico());
			
			assertNotNull(solicitud.getPersonaFotografia());
			assertNotNull(solicitud.getPersonaRUTAnverso());
			assertNotNull(solicitud.getPersonaRUTReverso());
			
			assertEquals("2360482", solicitud.getPersonaTelefono());
			assertEquals("", solicitud.getPersonaTelefonoMovil());
			assertEquals("av americo vespucio sur 4151-a ", solicitud.getPersonaDireccion());
			assertEquals("macul", solicitud.getPersonaComuna());
			assertEquals("santiago", solicitud.getPersonaCiudad());
			assertEquals("Chile", solicitud.getPersonaPais());
			assertEquals("", solicitud.getPersonaCodigoPostal());

			assertNotNull(solicitud.getContrato());
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}
	
	public void testSolicitudAceptaNueva() {
		try {
			XMLSolicitudPersona solicitud = loadSolicitudResource("/META-INF/samples/Solicitud2.xml");
			
			assertEquals("96919050-8", solicitud.getEmisor());
			assertEquals(XMLSolicitudPersona.DESPACHADOR_ACEPTA, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			assertEquals(createDateTime("2007-07-18T15:41:27"), solicitud.getTimeStamp());

			assertEquals("011-39281-65379014-1", solicitud.getNumeroSolicitud());
			assertEquals(createDate("2007-07-18"), solicitud.getFechaSolicitud());
			assertEquals("fCIvspJ9goryL1khNOiTJIBjfA0=", solicitud.getClaveActivacion());
			
			assertEquals("1-9", solicitud.getPersonaRut());
			assertEquals("TEST", solicitud.getPersonaNombres());
			assertEquals("TEST", solicitud.getPersonaApellidoPaterno());
			assertEquals("TEST", solicitud.getPersonaApellidoMaterno());
			
			assertEquals("", solicitud.getPersonaProfesion());
			assertEquals("test@test.cl", solicitud.getPersonaCorreoElectronico());
			
			assertNotNull(solicitud.getPersonaFotografia());
			assertNotNull(solicitud.getPersonaRUTAnverso());
			assertNotNull(solicitud.getPersonaRUTReverso());
			
			assertEquals("", solicitud.getPersonaTelefono());
			assertEquals("", solicitud.getPersonaTelefonoMovil());
			assertEquals(" ", solicitud.getPersonaDireccion());
			assertEquals("", solicitud.getPersonaComuna());
			assertEquals("", solicitud.getPersonaCiudad());
			assertEquals("Chile", solicitud.getPersonaPais());
			assertEquals("", solicitud.getPersonaCodigoPostal());
			
			assertEquals("-", solicitud.getEmpresaRut());
			assertEquals("", solicitud.getEmpresaRazonSocial());
			
			assertEquals(12, solicitud.getMesesVigencia());
			assertEquals(true, solicitud.isFirmaAvanzada());
			
			assertNotNull(solicitud.getContrato());
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}

	public void testSolicitudRegistroCivil() {
		try {
			XMLSolicitudPersona solicitud = loadSolicitudResource("/META-INF/samples/Solicitud3.xml");
			
			assertEquals("61002000-3", solicitud.getEmisor());
			assertEquals(XMLSolicitudPersona.DESPACHADOR_SRCEI, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			
			assertEquals(XMLSolicitudPersona.SRCEI_ID_ENVIO_PI_APROBACION, solicitud.getIDEnvioPI());
			assertEquals("30000100", solicitud.getIDEnvio());
			
			assertEquals(createDateTime("2003-12-30T18:53:41"), solicitud.getTimeStamp());

			assertEquals("89935", solicitud.getNumeroAtencion());
			assertEquals("32172699", solicitud.getNumeroSolicitud());
			assertEquals(createDate("2003-12-30"), solicitud.getFechaSolicitud());
			assertEquals(createDate("1900-01-01"), solicitud.getFechaVencimiento());
			assertEquals("(NO ESPECIFICADO)", solicitud.getInscripcion());
			assertEquals(PIN.encrypt(" "), solicitud.getClaveActivacion());
			
			assertEquals("11931145-4", solicitud.getPersonaRut());
			assertEquals("ROJAS", solicitud.getPersonaNombres());
			assertEquals("GONZ\u00C1LEZ", solicitud.getPersonaApellidoPaterno());
			assertEquals("NELSON JAVIER", solicitud.getPersonaApellidoMaterno());
			
			assertEquals("Masculino", solicitud.getPersonaSexo());
			assertEquals(createDate("1971-09-03"), solicitud.getPersonaFechaNacimiento());
			assertEquals("CHL", solicitud.getPersonaPaisNacimiento());
			
			assertEquals("NELSONROJAS39@HOTMAIL.COM", solicitud.getPersonaCorreoElectronico());
			
			assertEquals("Pulgar Derecho", solicitud.getPersonaNombreDedo());
			assertNotNull(solicitud.getPersonaImpresionDactilar());
			assertNotNull(solicitud.getPersonaFirmaManuscrita());
			assertNotNull(solicitud.getPersonaFotografia());
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}

	private XMLSolicitudPersona loadSolicitudResource(String name) throws SAXException, IOException {
		InputStream input = getClass().getResourceAsStream(name);
		try {
			return new XMLSolicitudPersona(input);
		}
		finally {
			input.close();
		}
	}
	
	private Date createDateTime(String text) throws ParseException
	{
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		return dateTimeFormat.parse(text);
	}
	
	private Date createDate(String text) throws ParseException
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(text);
	}
}
