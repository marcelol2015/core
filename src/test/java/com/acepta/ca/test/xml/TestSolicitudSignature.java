package com.acepta.ca.test.xml;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.xpath.XPathExpressionException;

import junit.framework.TestCase;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.acepta.ca.xml.XMLSolicitudPersona;
import com.acepta.ca.xml.crypto.XMLDocumentSignature;

public class TestSolicitudSignature extends TestCase {
	public void testValidateSignature() throws SAXException, IOException, XPathExpressionException, MarshalException, XMLSignatureException  {
		XMLDocumentSignature signature = new XMLDocumentSignature();

		Collection<X509Certificate> trustedCertificates = loadTrustedCertificates();
		
		for (int index = 1; index <= 3; index++) {
			XMLSolicitudPersona solicitud = loadSolicitudResource("/META-INF/samples/Solicitud" + index + ".xml");
			
			assertNotNull(solicitud);
			
			NodeList signatureNodes = solicitud.getSignatures();
			
			assertNotNull(signatureNodes);
			
			assertTrue(signatureNodes.getLength() != 0);
			
			for (int k = 0; k < signatureNodes.getLength(); k++)
				assertTrue(signature.validate(signatureNodes.item(k), trustedCertificates) != null);
		}
	}

	private XMLSolicitudPersona loadSolicitudResource(String name) throws SAXException, IOException {
		InputStream input = getClass().getResourceAsStream(name);
		try {
			return new XMLSolicitudPersona(input);
		}
		finally {
			input.close();
		}
	}
	
	private Collection<X509Certificate> loadTrustedCertificates() {
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/keystore/trustedcerts.jks");
			try {
				KeyStore keyStore = KeyStore.getInstance("JKS");
				
				keyStore.load(input, "acepta".toCharArray());
				
				ArrayList<X509Certificate> trustedCertificates = new ArrayList<X509Certificate>();
				
				Enumeration<String> aliases = keyStore.aliases();
				
				while (aliases.hasMoreElements()) {
					String alias = aliases.nextElement();
					
					if (keyStore.isCertificateEntry(alias))
						trustedCertificates.add((X509Certificate) keyStore.getCertificate(alias));
				}
				
				return trustedCertificates;
			}
			finally {
				input.close();
			}
		}
		catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}
}
