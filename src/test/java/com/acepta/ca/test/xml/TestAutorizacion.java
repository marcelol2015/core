package com.acepta.ca.test.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.xml.XMLAutorizacion;

public class TestAutorizacion extends TestCase {
	public void test() {
		try {
			InputStream input = getClass().getResourceAsStream("/META-INF/samples/Autorizacion.xml");
			try {
				XMLAutorizacion autorizacion = new XMLAutorizacion(input);

				assertEquals("96919050-8", autorizacion.getRutEmpresa());
				assertEquals("ACEPTA COM S A", autorizacion.getRazonSocial());
				assertEquals(61, autorizacion.getTipoDTE());
				assertEquals(1L, autorizacion.getDesdeFolio());
				assertEquals(100000L, autorizacion.getHastaFolio());
				assertEquals(createDate("2003-07-31"), autorizacion.getFechaAutorizacion());
				assertEquals(100, autorizacion.getLlaveID());
				assertEquals("1HlzhuVQjwqSaq9D7gbmjmIxMS1vF2/miDH6SBOwBYfsxO1a8sILijlqwZbI7hiydKpVnB0XYYrSKQRnZWS8rQ==", autorizacion.getRSAPublicKeyModulus());
				assertEquals("Aw==", autorizacion.getRSAPublicKeyExponent());
				
				assertNotNull(autorizacion.getRSAPublicKey());
				assertNotNull(autorizacion.getRSAPrivateKey());
				
				assertNotNull(autorizacion.getCAF());
			}
			finally {
				input.close();
			}
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}
	
	private Date createDate(String text) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(text);
	}
}
