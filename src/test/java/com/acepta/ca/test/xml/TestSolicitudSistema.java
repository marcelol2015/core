package com.acepta.ca.test.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

import junit.framework.TestCase;

import org.xml.sax.SAXException;

import com.acepta.ca.xml.XMLSolicitudSistema;

public class TestSolicitudSistema extends TestCase {
	public void testSolicitudSistema() throws MarshalException, XMLSignatureException {
		try {
			XMLSolicitudSistema solicitud = loadSolicitudResource("/META-INF/samples/SolicitudSistema.xml");

			assertEquals("96919050-8", solicitud.getEmisor());
			assertEquals(XMLSolicitudSistema.DESPACHADOR_ACEPTA, solicitud.getDespachador());
			assertEquals("96919050-8", solicitud.getDestinatario());
			assertEquals(createDateTime("2007-07-18T15:41:27"), solicitud.getTimeStamp());
			
			assertEquals("011-39281-65379014-1", solicitud.getNumeroSolicitud());
			assertEquals(createDate("2007-07-18"), solicitud.getFechaSolicitud());

			assertEquals("Punto de Firma Electronica Biometrica ID UareU\\12345", solicitud.getNombre());
			
			assertEquals("96919050-8", solicitud.getRut());
			assertEquals("Acepta.com S.A.", solicitud.getOrganizacion());
			assertEquals("Sistema de Firma Electronica Biometrica", solicitud.getUnidadOrganizacional());
			
			assertEquals("Metropolitana", solicitud.getEstado());
			assertEquals("Santiago", solicitud.getCiudad());
			assertEquals("CL", solicitud.getPais());
			
			assertEquals("1-9", solicitud.getRutRepresentanteLegal());
			assertEquals("REPRESENTANTE LEGAL PRUEBA", solicitud.getNombreRepresentanteLegal());
			assertEquals("5554444", solicitud.getTelefonoRepresentanteLegal());
			assertEquals("representante@prueba.cl", solicitud.getCorreoRepresentanteLegal());
			
			assertEquals("1.3.6.1.4.1.6891.1001.5.3, 1.3.6.1.4.1.6891.1001.5.4", solicitud.getPoliticasCertificacion());
			
			assertEquals(12, solicitud.getMesesVigencia());
			
			assertNotNull(solicitud.getPKCS10());
			
			// FIXME: firmar la solicitud de sistema de ejemplo con un certificado confiable
			// assertTrue(solicitud.validate(loadTrustedCertificates()));
		}
		catch (ParseException exception) {
			throw new AssertionError(exception);
		}
		catch (SAXException exception) {
			throw new AssertionError(exception);
		}
		catch (IOException exception) {
			throw new AssertionError(exception);
		}
	}
	
	public void testFirmarSolicitudSistema() throws SAXException, IOException, GeneralSecurityException, MarshalException, XMLSignatureException {
		XMLSolicitudSistema solicitud = loadSolicitudResource("/META-INF/samples/SolicitudSistema.xml");

		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		
		keyStore.load(new FileInputStream(new File("target/tmp/FulanitoTahl.p12")), "test".toCharArray());
		
		Key privateKey = null;
		X509Certificate certificate = null;
		
		Enumeration<String> aliases = keyStore.aliases();
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			privateKey = keyStore.getKey(alias, "test".toCharArray());
			certificate = (X509Certificate) keyStore.getCertificate(alias);
		}

		solicitud.sign(privateKey, certificate);
		
		new File("target/tmp").mkdirs();
		
		FileOutputStream output = new FileOutputStream(new File("target/tmp/TestSolicitudSistema.xml"));
		try {
			output.write(solicitud.toXML("UTF-8"));
		}
		finally {
			output.close();
		}
	}

	private XMLSolicitudSistema loadSolicitudResource(String name) throws SAXException, IOException {
		InputStream input = getClass().getResourceAsStream(name);
		try {
			return new XMLSolicitudSistema(input);
		}
		finally {
			input.close();
		}
	}
	
	private Date createDateTime(String text) throws ParseException
	{
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		return dateTimeFormat.parse(text);
	}
	
	private Date createDate(String text) throws ParseException
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.parse(text);
	}
}
