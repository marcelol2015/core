package com.acepta.ca.test.persistence;

import java.util.Date;

import org.apache.xml.security.utils.Base64;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.CRL;

public class TestCRL extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}
	
	public void testGenerarCRL() throws Exception {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();
			
			Autoridad emisor = manager.findAutoridadWithNombre(TEST_CA);

			Date fecha = new Date();

			CRL crl = new CRL();
			
			crl.setEmisor(emisor);
			crl.setNumeroSerie(emisor.generarNumeroSerieCRL());
			crl.setValidoDesde(fecha);
			crl.setValidoHasta(fecha);
			crl.setData(Base64.decode(
				"MIICPDCCASQCAQEwDQYJKoZIhvcNAQEFBQAwgZwxCzAJBgNVBAYTAkNMMRgwFgYD" +
				"VQQKEw9BY2VwdGEuY29tIFMuQS4xPjA8BgNVBAMTNUFjZXB0YS5jb20gQXV0b3Jp" +
				"ZGFkIENlcnRpZmljYWRvcmEgZGUgU2l0aW8gV2ViIC0gVlQxMR4wHAYJKoZIhvcN" +
				"AQkBFg9pbmZvQGFjZXB0YS5jb20xEzARBgNVBAUTCjk2OTE5MDUwLTgXDTExMDIy" +
				"MTE1NDMzM1oXDTExMDIyMTE1NDMzM1owIjAgAgEBFw0xMTAyMjExNTQzMzNaMAww" +
				"CgYDVR0VBAMKAQGgLzAtMB8GA1UdIwQYMBaAFCdP9sZSnBqJFI47J9e8SLWEvXzF" +
				"MAoGA1UdFAQDAgEBMA0GCSqGSIb3DQEBBQUAA4IBAQBdA+7TYnl5erDhvHRNrJaI" +
				"ktbdk2c27Diz0OE1qO9zP+KCIEchZWkTiOV86rOS36fu6lb5JaIiR3RjDw3UJWHB" +
				"xNMMAIFTIJ8xrNaUrUOoVQYyWwaVKtr91amIP6o6pdAuJz0ykErHCFRvkG8/eZ6T" +
				"HfILWYeECwPr83kLBpvjwZmHrSoc4UcIaeM2Ue126yW62iya5ta7yz43erE1WMVw" +
				"9zy+6F2OnpX/qf0qb50YeYFbQdoXQaabz7eCLKNJIztuW46cwsCFvPaYMPqEYlOV" +
				"99nTkmFASGfnAzUOiUC9V0NRGmWOD/Vl6ExIufGaYofV5CGnifEyopb6od1so3lD"));

			manager.persist(crl);
			
			assertEquals(crl.getId(), manager.findLatestCRLWithEmisor(emisor).getId());
			
			manager.commitTransaction();
		}
		finally {
			manager.close();
		}
	}
}
