package com.acepta.ca.test.persistence;

import java.util.Date;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.SelloTiempo;

public class TestSelloTiempo extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}

	public void testSelloTiempo() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();
			
			Autoridad emisor = manager.findAutoridadWithNombre(TEST_CA);
	
			for (int times = 0; times < 100; times++) {
				SelloTiempo selloTiempo = new SelloTiempo();
				
				selloTiempo.setEmisor(emisor);
				selloTiempo.setNumeroSerie(emisor.generarNumeroSerieSelloTiempo());
				selloTiempo.setTiempo(new Date());
				selloTiempo.setData(new byte[] { 1, 2, 3, 4 });
				
				manager.persist(selloTiempo);
			}
			
			manager.commitTransaction();
		}
		finally {
			manager.close();
		}
	}
}
