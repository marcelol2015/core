package com.acepta.ca.test.persistence;

import javax.persistence.NoResultException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.AceptaEntityManagerFactory;

import junit.framework.TestCase;

public abstract class TestPersistence extends TestCase {
	protected static final String TEST_CA = "Prueba";
	protected static final String TEST_RUT = "99999999-9";

	private AceptaEntityManagerFactory entityManagerFactory;

	public void setUp() {
		setEntityManagerFactory(new AceptaEntityManagerFactory("aceptaca-vt2-development"));
	}

	public void tearDown() {
		getEntityManagerFactory().close();
	}

	protected void assertExisteAutoridadDePrueba() {
		AceptaEntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			try {
				entityManager.findAutoridadWithNombre(TEST_CA);
			}
			catch (NoResultException exception) {
				throw new RuntimeException("Autoridad de Prueba no existe");
			}
		}
		finally {
			entityManager.close();
		}
		
	}
	protected AceptaEntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	protected void setEntityManagerFactory(AceptaEntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
}
