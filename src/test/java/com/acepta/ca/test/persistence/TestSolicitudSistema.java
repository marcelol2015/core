package com.acepta.ca.test.persistence;

import java.util.Date;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Sistema;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudSistema;

public class TestSolicitudSistema extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}

	public void testIngresarSolicitudSistema() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			SolicitudSistema solicitud = new SolicitudSistema();
			
			solicitud.setNumero(SolicitudCodigoGenerator.generateCodigo());
			solicitud.setMesesVigencia(12);
			
			Sistema sistema = new Sistema();
			
			sistema.setRut("1-9");
			sistema.setNombre("Punto de Firma Biometrica ID UareU 12345 PRUEBA");
			sistema.setUnidadOrganizacional("Sistema de Firma Electronica Biometrica PRUEBA");
			sistema.setOrganizacion("Acepta.com S.A. PRUEBA");
			sistema.setPais("CL");
			
			sistema.setPoliticas("1.2.3.4");
			
			solicitud.setSistema(sistema);
			
			Autoridad autoridad = manager.findAutoridadWithNombre(TEST_CA);

			solicitud.setAutoridad(autoridad);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(solicitud.getAutoridad().generarNumeroSerieCertificado());
			certificado.setValidoDesde(new Date());
			certificado.setValidoHasta(new Date());
			certificado.setSujeto("PUNTO DE FIRMA ELECTRONICA ID UAREU 1234 PRUEBA");
			certificado.setData(new byte[] { 4, 5, 6 });

			certificado.setEstado(Certificado.Estado.VALIDO);
			certificado.setFecha(new Date());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			manager.persist(solicitud);

			Documento documento = new Documento();
			
			documento.setFecha(new Date());
			documento.setData("<EnvioDE>...sistemas....</EnvioDE>".getBytes());
			documento.setSolicitud(solicitud);
			
			manager.persist(documento);

			manager.commitTransaction();
			
			checkSolicitudSistema(solicitud.getCodigo());
		}
		finally {
			manager.close();
		}
	}

	private void checkSolicitudSistema(String codigo) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			SolicitudSistema solicitud = manager.findSolicitudSistemaWithCodigo(codigo);
			
			Sistema sistema = solicitud.getSistema();
		
			assertEquals("Punto de Firma Biometrica ID UareU 12345 PRUEBA", sistema.getNombre());
			assertEquals("Sistema de Firma Electronica Biometrica PRUEBA", sistema.getUnidadOrganizacional());
			assertEquals("Acepta.com S.A. PRUEBA", sistema.getOrganizacion());
			assertEquals("CL", sistema.getPais());
			
			assertEquals("1.2.3.4", sistema.getPoliticas());
			
			assertNull(sistema.getEstado());
			assertNull(sistema.getCiudad());

			assertEquals(manager.findAutoridadWithNombre(TEST_CA), solicitud.getAutoridad());
		
			assertFalse(manager.findDocumentosWithSolicitud(solicitud).isEmpty());
		}
		finally {
			manager.close();
		}
	}
}
