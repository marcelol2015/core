package com.acepta.ca.test.persistence;

import java.math.BigInteger;
import java.util.ArrayList;

import javax.persistence.NoResultException;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.CRL;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.NumeroSerie;
import com.acepta.ca.persistence.SelloTiempo;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCAF;

public class TestAutoridad extends TestPersistence {
	public void testBorrarAutoridadDePrueba()
	{
		AceptaEntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.beginTransaction();

			try {
				entityManager.findAutoridadWithNombre(TEST_CA);
			}
			catch (NoResultException exception) {
				return;
			}

			borrarSolicitudesPersona(entityManager);			
			borrarSolicitudesCAF(entityManager);
			borrarSolicitudesSistema(entityManager);
			borrarSolicitudesSitioWeb(entityManager);
			borrarSolicitudesCodigo(entityManager);
			borrarCertificados(entityManager);
			borrarCRLs(entityManager);
			borrarSellosTiempos(entityManager);
			
			borrarAutoridad(entityManager);
			
			entityManager.commitTransaction();
		}
		finally {
			entityManager.close();
		}
	}
	
	public void testCrearAutoridadPrueba() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			Autoridad autoridad = new Autoridad();
			
			autoridad.setNombre(TEST_CA);
			autoridad.setEmisor(autoridad);
			
			autoridad.setNumerosSeries(new ArrayList<NumeroSerie>());
			
			autoridad.getNumerosSeries().add(new NumeroSerie(autoridad, NumeroSerie.Proposito.CERTIFICADO, BigInteger.ONE));
			autoridad.getNumerosSeries().add(new NumeroSerie(autoridad, NumeroSerie.Proposito.CRL, BigInteger.ONE));
			autoridad.getNumerosSeries().add(new NumeroSerie(autoridad, NumeroSerie.Proposito.SELLOTIEMPO, BigInteger.ONE));
			
			manager.persist(autoridad);

			manager.commitTransaction();
			
			assertNotNull(manager.findAutoridadWithNombre(TEST_CA));
		}
		finally {
			manager.close();
		}
	}

	public void testBuscarAutoridadPrueba() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			Autoridad autoridad = manager.findAutoridadWithNombre(TEST_CA);

			assertEquals(TEST_CA, autoridad.getNombre());
			assertEquals(autoridad, autoridad.getEmisor());
		}
		finally {
			manager.close();
		}
	}

	private void borrarAutoridad(AceptaEntityManager entityManager) {
		Autoridad autoridad = entityManager.findAutoridadWithNombre(TEST_CA);
		
		entityManager.remove(autoridad);
	}

	private void borrarSolicitudesCodigo(AceptaEntityManager entityManager) {
		for (Solicitud.Estado estado : Solicitud.Estado.values()) {
			for (Solicitud solicitud : entityManager.findSolicitudesCodigoWithEstado(estado)) {
				if (solicitud.getAutoridad().getNombre().equals(TEST_CA)) {
					for (Documento documento : entityManager.findDocumentosWithSolicitud(solicitud))
						entityManager.remove(documento);
					entityManager.remove(solicitud);
				}
			}
		}
	}

	private void borrarSolicitudesSitioWeb(AceptaEntityManager entityManager) {
		for (Solicitud.Estado estado : Solicitud.Estado.values()) {
			for (Solicitud solicitud : entityManager.findSolicitudesSitioWebWithEstado(estado)) {
				if (solicitud.getAutoridad().getNombre().equals(TEST_CA)) {
					for (Documento documento : entityManager.findDocumentosWithSolicitud(solicitud))
						entityManager.remove(documento);
					entityManager.remove(solicitud);
				}
			}
		}
	}
	
	private void borrarSolicitudesCAF(AceptaEntityManager entityManager) {
		for (SolicitudCAF solicitudCAF : entityManager.findSolicitudesCAFWithRutEmpresa(TEST_RUT)) {
			if (solicitudCAF.getAutoridad().getNombre().equals(TEST_CA)) {
				for (Documento documento : entityManager.findDocumentosWithSolicitud(solicitudCAF))
					entityManager.remove(documento);
				entityManager.remove(solicitudCAF);
			}
		}
	}

	private void borrarSolicitudesSistema(AceptaEntityManager entityManager) {
		for (Solicitud.Estado estado : Solicitud.Estado.values()) {
			for (Solicitud solicitud : entityManager.findSolicitudesSistemaWithEstado(estado)) {
				if (solicitud.getAutoridad().getNombre().equals(TEST_CA)) {
					for (Documento documento : entityManager.findDocumentosWithSolicitud(solicitud))
						entityManager.remove(documento);
					entityManager.remove(solicitud);
				}
			}
		}
	}

	private void borrarSolicitudesPersona(AceptaEntityManager entityManager) {
		for (Solicitud.Estado estado : Solicitud.Estado.values()) {
			for (Solicitud solicitud : entityManager.findSolicitudesPersonaWithEstado(estado)) {
				if (solicitud.getAutoridad().getNombre().equals(TEST_CA)) {
					for (Documento documento : entityManager.findDocumentosWithSolicitud(solicitud))
						entityManager.remove(documento);
					entityManager.remove(solicitud);
				}
			}
		}
	}
	
	private void borrarCertificados(AceptaEntityManager entityManager) {
		Autoridad autoridad = entityManager.findAutoridadWithNombre(TEST_CA);
		
		for (Certificado.Estado estado: Certificado.Estado.values()) {
			for (Certificado certificado : entityManager.findCertificadosWithEmisorEstado(autoridad, estado))
				entityManager.remove(certificado);
		}
	}

	private void borrarCRLs(AceptaEntityManager entityManager) {
		Autoridad autoridad = entityManager.findAutoridadWithNombre(TEST_CA);
		
		for (CRL crl : entityManager.findCRLsWithEmisor(autoridad))
			entityManager.remove(crl);
	}

	private void borrarSellosTiempos(AceptaEntityManager entityManager) {
		Autoridad autoridad = entityManager.findAutoridadWithNombre(TEST_CA);

		for (SelloTiempo selloTiempo : entityManager.findSellosTiemposWithEmisor(autoridad))
			entityManager.remove(selloTiempo);
	}
}
