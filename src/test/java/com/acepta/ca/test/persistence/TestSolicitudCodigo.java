package com.acepta.ca.test.persistence;

import java.util.Date;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Compania;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudCodigo;

public class TestSolicitudCodigo extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}

	public void testIngresarSolicitudCodigo() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			SolicitudCodigo solicitud = new SolicitudCodigo();
			
			solicitud.setNumero(SolicitudCodigoGenerator.generateCodigo());
			solicitud.setClave("abcd");
			solicitud.setMesesVigencia(12);
			
			Compania compania = new Compania();

			compania.setRut("1-9");
			compania.setNombre("Los Tres Chanchitos");
			compania.setOrganizacion("Empresa de Prueba S.A.");
			compania.setUnidadOrganizacional("Desarrollo");
			compania.setEstado("Metropolitana");
			compania.setCiudad("Santiago");
			compania.setPais("CL");
			
			solicitud.setCompania(compania);
			
			Autoridad autoridad = manager.findAutoridadWithNombre(TEST_CA);

			solicitud.setAutoridad(autoridad);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(solicitud.getAutoridad().generarNumeroSerieCertificado());
			certificado.setValidoDesde(new Date());
			certificado.setValidoHasta(new Date());
			certificado.setSujeto("LOS TRES CHANCHITOS");
			certificado.setData(new byte[] { 4, 5, 6 });

			certificado.setEstado(Certificado.Estado.VALIDO);
			certificado.setFecha(new Date());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			manager.persist(solicitud);

			Documento documento = new Documento();
			
			documento.setFecha(new Date());
			documento.setData("<EnvioDE>...solicitud firma codigo....</EnvioDE>".getBytes());
			documento.setSolicitud(solicitud);
			
			manager.persist(documento);

			manager.commitTransaction();
			
			checkSolicitudCodigo(solicitud.getCodigo());
		}
		finally {
			manager.close();
		}
	}

	private void checkSolicitudCodigo(String codigo) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			SolicitudCodigo solicitud = manager.findSolicitudCodigoWithCodigo(codigo);

			assertEquals(12, solicitud.getMesesVigencia());
			assertEquals("abcd", solicitud.getClave());
			
			Compania sitioWeb = solicitud.getCompania();
		
			assertEquals("1-9", sitioWeb.getRut());
			assertEquals("Los Tres Chanchitos", sitioWeb.getNombre());
			assertEquals("Empresa de Prueba S.A.", sitioWeb.getOrganizacion());
			assertEquals("Desarrollo", sitioWeb.getUnidadOrganizacional());
			assertEquals("Metropolitana", sitioWeb.getEstado());
			assertEquals("Santiago", sitioWeb.getCiudad());
			assertEquals("CL", sitioWeb.getPais());

			assertEquals(manager.findAutoridadWithNombre(TEST_CA), solicitud.getAutoridad());
		
			assertFalse(manager.findDocumentosWithSolicitud(solicitud).isEmpty());
		}
		finally {
			manager.close();
		}
	}
}
