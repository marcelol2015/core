package com.acepta.ca.test.persistence;

import java.util.Date;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.SitioWeb;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudSitioWeb;
import com.acepta.ca.security.util.BASE64;

public class TestSolicitudSitioWeb extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}

	public void testIngresarSolicitudSitioWeb() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			SolicitudSitioWeb solicitud = new SolicitudSitioWeb();
			
			solicitud.setNumero(SolicitudCodigoGenerator.generateCodigo());
			solicitud.setClave("abcd");
			solicitud.setMesesVigencia(12);
			
			SitioWeb sitioWeb = new SitioWeb();

			sitioWeb.setRut("1-9");
			sitioWeb.setDominio("www.prueba.cl");
			sitioWeb.setOrganizacion("Empresa de Prueba S.A.");
			sitioWeb.setUnidadOrganizacional("Desarrollo");
			sitioWeb.setEstado("Metropolitana");
			sitioWeb.setCiudad("Santiago");
			sitioWeb.setPais("CL");
			
			solicitud.setSitioWeb(sitioWeb);
			
			Autoridad autoridad = manager.findAutoridadWithNombre(TEST_CA);

			solicitud.setAutoridad(autoridad);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(solicitud.getAutoridad().generarNumeroSerieCertificado());
			certificado.setValidoDesde(new Date());
			certificado.setValidoHasta(new Date());
			certificado.setSujeto("EMPRESA DE PRUEBA S.A.");
			
			certificado.setData(BASE64.decode(
				"MIIGAzCCBOugAwIBAgIBBDANBgkqhkiG9w0BAQUFADCBlDELMAkGA1UEBhMCQ0wx" +
				"GDAWBgNVBAoTD0FjZXB0YS5jb20gUy5BLjE2MDQGA1UEAxMtQWNlcHRhLmNvbSBB" +
				"dXRvcmlkYWQgQ2VydGlmaWNhZG9yYSBSYWl6IC0gVlQxMR4wHAYJKoZIhvcNAQkB" +
				"Fg9pbmZvQGFjZXB0YS5jb20xEzARBgNVBAUTCjk2OTE5MDUwLTgwHhcNMTEwMjIx" +
				"MTUzODE5WhcNMzEwMjIxMTUzODE5WjCBnDELMAkGA1UEBhMCQ0wxGDAWBgNVBAoT" +
				"D0FjZXB0YS5jb20gUy5BLjE+MDwGA1UEAxM1QWNlcHRhLmNvbSBBdXRvcmlkYWQg" +
				"Q2VydGlmaWNhZG9yYSBkZSBTaXRpbyBXZWIgLSBWVDExHjAcBgkqhkiG9w0BCQEW" +
				"D2luZm9AYWNlcHRhLmNvbTETMBEGA1UEBRMKOTY5MTkwNTAtODCCASIwDQYJKoZI" +
				"hvcNAQEBBQADggEPADCCAQoCggEBAI93X6B33YiIiB4vFm9ORhntU1iPu375DEU2" +
				"VEHi7rIoIRkY9t5NxwkypQWVJkLMYGLTYTK8SGadEatuzHUIqTBRmhLDZFqAS2Zi" +
				"/dBXxISZ7hXg6jRXZDAZRxTWgW5D/iEzwK7yv4i7QgPnW9bUvXLZM1+IqwidfHaz" +
				"Ji+yCVFfmyQqEQ/qrEg+j2GK20gS7vG5zV8kcOF7+9XnmGZnLm6DwGDtSycK+d8I" +
				"22Kdd07vUHp7jztjAuOt43XngkqxonG7AoJWjs6GIPce97cJrErI985FmvrFjJUb" +
				"OTRJnwF3QCdYKTKFFEtUrtGv36r0CpAgmSzB+GdvcJMUDE7TddECAwEAAaOCAlQw" +
				"ggJQMA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUM202Uso+Sm3km7i8ANES" +
				"ggjRA+YwHQYDVR0OBBYEFCdP9sZSnBqJFI47J9e8SLWEvXzFMAsGA1UdDwQEAwIB" +
				"BjCCATYGA1UdIASCAS0wggEpMIIBJQYKKwYBBAG1a4dpBzCCARUwKAYIKwYBBQUH" +
				"AgEWHGh0dHA6Ly93d3cuYWNlcHRhLmNvbS9DUFNWVDEwgegGCCsGAQUFBwICMIHb" +
				"MBYWD0FjZXB0YS5jb20gUy5BLjADAgEHGoHATGEgdXRpbGl6YWNpb24gZGUgZXN0" +
				"ZSBjZXJ0aWZpY2FkbyBlc3RhIHN1amV0YSBhIGxhcyBwb2xpdGljYXMgZGUgY2Vy" +
				"dGlmaWNhZG8gKENQKSB5IHByYWN0aWNhcyBkZSBjZXJ0aWZpY2FjaW9uIChDUFMp" +
				"IGVzdGFibGVjaWRhcyBwb3IgQWNlcHRhLmNvbSwgeSBkaXNwb25pYmxlcyBwdWJs" +
				"aWNhbWVudGUgZW4gd3d3LmFjZXB0YS5jb20uMFoGA1UdEgRTMFGgGAYIKwYBBAHB" +
				"AQKgDBYKOTY5MTkwNTAtOKAkBggrBgEFBQcIA6AYMBYMCjk2OTE5MDUwLTgGCCsG" +
				"AQQBwQECgQ9pbmZvQGFjZXB0YS5jb20wWgYDVR0RBFMwUaAYBggrBgEEAcEBAaAM" +
				"Fgo5NjkxOTA1MC04oCQGCCsGAQUFBwgDoBgwFgwKOTY5MTkwNTAtOAYIKwYBBAHB" +
				"AQKBD2luZm9AYWNlcHRhLmNvbTANBgkqhkiG9w0BAQUFAAOCAQEA1O6h/B1ZySgY" +
				"LRoUMLxWtdWS+4hy+Er7CYR941F3PoNERuYMQV+FODURypbzXlw8QxCxflQgfSxR" +
				"vgK4BMgpjVJCdVfm/KleWXC1K+NM38rpzSkFchXG8BsxxfcbONAovrj50JP1b6SlQ" +
				"pV8cHCAdy0lWwLmLydjhJJh9Ltakp1fWNBBxyBLlea7q3+SocbGccOBFlsCAEJTU" +
				"of4jVAvkFi/83s8/R1o6+kR/Aqx2IfVI5x2DMh75HP2MI2G0DnxSUfsnXGc4nldf" +
				"+tND8Iq5TWiBbGMh55Ju6Y1fnAPsNROxpJPcpGZtCndTSC3X7pygP1DcZux30v3+" +
				"Xnx/zytKnw=="));

			certificado.setEstado(Certificado.Estado.VALIDO);
			certificado.setFecha(new Date());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			manager.persist(solicitud);

			Documento documento = new Documento();
			
			documento.setFecha(new Date());
			documento.setData("<EnvioDE>...solicitud sitio web....</EnvioDE>".getBytes());
			documento.setSolicitud(solicitud);
			
			manager.persist(documento);

			manager.commitTransaction();
			
			checkSolicitudSitioWeb(solicitud.getCodigo());
		}
		finally {
			manager.close();
		}
	}

	private void checkSolicitudSitioWeb(String codigo) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			SolicitudSitioWeb solicitud = manager.findSolicitudSitioWebWithCodigo(codigo);

			assertEquals(12, solicitud.getMesesVigencia());
			assertEquals("abcd", solicitud.getClave());
			
			SitioWeb sitioWeb = solicitud.getSitioWeb();
		
			assertEquals("1-9", sitioWeb.getRut());
			assertEquals("www.prueba.cl", sitioWeb.getDominio());
			assertEquals("Empresa de Prueba S.A.", sitioWeb.getOrganizacion());
			assertEquals("Desarrollo", sitioWeb.getUnidadOrganizacional());
			assertEquals("Metropolitana", sitioWeb.getEstado());
			assertEquals("Santiago", sitioWeb.getCiudad());
			assertEquals("CL", sitioWeb.getPais());

			assertEquals(manager.findAutoridadWithNombre(TEST_CA), solicitud.getAutoridad());
		
			assertFalse(manager.findDocumentosWithSolicitud(solicitud).isEmpty());
		}
		finally {
			manager.close();
		}
	}

}
