package com.acepta.ca.test.persistence;

import java.util.Date;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.CAF;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCAF;

public class TestSolicitudCAF extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}

	public void testIngresarSolicitudCAF() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			SolicitudCAF solicitud = new SolicitudCAF();
			
			CAF caf = new CAF();

			caf.setRutEmpresa(TEST_RUT);
			caf.setRazonSocial("Prueba S.A.");
			caf.setTipoDTE(33);
			caf.setDesdeFolio(1L);
			caf.setHastaFolio(1000L);
			caf.setFechaAutorizacion(new Date());
			caf.setLlaveID(123);
			
			solicitud.setCAF(caf);
			
			Autoridad autoridad = manager.findAutoridadWithNombre(TEST_CA);

			solicitud.setAutoridad(autoridad);

			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(solicitud.getAutoridad().generarNumeroSerieCertificado());
			certificado.setValidoDesde(new Date());
			certificado.setValidoHasta(new Date());
			certificado.setSujeto("PRUEBA S A FACTURAS FOLIOS 1 AL 1000");
			certificado.setData(new byte[] { 4, 5, 6 });

			certificado.setEstado(Certificado.Estado.VALIDO);
			certificado.setFecha(new Date());

			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			manager.persist(solicitud);

			Documento documento = new Documento();
			
			documento.setFecha(new Date());
			documento.setData("<AUTORIZACION>...</AUTORIZACION>".getBytes());
			documento.setSolicitud(solicitud);
			
			manager.persist(documento);

			manager.commitTransaction();
		}
		finally {
			manager.close();
		}
	}

	public void testCheckSolicitudCAF() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			SolicitudCAF solicitud = manager.findSolicitudCAFWithRutTipoFolio(TEST_RUT, 33, 150);

			CAF caf = solicitud.getCAF();
			
			assertEquals(TEST_RUT, caf.getRutEmpresa());
			assertEquals("Prueba S.A.", caf.getRazonSocial());
			assertEquals(33, caf.getTipoDTE());
			assertEquals(1L, caf.getDesdeFolio());
			assertEquals(1000L, caf.getHastaFolio());
			assertEquals(123, caf.getLlaveID());
			assertNotNull(solicitud.getCertificado());
			assertEquals(manager.findAutoridadWithNombre(TEST_CA), solicitud.getAutoridad());
			
			assertFalse(manager.findDocumentosWithSolicitud(solicitud).isEmpty());
		}
		finally {
			manager.close();
		}
	}
	
}
