package com.acepta.ca.test.persistence;

import java.math.BigInteger;
import java.util.Date;

import com.acepta.ca.persistence.AceptaEntityManager;
import com.acepta.ca.persistence.Autoridad;
import com.acepta.ca.persistence.Certificado;
import com.acepta.ca.persistence.Documento;
import com.acepta.ca.persistence.Persona;
import com.acepta.ca.persistence.Solicitud;
import com.acepta.ca.persistence.SolicitudCodigoGenerator;
import com.acepta.ca.persistence.SolicitudPersona;

public class TestSolicitudPersona extends TestPersistence {
	public void testPrecondiciones() {
		assertExisteAutoridadDePrueba();
	}

	public void testSolicitud() {
		String codigo = registrarSolicitud();
		checkSolicitudRegistrada(codigo);
		aprobarSolicitud(codigo);
		BigInteger numeroSerie = emitirSolicitud(codigo);
		checkSolicitudEmitida(codigo, numeroSerie);
		revocarCertificado(numeroSerie);
		checkCertificadoRevocado(numeroSerie);
	}

	private String registrarSolicitud() {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			// create solicitud
			SolicitudPersona solicitud = new SolicitudPersona();

			solicitud.setNumero(SolicitudCodigoGenerator.generateCodigo());
			solicitud.setClave("abc");

			Persona titular = new Persona();

			titular.setRut(TEST_RUT);
			titular.setNombres("JUAN");
			titular.setApellidoPaterno("PEREZ");
			titular.setApellidoMaterno("ITURRA");
			titular.setCorreo("juan@perez.cl");

			solicitud.setTitular(titular);

			Autoridad autoridad = manager.findAutoridadWithNombre(TEST_CA);

			solicitud.setAutoridad(autoridad);

			solicitud.changeEstado(Solicitud.Estado.PENDIENTE);

			manager.persist(solicitud);

			Documento documento = new Documento();
			
			documento.setFecha(new Date());
			documento.setData("<EnvioDE><DATA><Emisor>9-9</Emisor><Destinatario>9-9</Destinatario><Contenidos><NumeroSolicitud>123</NumeroSolicitud></Contenidos></DATA></EnvioDE>".getBytes());
			documento.setSolicitud(solicitud);
			
			manager.persist(documento);

			manager.commitTransaction();

			return solicitud.getCodigo();
		}
		finally {
			manager.close();
		}
	}

	private void checkSolicitudRegistrada(String codigo) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			int count = 0;

			for (Solicitud solicitud : manager.findSolicitudesPersonaWithEstado(Solicitud.Estado.PENDIENTE)) {
				if (!solicitud.getAutoridad().getNombre().equals(TEST_CA))
					continue;
				
				assertEquals(codigo, solicitud.getCodigo());
				count++;
			}

			assertEquals(1, count);
		}
		finally {
			manager.close();
		}
	}
	
	private void aprobarSolicitud(String codigo) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			SolicitudPersona solicitud = manager.findSolicitudPersonaWithCodigo(codigo);

			assertEquals(codigo, solicitud.getCodigo());

			assertEquals("abc", solicitud.getClave());

			assertEquals(TEST_RUT, solicitud.getTitular().getRut());
			assertEquals("JUAN", solicitud.getTitular().getNombres());
			assertEquals("PEREZ", solicitud.getTitular().getApellidoPaterno());
			assertEquals("ITURRA", solicitud.getTitular().getApellidoMaterno());

			assertEquals("juan@perez.cl", solicitud.getTitular().getCorreo());

			assertEquals(manager.findAutoridadWithNombre(TEST_CA), solicitud.getAutoridad());

			assertTrue(solicitud.isPendiente());

			assertNull(solicitud.getCertificado());

			// change state
			solicitud.changeEstado(Solicitud.Estado.APROBADA);

			manager.commitTransaction();
		}
		finally {
			manager.close();
		}
	}

	private BigInteger emitirSolicitud(String codigo) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			Solicitud solicitud = manager.findSolicitudPersonaWithCodigo(codigo);

			assertEquals(codigo, solicitud.getCodigo());

			assertTrue(solicitud.isAprobada());

			assertNull(solicitud.getCertificado());

			BigInteger numeroSerie = solicitud.getAutoridad().generarNumeroSerieCertificado();

			// create certificate
			Certificado certificado = new Certificado();

			certificado.setEmisor(solicitud.getAutoridad());
			certificado.setNumeroSerie(numeroSerie);
			certificado.setValidoDesde(new Date());
			certificado.setValidoHasta(new Date());
			certificado.setSujeto("PRUEBA PRUEBA");
			certificado.setData(new byte[] { 4, 5, 6 });

			certificado.setEstado(Certificado.Estado.VALIDO);
			certificado.setFecha(new Date());
			
			// change state
			solicitud.setCertificado(certificado);

			solicitud.changeEstado(Solicitud.Estado.CERRADA);
			
			manager.commitTransaction();
			
			return numeroSerie;
		}
		finally {
			manager.close();
		}
	}
	
	private void checkSolicitudEmitida(String codigo, BigInteger numeroSerie) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			int count = 0;

			for (Solicitud solicitud : manager.findSolicitudesPersonaWithEstado(Solicitud.Estado.CERRADA)) {
				if (!solicitud.getAutoridad().getNombre().equals(TEST_CA))
					continue;
				
				if (!solicitud.getCodigo().equals(codigo))
					continue;

				assertTrue(solicitud.isCerrada());

				Certificado certificado = solicitud.getCertificado();

				assertNotNull(certificado);

				assertEquals(numeroSerie, certificado.getNumeroSerie());
				assertEquals(manager.findAutoridadWithNombre(TEST_CA), certificado.getEmisor());

				assertTrue(certificado.isValido());

				boolean expiresOk = false;
				
				for (Solicitud solicitudExpirada : manager.findSolicitudesPersonaWithExpiracionEntre(certificado.getValidoHasta(), certificado.getValidoHasta())) {
					if (solicitudExpirada.getId() == solicitud.getId())
						expiresOk = true;
				}
				
				assertTrue(expiresOk);
				
				count++;
			}

			assertEquals(1, count);
		}
		finally {
			manager.close();
		}
	}
	
	private void revocarCertificado(BigInteger numeroSerie) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			manager.beginTransaction();

			Autoridad emisor = manager.findAutoridadWithNombre(TEST_CA);

			Certificado certificado = manager.findCertificadoWithEmisorNumeroSerie(emisor, numeroSerie);

			assertTrue(certificado.isValido());

			// revocar
			certificado.setEstado(Certificado.Estado.REVOCADO);
			certificado.setRazon(Certificado.Razon.KEY_COMPROMISE);
			certificado.setFecha(new Date());
			
			manager.commitTransaction();
		}
		finally {
			manager.close();
		}
	}

	private void checkCertificadoRevocado(BigInteger numeroSerie) {
		AceptaEntityManager manager = getEntityManagerFactory().createEntityManager();
		try {
			Autoridad emisor = manager.findAutoridadWithNombre(TEST_CA);

			Certificado certificado = manager.findCertificadoWithEmisorNumeroSerie(emisor, numeroSerie);

			assertTrue(certificado.isRevocado());

			int count = 0;

			for (Certificado certificadoRevocado : manager.findCertificadosWithEmisorEstado(emisor, Certificado.Estado.REVOCADO)) {
				if (certificado.getId().equals(certificadoRevocado.getId()))
					count++;
			}

			assertEquals(1, count);
		}
		finally {
			manager.close();
		}
	}
}
